package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.PaymentMethod;
import com.mycompany.myapp.domain.enumeration.JobRequirement;

/**
 * A DTO for the JobTemplate entity.
 */
public class JobTemplateDTO implements Serializable {

    private Long id;

    private String address;

    private PaymentMethod paymentMethod;

    private JobRequirement requirement;

    private String formFillIn;

    private LocalDate createDate;

    @NotNull
    private LocalDate startDate;

    private Long serviceId;

    private Long sysUserId;

    private String sysUserLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public JobRequirement getRequirement() {
        return requirement;
    }

    public void setRequirement(JobRequirement requirement) {
        this.requirement = requirement;
    }

    public String getFormFillIn() {
        return formFillIn;
    }

    public void setFormFillIn(String formFillIn) {
        this.formFillIn = formFillIn;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    public String getSysUserLogin() {
        return sysUserLogin;
    }

    public void setSysUserLogin(String sysUserLogin) {
        this.sysUserLogin = sysUserLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JobTemplateDTO jobTemplateDTO = (JobTemplateDTO) o;
        if(jobTemplateDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobTemplateDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobTemplateDTO{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", paymentMethod='" + getPaymentMethod() + "'" +
            ", requirement='" + getRequirement() + "'" +
            ", formFillIn='" + getFormFillIn() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", startDate='" + getStartDate() + "'" +
            "}";
    }
}
