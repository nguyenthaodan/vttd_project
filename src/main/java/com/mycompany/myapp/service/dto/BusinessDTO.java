package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.BusinessType;
import com.mycompany.myapp.domain.enumeration.BusinessStatus;
import com.mycompany.myapp.domain.enumeration.UserRequirement;

/**
 * A DTO for the Business entity.
 */
public class BusinessDTO implements Serializable {

    private Long id;

    private String businessName;

    private BusinessType type;

    private String description;

    private LocalDate dateOperate;

    private Integer totalJobDone;

    private String website;

    private BusinessStatus status;

    private UserRequirement user;

    private String note;

    private Set<ServiceDTO> services = new HashSet<>();

    private Set<LocationDTO> locations = new HashSet<>();

    private Long sysUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public BusinessType getType() {
        return type;
    }

    public void setType(BusinessType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateOperate() {
        return dateOperate;
    }

    public void setDateOperate(LocalDate dateOperate) {
        this.dateOperate = dateOperate;
    }

    public Integer getTotalJobDone() {
        return totalJobDone;
    }

    public void setTotalJobDone(Integer totalJobDone) {
        this.totalJobDone = totalJobDone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BusinessStatus getStatus() {
        return status;
    }

    public void setStatus(BusinessStatus status) {
        this.status = status;
    }

    public UserRequirement getUser() {
        return user;
    }

    public void setUser(UserRequirement user) {
        this.user = user;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<ServiceDTO> getServices() {
        return services;
    }

    public void setServices(Set<ServiceDTO> services) {
        this.services = services;
    }

    public Set<LocationDTO> getLocations() {
        return locations;
    }

    public void setLocations(Set<LocationDTO> locations) {
        this.locations = locations;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BusinessDTO businessDTO = (BusinessDTO) o;
        if(businessDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), businessDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BusinessDTO{" +
            "id=" + getId() +
            ", businessName='" + getBusinessName() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateOperate='" + getDateOperate() + "'" +
            ", totalJobDone='" + getTotalJobDone() + "'" +
            ", website='" + getWebsite() + "'" +
            ", status='" + getStatus() + "'" +
            ", user='" + getUser() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
