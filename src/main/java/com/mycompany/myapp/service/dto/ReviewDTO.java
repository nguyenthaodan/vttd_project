package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.Rating;

/**
 * A DTO for the Review entity.
 */
public class ReviewDTO implements Serializable {

    private Long id;

    private Rating rating;

    @NotNull
    private String reviewText;

    private LocalDate reviewDate;

    private Long completedId;

    private Long sysUserId;

    private String sysUserLogin;

    private Long businessId;

    private String businessBusinessName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public LocalDate getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(LocalDate reviewDate) {
        this.reviewDate = reviewDate;
    }

    public Long getCompletedId() {
        return completedId;
    }

    public void setCompletedId(Long completedId) {
        this.completedId = completedId;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    public String getSysUserLogin() {
        return sysUserLogin;
    }

    public void setSysUserLogin(String sysUserLogin) {
        this.sysUserLogin = sysUserLogin;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessBusinessName() {
        return businessBusinessName;
    }

    public void setBusinessBusinessName(String businessBusinessName) {
        this.businessBusinessName = businessBusinessName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReviewDTO reviewDTO = (ReviewDTO) o;
        if(reviewDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reviewDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReviewDTO{" +
            "id=" + getId() +
            ", rating='" + getRating() + "'" +
            ", reviewText='" + getReviewText() + "'" +
            ", reviewDate='" + getReviewDate() + "'" +
            "}";
    }
}
