package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the JobInfo entity.
 */
public class JobInfoDTO implements Serializable {

    private Long id;

    private Integer finished;

    private Integer canceled;

    private Integer unfinished;

    private Integer pending;

    private Float totalEarnt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFinished() {
        return finished;
    }

    public void setFinished(Integer finished) {
        this.finished = finished;
    }

    public Integer getCanceled() {
        return canceled;
    }

    public void setCanceled(Integer canceled) {
        this.canceled = canceled;
    }

    public Integer getUnfinished() {
        return unfinished;
    }

    public void setUnfinished(Integer unfinished) {
        this.unfinished = unfinished;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    public Float getTotalEarnt() {
        return totalEarnt;
    }

    public void setTotalEarnt(Float totalEarnt) {
        this.totalEarnt = totalEarnt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JobInfoDTO jobInfoDTO = (JobInfoDTO) o;
        if(jobInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobInfoDTO{" +
            "id=" + getId() +
            ", finished='" + getFinished() + "'" +
            ", canceled='" + getCanceled() + "'" +
            ", unfinished='" + getUnfinished() + "'" +
            ", pending='" + getPending() + "'" +
            ", totalEarnt='" + getTotalEarnt() + "'" +
            "}";
    }
}
