package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.UserRole;
import com.mycompany.myapp.domain.enumeration.UserType;
import com.mycompany.myapp.domain.enumeration.UserSecurity;

/**
 * A DTO for the SysUser entity.
 */
public class SysUserDTO implements Serializable {

    private Long id;

    private UserRole role;

    private UserType type;

    private UserSecurity security;

    private Integer points;

    private Long creditInfoId;

    private Long chatInfoId;

    private Long pushInfoId;

    private Long reviewInfoId;

    private Long jobInfoId;

    private Long Id;

    private Long languageId;

    private Long locationId;

    private Long canceledId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public UserSecurity getSecurity() {
        return security;
    }

    public void setSecurity(UserSecurity security) {
        this.security = security;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Long getCreditInfoId() {
        return creditInfoId;
    }

    public void setCreditInfoId(Long creditInfoId) {
        this.creditInfoId = creditInfoId;
    }

    public Long getChatInfoId() {
        return chatInfoId;
    }

    public void setChatInfoId(Long chatInfoId) {
        this.chatInfoId = chatInfoId;
    }

    public Long getPushInfoId() {
        return pushInfoId;
    }

    public void setPushInfoId(Long pushInfoId) {
        this.pushInfoId = pushInfoId;
    }

    public Long getReviewInfoId() {
        return reviewInfoId;
    }

    public void setReviewInfoId(Long reviewInfoId) {
        this.reviewInfoId = reviewInfoId;
    }

    public Long getJobInfoId() {
        return jobInfoId;
    }

    public void setJobInfoId(Long jobInfoId) {
        this.jobInfoId = jobInfoId;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long loginInfoId) {
        this.Id = loginInfoId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getCanceledId() {
        return canceledId;
    }

    public void setCanceledId(Long canceledId) {
        this.canceledId = canceledId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SysUserDTO sysUserDTO = (SysUserDTO) o;
        if(sysUserDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sysUserDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SysUserDTO{" +
            "id=" + getId() +
            ", role='" + getRole() + "'" +
            ", type='" + getType() + "'" +
            ", security='" + getSecurity() + "'" +
            ", points='" + getPoints() + "'" +
            "}";
    }
}
