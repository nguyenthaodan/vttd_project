package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Bidding entity.
 */
public class BiddingDTO implements Serializable {

    private Long id;

    private LocalDate closingDate;

    private Integer biddersCount;

    private Long jobTemplateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(LocalDate closingDate) {
        this.closingDate = closingDate;
    }

    public Integer getBiddersCount() {
        return biddersCount;
    }

    public void setBiddersCount(Integer biddersCount) {
        this.biddersCount = biddersCount;
    }

    public Long getJobTemplateId() {
        return jobTemplateId;
    }

    public void setJobTemplateId(Long jobTemplateId) {
        this.jobTemplateId = jobTemplateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BiddingDTO biddingDTO = (BiddingDTO) o;
        if(biddingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), biddingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BiddingDTO{" +
            "id=" + getId() +
            ", closingDate='" + getClosingDate() + "'" +
            ", biddersCount='" + getBiddersCount() + "'" +
            "}";
    }
}
