package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the InvoiceInfo entity.
 */
public class InvoiceInfoDTO implements Serializable {

    private Long id;

    private String businessAddress;

    private String invoiceAddress;

    private String taxNumber;

    private String accountNumber;

    private String businessName;

    private String personName;

    private Long Id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(String invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long businessId) {
        this.Id = businessId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InvoiceInfoDTO invoiceInfoDTO = (InvoiceInfoDTO) o;
        if(invoiceInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceInfoDTO{" +
            "id=" + getId() +
            ", businessAddress='" + getBusinessAddress() + "'" +
            ", invoiceAddress='" + getInvoiceAddress() + "'" +
            ", taxNumber='" + getTaxNumber() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            ", businessName='" + getBusinessName() + "'" +
            ", personName='" + getPersonName() + "'" +
            "}";
    }
}
