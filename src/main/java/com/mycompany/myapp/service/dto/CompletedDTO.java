package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.CompleteStatus;

/**
 * A DTO for the Completed entity.
 */
public class CompletedDTO implements Serializable {

    private Long id;

    private CompleteStatus status;

    private LocalDate completeDate;

    private Boolean reviewed;

    private Integer rating;

    private Long jobTemplateId;

    private Long userInvoiceId;

    private Long providerInvoiceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CompleteStatus getStatus() {
        return status;
    }

    public void setStatus(CompleteStatus status) {
        this.status = status;
    }

    public LocalDate getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(LocalDate completeDate) {
        this.completeDate = completeDate;
    }

    public Boolean isReviewed() {
        return reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Long getJobTemplateId() {
        return jobTemplateId;
    }

    public void setJobTemplateId(Long jobTemplateId) {
        this.jobTemplateId = jobTemplateId;
    }

    public Long getUserInvoiceId() {
        return userInvoiceId;
    }

    public void setUserInvoiceId(Long invoiceId) {
        this.userInvoiceId = invoiceId;
    }

    public Long getProviderInvoiceId() {
        return providerInvoiceId;
    }

    public void setProviderInvoiceId(Long invoiceId) {
        this.providerInvoiceId = invoiceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CompletedDTO completedDTO = (CompletedDTO) o;
        if(completedDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), completedDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CompletedDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", completeDate='" + getCompleteDate() + "'" +
            ", reviewed='" + isReviewed() + "'" +
            ", rating='" + getRating() + "'" +
            "}";
    }
}
