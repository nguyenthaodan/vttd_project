package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.PaymentMethod;

/**
 * A DTO for the CreditInfo entity.
 */
public class CreditInfoDTO implements Serializable {

    private Long id;

    private Float credit;

    private PaymentMethod method;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getCredit() {
        return credit;
    }

    public void setCredit(Float credit) {
        this.credit = credit;
    }

    public PaymentMethod getMethod() {
        return method;
    }

    public void setMethod(PaymentMethod method) {
        this.method = method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CreditInfoDTO creditInfoDTO = (CreditInfoDTO) o;
        if(creditInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), creditInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CreditInfoDTO{" +
            "id=" + getId() +
            ", credit='" + getCredit() + "'" +
            ", method='" + getMethod() + "'" +
            "}";
    }
}
