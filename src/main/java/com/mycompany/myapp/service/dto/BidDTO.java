package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Bid entity.
 */
public class BidDTO implements Serializable {

    private Long id;

    private Long price;

    @NotNull
    private LocalDate timeToFinish;

    private LocalDate bidTime;

    @NotNull
    private String message;

    private Long biddingId;

    private Long sysUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public LocalDate getTimeToFinish() {
        return timeToFinish;
    }

    public void setTimeToFinish(LocalDate timeToFinish) {
        this.timeToFinish = timeToFinish;
    }

    public LocalDate getBidTime() {
        return bidTime;
    }

    public void setBidTime(LocalDate bidTime) {
        this.bidTime = bidTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getBiddingId() {
        return biddingId;
    }

    public void setBiddingId(Long biddingId) {
        this.biddingId = biddingId;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BidDTO bidDTO = (BidDTO) o;
        if(bidDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bidDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BidDTO{" +
            "id=" + getId() +
            ", price='" + getPrice() + "'" +
            ", timeToFinish='" + getTimeToFinish() + "'" +
            ", bidTime='" + getBidTime() + "'" +
            ", message='" + getMessage() + "'" +
            "}";
    }
}
