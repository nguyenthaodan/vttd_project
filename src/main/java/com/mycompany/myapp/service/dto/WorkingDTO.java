package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.mycompany.myapp.domain.enumeration.WorkingStatus;
import com.mycompany.myapp.domain.enumeration.WorkingStatus;

/**
 * A DTO for the Working entity.
 */
public class WorkingDTO implements Serializable {

    private Long id;

    private WorkingStatus userstatus;

    private WorkingStatus supplierstatus;

    private Float amountDeposit;

    @NotNull
    private Float price;

    private LocalDate bookDate;

    private LocalDate startDate;

    private LocalDate finishDate;

    @NotNull
    private LocalDate lastMeeting;

    @NotNull
    private LocalDate nextMeeting;

    private Long jobTemplateId;

    private Long sysUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public WorkingStatus getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(WorkingStatus userstatus) {
        this.userstatus = userstatus;
    }

    public WorkingStatus getSupplierstatus() {
        return supplierstatus;
    }

    public void setSupplierstatus(WorkingStatus supplierstatus) {
        this.supplierstatus = supplierstatus;
    }

    public Float getAmountDeposit() {
        return amountDeposit;
    }

    public void setAmountDeposit(Float amountDeposit) {
        this.amountDeposit = amountDeposit;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public LocalDate getBookDate() {
        return bookDate;
    }

    public void setBookDate(LocalDate bookDate) {
        this.bookDate = bookDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    public LocalDate getLastMeeting() {
        return lastMeeting;
    }

    public void setLastMeeting(LocalDate lastMeeting) {
        this.lastMeeting = lastMeeting;
    }

    public LocalDate getNextMeeting() {
        return nextMeeting;
    }

    public void setNextMeeting(LocalDate nextMeeting) {
        this.nextMeeting = nextMeeting;
    }

    public Long getJobTemplateId() {
        return jobTemplateId;
    }

    public void setJobTemplateId(Long jobTemplateId) {
        this.jobTemplateId = jobTemplateId;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WorkingDTO workingDTO = (WorkingDTO) o;
        if(workingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), workingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WorkingDTO{" +
            "id=" + getId() +
            ", userstatus='" + getUserstatus() + "'" +
            ", supplierstatus='" + getSupplierstatus() + "'" +
            ", amountDeposit='" + getAmountDeposit() + "'" +
            ", price='" + getPrice() + "'" +
            ", bookDate='" + getBookDate() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", finishDate='" + getFinishDate() + "'" +
            ", lastMeeting='" + getLastMeeting() + "'" +
            ", nextMeeting='" + getNextMeeting() + "'" +
            "}";
    }
}
