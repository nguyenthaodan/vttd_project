package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the EventInfo entity.
 */
public class EventInfoDTO implements Serializable {

    private Long id;

    private Long sysUserId;

    private Long jobTemplateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    public Long getJobTemplateId() {
        return jobTemplateId;
    }

    public void setJobTemplateId(Long jobTemplateId) {
        this.jobTemplateId = jobTemplateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventInfoDTO eventInfoDTO = (EventInfoDTO) o;
        if(eventInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventInfoDTO{" +
            "id=" + getId() +
            "}";
    }
}
