package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PageTable entity.
 */
public class PageTableDTO implements Serializable {

    private Long id;

    private String pageName;

    private String pageTitle;

    private String pageInfo;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(String pageInfo) {
        this.pageInfo = pageInfo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PageTableDTO pageTableDTO = (PageTableDTO) o;
        if(pageTableDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pageTableDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PageTableDTO{" +
            "id=" + getId() +
            ", pageName='" + getPageName() + "'" +
            ", pageTitle='" + getPageTitle() + "'" +
            ", pageInfo='" + getPageInfo() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}
