package com.mycompany.myapp.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Canceled entity.
 */
public class CanceledDTO implements Serializable {

    private Long id;

    private LocalDate cancelDate;

    private String reason;

    private Long jobTemplateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(LocalDate cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getJobTemplateId() {
        return jobTemplateId;
    }

    public void setJobTemplateId(Long jobTemplateId) {
        this.jobTemplateId = jobTemplateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CanceledDTO canceledDTO = (CanceledDTO) o;
        if(canceledDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), canceledDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CanceledDTO{" +
            "id=" + getId() +
            ", cancelDate='" + getCancelDate() + "'" +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
