package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.CompletedDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Completed.
 */
public interface CompletedService {

    /**
     * Save a completed.
     *
     * @param completedDTO the entity to save
     * @return the persisted entity
     */
    CompletedDTO save(CompletedDTO completedDTO);

    /**
     *  Get all the completeds.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CompletedDTO> findAll(Pageable pageable);
    /**
     *  Get all the CompletedDTO where Review is null.
     *
     *  @return the list of entities
     */
    List<CompletedDTO> findAllWhereReviewIsNull();

    /**
     *  Get the "id" completed.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CompletedDTO findOne(Long id);

    /**
     *  Delete the "id" completed.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
