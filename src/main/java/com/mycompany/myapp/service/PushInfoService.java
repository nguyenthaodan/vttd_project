package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.PushInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PushInfo.
 */
public interface PushInfoService {

    /**
     * Save a pushInfo.
     *
     * @param pushInfoDTO the entity to save
     * @return the persisted entity
     */
    PushInfoDTO save(PushInfoDTO pushInfoDTO);

    /**
     *  Get all the pushInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PushInfoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" pushInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PushInfoDTO findOne(Long id);

    /**
     *  Delete the "id" pushInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
