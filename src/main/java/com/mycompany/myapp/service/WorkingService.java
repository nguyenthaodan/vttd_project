package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.WorkingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Working.
 */
public interface WorkingService {

    /**
     * Save a working.
     *
     * @param workingDTO the entity to save
     * @return the persisted entity
     */
    WorkingDTO save(WorkingDTO workingDTO);

    /**
     *  Get all the workings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<WorkingDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" working.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    WorkingDTO findOne(Long id);

    /**
     *  Delete the "id" working.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
