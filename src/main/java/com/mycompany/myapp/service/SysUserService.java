package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.SysUserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing SysUser.
 */
public interface SysUserService {

    /**
     * Save a sysUser.
     *
     * @param sysUserDTO the entity to save
     * @return the persisted entity
     */
    SysUserDTO save(SysUserDTO sysUserDTO);

    /**
     *  Get all the sysUsers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<SysUserDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" sysUser.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    SysUserDTO findOne(Long id);

    /**
     *  Delete the "id" sysUser.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
