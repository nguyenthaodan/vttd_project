package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.BusinessDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Business.
 */
public interface BusinessService {

    /**
     * Save a business.
     *
     * @param businessDTO the entity to save
     * @return the persisted entity
     */
    BusinessDTO save(BusinessDTO businessDTO);

    /**
     *  Get all the businesses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BusinessDTO> findAll(Pageable pageable);
    /**
     *  Get all the BusinessDTO where InvoiceInfo is null.
     *
     *  @return the list of entities
     */
    List<BusinessDTO> findAllWhereInvoiceInfoIsNull();

    /**
     *  Get the "id" business.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    BusinessDTO findOne(Long id);

    /**
     *  Delete the "id" business.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
