package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.JobInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing JobInfo.
 */
public interface JobInfoService {

    /**
     * Save a jobInfo.
     *
     * @param jobInfoDTO the entity to save
     * @return the persisted entity
     */
    JobInfoDTO save(JobInfoDTO jobInfoDTO);

    /**
     *  Get all the jobInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<JobInfoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" jobInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    JobInfoDTO findOne(Long id);

    /**
     *  Delete the "id" jobInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
