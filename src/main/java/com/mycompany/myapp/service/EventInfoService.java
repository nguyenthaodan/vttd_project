package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.EventInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing EventInfo.
 */
public interface EventInfoService {

    /**
     * Save a eventInfo.
     *
     * @param eventInfoDTO the entity to save
     * @return the persisted entity
     */
    EventInfoDTO save(EventInfoDTO eventInfoDTO);

    /**
     *  Get all the eventInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<EventInfoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" eventInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    EventInfoDTO findOne(Long id);

    /**
     *  Delete the "id" eventInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
