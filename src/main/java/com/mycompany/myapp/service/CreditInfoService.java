package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.CreditInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing CreditInfo.
 */
public interface CreditInfoService {

    /**
     * Save a creditInfo.
     *
     * @param creditInfoDTO the entity to save
     * @return the persisted entity
     */
    CreditInfoDTO save(CreditInfoDTO creditInfoDTO);

    /**
     *  Get all the creditInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CreditInfoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" creditInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CreditInfoDTO findOne(Long id);

    /**
     *  Delete the "id" creditInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
