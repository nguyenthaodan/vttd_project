package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.FeeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Fee and its DTO FeeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FeeMapper extends EntityMapper <FeeDTO, Fee> {
    
    @Mapping(target = "services", ignore = true)
    @Mapping(target = "categories", ignore = true)
    Fee toEntity(FeeDTO feeDTO); 
    default Fee fromId(Long id) {
        if (id == null) {
            return null;
        }
        Fee fee = new Fee();
        fee.setId(id);
        return fee;
    }
}
