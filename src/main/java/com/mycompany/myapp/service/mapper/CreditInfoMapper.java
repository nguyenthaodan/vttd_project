package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.CreditInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CreditInfo and its DTO CreditInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CreditInfoMapper extends EntityMapper <CreditInfoDTO, CreditInfo> {
    
    
    default CreditInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        CreditInfo creditInfo = new CreditInfo();
        creditInfo.setId(id);
        return creditInfo;
    }
}
