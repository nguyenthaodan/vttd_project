package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.EventInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EventInfo and its DTO EventInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {SysUserMapper.class, JobTemplateMapper.class, })
public interface EventInfoMapper extends EntityMapper <EventInfoDTO, EventInfo> {

    @Mapping(source = "sysUser.id", target = "sysUserId")

    @Mapping(source = "jobTemplate.id", target = "jobTemplateId")
    EventInfoDTO toDto(EventInfo eventInfo); 

    @Mapping(source = "sysUserId", target = "sysUser")

    @Mapping(source = "jobTemplateId", target = "jobTemplate")
    EventInfo toEntity(EventInfoDTO eventInfoDTO); 
    default EventInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        EventInfo eventInfo = new EventInfo();
        eventInfo.setId(id);
        return eventInfo;
    }
}
