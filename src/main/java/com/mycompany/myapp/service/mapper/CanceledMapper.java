package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.CanceledDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Canceled and its DTO CanceledDTO.
 */
@Mapper(componentModel = "spring", uses = {JobTemplateMapper.class, })
public interface CanceledMapper extends EntityMapper <CanceledDTO, Canceled> {

    @Mapping(source = "jobTemplate.id", target = "jobTemplateId")
    CanceledDTO toDto(Canceled canceled); 

    @Mapping(source = "jobTemplateId", target = "jobTemplate")
    @Mapping(target = "sysUsers", ignore = true)
    Canceled toEntity(CanceledDTO canceledDTO); 
    default Canceled fromId(Long id) {
        if (id == null) {
            return null;
        }
        Canceled canceled = new Canceled();
        canceled.setId(id);
        return canceled;
    }
}
