package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.EventDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Event and its DTO EventDTO.
 */
@Mapper(componentModel = "spring", uses = {SysUserMapper.class, EventInfoMapper.class, })
public interface EventMapper extends EntityMapper <EventDTO, Event> {

    @Mapping(source = "sysUser.id", target = "sysUserId")

    @Mapping(source = "eventInfo.id", target = "eventInfoId")
    EventDTO toDto(Event event); 

    @Mapping(source = "sysUserId", target = "sysUser")

    @Mapping(source = "eventInfoId", target = "eventInfo")
    Event toEntity(EventDTO eventDTO); 
    default Event fromId(Long id) {
        if (id == null) {
            return null;
        }
        Event event = new Event();
        event.setId(id);
        return event;
    }
}
