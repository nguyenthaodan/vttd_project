package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ServiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Service and its DTO ServiceDTO.
 */
@Mapper(componentModel = "spring", uses = {PromotionMapper.class, FeeMapper.class, })
public interface ServiceMapper extends EntityMapper <ServiceDTO, Service> {

    @Mapping(source = "fee.id", target = "feeId")
    ServiceDTO toDto(Service service); 
    @Mapping(target = "categories", ignore = true)
    @Mapping(target = "form", ignore = true)

    @Mapping(source = "feeId", target = "fee")
    Service toEntity(ServiceDTO serviceDTO); 
    default Service fromId(Long id) {
        if (id == null) {
            return null;
        }
        Service service = new Service();
        service.setId(id);
        return service;
    }
}
