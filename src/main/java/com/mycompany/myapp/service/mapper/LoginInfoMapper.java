package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.LoginInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity LoginInfo and its DTO LoginInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LoginInfoMapper extends EntityMapper <LoginInfoDTO, LoginInfo> {
    
    @Mapping(target = "sysUser", ignore = true)
    LoginInfo toEntity(LoginInfoDTO loginInfoDTO); 
    default LoginInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setId(id);
        return loginInfo;
    }
}
