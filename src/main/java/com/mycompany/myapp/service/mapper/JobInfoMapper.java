package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.JobInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity JobInfo and its DTO JobInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JobInfoMapper extends EntityMapper <JobInfoDTO, JobInfo> {
    
    
    default JobInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        JobInfo jobInfo = new JobInfo();
        jobInfo.setId(id);
        return jobInfo;
    }
}
