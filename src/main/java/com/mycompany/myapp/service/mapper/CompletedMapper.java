package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.CompletedDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Completed and its DTO CompletedDTO.
 */
@Mapper(componentModel = "spring", uses = {JobTemplateMapper.class, InvoiceMapper.class, })
public interface CompletedMapper extends EntityMapper <CompletedDTO, Completed> {

    @Mapping(source = "jobTemplate.id", target = "jobTemplateId")

    @Mapping(source = "userInvoice.id", target = "userInvoiceId")

    @Mapping(source = "providerInvoice.id", target = "providerInvoiceId")
    CompletedDTO toDto(Completed completed); 

    @Mapping(source = "jobTemplateId", target = "jobTemplate")

    @Mapping(source = "userInvoiceId", target = "userInvoice")

    @Mapping(source = "providerInvoiceId", target = "providerInvoice")
    @Mapping(target = "review", ignore = true)
    Completed toEntity(CompletedDTO completedDTO); 
    default Completed fromId(Long id) {
        if (id == null) {
            return null;
        }
        Completed completed = new Completed();
        completed.setId(id);
        return completed;
    }
}
