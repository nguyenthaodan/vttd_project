package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ChatInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ChatInfo and its DTO ChatInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChatInfoMapper extends EntityMapper <ChatInfoDTO, ChatInfo> {
    
    
    default ChatInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChatInfo chatInfo = new ChatInfo();
        chatInfo.setId(id);
        return chatInfo;
    }
}
