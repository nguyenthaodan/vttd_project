package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.InvoiceInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity InvoiceInfo and its DTO InvoiceInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {BusinessMapper.class, })
public interface InvoiceInfoMapper extends EntityMapper <InvoiceInfoDTO, InvoiceInfo> {

    @Mapping(source = ".id", target = "Id")
    InvoiceInfoDTO toDto(InvoiceInfo invoiceInfo); 

    @Mapping(source = "Id", target = "")
    InvoiceInfo toEntity(InvoiceInfoDTO invoiceInfoDTO); 
    default InvoiceInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        InvoiceInfo invoiceInfo = new InvoiceInfo();
        invoiceInfo.setId(id);
        return invoiceInfo;
    }
}
