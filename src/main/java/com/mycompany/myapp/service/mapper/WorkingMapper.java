package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.WorkingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Working and its DTO WorkingDTO.
 */
@Mapper(componentModel = "spring", uses = {JobTemplateMapper.class, SysUserMapper.class, })
public interface WorkingMapper extends EntityMapper <WorkingDTO, Working> {

    @Mapping(source = "jobTemplate.id", target = "jobTemplateId")

    @Mapping(source = "sysUser.id", target = "sysUserId")
    WorkingDTO toDto(Working working); 

    @Mapping(source = "jobTemplateId", target = "jobTemplate")

    @Mapping(source = "sysUserId", target = "sysUser")
    Working toEntity(WorkingDTO workingDTO); 
    default Working fromId(Long id) {
        if (id == null) {
            return null;
        }
        Working working = new Working();
        working.setId(id);
        return working;
    }
}
