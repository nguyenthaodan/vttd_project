package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.JobTemplateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity JobTemplate and its DTO JobTemplateDTO.
 */
@Mapper(componentModel = "spring", uses = {ServiceMapper.class, SysUserMapper.class, })
public interface JobTemplateMapper extends EntityMapper <JobTemplateDTO, JobTemplate> {

    @Mapping(source = "service.id", target = "serviceId")

    @Mapping(source = "sysUser.id", target = "sysUserId")
    @Mapping(source = "sysUser.login", target = "sysUserLogin")
    JobTemplateDTO toDto(JobTemplate jobTemplate); 

    @Mapping(source = "serviceId", target = "service")
    @Mapping(target = "eventInfos", ignore = true)
    @Mapping(target = "locations", ignore = true)

    @Mapping(source = "sysUserId", target = "sysUser")
    JobTemplate toEntity(JobTemplateDTO jobTemplateDTO); 
    default JobTemplate fromId(Long id) {
        if (id == null) {
            return null;
        }
        JobTemplate jobTemplate = new JobTemplate();
        jobTemplate.setId(id);
        return jobTemplate;
    }
}
