package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.PushInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PushInfo and its DTO PushInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PushInfoMapper extends EntityMapper <PushInfoDTO, PushInfo> {
    
    
    default PushInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        PushInfo pushInfo = new PushInfo();
        pushInfo.setId(id);
        return pushInfo;
    }
}
