package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.InvoiceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Invoice and its DTO InvoiceDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, BusinessMapper.class, })
public interface InvoiceMapper extends EntityMapper <InvoiceDTO, Invoice> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")

    @Mapping(source = "business.id", target = "businessId")
    InvoiceDTO toDto(Invoice invoice); 

    @Mapping(source = "userId", target = "user")

    @Mapping(source = "businessId", target = "business")
    @Mapping(target = "completed", ignore = true)
    @Mapping(target = "completed", ignore = true)
    Invoice toEntity(InvoiceDTO invoiceDTO); 
    default Invoice fromId(Long id) {
        if (id == null) {
            return null;
        }
        Invoice invoice = new Invoice();
        invoice.setId(id);
        return invoice;
    }
}
