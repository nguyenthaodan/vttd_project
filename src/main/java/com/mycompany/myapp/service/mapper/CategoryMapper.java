package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.CategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Category and its DTO CategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {FeeMapper.class, ServiceMapper.class, })
public interface CategoryMapper extends EntityMapper <CategoryDTO, Category> {

    @Mapping(source = "category.id", target = "categoryId")

    @Mapping(source = "fee.id", target = "feeId")

    @Mapping(source = "service.id", target = "serviceId")
    CategoryDTO toDto(Category category); 

    @Mapping(source = "categoryId", target = "category")
    @Mapping(target = "parents", ignore = true)

    @Mapping(source = "feeId", target = "fee")

    @Mapping(source = "serviceId", target = "service")
    Category toEntity(CategoryDTO categoryDTO); 
    default Category fromId(Long id) {
        if (id == null) {
            return null;
        }
        Category category = new Category();
        category.setId(id);
        return category;
    }
}
