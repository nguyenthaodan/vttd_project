package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.NotificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Notification and its DTO NotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {EventMapper.class, SysUserMapper.class, })
public interface NotificationMapper extends EntityMapper <NotificationDTO, Notification> {

    @Mapping(source = "event.id", target = "eventId")
    NotificationDTO toDto(Notification notification); 

    @Mapping(source = "eventId", target = "event")
    Notification toEntity(NotificationDTO notificationDTO); 
    default Notification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Notification notification = new Notification();
        notification.setId(id);
        return notification;
    }
}
