package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ReviewDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Review and its DTO ReviewDTO.
 */
@Mapper(componentModel = "spring", uses = {CompletedMapper.class, SysUserMapper.class, BusinessMapper.class, })
public interface ReviewMapper extends EntityMapper <ReviewDTO, Review> {

    @Mapping(source = "completed.id", target = "completedId")

    @Mapping(source = "sysUser.id", target = "sysUserId")
    @Mapping(source = "sysUser.login", target = "sysUserLogin")

    @Mapping(source = "business.id", target = "businessId")
    @Mapping(source = "business.businessName", target = "businessBusinessName")
    ReviewDTO toDto(Review review); 

    @Mapping(source = "completedId", target = "completed")

    @Mapping(source = "sysUserId", target = "sysUser")

    @Mapping(source = "businessId", target = "business")
    Review toEntity(ReviewDTO reviewDTO); 
    default Review fromId(Long id) {
        if (id == null) {
            return null;
        }
        Review review = new Review();
        review.setId(id);
        return review;
    }
}
