package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ReviewInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ReviewInfo and its DTO ReviewInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReviewInfoMapper extends EntityMapper <ReviewInfoDTO, ReviewInfo> {
    
    
    default ReviewInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        ReviewInfo reviewInfo = new ReviewInfo();
        reviewInfo.setId(id);
        return reviewInfo;
    }
}
