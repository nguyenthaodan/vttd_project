package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.SysUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SysUser and its DTO SysUserDTO.
 */
@Mapper(componentModel = "spring", uses = {CreditInfoMapper.class, ChatInfoMapper.class, PushInfoMapper.class, ReviewInfoMapper.class, JobInfoMapper.class, LoginInfoMapper.class, LanguageMapper.class, LocationMapper.class, CanceledMapper.class, })
public interface SysUserMapper extends EntityMapper <SysUserDTO, SysUser> {

    @Mapping(source = "creditInfo.id", target = "creditInfoId")

    @Mapping(source = "chatInfo.id", target = "chatInfoId")

    @Mapping(source = "pushInfo.id", target = "pushInfoId")

    @Mapping(source = "reviewInfo.id", target = "reviewInfoId")

    @Mapping(source = "jobInfo.id", target = "jobInfoId")

    @Mapping(source = ".id", target = "Id")

    @Mapping(source = "language.id", target = "languageId")

    @Mapping(source = "location.id", target = "locationId")

    @Mapping(source = "canceled.id", target = "canceledId")
    SysUserDTO toDto(SysUser sysUser); 

    @Mapping(source = "creditInfoId", target = "creditInfo")

    @Mapping(source = "chatInfoId", target = "chatInfo")

    @Mapping(source = "pushInfoId", target = "pushInfo")

    @Mapping(source = "reviewInfoId", target = "reviewInfo")

    @Mapping(source = "jobInfoId", target = "jobInfo")

    @Mapping(source = "Id", target = "")
    @Mapping(target = "events", ignore = true)
    @Mapping(target = "eventInfos", ignore = true)
    @Mapping(target = "businesses", ignore = true)
    @Mapping(target = "workings", ignore = true)

    @Mapping(source = "languageId", target = "language")

    @Mapping(source = "locationId", target = "location")
    @Mapping(target = "jobTemplates", ignore = true)
    @Mapping(target = "reviews", ignore = true)

    @Mapping(source = "canceledId", target = "canceled")
    SysUser toEntity(SysUserDTO sysUserDTO); 
    default SysUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        SysUser sysUser = new SysUser();
        sysUser.setId(id);
        return sysUser;
    }
}
