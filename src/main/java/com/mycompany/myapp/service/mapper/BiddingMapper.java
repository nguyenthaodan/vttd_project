package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.BiddingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Bidding and its DTO BiddingDTO.
 */
@Mapper(componentModel = "spring", uses = {JobTemplateMapper.class, })
public interface BiddingMapper extends EntityMapper <BiddingDTO, Bidding> {

    @Mapping(source = "jobTemplate.id", target = "jobTemplateId")
    BiddingDTO toDto(Bidding bidding); 

    @Mapping(source = "jobTemplateId", target = "jobTemplate")
    @Mapping(target = "bids", ignore = true)
    Bidding toEntity(BiddingDTO biddingDTO); 
    default Bidding fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bidding bidding = new Bidding();
        bidding.setId(id);
        return bidding;
    }
}
