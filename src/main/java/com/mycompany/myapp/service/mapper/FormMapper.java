package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.FormDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Form and its DTO FormDTO.
 */
@Mapper(componentModel = "spring", uses = {ServiceMapper.class, })
public interface FormMapper extends EntityMapper <FormDTO, Form> {

    @Mapping(source = ".id", target = "Id")
    FormDTO toDto(Form form); 

    @Mapping(source = "Id", target = "")
    Form toEntity(FormDTO formDTO); 
    default Form fromId(Long id) {
        if (id == null) {
            return null;
        }
        Form form = new Form();
        form.setId(id);
        return form;
    }
}
