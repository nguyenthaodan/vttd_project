package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.BidDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Bid and its DTO BidDTO.
 */
@Mapper(componentModel = "spring", uses = {BiddingMapper.class, SysUserMapper.class, })
public interface BidMapper extends EntityMapper <BidDTO, Bid> {

    @Mapping(source = "bidding.id", target = "biddingId")

    @Mapping(source = "sysUser.id", target = "sysUserId")
    BidDTO toDto(Bid bid); 

    @Mapping(source = "biddingId", target = "bidding")

    @Mapping(source = "sysUserId", target = "sysUser")
    Bid toEntity(BidDTO bidDTO); 
    default Bid fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bid bid = new Bid();
        bid.setId(id);
        return bid;
    }
}
