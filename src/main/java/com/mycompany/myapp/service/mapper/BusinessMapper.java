package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.BusinessDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Business and its DTO BusinessDTO.
 */
@Mapper(componentModel = "spring", uses = {ServiceMapper.class, LocationMapper.class, SysUserMapper.class, })
public interface BusinessMapper extends EntityMapper <BusinessDTO, Business> {

    @Mapping(source = "sysUser.id", target = "sysUserId")
    BusinessDTO toDto(Business business); 
    @Mapping(target = "invoiceInfo", ignore = true)

    @Mapping(source = "sysUserId", target = "sysUser")
    @Mapping(target = "reviews", ignore = true)
    Business toEntity(BusinessDTO businessDTO); 
    default Business fromId(Long id) {
        if (id == null) {
            return null;
        }
        Business business = new Business();
        business.setId(id);
        return business;
    }
}
