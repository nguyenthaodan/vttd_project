package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.PageTableDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PageTable and its DTO PageTableDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PageTableMapper extends EntityMapper <PageTableDTO, PageTable> {
    
    
    default PageTable fromId(Long id) {
        if (id == null) {
            return null;
        }
        PageTable pageTable = new PageTable();
        pageTable.setId(id);
        return pageTable;
    }
}
