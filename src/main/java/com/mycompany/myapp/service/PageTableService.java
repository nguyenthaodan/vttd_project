package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.PageTableDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PageTable.
 */
public interface PageTableService {

    /**
     * Save a pageTable.
     *
     * @param pageTableDTO the entity to save
     * @return the persisted entity
     */
    PageTableDTO save(PageTableDTO pageTableDTO);

    /**
     *  Get all the pageTables.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PageTableDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" pageTable.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PageTableDTO findOne(Long id);

    /**
     *  Delete the "id" pageTable.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
