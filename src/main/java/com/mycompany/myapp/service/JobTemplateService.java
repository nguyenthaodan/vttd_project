package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.JobTemplateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing JobTemplate.
 */
public interface JobTemplateService {

    /**
     * Save a jobTemplate.
     *
     * @param jobTemplateDTO the entity to save
     * @return the persisted entity
     */
    JobTemplateDTO save(JobTemplateDTO jobTemplateDTO);

    /**
     *  Get all the jobTemplates.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<JobTemplateDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" jobTemplate.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    JobTemplateDTO findOne(Long id);

    /**
     *  Delete the "id" jobTemplate.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
