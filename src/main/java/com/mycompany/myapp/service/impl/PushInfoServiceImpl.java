package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.PushInfoService;
import com.mycompany.myapp.domain.PushInfo;
import com.mycompany.myapp.repository.PushInfoRepository;
import com.mycompany.myapp.service.dto.PushInfoDTO;
import com.mycompany.myapp.service.mapper.PushInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PushInfo.
 */
@Service
@Transactional
public class PushInfoServiceImpl implements PushInfoService{

    private final Logger log = LoggerFactory.getLogger(PushInfoServiceImpl.class);

    private final PushInfoRepository pushInfoRepository;

    private final PushInfoMapper pushInfoMapper;

    public PushInfoServiceImpl(PushInfoRepository pushInfoRepository, PushInfoMapper pushInfoMapper) {
        this.pushInfoRepository = pushInfoRepository;
        this.pushInfoMapper = pushInfoMapper;
    }

    /**
     * Save a pushInfo.
     *
     * @param pushInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PushInfoDTO save(PushInfoDTO pushInfoDTO) {
        log.debug("Request to save PushInfo : {}", pushInfoDTO);
        PushInfo pushInfo = pushInfoMapper.toEntity(pushInfoDTO);
        pushInfo = pushInfoRepository.save(pushInfo);
        return pushInfoMapper.toDto(pushInfo);
    }

    /**
     *  Get all the pushInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PushInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PushInfos");
        return pushInfoRepository.findAll(pageable)
            .map(pushInfoMapper::toDto);
    }

    /**
     *  Get one pushInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PushInfoDTO findOne(Long id) {
        log.debug("Request to get PushInfo : {}", id);
        PushInfo pushInfo = pushInfoRepository.findOne(id);
        return pushInfoMapper.toDto(pushInfo);
    }

    /**
     *  Delete the  pushInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PushInfo : {}", id);
        pushInfoRepository.delete(id);
    }
}
