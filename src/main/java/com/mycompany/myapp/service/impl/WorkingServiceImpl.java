package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.WorkingService;
import com.mycompany.myapp.domain.Working;
import com.mycompany.myapp.repository.WorkingRepository;
import com.mycompany.myapp.service.dto.WorkingDTO;
import com.mycompany.myapp.service.mapper.WorkingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Working.
 */
@Service
@Transactional
public class WorkingServiceImpl implements WorkingService{

    private final Logger log = LoggerFactory.getLogger(WorkingServiceImpl.class);

    private final WorkingRepository workingRepository;

    private final WorkingMapper workingMapper;

    public WorkingServiceImpl(WorkingRepository workingRepository, WorkingMapper workingMapper) {
        this.workingRepository = workingRepository;
        this.workingMapper = workingMapper;
    }

    /**
     * Save a working.
     *
     * @param workingDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WorkingDTO save(WorkingDTO workingDTO) {
        log.debug("Request to save Working : {}", workingDTO);
        Working working = workingMapper.toEntity(workingDTO);
        working = workingRepository.save(working);
        return workingMapper.toDto(working);
    }

    /**
     *  Get all the workings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WorkingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Workings");
        return workingRepository.findAll(pageable)
            .map(workingMapper::toDto);
    }

    /**
     *  Get one working by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public WorkingDTO findOne(Long id) {
        log.debug("Request to get Working : {}", id);
        Working working = workingRepository.findOne(id);
        return workingMapper.toDto(working);
    }

    /**
     *  Delete the  working by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Working : {}", id);
        workingRepository.delete(id);
    }
}
