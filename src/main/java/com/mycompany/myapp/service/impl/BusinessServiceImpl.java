package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.BusinessService;
import com.mycompany.myapp.domain.Business;
import com.mycompany.myapp.repository.BusinessRepository;
import com.mycompany.myapp.service.dto.BusinessDTO;
import com.mycompany.myapp.service.mapper.BusinessMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Business.
 */
@Service
@Transactional
public class BusinessServiceImpl implements BusinessService{

    private final Logger log = LoggerFactory.getLogger(BusinessServiceImpl.class);

    private final BusinessRepository businessRepository;

    private final BusinessMapper businessMapper;

    public BusinessServiceImpl(BusinessRepository businessRepository, BusinessMapper businessMapper) {
        this.businessRepository = businessRepository;
        this.businessMapper = businessMapper;
    }

    /**
     * Save a business.
     *
     * @param businessDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BusinessDTO save(BusinessDTO businessDTO) {
        log.debug("Request to save Business : {}", businessDTO);
        Business business = businessMapper.toEntity(businessDTO);
        business = businessRepository.save(business);
        return businessMapper.toDto(business);
    }

    /**
     *  Get all the businesses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BusinessDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Businesses");
        return businessRepository.findAll(pageable)
            .map(businessMapper::toDto);
    }


    /**
     *  get all the businesses where InvoiceInfo is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<BusinessDTO> findAllWhereInvoiceInfoIsNull() {
        log.debug("Request to get all businesses where InvoiceInfo is null");
        return StreamSupport
            .stream(businessRepository.findAll().spliterator(), false)
            .filter(business -> business.getInvoiceInfo() == null)
            .map(businessMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one business by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public BusinessDTO findOne(Long id) {
        log.debug("Request to get Business : {}", id);
        Business business = businessRepository.findOneWithEagerRelationships(id);
        return businessMapper.toDto(business);
    }

    /**
     *  Delete the  business by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Business : {}", id);
        businessRepository.delete(id);
    }
}
