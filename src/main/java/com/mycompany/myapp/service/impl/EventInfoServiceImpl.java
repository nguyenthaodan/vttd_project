package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.EventInfoService;
import com.mycompany.myapp.domain.EventInfo;
import com.mycompany.myapp.repository.EventInfoRepository;
import com.mycompany.myapp.service.dto.EventInfoDTO;
import com.mycompany.myapp.service.mapper.EventInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing EventInfo.
 */
@Service
@Transactional
public class EventInfoServiceImpl implements EventInfoService{

    private final Logger log = LoggerFactory.getLogger(EventInfoServiceImpl.class);

    private final EventInfoRepository eventInfoRepository;

    private final EventInfoMapper eventInfoMapper;

    public EventInfoServiceImpl(EventInfoRepository eventInfoRepository, EventInfoMapper eventInfoMapper) {
        this.eventInfoRepository = eventInfoRepository;
        this.eventInfoMapper = eventInfoMapper;
    }

    /**
     * Save a eventInfo.
     *
     * @param eventInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EventInfoDTO save(EventInfoDTO eventInfoDTO) {
        log.debug("Request to save EventInfo : {}", eventInfoDTO);
        EventInfo eventInfo = eventInfoMapper.toEntity(eventInfoDTO);
        eventInfo = eventInfoRepository.save(eventInfo);
        return eventInfoMapper.toDto(eventInfo);
    }

    /**
     *  Get all the eventInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EventInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EventInfos");
        return eventInfoRepository.findAll(pageable)
            .map(eventInfoMapper::toDto);
    }

    /**
     *  Get one eventInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EventInfoDTO findOne(Long id) {
        log.debug("Request to get EventInfo : {}", id);
        EventInfo eventInfo = eventInfoRepository.findOne(id);
        return eventInfoMapper.toDto(eventInfo);
    }

    /**
     *  Delete the  eventInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EventInfo : {}", id);
        eventInfoRepository.delete(id);
    }
}
