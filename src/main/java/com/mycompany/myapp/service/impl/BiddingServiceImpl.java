package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.BiddingService;
import com.mycompany.myapp.domain.Bidding;
import com.mycompany.myapp.repository.BiddingRepository;
import com.mycompany.myapp.service.dto.BiddingDTO;
import com.mycompany.myapp.service.mapper.BiddingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Bidding.
 */
@Service
@Transactional
public class BiddingServiceImpl implements BiddingService{

    private final Logger log = LoggerFactory.getLogger(BiddingServiceImpl.class);

    private final BiddingRepository biddingRepository;

    private final BiddingMapper biddingMapper;

    public BiddingServiceImpl(BiddingRepository biddingRepository, BiddingMapper biddingMapper) {
        this.biddingRepository = biddingRepository;
        this.biddingMapper = biddingMapper;
    }

    /**
     * Save a bidding.
     *
     * @param biddingDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BiddingDTO save(BiddingDTO biddingDTO) {
        log.debug("Request to save Bidding : {}", biddingDTO);
        Bidding bidding = biddingMapper.toEntity(biddingDTO);
        bidding = biddingRepository.save(bidding);
        return biddingMapper.toDto(bidding);
    }

    /**
     *  Get all the biddings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BiddingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Biddings");
        return biddingRepository.findAll(pageable)
            .map(biddingMapper::toDto);
    }

    /**
     *  Get one bidding by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public BiddingDTO findOne(Long id) {
        log.debug("Request to get Bidding : {}", id);
        Bidding bidding = biddingRepository.findOne(id);
        return biddingMapper.toDto(bidding);
    }

    /**
     *  Delete the  bidding by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bidding : {}", id);
        biddingRepository.delete(id);
    }
}
