package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.InvoiceInfoService;
import com.mycompany.myapp.domain.InvoiceInfo;
import com.mycompany.myapp.repository.InvoiceInfoRepository;
import com.mycompany.myapp.service.dto.InvoiceInfoDTO;
import com.mycompany.myapp.service.mapper.InvoiceInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing InvoiceInfo.
 */
@Service
@Transactional
public class InvoiceInfoServiceImpl implements InvoiceInfoService{

    private final Logger log = LoggerFactory.getLogger(InvoiceInfoServiceImpl.class);

    private final InvoiceInfoRepository invoiceInfoRepository;

    private final InvoiceInfoMapper invoiceInfoMapper;

    public InvoiceInfoServiceImpl(InvoiceInfoRepository invoiceInfoRepository, InvoiceInfoMapper invoiceInfoMapper) {
        this.invoiceInfoRepository = invoiceInfoRepository;
        this.invoiceInfoMapper = invoiceInfoMapper;
    }

    /**
     * Save a invoiceInfo.
     *
     * @param invoiceInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public InvoiceInfoDTO save(InvoiceInfoDTO invoiceInfoDTO) {
        log.debug("Request to save InvoiceInfo : {}", invoiceInfoDTO);
        InvoiceInfo invoiceInfo = invoiceInfoMapper.toEntity(invoiceInfoDTO);
        invoiceInfo = invoiceInfoRepository.save(invoiceInfo);
        return invoiceInfoMapper.toDto(invoiceInfo);
    }

    /**
     *  Get all the invoiceInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<InvoiceInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InvoiceInfos");
        return invoiceInfoRepository.findAll(pageable)
            .map(invoiceInfoMapper::toDto);
    }

    /**
     *  Get one invoiceInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public InvoiceInfoDTO findOne(Long id) {
        log.debug("Request to get InvoiceInfo : {}", id);
        InvoiceInfo invoiceInfo = invoiceInfoRepository.findOne(id);
        return invoiceInfoMapper.toDto(invoiceInfo);
    }

    /**
     *  Delete the  invoiceInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete InvoiceInfo : {}", id);
        invoiceInfoRepository.delete(id);
    }
}
