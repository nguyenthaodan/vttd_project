package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.CanceledService;
import com.mycompany.myapp.domain.Canceled;
import com.mycompany.myapp.repository.CanceledRepository;
import com.mycompany.myapp.service.dto.CanceledDTO;
import com.mycompany.myapp.service.mapper.CanceledMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Canceled.
 */
@Service
@Transactional
public class CanceledServiceImpl implements CanceledService{

    private final Logger log = LoggerFactory.getLogger(CanceledServiceImpl.class);

    private final CanceledRepository canceledRepository;

    private final CanceledMapper canceledMapper;

    public CanceledServiceImpl(CanceledRepository canceledRepository, CanceledMapper canceledMapper) {
        this.canceledRepository = canceledRepository;
        this.canceledMapper = canceledMapper;
    }

    /**
     * Save a canceled.
     *
     * @param canceledDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CanceledDTO save(CanceledDTO canceledDTO) {
        log.debug("Request to save Canceled : {}", canceledDTO);
        Canceled canceled = canceledMapper.toEntity(canceledDTO);
        canceled = canceledRepository.save(canceled);
        return canceledMapper.toDto(canceled);
    }

    /**
     *  Get all the canceleds.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CanceledDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Canceleds");
        return canceledRepository.findAll(pageable)
            .map(canceledMapper::toDto);
    }

    /**
     *  Get one canceled by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CanceledDTO findOne(Long id) {
        log.debug("Request to get Canceled : {}", id);
        Canceled canceled = canceledRepository.findOne(id);
        return canceledMapper.toDto(canceled);
    }

    /**
     *  Delete the  canceled by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Canceled : {}", id);
        canceledRepository.delete(id);
    }
}
