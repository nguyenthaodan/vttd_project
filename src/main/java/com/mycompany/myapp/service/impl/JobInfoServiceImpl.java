package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.JobInfoService;
import com.mycompany.myapp.domain.JobInfo;
import com.mycompany.myapp.repository.JobInfoRepository;
import com.mycompany.myapp.service.dto.JobInfoDTO;
import com.mycompany.myapp.service.mapper.JobInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing JobInfo.
 */
@Service
@Transactional
public class JobInfoServiceImpl implements JobInfoService{

    private final Logger log = LoggerFactory.getLogger(JobInfoServiceImpl.class);

    private final JobInfoRepository jobInfoRepository;

    private final JobInfoMapper jobInfoMapper;

    public JobInfoServiceImpl(JobInfoRepository jobInfoRepository, JobInfoMapper jobInfoMapper) {
        this.jobInfoRepository = jobInfoRepository;
        this.jobInfoMapper = jobInfoMapper;
    }

    /**
     * Save a jobInfo.
     *
     * @param jobInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public JobInfoDTO save(JobInfoDTO jobInfoDTO) {
        log.debug("Request to save JobInfo : {}", jobInfoDTO);
        JobInfo jobInfo = jobInfoMapper.toEntity(jobInfoDTO);
        jobInfo = jobInfoRepository.save(jobInfo);
        return jobInfoMapper.toDto(jobInfo);
    }

    /**
     *  Get all the jobInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JobInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all JobInfos");
        return jobInfoRepository.findAll(pageable)
            .map(jobInfoMapper::toDto);
    }

    /**
     *  Get one jobInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public JobInfoDTO findOne(Long id) {
        log.debug("Request to get JobInfo : {}", id);
        JobInfo jobInfo = jobInfoRepository.findOne(id);
        return jobInfoMapper.toDto(jobInfo);
    }

    /**
     *  Delete the  jobInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete JobInfo : {}", id);
        jobInfoRepository.delete(id);
    }
}
