package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.CreditInfoService;
import com.mycompany.myapp.domain.CreditInfo;
import com.mycompany.myapp.repository.CreditInfoRepository;
import com.mycompany.myapp.service.dto.CreditInfoDTO;
import com.mycompany.myapp.service.mapper.CreditInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing CreditInfo.
 */
@Service
@Transactional
public class CreditInfoServiceImpl implements CreditInfoService{

    private final Logger log = LoggerFactory.getLogger(CreditInfoServiceImpl.class);

    private final CreditInfoRepository creditInfoRepository;

    private final CreditInfoMapper creditInfoMapper;

    public CreditInfoServiceImpl(CreditInfoRepository creditInfoRepository, CreditInfoMapper creditInfoMapper) {
        this.creditInfoRepository = creditInfoRepository;
        this.creditInfoMapper = creditInfoMapper;
    }

    /**
     * Save a creditInfo.
     *
     * @param creditInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CreditInfoDTO save(CreditInfoDTO creditInfoDTO) {
        log.debug("Request to save CreditInfo : {}", creditInfoDTO);
        CreditInfo creditInfo = creditInfoMapper.toEntity(creditInfoDTO);
        creditInfo = creditInfoRepository.save(creditInfo);
        return creditInfoMapper.toDto(creditInfo);
    }

    /**
     *  Get all the creditInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CreditInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CreditInfos");
        return creditInfoRepository.findAll(pageable)
            .map(creditInfoMapper::toDto);
    }

    /**
     *  Get one creditInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CreditInfoDTO findOne(Long id) {
        log.debug("Request to get CreditInfo : {}", id);
        CreditInfo creditInfo = creditInfoRepository.findOne(id);
        return creditInfoMapper.toDto(creditInfo);
    }

    /**
     *  Delete the  creditInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CreditInfo : {}", id);
        creditInfoRepository.delete(id);
    }
}
