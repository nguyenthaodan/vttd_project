package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.PageTableService;
import com.mycompany.myapp.domain.PageTable;
import com.mycompany.myapp.repository.PageTableRepository;
import com.mycompany.myapp.service.dto.PageTableDTO;
import com.mycompany.myapp.service.mapper.PageTableMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PageTable.
 */
@Service
@Transactional
public class PageTableServiceImpl implements PageTableService{

    private final Logger log = LoggerFactory.getLogger(PageTableServiceImpl.class);

    private final PageTableRepository pageTableRepository;

    private final PageTableMapper pageTableMapper;

    public PageTableServiceImpl(PageTableRepository pageTableRepository, PageTableMapper pageTableMapper) {
        this.pageTableRepository = pageTableRepository;
        this.pageTableMapper = pageTableMapper;
    }

    /**
     * Save a pageTable.
     *
     * @param pageTableDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PageTableDTO save(PageTableDTO pageTableDTO) {
        log.debug("Request to save PageTable : {}", pageTableDTO);
        PageTable pageTable = pageTableMapper.toEntity(pageTableDTO);
        pageTable = pageTableRepository.save(pageTable);
        return pageTableMapper.toDto(pageTable);
    }

    /**
     *  Get all the pageTables.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PageTableDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PageTables");
        return pageTableRepository.findAll(pageable)
            .map(pageTableMapper::toDto);
    }

    /**
     *  Get one pageTable by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PageTableDTO findOne(Long id) {
        log.debug("Request to get PageTable : {}", id);
        PageTable pageTable = pageTableRepository.findOne(id);
        return pageTableMapper.toDto(pageTable);
    }

    /**
     *  Delete the  pageTable by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PageTable : {}", id);
        pageTableRepository.delete(id);
    }
}
