package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.CompletedService;
import com.mycompany.myapp.domain.Completed;
import com.mycompany.myapp.repository.CompletedRepository;
import com.mycompany.myapp.service.dto.CompletedDTO;
import com.mycompany.myapp.service.mapper.CompletedMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Completed.
 */
@Service
@Transactional
public class CompletedServiceImpl implements CompletedService{

    private final Logger log = LoggerFactory.getLogger(CompletedServiceImpl.class);

    private final CompletedRepository completedRepository;

    private final CompletedMapper completedMapper;

    public CompletedServiceImpl(CompletedRepository completedRepository, CompletedMapper completedMapper) {
        this.completedRepository = completedRepository;
        this.completedMapper = completedMapper;
    }

    /**
     * Save a completed.
     *
     * @param completedDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CompletedDTO save(CompletedDTO completedDTO) {
        log.debug("Request to save Completed : {}", completedDTO);
        Completed completed = completedMapper.toEntity(completedDTO);
        completed = completedRepository.save(completed);
        return completedMapper.toDto(completed);
    }

    /**
     *  Get all the completeds.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CompletedDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Completeds");
        return completedRepository.findAll(pageable)
            .map(completedMapper::toDto);
    }


    /**
     *  get all the completeds where Review is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<CompletedDTO> findAllWhereReviewIsNull() {
        log.debug("Request to get all completeds where Review is null");
        return StreamSupport
            .stream(completedRepository.findAll().spliterator(), false)
            .filter(completed -> completed.getReview() == null)
            .map(completedMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one completed by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CompletedDTO findOne(Long id) {
        log.debug("Request to get Completed : {}", id);
        Completed completed = completedRepository.findOne(id);
        return completedMapper.toDto(completed);
    }

    /**
     *  Delete the  completed by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Completed : {}", id);
        completedRepository.delete(id);
    }
}
