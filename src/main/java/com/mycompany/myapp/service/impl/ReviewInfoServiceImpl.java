package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.ReviewInfoService;
import com.mycompany.myapp.domain.ReviewInfo;
import com.mycompany.myapp.repository.ReviewInfoRepository;
import com.mycompany.myapp.service.dto.ReviewInfoDTO;
import com.mycompany.myapp.service.mapper.ReviewInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ReviewInfo.
 */
@Service
@Transactional
public class ReviewInfoServiceImpl implements ReviewInfoService{

    private final Logger log = LoggerFactory.getLogger(ReviewInfoServiceImpl.class);

    private final ReviewInfoRepository reviewInfoRepository;

    private final ReviewInfoMapper reviewInfoMapper;

    public ReviewInfoServiceImpl(ReviewInfoRepository reviewInfoRepository, ReviewInfoMapper reviewInfoMapper) {
        this.reviewInfoRepository = reviewInfoRepository;
        this.reviewInfoMapper = reviewInfoMapper;
    }

    /**
     * Save a reviewInfo.
     *
     * @param reviewInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ReviewInfoDTO save(ReviewInfoDTO reviewInfoDTO) {
        log.debug("Request to save ReviewInfo : {}", reviewInfoDTO);
        ReviewInfo reviewInfo = reviewInfoMapper.toEntity(reviewInfoDTO);
        reviewInfo = reviewInfoRepository.save(reviewInfo);
        return reviewInfoMapper.toDto(reviewInfo);
    }

    /**
     *  Get all the reviewInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ReviewInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ReviewInfos");
        return reviewInfoRepository.findAll(pageable)
            .map(reviewInfoMapper::toDto);
    }

    /**
     *  Get one reviewInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ReviewInfoDTO findOne(Long id) {
        log.debug("Request to get ReviewInfo : {}", id);
        ReviewInfo reviewInfo = reviewInfoRepository.findOne(id);
        return reviewInfoMapper.toDto(reviewInfo);
    }

    /**
     *  Delete the  reviewInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ReviewInfo : {}", id);
        reviewInfoRepository.delete(id);
    }
}
