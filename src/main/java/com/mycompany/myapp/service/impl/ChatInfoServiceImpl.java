package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.ChatInfoService;
import com.mycompany.myapp.domain.ChatInfo;
import com.mycompany.myapp.repository.ChatInfoRepository;
import com.mycompany.myapp.service.dto.ChatInfoDTO;
import com.mycompany.myapp.service.mapper.ChatInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ChatInfo.
 */
@Service
@Transactional
public class ChatInfoServiceImpl implements ChatInfoService{

    private final Logger log = LoggerFactory.getLogger(ChatInfoServiceImpl.class);

    private final ChatInfoRepository chatInfoRepository;

    private final ChatInfoMapper chatInfoMapper;

    public ChatInfoServiceImpl(ChatInfoRepository chatInfoRepository, ChatInfoMapper chatInfoMapper) {
        this.chatInfoRepository = chatInfoRepository;
        this.chatInfoMapper = chatInfoMapper;
    }

    /**
     * Save a chatInfo.
     *
     * @param chatInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ChatInfoDTO save(ChatInfoDTO chatInfoDTO) {
        log.debug("Request to save ChatInfo : {}", chatInfoDTO);
        ChatInfo chatInfo = chatInfoMapper.toEntity(chatInfoDTO);
        chatInfo = chatInfoRepository.save(chatInfo);
        return chatInfoMapper.toDto(chatInfo);
    }

    /**
     *  Get all the chatInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ChatInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ChatInfos");
        return chatInfoRepository.findAll(pageable)
            .map(chatInfoMapper::toDto);
    }

    /**
     *  Get one chatInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ChatInfoDTO findOne(Long id) {
        log.debug("Request to get ChatInfo : {}", id);
        ChatInfo chatInfo = chatInfoRepository.findOne(id);
        return chatInfoMapper.toDto(chatInfo);
    }

    /**
     *  Delete the  chatInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChatInfo : {}", id);
        chatInfoRepository.delete(id);
    }
}
