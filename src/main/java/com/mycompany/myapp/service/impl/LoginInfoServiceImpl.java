package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.LoginInfoService;
import com.mycompany.myapp.domain.LoginInfo;
import com.mycompany.myapp.repository.LoginInfoRepository;
import com.mycompany.myapp.service.dto.LoginInfoDTO;
import com.mycompany.myapp.service.mapper.LoginInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing LoginInfo.
 */
@Service
@Transactional
public class LoginInfoServiceImpl implements LoginInfoService{

    private final Logger log = LoggerFactory.getLogger(LoginInfoServiceImpl.class);

    private final LoginInfoRepository loginInfoRepository;

    private final LoginInfoMapper loginInfoMapper;

    public LoginInfoServiceImpl(LoginInfoRepository loginInfoRepository, LoginInfoMapper loginInfoMapper) {
        this.loginInfoRepository = loginInfoRepository;
        this.loginInfoMapper = loginInfoMapper;
    }

    /**
     * Save a loginInfo.
     *
     * @param loginInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public LoginInfoDTO save(LoginInfoDTO loginInfoDTO) {
        log.debug("Request to save LoginInfo : {}", loginInfoDTO);
        LoginInfo loginInfo = loginInfoMapper.toEntity(loginInfoDTO);
        loginInfo = loginInfoRepository.save(loginInfo);
        return loginInfoMapper.toDto(loginInfo);
    }

    /**
     *  Get all the loginInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoginInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoginInfos");
        return loginInfoRepository.findAll(pageable)
            .map(loginInfoMapper::toDto);
    }


    /**
     *  get all the loginInfos where SysUser is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<LoginInfoDTO> findAllWhereSysUserIsNull() {
        log.debug("Request to get all loginInfos where SysUser is null");
        return StreamSupport
            .stream(loginInfoRepository.findAll().spliterator(), false)
            .filter(loginInfo -> loginInfo.getSysUser() == null)
            .map(loginInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one loginInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public LoginInfoDTO findOne(Long id) {
        log.debug("Request to get LoginInfo : {}", id);
        LoginInfo loginInfo = loginInfoRepository.findOne(id);
        return loginInfoMapper.toDto(loginInfo);
    }

    /**
     *  Delete the  loginInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoginInfo : {}", id);
        loginInfoRepository.delete(id);
    }
}
