package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.JobTemplateService;
import com.mycompany.myapp.domain.JobTemplate;
import com.mycompany.myapp.repository.JobTemplateRepository;
import com.mycompany.myapp.service.dto.JobTemplateDTO;
import com.mycompany.myapp.service.mapper.JobTemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing JobTemplate.
 */
@Service
@Transactional
public class JobTemplateServiceImpl implements JobTemplateService{

    private final Logger log = LoggerFactory.getLogger(JobTemplateServiceImpl.class);

    private final JobTemplateRepository jobTemplateRepository;

    private final JobTemplateMapper jobTemplateMapper;

    public JobTemplateServiceImpl(JobTemplateRepository jobTemplateRepository, JobTemplateMapper jobTemplateMapper) {
        this.jobTemplateRepository = jobTemplateRepository;
        this.jobTemplateMapper = jobTemplateMapper;
    }

    /**
     * Save a jobTemplate.
     *
     * @param jobTemplateDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public JobTemplateDTO save(JobTemplateDTO jobTemplateDTO) {
        log.debug("Request to save JobTemplate : {}", jobTemplateDTO);
        JobTemplate jobTemplate = jobTemplateMapper.toEntity(jobTemplateDTO);
        jobTemplate = jobTemplateRepository.save(jobTemplate);
        return jobTemplateMapper.toDto(jobTemplate);
    }

    /**
     *  Get all the jobTemplates.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JobTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all JobTemplates");
        return jobTemplateRepository.findAll(pageable)
            .map(jobTemplateMapper::toDto);
    }

    /**
     *  Get one jobTemplate by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public JobTemplateDTO findOne(Long id) {
        log.debug("Request to get JobTemplate : {}", id);
        JobTemplate jobTemplate = jobTemplateRepository.findOne(id);
        return jobTemplateMapper.toDto(jobTemplate);
    }

    /**
     *  Delete the  jobTemplate by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete JobTemplate : {}", id);
        jobTemplateRepository.delete(id);
    }
}
