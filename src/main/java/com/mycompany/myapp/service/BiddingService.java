package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.BiddingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Bidding.
 */
public interface BiddingService {

    /**
     * Save a bidding.
     *
     * @param biddingDTO the entity to save
     * @return the persisted entity
     */
    BiddingDTO save(BiddingDTO biddingDTO);

    /**
     *  Get all the biddings.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<BiddingDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" bidding.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    BiddingDTO findOne(Long id);

    /**
     *  Delete the "id" bidding.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
