package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.InvoiceInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing InvoiceInfo.
 */
public interface InvoiceInfoService {

    /**
     * Save a invoiceInfo.
     *
     * @param invoiceInfoDTO the entity to save
     * @return the persisted entity
     */
    InvoiceInfoDTO save(InvoiceInfoDTO invoiceInfoDTO);

    /**
     *  Get all the invoiceInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<InvoiceInfoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" invoiceInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    InvoiceInfoDTO findOne(Long id);

    /**
     *  Delete the "id" invoiceInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
