package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.ReviewInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ReviewInfo.
 */
public interface ReviewInfoService {

    /**
     * Save a reviewInfo.
     *
     * @param reviewInfoDTO the entity to save
     * @return the persisted entity
     */
    ReviewInfoDTO save(ReviewInfoDTO reviewInfoDTO);

    /**
     *  Get all the reviewInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ReviewInfoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" reviewInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ReviewInfoDTO findOne(Long id);

    /**
     *  Delete the "id" reviewInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
