package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.ChatInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ChatInfo.
 */
public interface ChatInfoService {

    /**
     * Save a chatInfo.
     *
     * @param chatInfoDTO the entity to save
     * @return the persisted entity
     */
    ChatInfoDTO save(ChatInfoDTO chatInfoDTO);

    /**
     *  Get all the chatInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ChatInfoDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" chatInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ChatInfoDTO findOne(Long id);

    /**
     *  Delete the "id" chatInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
