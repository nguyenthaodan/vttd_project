package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.CanceledDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Canceled.
 */
public interface CanceledService {

    /**
     * Save a canceled.
     *
     * @param canceledDTO the entity to save
     * @return the persisted entity
     */
    CanceledDTO save(CanceledDTO canceledDTO);

    /**
     *  Get all the canceleds.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CanceledDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" canceled.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CanceledDTO findOne(Long id);

    /**
     *  Delete the "id" canceled.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
