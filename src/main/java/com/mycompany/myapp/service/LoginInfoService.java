package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.LoginInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing LoginInfo.
 */
public interface LoginInfoService {

    /**
     * Save a loginInfo.
     *
     * @param loginInfoDTO the entity to save
     * @return the persisted entity
     */
    LoginInfoDTO save(LoginInfoDTO loginInfoDTO);

    /**
     *  Get all the loginInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<LoginInfoDTO> findAll(Pageable pageable);
    /**
     *  Get all the LoginInfoDTO where SysUser is null.
     *
     *  @return the list of entities
     */
    List<LoginInfoDTO> findAllWhereSysUserIsNull();

    /**
     *  Get the "id" loginInfo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    LoginInfoDTO findOne(Long id);

    /**
     *  Delete the "id" loginInfo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
