package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.BiddingService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.BiddingDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Bidding.
 */
@RestController
@RequestMapping("/api")
public class BiddingResource {

    private final Logger log = LoggerFactory.getLogger(BiddingResource.class);

    private static final String ENTITY_NAME = "bidding";

    private final BiddingService biddingService;

    public BiddingResource(BiddingService biddingService) {
        this.biddingService = biddingService;
    }

    /**
     * POST  /biddings : Create a new bidding.
     *
     * @param biddingDTO the biddingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new biddingDTO, or with status 400 (Bad Request) if the bidding has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/biddings")
    @Timed
    public ResponseEntity<BiddingDTO> createBidding(@RequestBody BiddingDTO biddingDTO) throws URISyntaxException {
        log.debug("REST request to save Bidding : {}", biddingDTO);
        if (biddingDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new bidding cannot already have an ID")).body(null);
        }
        BiddingDTO result = biddingService.save(biddingDTO);
        return ResponseEntity.created(new URI("/api/biddings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /biddings : Updates an existing bidding.
     *
     * @param biddingDTO the biddingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated biddingDTO,
     * or with status 400 (Bad Request) if the biddingDTO is not valid,
     * or with status 500 (Internal Server Error) if the biddingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/biddings")
    @Timed
    public ResponseEntity<BiddingDTO> updateBidding(@RequestBody BiddingDTO biddingDTO) throws URISyntaxException {
        log.debug("REST request to update Bidding : {}", biddingDTO);
        if (biddingDTO.getId() == null) {
            return createBidding(biddingDTO);
        }
        BiddingDTO result = biddingService.save(biddingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, biddingDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /biddings : get all the biddings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of biddings in body
     */
    @GetMapping("/biddings")
    @Timed
    public ResponseEntity<List<BiddingDTO>> getAllBiddings(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Biddings");
        Page<BiddingDTO> page = biddingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/biddings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /biddings/:id : get the "id" bidding.
     *
     * @param id the id of the biddingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the biddingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/biddings/{id}")
    @Timed
    public ResponseEntity<BiddingDTO> getBidding(@PathVariable Long id) {
        log.debug("REST request to get Bidding : {}", id);
        BiddingDTO biddingDTO = biddingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(biddingDTO));
    }

    /**
     * DELETE  /biddings/:id : delete the "id" bidding.
     *
     * @param id the id of the biddingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/biddings/{id}")
    @Timed
    public ResponseEntity<Void> deleteBidding(@PathVariable Long id) {
        log.debug("REST request to delete Bidding : {}", id);
        biddingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
