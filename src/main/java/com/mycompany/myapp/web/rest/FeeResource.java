package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.FeeService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.FeeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Fee.
 */
@RestController
@RequestMapping("/api")
public class FeeResource {

    private final Logger log = LoggerFactory.getLogger(FeeResource.class);

    private static final String ENTITY_NAME = "fee";

    private final FeeService feeService;

    public FeeResource(FeeService feeService) {
        this.feeService = feeService;
    }

    /**
     * POST  /fees : Create a new fee.
     *
     * @param feeDTO the feeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new feeDTO, or with status 400 (Bad Request) if the fee has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/fees")
    @Timed
    public ResponseEntity<FeeDTO> createFee(@RequestBody FeeDTO feeDTO) throws URISyntaxException {
        log.debug("REST request to save Fee : {}", feeDTO);
        if (feeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new fee cannot already have an ID")).body(null);
        }
        FeeDTO result = feeService.save(feeDTO);
        return ResponseEntity.created(new URI("/api/fees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /fees : Updates an existing fee.
     *
     * @param feeDTO the feeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated feeDTO,
     * or with status 400 (Bad Request) if the feeDTO is not valid,
     * or with status 500 (Internal Server Error) if the feeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/fees")
    @Timed
    public ResponseEntity<FeeDTO> updateFee(@RequestBody FeeDTO feeDTO) throws URISyntaxException {
        log.debug("REST request to update Fee : {}", feeDTO);
        if (feeDTO.getId() == null) {
            return createFee(feeDTO);
        }
        FeeDTO result = feeService.save(feeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, feeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /fees : get all the fees.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of fees in body
     */
    @GetMapping("/fees")
    @Timed
    public ResponseEntity<List<FeeDTO>> getAllFees(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Fees");
        Page<FeeDTO> page = feeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/fees");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /fees/:id : get the "id" fee.
     *
     * @param id the id of the feeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the feeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/fees/{id}")
    @Timed
    public ResponseEntity<FeeDTO> getFee(@PathVariable Long id) {
        log.debug("REST request to get Fee : {}", id);
        FeeDTO feeDTO = feeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(feeDTO));
    }

    /**
     * DELETE  /fees/:id : delete the "id" fee.
     *
     * @param id the id of the feeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fees/{id}")
    @Timed
    public ResponseEntity<Void> deleteFee(@PathVariable Long id) {
        log.debug("REST request to delete Fee : {}", id);
        feeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
