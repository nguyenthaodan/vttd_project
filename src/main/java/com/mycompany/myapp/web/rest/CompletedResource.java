package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.CompletedService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.CompletedDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Completed.
 */
@RestController
@RequestMapping("/api")
public class CompletedResource {

    private final Logger log = LoggerFactory.getLogger(CompletedResource.class);

    private static final String ENTITY_NAME = "completed";

    private final CompletedService completedService;

    public CompletedResource(CompletedService completedService) {
        this.completedService = completedService;
    }

    /**
     * POST  /completeds : Create a new completed.
     *
     * @param completedDTO the completedDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new completedDTO, or with status 400 (Bad Request) if the completed has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/completeds")
    @Timed
    public ResponseEntity<CompletedDTO> createCompleted(@RequestBody CompletedDTO completedDTO) throws URISyntaxException {
        log.debug("REST request to save Completed : {}", completedDTO);
        if (completedDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new completed cannot already have an ID")).body(null);
        }
        CompletedDTO result = completedService.save(completedDTO);
        return ResponseEntity.created(new URI("/api/completeds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /completeds : Updates an existing completed.
     *
     * @param completedDTO the completedDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated completedDTO,
     * or with status 400 (Bad Request) if the completedDTO is not valid,
     * or with status 500 (Internal Server Error) if the completedDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/completeds")
    @Timed
    public ResponseEntity<CompletedDTO> updateCompleted(@RequestBody CompletedDTO completedDTO) throws URISyntaxException {
        log.debug("REST request to update Completed : {}", completedDTO);
        if (completedDTO.getId() == null) {
            return createCompleted(completedDTO);
        }
        CompletedDTO result = completedService.save(completedDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, completedDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /completeds : get all the completeds.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of completeds in body
     */
    @GetMapping("/completeds")
    @Timed
    public ResponseEntity<List<CompletedDTO>> getAllCompleteds(@ApiParam Pageable pageable, @RequestParam(required = false) String filter) {
        if ("review-is-null".equals(filter)) {
            log.debug("REST request to get all Completeds where review is null");
            return new ResponseEntity<>(completedService.findAllWhereReviewIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of Completeds");
        Page<CompletedDTO> page = completedService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/completeds");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /completeds/:id : get the "id" completed.
     *
     * @param id the id of the completedDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the completedDTO, or with status 404 (Not Found)
     */
    @GetMapping("/completeds/{id}")
    @Timed
    public ResponseEntity<CompletedDTO> getCompleted(@PathVariable Long id) {
        log.debug("REST request to get Completed : {}", id);
        CompletedDTO completedDTO = completedService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(completedDTO));
    }

    /**
     * DELETE  /completeds/:id : delete the "id" completed.
     *
     * @param id the id of the completedDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/completeds/{id}")
    @Timed
    public ResponseEntity<Void> deleteCompleted(@PathVariable Long id) {
        log.debug("REST request to delete Completed : {}", id);
        completedService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
