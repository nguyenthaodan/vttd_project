package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.JobTemplateService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.JobTemplateDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing JobTemplate.
 */
@RestController
@RequestMapping("/api")
public class JobTemplateResource {

    private final Logger log = LoggerFactory.getLogger(JobTemplateResource.class);

    private static final String ENTITY_NAME = "jobTemplate";

    private final JobTemplateService jobTemplateService;

    public JobTemplateResource(JobTemplateService jobTemplateService) {
        this.jobTemplateService = jobTemplateService;
    }

    /**
     * POST  /job-templates : Create a new jobTemplate.
     *
     * @param jobTemplateDTO the jobTemplateDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jobTemplateDTO, or with status 400 (Bad Request) if the jobTemplate has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/job-templates")
    @Timed
    public ResponseEntity<JobTemplateDTO> createJobTemplate(@Valid @RequestBody JobTemplateDTO jobTemplateDTO) throws URISyntaxException {
        log.debug("REST request to save JobTemplate : {}", jobTemplateDTO);
        if (jobTemplateDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new jobTemplate cannot already have an ID")).body(null);
        }
        JobTemplateDTO result = jobTemplateService.save(jobTemplateDTO);
        return ResponseEntity.created(new URI("/api/job-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /job-templates : Updates an existing jobTemplate.
     *
     * @param jobTemplateDTO the jobTemplateDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jobTemplateDTO,
     * or with status 400 (Bad Request) if the jobTemplateDTO is not valid,
     * or with status 500 (Internal Server Error) if the jobTemplateDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/job-templates")
    @Timed
    public ResponseEntity<JobTemplateDTO> updateJobTemplate(@Valid @RequestBody JobTemplateDTO jobTemplateDTO) throws URISyntaxException {
        log.debug("REST request to update JobTemplate : {}", jobTemplateDTO);
        if (jobTemplateDTO.getId() == null) {
            return createJobTemplate(jobTemplateDTO);
        }
        JobTemplateDTO result = jobTemplateService.save(jobTemplateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jobTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /job-templates : get all the jobTemplates.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jobTemplates in body
     */
    @GetMapping("/job-templates")
    @Timed
    public ResponseEntity<List<JobTemplateDTO>> getAllJobTemplates(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of JobTemplates");
        Page<JobTemplateDTO> page = jobTemplateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/job-templates");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /job-templates/:id : get the "id" jobTemplate.
     *
     * @param id the id of the jobTemplateDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jobTemplateDTO, or with status 404 (Not Found)
     */
    @GetMapping("/job-templates/{id}")
    @Timed
    public ResponseEntity<JobTemplateDTO> getJobTemplate(@PathVariable Long id) {
        log.debug("REST request to get JobTemplate : {}", id);
        JobTemplateDTO jobTemplateDTO = jobTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jobTemplateDTO));
    }

    /**
     * DELETE  /job-templates/:id : delete the "id" jobTemplate.
     *
     * @param id the id of the jobTemplateDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/job-templates/{id}")
    @Timed
    public ResponseEntity<Void> deleteJobTemplate(@PathVariable Long id) {
        log.debug("REST request to delete JobTemplate : {}", id);
        jobTemplateService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
