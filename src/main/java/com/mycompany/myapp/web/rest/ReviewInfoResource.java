package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.ReviewInfoService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.ReviewInfoDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ReviewInfo.
 */
@RestController
@RequestMapping("/api")
public class ReviewInfoResource {

    private final Logger log = LoggerFactory.getLogger(ReviewInfoResource.class);

    private static final String ENTITY_NAME = "reviewInfo";

    private final ReviewInfoService reviewInfoService;

    public ReviewInfoResource(ReviewInfoService reviewInfoService) {
        this.reviewInfoService = reviewInfoService;
    }

    /**
     * POST  /review-infos : Create a new reviewInfo.
     *
     * @param reviewInfoDTO the reviewInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reviewInfoDTO, or with status 400 (Bad Request) if the reviewInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/review-infos")
    @Timed
    public ResponseEntity<ReviewInfoDTO> createReviewInfo(@RequestBody ReviewInfoDTO reviewInfoDTO) throws URISyntaxException {
        log.debug("REST request to save ReviewInfo : {}", reviewInfoDTO);
        if (reviewInfoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new reviewInfo cannot already have an ID")).body(null);
        }
        ReviewInfoDTO result = reviewInfoService.save(reviewInfoDTO);
        return ResponseEntity.created(new URI("/api/review-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /review-infos : Updates an existing reviewInfo.
     *
     * @param reviewInfoDTO the reviewInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reviewInfoDTO,
     * or with status 400 (Bad Request) if the reviewInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the reviewInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/review-infos")
    @Timed
    public ResponseEntity<ReviewInfoDTO> updateReviewInfo(@RequestBody ReviewInfoDTO reviewInfoDTO) throws URISyntaxException {
        log.debug("REST request to update ReviewInfo : {}", reviewInfoDTO);
        if (reviewInfoDTO.getId() == null) {
            return createReviewInfo(reviewInfoDTO);
        }
        ReviewInfoDTO result = reviewInfoService.save(reviewInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reviewInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /review-infos : get all the reviewInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of reviewInfos in body
     */
    @GetMapping("/review-infos")
    @Timed
    public ResponseEntity<List<ReviewInfoDTO>> getAllReviewInfos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ReviewInfos");
        Page<ReviewInfoDTO> page = reviewInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/review-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /review-infos/:id : get the "id" reviewInfo.
     *
     * @param id the id of the reviewInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reviewInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/review-infos/{id}")
    @Timed
    public ResponseEntity<ReviewInfoDTO> getReviewInfo(@PathVariable Long id) {
        log.debug("REST request to get ReviewInfo : {}", id);
        ReviewInfoDTO reviewInfoDTO = reviewInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reviewInfoDTO));
    }

    /**
     * DELETE  /review-infos/:id : delete the "id" reviewInfo.
     *
     * @param id the id of the reviewInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/review-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteReviewInfo(@PathVariable Long id) {
        log.debug("REST request to delete ReviewInfo : {}", id);
        reviewInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
