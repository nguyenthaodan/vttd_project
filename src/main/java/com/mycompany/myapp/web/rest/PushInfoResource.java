package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.PushInfoService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.PushInfoDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PushInfo.
 */
@RestController
@RequestMapping("/api")
public class PushInfoResource {

    private final Logger log = LoggerFactory.getLogger(PushInfoResource.class);

    private static final String ENTITY_NAME = "pushInfo";

    private final PushInfoService pushInfoService;

    public PushInfoResource(PushInfoService pushInfoService) {
        this.pushInfoService = pushInfoService;
    }

    /**
     * POST  /push-infos : Create a new pushInfo.
     *
     * @param pushInfoDTO the pushInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pushInfoDTO, or with status 400 (Bad Request) if the pushInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/push-infos")
    @Timed
    public ResponseEntity<PushInfoDTO> createPushInfo(@RequestBody PushInfoDTO pushInfoDTO) throws URISyntaxException {
        log.debug("REST request to save PushInfo : {}", pushInfoDTO);
        if (pushInfoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pushInfo cannot already have an ID")).body(null);
        }
        PushInfoDTO result = pushInfoService.save(pushInfoDTO);
        return ResponseEntity.created(new URI("/api/push-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /push-infos : Updates an existing pushInfo.
     *
     * @param pushInfoDTO the pushInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pushInfoDTO,
     * or with status 400 (Bad Request) if the pushInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the pushInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/push-infos")
    @Timed
    public ResponseEntity<PushInfoDTO> updatePushInfo(@RequestBody PushInfoDTO pushInfoDTO) throws URISyntaxException {
        log.debug("REST request to update PushInfo : {}", pushInfoDTO);
        if (pushInfoDTO.getId() == null) {
            return createPushInfo(pushInfoDTO);
        }
        PushInfoDTO result = pushInfoService.save(pushInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pushInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /push-infos : get all the pushInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pushInfos in body
     */
    @GetMapping("/push-infos")
    @Timed
    public ResponseEntity<List<PushInfoDTO>> getAllPushInfos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PushInfos");
        Page<PushInfoDTO> page = pushInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/push-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /push-infos/:id : get the "id" pushInfo.
     *
     * @param id the id of the pushInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pushInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/push-infos/{id}")
    @Timed
    public ResponseEntity<PushInfoDTO> getPushInfo(@PathVariable Long id) {
        log.debug("REST request to get PushInfo : {}", id);
        PushInfoDTO pushInfoDTO = pushInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pushInfoDTO));
    }

    /**
     * DELETE  /push-infos/:id : delete the "id" pushInfo.
     *
     * @param id the id of the pushInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/push-infos/{id}")
    @Timed
    public ResponseEntity<Void> deletePushInfo(@PathVariable Long id) {
        log.debug("REST request to delete PushInfo : {}", id);
        pushInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
