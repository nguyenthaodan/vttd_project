package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.InvoiceInfoService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.InvoiceInfoDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InvoiceInfo.
 */
@RestController
@RequestMapping("/api")
public class InvoiceInfoResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceInfoResource.class);

    private static final String ENTITY_NAME = "invoiceInfo";

    private final InvoiceInfoService invoiceInfoService;

    public InvoiceInfoResource(InvoiceInfoService invoiceInfoService) {
        this.invoiceInfoService = invoiceInfoService;
    }

    /**
     * POST  /invoice-infos : Create a new invoiceInfo.
     *
     * @param invoiceInfoDTO the invoiceInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new invoiceInfoDTO, or with status 400 (Bad Request) if the invoiceInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invoice-infos")
    @Timed
    public ResponseEntity<InvoiceInfoDTO> createInvoiceInfo(@RequestBody InvoiceInfoDTO invoiceInfoDTO) throws URISyntaxException {
        log.debug("REST request to save InvoiceInfo : {}", invoiceInfoDTO);
        if (invoiceInfoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new invoiceInfo cannot already have an ID")).body(null);
        }
        InvoiceInfoDTO result = invoiceInfoService.save(invoiceInfoDTO);
        return ResponseEntity.created(new URI("/api/invoice-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invoice-infos : Updates an existing invoiceInfo.
     *
     * @param invoiceInfoDTO the invoiceInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated invoiceInfoDTO,
     * or with status 400 (Bad Request) if the invoiceInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the invoiceInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invoice-infos")
    @Timed
    public ResponseEntity<InvoiceInfoDTO> updateInvoiceInfo(@RequestBody InvoiceInfoDTO invoiceInfoDTO) throws URISyntaxException {
        log.debug("REST request to update InvoiceInfo : {}", invoiceInfoDTO);
        if (invoiceInfoDTO.getId() == null) {
            return createInvoiceInfo(invoiceInfoDTO);
        }
        InvoiceInfoDTO result = invoiceInfoService.save(invoiceInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invoiceInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invoice-infos : get all the invoiceInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of invoiceInfos in body
     */
    @GetMapping("/invoice-infos")
    @Timed
    public ResponseEntity<List<InvoiceInfoDTO>> getAllInvoiceInfos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InvoiceInfos");
        Page<InvoiceInfoDTO> page = invoiceInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/invoice-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /invoice-infos/:id : get the "id" invoiceInfo.
     *
     * @param id the id of the invoiceInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the invoiceInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/invoice-infos/{id}")
    @Timed
    public ResponseEntity<InvoiceInfoDTO> getInvoiceInfo(@PathVariable Long id) {
        log.debug("REST request to get InvoiceInfo : {}", id);
        InvoiceInfoDTO invoiceInfoDTO = invoiceInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(invoiceInfoDTO));
    }

    /**
     * DELETE  /invoice-infos/:id : delete the "id" invoiceInfo.
     *
     * @param id the id of the invoiceInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invoice-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteInvoiceInfo(@PathVariable Long id) {
        log.debug("REST request to delete InvoiceInfo : {}", id);
        invoiceInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
