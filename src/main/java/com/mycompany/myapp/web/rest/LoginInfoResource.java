package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.LoginInfoService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.LoginInfoDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing LoginInfo.
 */
@RestController
@RequestMapping("/api")
public class LoginInfoResource {

    private final Logger log = LoggerFactory.getLogger(LoginInfoResource.class);

    private static final String ENTITY_NAME = "loginInfo";

    private final LoginInfoService loginInfoService;

    public LoginInfoResource(LoginInfoService loginInfoService) {
        this.loginInfoService = loginInfoService;
    }

    /**
     * POST  /login-infos : Create a new loginInfo.
     *
     * @param loginInfoDTO the loginInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new loginInfoDTO, or with status 400 (Bad Request) if the loginInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/login-infos")
    @Timed
    public ResponseEntity<LoginInfoDTO> createLoginInfo(@RequestBody LoginInfoDTO loginInfoDTO) throws URISyntaxException {
        log.debug("REST request to save LoginInfo : {}", loginInfoDTO);
        if (loginInfoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new loginInfo cannot already have an ID")).body(null);
        }
        LoginInfoDTO result = loginInfoService.save(loginInfoDTO);
        return ResponseEntity.created(new URI("/api/login-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /login-infos : Updates an existing loginInfo.
     *
     * @param loginInfoDTO the loginInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated loginInfoDTO,
     * or with status 400 (Bad Request) if the loginInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the loginInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/login-infos")
    @Timed
    public ResponseEntity<LoginInfoDTO> updateLoginInfo(@RequestBody LoginInfoDTO loginInfoDTO) throws URISyntaxException {
        log.debug("REST request to update LoginInfo : {}", loginInfoDTO);
        if (loginInfoDTO.getId() == null) {
            return createLoginInfo(loginInfoDTO);
        }
        LoginInfoDTO result = loginInfoService.save(loginInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, loginInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /login-infos : get all the loginInfos.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of loginInfos in body
     */
    @GetMapping("/login-infos")
    @Timed
    public ResponseEntity<List<LoginInfoDTO>> getAllLoginInfos(@ApiParam Pageable pageable, @RequestParam(required = false) String filter) {
        if ("sysuser-is-null".equals(filter)) {
            log.debug("REST request to get all LoginInfos where sysUser is null");
            return new ResponseEntity<>(loginInfoService.findAllWhereSysUserIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of LoginInfos");
        Page<LoginInfoDTO> page = loginInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/login-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /login-infos/:id : get the "id" loginInfo.
     *
     * @param id the id of the loginInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the loginInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/login-infos/{id}")
    @Timed
    public ResponseEntity<LoginInfoDTO> getLoginInfo(@PathVariable Long id) {
        log.debug("REST request to get LoginInfo : {}", id);
        LoginInfoDTO loginInfoDTO = loginInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(loginInfoDTO));
    }

    /**
     * DELETE  /login-infos/:id : delete the "id" loginInfo.
     *
     * @param id the id of the loginInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/login-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteLoginInfo(@PathVariable Long id) {
        log.debug("REST request to delete LoginInfo : {}", id);
        loginInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
