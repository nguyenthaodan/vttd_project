package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.JobInfoService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.JobInfoDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing JobInfo.
 */
@RestController
@RequestMapping("/api")
public class JobInfoResource {

    private final Logger log = LoggerFactory.getLogger(JobInfoResource.class);

    private static final String ENTITY_NAME = "jobInfo";

    private final JobInfoService jobInfoService;

    public JobInfoResource(JobInfoService jobInfoService) {
        this.jobInfoService = jobInfoService;
    }

    /**
     * POST  /job-infos : Create a new jobInfo.
     *
     * @param jobInfoDTO the jobInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jobInfoDTO, or with status 400 (Bad Request) if the jobInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/job-infos")
    @Timed
    public ResponseEntity<JobInfoDTO> createJobInfo(@RequestBody JobInfoDTO jobInfoDTO) throws URISyntaxException {
        log.debug("REST request to save JobInfo : {}", jobInfoDTO);
        if (jobInfoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new jobInfo cannot already have an ID")).body(null);
        }
        JobInfoDTO result = jobInfoService.save(jobInfoDTO);
        return ResponseEntity.created(new URI("/api/job-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /job-infos : Updates an existing jobInfo.
     *
     * @param jobInfoDTO the jobInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jobInfoDTO,
     * or with status 400 (Bad Request) if the jobInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the jobInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/job-infos")
    @Timed
    public ResponseEntity<JobInfoDTO> updateJobInfo(@RequestBody JobInfoDTO jobInfoDTO) throws URISyntaxException {
        log.debug("REST request to update JobInfo : {}", jobInfoDTO);
        if (jobInfoDTO.getId() == null) {
            return createJobInfo(jobInfoDTO);
        }
        JobInfoDTO result = jobInfoService.save(jobInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jobInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /job-infos : get all the jobInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jobInfos in body
     */
    @GetMapping("/job-infos")
    @Timed
    public ResponseEntity<List<JobInfoDTO>> getAllJobInfos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of JobInfos");
        Page<JobInfoDTO> page = jobInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/job-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /job-infos/:id : get the "id" jobInfo.
     *
     * @param id the id of the jobInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jobInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/job-infos/{id}")
    @Timed
    public ResponseEntity<JobInfoDTO> getJobInfo(@PathVariable Long id) {
        log.debug("REST request to get JobInfo : {}", id);
        JobInfoDTO jobInfoDTO = jobInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jobInfoDTO));
    }

    /**
     * DELETE  /job-infos/:id : delete the "id" jobInfo.
     *
     * @param id the id of the jobInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/job-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteJobInfo(@PathVariable Long id) {
        log.debug("REST request to delete JobInfo : {}", id);
        jobInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
