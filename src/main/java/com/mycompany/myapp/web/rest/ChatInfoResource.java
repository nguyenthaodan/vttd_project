package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.ChatInfoService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.ChatInfoDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChatInfo.
 */
@RestController
@RequestMapping("/api")
public class ChatInfoResource {

    private final Logger log = LoggerFactory.getLogger(ChatInfoResource.class);

    private static final String ENTITY_NAME = "chatInfo";

    private final ChatInfoService chatInfoService;

    public ChatInfoResource(ChatInfoService chatInfoService) {
        this.chatInfoService = chatInfoService;
    }

    /**
     * POST  /chat-infos : Create a new chatInfo.
     *
     * @param chatInfoDTO the chatInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chatInfoDTO, or with status 400 (Bad Request) if the chatInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chat-infos")
    @Timed
    public ResponseEntity<ChatInfoDTO> createChatInfo(@RequestBody ChatInfoDTO chatInfoDTO) throws URISyntaxException {
        log.debug("REST request to save ChatInfo : {}", chatInfoDTO);
        if (chatInfoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new chatInfo cannot already have an ID")).body(null);
        }
        ChatInfoDTO result = chatInfoService.save(chatInfoDTO);
        return ResponseEntity.created(new URI("/api/chat-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chat-infos : Updates an existing chatInfo.
     *
     * @param chatInfoDTO the chatInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chatInfoDTO,
     * or with status 400 (Bad Request) if the chatInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the chatInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chat-infos")
    @Timed
    public ResponseEntity<ChatInfoDTO> updateChatInfo(@RequestBody ChatInfoDTO chatInfoDTO) throws URISyntaxException {
        log.debug("REST request to update ChatInfo : {}", chatInfoDTO);
        if (chatInfoDTO.getId() == null) {
            return createChatInfo(chatInfoDTO);
        }
        ChatInfoDTO result = chatInfoService.save(chatInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chatInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chat-infos : get all the chatInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of chatInfos in body
     */
    @GetMapping("/chat-infos")
    @Timed
    public ResponseEntity<List<ChatInfoDTO>> getAllChatInfos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ChatInfos");
        Page<ChatInfoDTO> page = chatInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/chat-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /chat-infos/:id : get the "id" chatInfo.
     *
     * @param id the id of the chatInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chatInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/chat-infos/{id}")
    @Timed
    public ResponseEntity<ChatInfoDTO> getChatInfo(@PathVariable Long id) {
        log.debug("REST request to get ChatInfo : {}", id);
        ChatInfoDTO chatInfoDTO = chatInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(chatInfoDTO));
    }

    /**
     * DELETE  /chat-infos/:id : delete the "id" chatInfo.
     *
     * @param id the id of the chatInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chat-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteChatInfo(@PathVariable Long id) {
        log.debug("REST request to delete ChatInfo : {}", id);
        chatInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
