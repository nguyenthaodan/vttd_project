package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.CreditInfoService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.CreditInfoDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CreditInfo.
 */
@RestController
@RequestMapping("/api")
public class CreditInfoResource {

    private final Logger log = LoggerFactory.getLogger(CreditInfoResource.class);

    private static final String ENTITY_NAME = "creditInfo";

    private final CreditInfoService creditInfoService;

    public CreditInfoResource(CreditInfoService creditInfoService) {
        this.creditInfoService = creditInfoService;
    }

    /**
     * POST  /credit-infos : Create a new creditInfo.
     *
     * @param creditInfoDTO the creditInfoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new creditInfoDTO, or with status 400 (Bad Request) if the creditInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/credit-infos")
    @Timed
    public ResponseEntity<CreditInfoDTO> createCreditInfo(@RequestBody CreditInfoDTO creditInfoDTO) throws URISyntaxException {
        log.debug("REST request to save CreditInfo : {}", creditInfoDTO);
        if (creditInfoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new creditInfo cannot already have an ID")).body(null);
        }
        CreditInfoDTO result = creditInfoService.save(creditInfoDTO);
        return ResponseEntity.created(new URI("/api/credit-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /credit-infos : Updates an existing creditInfo.
     *
     * @param creditInfoDTO the creditInfoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated creditInfoDTO,
     * or with status 400 (Bad Request) if the creditInfoDTO is not valid,
     * or with status 500 (Internal Server Error) if the creditInfoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/credit-infos")
    @Timed
    public ResponseEntity<CreditInfoDTO> updateCreditInfo(@RequestBody CreditInfoDTO creditInfoDTO) throws URISyntaxException {
        log.debug("REST request to update CreditInfo : {}", creditInfoDTO);
        if (creditInfoDTO.getId() == null) {
            return createCreditInfo(creditInfoDTO);
        }
        CreditInfoDTO result = creditInfoService.save(creditInfoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, creditInfoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /credit-infos : get all the creditInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of creditInfos in body
     */
    @GetMapping("/credit-infos")
    @Timed
    public ResponseEntity<List<CreditInfoDTO>> getAllCreditInfos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CreditInfos");
        Page<CreditInfoDTO> page = creditInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/credit-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /credit-infos/:id : get the "id" creditInfo.
     *
     * @param id the id of the creditInfoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the creditInfoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/credit-infos/{id}")
    @Timed
    public ResponseEntity<CreditInfoDTO> getCreditInfo(@PathVariable Long id) {
        log.debug("REST request to get CreditInfo : {}", id);
        CreditInfoDTO creditInfoDTO = creditInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(creditInfoDTO));
    }

    /**
     * DELETE  /credit-infos/:id : delete the "id" creditInfo.
     *
     * @param id the id of the creditInfoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/credit-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteCreditInfo(@PathVariable Long id) {
        log.debug("REST request to delete CreditInfo : {}", id);
        creditInfoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
