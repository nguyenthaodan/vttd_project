package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.SysUserService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.SysUserDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SysUser.
 */
@RestController
@RequestMapping("/api")
public class SysUserResource {

    private final Logger log = LoggerFactory.getLogger(SysUserResource.class);

    private static final String ENTITY_NAME = "sysUser";

    private final SysUserService sysUserService;

    public SysUserResource(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    /**
     * POST  /sys-users : Create a new sysUser.
     *
     * @param sysUserDTO the sysUserDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sysUserDTO, or with status 400 (Bad Request) if the sysUser has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sys-users")
    @Timed
    public ResponseEntity<SysUserDTO> createSysUser(@RequestBody SysUserDTO sysUserDTO) throws URISyntaxException {
        log.debug("REST request to save SysUser : {}", sysUserDTO);
        if (sysUserDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new sysUser cannot already have an ID")).body(null);
        }
        SysUserDTO result = sysUserService.save(sysUserDTO);
        return ResponseEntity.created(new URI("/api/sys-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sys-users : Updates an existing sysUser.
     *
     * @param sysUserDTO the sysUserDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sysUserDTO,
     * or with status 400 (Bad Request) if the sysUserDTO is not valid,
     * or with status 500 (Internal Server Error) if the sysUserDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sys-users")
    @Timed
    public ResponseEntity<SysUserDTO> updateSysUser(@RequestBody SysUserDTO sysUserDTO) throws URISyntaxException {
        log.debug("REST request to update SysUser : {}", sysUserDTO);
        if (sysUserDTO.getId() == null) {
            return createSysUser(sysUserDTO);
        }
        SysUserDTO result = sysUserService.save(sysUserDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sysUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sys-users : get all the sysUsers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sysUsers in body
     */
    @GetMapping("/sys-users")
    @Timed
    public ResponseEntity<List<SysUserDTO>> getAllSysUsers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SysUsers");
        Page<SysUserDTO> page = sysUserService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sys-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sys-users/:id : get the "id" sysUser.
     *
     * @param id the id of the sysUserDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sysUserDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sys-users/{id}")
    @Timed
    public ResponseEntity<SysUserDTO> getSysUser(@PathVariable Long id) {
        log.debug("REST request to get SysUser : {}", id);
        SysUserDTO sysUserDTO = sysUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sysUserDTO));
    }

    /**
     * DELETE  /sys-users/:id : delete the "id" sysUser.
     *
     * @param id the id of the sysUserDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sys-users/{id}")
    @Timed
    public ResponseEntity<Void> deleteSysUser(@PathVariable Long id) {
        log.debug("REST request to delete SysUser : {}", id);
        sysUserService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
