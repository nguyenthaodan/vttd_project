package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.WorkingService;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.WorkingDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Working.
 */
@RestController
@RequestMapping("/api")
public class WorkingResource {

    private final Logger log = LoggerFactory.getLogger(WorkingResource.class);

    private static final String ENTITY_NAME = "working";

    private final WorkingService workingService;

    public WorkingResource(WorkingService workingService) {
        this.workingService = workingService;
    }

    /**
     * POST  /workings : Create a new working.
     *
     * @param workingDTO the workingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new workingDTO, or with status 400 (Bad Request) if the working has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/workings")
    @Timed
    public ResponseEntity<WorkingDTO> createWorking(@Valid @RequestBody WorkingDTO workingDTO) throws URISyntaxException {
        log.debug("REST request to save Working : {}", workingDTO);
        if (workingDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new working cannot already have an ID")).body(null);
        }
        WorkingDTO result = workingService.save(workingDTO);
        return ResponseEntity.created(new URI("/api/workings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /workings : Updates an existing working.
     *
     * @param workingDTO the workingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated workingDTO,
     * or with status 400 (Bad Request) if the workingDTO is not valid,
     * or with status 500 (Internal Server Error) if the workingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/workings")
    @Timed
    public ResponseEntity<WorkingDTO> updateWorking(@Valid @RequestBody WorkingDTO workingDTO) throws URISyntaxException {
        log.debug("REST request to update Working : {}", workingDTO);
        if (workingDTO.getId() == null) {
            return createWorking(workingDTO);
        }
        WorkingDTO result = workingService.save(workingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, workingDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /workings : get all the workings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of workings in body
     */
    @GetMapping("/workings")
    @Timed
    public ResponseEntity<List<WorkingDTO>> getAllWorkings(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Workings");
        Page<WorkingDTO> page = workingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/workings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /workings/:id : get the "id" working.
     *
     * @param id the id of the workingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the workingDTO, or with status 404 (Not Found)
     */
    @GetMapping("/workings/{id}")
    @Timed
    public ResponseEntity<WorkingDTO> getWorking(@PathVariable Long id) {
        log.debug("REST request to get Working : {}", id);
        WorkingDTO workingDTO = workingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(workingDTO));
    }

    /**
     * DELETE  /workings/:id : delete the "id" working.
     *
     * @param id the id of the workingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/workings/{id}")
    @Timed
    public ResponseEntity<Void> deleteWorking(@PathVariable Long id) {
        log.debug("REST request to delete Working : {}", id);
        workingService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
