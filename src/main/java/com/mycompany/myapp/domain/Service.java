package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Service.
 */
@Entity
@Table(name = "service")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "jhi_desc")
    private String desc;

    @Column(name = "jhi_restricted")
    private Boolean restricted;

    @Column(name = "provider_count")
    private Integer providerCount;

    @OneToMany(mappedBy = "service")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Category> categories = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "service_promotion",
               joinColumns = @JoinColumn(name="services_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="promotions_id", referencedColumnName="id"))
    private Set<Promotion> promotions = new HashSet<>();

    @OneToOne(mappedBy = "")
    @JsonIgnore
    private Form form;

    @ManyToOne
    private Fee fee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Service name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public Service desc(String desc) {
        this.desc = desc;
        return this;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean isRestricted() {
        return restricted;
    }

    public Service restricted(Boolean restricted) {
        this.restricted = restricted;
        return this;
    }

    public void setRestricted(Boolean restricted) {
        this.restricted = restricted;
    }

    public Integer getProviderCount() {
        return providerCount;
    }

    public Service providerCount(Integer providerCount) {
        this.providerCount = providerCount;
        return this;
    }

    public void setProviderCount(Integer providerCount) {
        this.providerCount = providerCount;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public Service categories(Set<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Service addCategory(Category category) {
        this.categories.add(category);
        category.setService(this);
        return this;
    }

    public Service removeCategory(Category category) {
        this.categories.remove(category);
        category.setService(null);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Set<Promotion> getPromotions() {
        return promotions;
    }

    public Service promotions(Set<Promotion> promotions) {
        this.promotions = promotions;
        return this;
    }

    public Service addPromotion(Promotion promotion) {
        this.promotions.add(promotion);
        return this;
    }

    public Service removePromotion(Promotion promotion) {
        this.promotions.remove(promotion);
        return this;
    }

    public void setPromotions(Set<Promotion> promotions) {
        this.promotions = promotions;
    }

    public Form getForm() {
        return form;
    }

    public Service form(Form form) {
        this.form = form;
        return this;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Fee getFee() {
        return fee;
    }

    public Service fee(Fee fee) {
        this.fee = fee;
        return this;
    }

    public void setFee(Fee fee) {
        this.fee = fee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Service service = (Service) o;
        if (service.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), service.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Service{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", desc='" + getDesc() + "'" +
            ", restricted='" + isRestricted() + "'" +
            ", providerCount='" + getProviderCount() + "'" +
            "}";
    }
}
