package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.BusinessType;

import com.mycompany.myapp.domain.enumeration.BusinessStatus;

import com.mycompany.myapp.domain.enumeration.UserRequirement;

/**
 * A Business.
 */
@Entity
@Table(name = "business")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Business implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "business_name")
    private String businessName;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private BusinessType type;

    @Column(name = "description")
    private String description;

    @Column(name = "date_operate")
    private LocalDate dateOperate;

    @Column(name = "total_job_done")
    private Integer totalJobDone;

    @Column(name = "website")
    private String website;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private BusinessStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_user")
    private UserRequirement user;

    @Column(name = "note")
    private String note;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "business_service",
               joinColumns = @JoinColumn(name="businesses_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="services_id", referencedColumnName="id"))
    private Set<Service> services = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "business_location",
               joinColumns = @JoinColumn(name="businesses_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="locations_id", referencedColumnName="id"))
    private Set<Location> locations = new HashSet<>();

    @OneToOne(mappedBy = "")
    @JsonIgnore
    private InvoiceInfo invoiceInfo;

    @ManyToOne
    private SysUser sysUser;

    @OneToMany(mappedBy = "business")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Review> reviews = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public Business businessName(String businessName) {
        this.businessName = businessName;
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public BusinessType getType() {
        return type;
    }

    public Business type(BusinessType type) {
        this.type = type;
        return this;
    }

    public void setType(BusinessType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public Business description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateOperate() {
        return dateOperate;
    }

    public Business dateOperate(LocalDate dateOperate) {
        this.dateOperate = dateOperate;
        return this;
    }

    public void setDateOperate(LocalDate dateOperate) {
        this.dateOperate = dateOperate;
    }

    public Integer getTotalJobDone() {
        return totalJobDone;
    }

    public Business totalJobDone(Integer totalJobDone) {
        this.totalJobDone = totalJobDone;
        return this;
    }

    public void setTotalJobDone(Integer totalJobDone) {
        this.totalJobDone = totalJobDone;
    }

    public String getWebsite() {
        return website;
    }

    public Business website(String website) {
        this.website = website;
        return this;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BusinessStatus getStatus() {
        return status;
    }

    public Business status(BusinessStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(BusinessStatus status) {
        this.status = status;
    }

    public UserRequirement getUser() {
        return user;
    }

    public Business user(UserRequirement user) {
        this.user = user;
        return this;
    }

    public void setUser(UserRequirement user) {
        this.user = user;
    }

    public String getNote() {
        return note;
    }

    public Business note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Service> getServices() {
        return services;
    }

    public Business services(Set<Service> services) {
        this.services = services;
        return this;
    }

    public Business addService(Service service) {
        this.services.add(service);
        return this;
    }

    public Business removeService(Service service) {
        this.services.remove(service);
        return this;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public Business locations(Set<Location> locations) {
        this.locations = locations;
        return this;
    }

    public Business addLocation(Location location) {
        this.locations.add(location);
        return this;
    }

    public Business removeLocation(Location location) {
        this.locations.remove(location);
        return this;
    }

    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

    public InvoiceInfo getInvoiceInfo() {
        return invoiceInfo;
    }

    public Business invoiceInfo(InvoiceInfo invoiceInfo) {
        this.invoiceInfo = invoiceInfo;
        return this;
    }

    public void setInvoiceInfo(InvoiceInfo invoiceInfo) {
        this.invoiceInfo = invoiceInfo;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public Business sysUser(SysUser sysUser) {
        this.sysUser = sysUser;
        return this;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public Business reviews(Set<Review> reviews) {
        this.reviews = reviews;
        return this;
    }

    public Business addReview(Review review) {
        this.reviews.add(review);
        review.setBusiness(this);
        return this;
    }

    public Business removeReview(Review review) {
        this.reviews.remove(review);
        review.setBusiness(null);
        return this;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Business business = (Business) o;
        if (business.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), business.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Business{" +
            "id=" + getId() +
            ", businessName='" + getBusinessName() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", dateOperate='" + getDateOperate() + "'" +
            ", totalJobDone='" + getTotalJobDone() + "'" +
            ", website='" + getWebsite() + "'" +
            ", status='" + getStatus() + "'" +
            ", user='" + getUser() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
