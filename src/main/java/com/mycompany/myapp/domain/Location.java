package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Location.
 */
@Entity
@Table(name = "location")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "abbre")
    private String abbre;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "longitude")
    private Float longitude;

    @Column(name = "lagitude")
    private Float lagitude;

    @OneToMany(mappedBy = "location")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SysUser> sysUsers = new HashSet<>();

    @ManyToOne
    private JobTemplate jobTemplate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Location name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbre() {
        return abbre;
    }

    public Location abbre(String abbre) {
        this.abbre = abbre;
        return this;
    }

    public void setAbbre(String abbre) {
        this.abbre = abbre;
    }

    public String getCity() {
        return city;
    }

    public Location city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public Location country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Float getLongitude() {
        return longitude;
    }

    public Location longitude(Float longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLagitude() {
        return lagitude;
    }

    public Location lagitude(Float lagitude) {
        this.lagitude = lagitude;
        return this;
    }

    public void setLagitude(Float lagitude) {
        this.lagitude = lagitude;
    }

    public Set<SysUser> getSysUsers() {
        return sysUsers;
    }

    public Location sysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
        return this;
    }

    public Location addSysUser(SysUser sysUser) {
        this.sysUsers.add(sysUser);
        sysUser.setLocation(this);
        return this;
    }

    public Location removeSysUser(SysUser sysUser) {
        this.sysUsers.remove(sysUser);
        sysUser.setLocation(null);
        return this;
    }

    public void setSysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
    }

    public JobTemplate getJobTemplate() {
        return jobTemplate;
    }

    public Location jobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
        return this;
    }

    public void setJobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        if (location.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), location.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Location{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", abbre='" + getAbbre() + "'" +
            ", city='" + getCity() + "'" +
            ", country='" + getCountry() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", lagitude='" + getLagitude() + "'" +
            "}";
    }
}
