package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Category.
 */
@Entity
@Table(name = "category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "jhi_desc")
    private String desc;

    @Column(name = "services_count")
    private Integer servicesCount;

    @Column(name = "provider_count")
    private Integer providerCount;

    @Column(name = "jhi_restricted")
    private Boolean restricted;

    @ManyToOne
    private Category category;

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Category> parents = new HashSet<>();

    @ManyToOne
    private Fee fee;

    @ManyToOne
    private Service service;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Category name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public Category desc(String desc) {
        this.desc = desc;
        return this;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getServicesCount() {
        return servicesCount;
    }

    public Category servicesCount(Integer servicesCount) {
        this.servicesCount = servicesCount;
        return this;
    }

    public void setServicesCount(Integer servicesCount) {
        this.servicesCount = servicesCount;
    }

    public Integer getProviderCount() {
        return providerCount;
    }

    public Category providerCount(Integer providerCount) {
        this.providerCount = providerCount;
        return this;
    }

    public void setProviderCount(Integer providerCount) {
        this.providerCount = providerCount;
    }

    public Boolean isRestricted() {
        return restricted;
    }

    public Category restricted(Boolean restricted) {
        this.restricted = restricted;
        return this;
    }

    public void setRestricted(Boolean restricted) {
        this.restricted = restricted;
    }

    public Category getCategory() {
        return category;
    }

    public Category category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Category> getParents() {
        return parents;
    }

    public Category parents(Set<Category> categories) {
        this.parents = categories;
        return this;
    }

    public Category addParent(Category category) {
        this.parents.add(category);
        category.setCategory(this);
        return this;
    }

    public Category removeParent(Category category) {
        this.parents.remove(category);
        category.setCategory(null);
        return this;
    }

    public void setParents(Set<Category> categories) {
        this.parents = categories;
    }

    public Fee getFee() {
        return fee;
    }

    public Category fee(Fee fee) {
        this.fee = fee;
        return this;
    }

    public void setFee(Fee fee) {
        this.fee = fee;
    }

    public Service getService() {
        return service;
    }

    public Category service(Service service) {
        this.service = service;
        return this;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        if (category.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), category.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Category{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", desc='" + getDesc() + "'" +
            ", servicesCount='" + getServicesCount() + "'" +
            ", providerCount='" + getProviderCount() + "'" +
            ", restricted='" + isRestricted() + "'" +
            "}";
    }
}
