package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.PromotionType;

import com.mycompany.myapp.domain.enumeration.UserClass;

/**
 * A Promotion.
 */
@Entity
@Table(name = "promotion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Promotion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "rate")
    private Float rate;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private PromotionType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "applied_to")
    private UserClass appliedTo;

    @Column(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Promotion startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Promotion endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Float getRate() {
        return rate;
    }

    public Promotion rate(Float rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public PromotionType getType() {
        return type;
    }

    public Promotion type(PromotionType type) {
        this.type = type;
        return this;
    }

    public void setType(PromotionType type) {
        this.type = type;
    }

    public UserClass getAppliedTo() {
        return appliedTo;
    }

    public Promotion appliedTo(UserClass appliedTo) {
        this.appliedTo = appliedTo;
        return this;
    }

    public void setAppliedTo(UserClass appliedTo) {
        this.appliedTo = appliedTo;
    }

    public String getDescription() {
        return description;
    }

    public Promotion description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Promotion promotion = (Promotion) o;
        if (promotion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), promotion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Promotion{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", rate='" + getRate() + "'" +
            ", type='" + getType() + "'" +
            ", appliedTo='" + getAppliedTo() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
