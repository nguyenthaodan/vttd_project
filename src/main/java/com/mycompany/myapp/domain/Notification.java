package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.NotificationStatus;

import com.mycompany.myapp.domain.enumeration.NotificationMethod;

/**
 * A Notification.
 */
@Entity
@Table(name = "notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jhi_date")
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private NotificationStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "method")
    private NotificationMethod method;

    @Column(name = "message")
    private String message;

    @ManyToOne
    private Event event;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "notification_sys_user",
               joinColumns = @JoinColumn(name="notifications_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="sys_users_id", referencedColumnName="id"))
    private Set<SysUser> sysUsers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Notification date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public Notification status(NotificationStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(NotificationStatus status) {
        this.status = status;
    }

    public NotificationMethod getMethod() {
        return method;
    }

    public Notification method(NotificationMethod method) {
        this.method = method;
        return this;
    }

    public void setMethod(NotificationMethod method) {
        this.method = method;
    }

    public String getMessage() {
        return message;
    }

    public Notification message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Event getEvent() {
        return event;
    }

    public Notification event(Event event) {
        this.event = event;
        return this;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Set<SysUser> getSysUsers() {
        return sysUsers;
    }

    public Notification sysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
        return this;
    }

    public Notification addSysUser(SysUser sysUser) {
        this.sysUsers.add(sysUser);
        return this;
    }

    public Notification removeSysUser(SysUser sysUser) {
        this.sysUsers.remove(sysUser);
        return this;
    }

    public void setSysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Notification notification = (Notification) o;
        if (notification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), notification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Notification{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", status='" + getStatus() + "'" +
            ", method='" + getMethod() + "'" +
            ", message='" + getMessage() + "'" +
            "}";
    }
}
