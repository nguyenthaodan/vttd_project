package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Invoice.
 */
@Entity
@Table(name = "invoice")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "receipt_date")
    private LocalDate receiptDate;

    @Column(name = "receipt_amount")
    private Float receiptAmount;

    @Column(name = "tax_rate")
    private Float taxRate;

    @NotNull
    @Column(name = "receipt_info", nullable = false)
    private String receiptInfo;

    @ManyToOne
    private User user;

    @ManyToOne
    private Business business;

    @OneToOne(mappedBy = "userInvoice")
    @JsonIgnore
    private Completed completed;

    @OneToOne(mappedBy = "providerInvoice")
    @JsonIgnore
    private Completed completed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getReceiptDate() {
        return receiptDate;
    }

    public Invoice receiptDate(LocalDate receiptDate) {
        this.receiptDate = receiptDate;
        return this;
    }

    public void setReceiptDate(LocalDate receiptDate) {
        this.receiptDate = receiptDate;
    }

    public Float getReceiptAmount() {
        return receiptAmount;
    }

    public Invoice receiptAmount(Float receiptAmount) {
        this.receiptAmount = receiptAmount;
        return this;
    }

    public void setReceiptAmount(Float receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public Float getTaxRate() {
        return taxRate;
    }

    public Invoice taxRate(Float taxRate) {
        this.taxRate = taxRate;
        return this;
    }

    public void setTaxRate(Float taxRate) {
        this.taxRate = taxRate;
    }

    public String getReceiptInfo() {
        return receiptInfo;
    }

    public Invoice receiptInfo(String receiptInfo) {
        this.receiptInfo = receiptInfo;
        return this;
    }

    public void setReceiptInfo(String receiptInfo) {
        this.receiptInfo = receiptInfo;
    }

    public User getUser() {
        return user;
    }

    public Invoice user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public Invoice business(Business business) {
        this.business = business;
        return this;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Completed getCompleted() {
        return completed;
    }

    public Invoice completed(Completed completed) {
        this.completed = completed;
        return this;
    }

    public void setCompleted(Completed completed) {
        this.completed = completed;
    }

    public Completed getCompleted() {
        return completed;
    }

    public Invoice completed(Completed completed) {
        this.completed = completed;
        return this;
    }

    public void setCompleted(Completed completed) {
        this.completed = completed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Invoice invoice = (Invoice) o;
        if (invoice.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoice.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Invoice{" +
            "id=" + getId() +
            ", receiptDate='" + getReceiptDate() + "'" +
            ", receiptAmount='" + getReceiptAmount() + "'" +
            ", taxRate='" + getTaxRate() + "'" +
            ", receiptInfo='" + getReceiptInfo() + "'" +
            "}";
    }
}
