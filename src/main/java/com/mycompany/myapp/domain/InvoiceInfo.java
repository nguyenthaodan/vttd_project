package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A InvoiceInfo.
 */
@Entity
@Table(name = "invoice_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InvoiceInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "business_address")
    private String businessAddress;

    @Column(name = "invoice_address")
    private String invoiceAddress;

    @Column(name = "tax_number")
    private String taxNumber;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "business_name")
    private String businessName;

    @Column(name = "person_name")
    private String personName;

    @OneToOne
    @JoinColumn(unique = true)
    private Business ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public InvoiceInfo businessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
        return this;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getInvoiceAddress() {
        return invoiceAddress;
    }

    public InvoiceInfo invoiceAddress(String invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
        return this;
    }

    public void setInvoiceAddress(String invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public InvoiceInfo taxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
        return this;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public InvoiceInfo accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBusinessName() {
        return businessName;
    }

    public InvoiceInfo businessName(String businessName) {
        this.businessName = businessName;
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPersonName() {
        return personName;
    }

    public InvoiceInfo personName(String personName) {
        this.personName = personName;
        return this;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Business get() {
        return ;
    }

    public InvoiceInfo (Business business) {
        this. = business;
        return this;
    }

    public void set(Business business) {
        this. = business;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InvoiceInfo invoiceInfo = (InvoiceInfo) o;
        if (invoiceInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceInfo{" +
            "id=" + getId() +
            ", businessAddress='" + getBusinessAddress() + "'" +
            ", invoiceAddress='" + getInvoiceAddress() + "'" +
            ", taxNumber='" + getTaxNumber() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            ", businessName='" + getBusinessName() + "'" +
            ", personName='" + getPersonName() + "'" +
            "}";
    }
}
