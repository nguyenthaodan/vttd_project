package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.PaymentMethod;

/**
 * A CreditInfo.
 */
@Entity
@Table(name = "credit_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CreditInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "credit")
    private Float credit;

    @Enumerated(EnumType.STRING)
    @Column(name = "method")
    private PaymentMethod method;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getCredit() {
        return credit;
    }

    public CreditInfo credit(Float credit) {
        this.credit = credit;
        return this;
    }

    public void setCredit(Float credit) {
        this.credit = credit;
    }

    public PaymentMethod getMethod() {
        return method;
    }

    public CreditInfo method(PaymentMethod method) {
        this.method = method;
        return this;
    }

    public void setMethod(PaymentMethod method) {
        this.method = method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CreditInfo creditInfo = (CreditInfo) o;
        if (creditInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), creditInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CreditInfo{" +
            "id=" + getId() +
            ", credit='" + getCredit() + "'" +
            ", method='" + getMethod() + "'" +
            "}";
    }
}
