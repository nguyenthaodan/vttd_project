package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PageTable.
 */
@Entity
@Table(name = "page_table")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PageTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "page_name")
    private String pageName;

    @Column(name = "page_title")
    private String pageTitle;

    @Column(name = "page_info")
    private String pageInfo;

    @Column(name = "content")
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPageName() {
        return pageName;
    }

    public PageTable pageName(String pageName) {
        this.pageName = pageName;
        return this;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public PageTable pageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
        return this;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageInfo() {
        return pageInfo;
    }

    public PageTable pageInfo(String pageInfo) {
        this.pageInfo = pageInfo;
        return this;
    }

    public void setPageInfo(String pageInfo) {
        this.pageInfo = pageInfo;
    }

    public String getContent() {
        return content;
    }

    public PageTable content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PageTable pageTable = (PageTable) o;
        if (pageTable.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pageTable.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PageTable{" +
            "id=" + getId() +
            ", pageName='" + getPageName() + "'" +
            ", pageTitle='" + getPageTitle() + "'" +
            ", pageInfo='" + getPageInfo() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}
