package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.PaymentMethod;

import com.mycompany.myapp.domain.enumeration.JobRequirement;

/**
 * A JobTemplate.
 */
@Entity
@Table(name = "job_template")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class JobTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address")
    private String address;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;

    @Enumerated(EnumType.STRING)
    @Column(name = "requirement")
    private JobRequirement requirement;

    @Column(name = "form_fill_in")
    private String formFillIn;

    @Column(name = "create_date")
    private LocalDate createDate;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @OneToOne
    @JoinColumn(unique = true)
    private Service service;

    @OneToMany(mappedBy = "jobTemplate")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<EventInfo> eventInfos = new HashSet<>();

    @OneToMany(mappedBy = "jobTemplate")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Location> locations = new HashSet<>();

    @ManyToOne
    private SysUser sysUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public JobTemplate address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public JobTemplate paymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public JobRequirement getRequirement() {
        return requirement;
    }

    public JobTemplate requirement(JobRequirement requirement) {
        this.requirement = requirement;
        return this;
    }

    public void setRequirement(JobRequirement requirement) {
        this.requirement = requirement;
    }

    public String getFormFillIn() {
        return formFillIn;
    }

    public JobTemplate formFillIn(String formFillIn) {
        this.formFillIn = formFillIn;
        return this;
    }

    public void setFormFillIn(String formFillIn) {
        this.formFillIn = formFillIn;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public JobTemplate createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public JobTemplate startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Service getService() {
        return service;
    }

    public JobTemplate service(Service service) {
        this.service = service;
        return this;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Set<EventInfo> getEventInfos() {
        return eventInfos;
    }

    public JobTemplate eventInfos(Set<EventInfo> eventInfos) {
        this.eventInfos = eventInfos;
        return this;
    }

    public JobTemplate addEventInfo(EventInfo eventInfo) {
        this.eventInfos.add(eventInfo);
        eventInfo.setJobTemplate(this);
        return this;
    }

    public JobTemplate removeEventInfo(EventInfo eventInfo) {
        this.eventInfos.remove(eventInfo);
        eventInfo.setJobTemplate(null);
        return this;
    }

    public void setEventInfos(Set<EventInfo> eventInfos) {
        this.eventInfos = eventInfos;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public JobTemplate locations(Set<Location> locations) {
        this.locations = locations;
        return this;
    }

    public JobTemplate addLocation(Location location) {
        this.locations.add(location);
        location.setJobTemplate(this);
        return this;
    }

    public JobTemplate removeLocation(Location location) {
        this.locations.remove(location);
        location.setJobTemplate(null);
        return this;
    }

    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public JobTemplate sysUser(SysUser sysUser) {
        this.sysUser = sysUser;
        return this;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JobTemplate jobTemplate = (JobTemplate) o;
        if (jobTemplate.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobTemplate.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobTemplate{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", paymentMethod='" + getPaymentMethod() + "'" +
            ", requirement='" + getRequirement() + "'" +
            ", formFillIn='" + getFormFillIn() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", startDate='" + getStartDate() + "'" +
            "}";
    }
}
