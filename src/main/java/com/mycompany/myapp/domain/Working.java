package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.WorkingStatus;

/**
 * A Working.
 */
@Entity
@Table(name = "working")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Working implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "userstatus")
    private WorkingStatus userstatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "supplierstatus")
    private WorkingStatus supplierstatus;

    @Column(name = "amount_deposit")
    private Float amountDeposit;

    @NotNull
    @Column(name = "price", nullable = false)
    private Float price;

    @Column(name = "book_date")
    private LocalDate bookDate;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "finish_date")
    private LocalDate finishDate;

    @NotNull
    @Column(name = "last_meeting", nullable = false)
    private LocalDate lastMeeting;

    @NotNull
    @Column(name = "next_meeting", nullable = false)
    private LocalDate nextMeeting;

    @OneToOne
    @JoinColumn(unique = true)
    private JobTemplate jobTemplate;

    @ManyToOne
    private SysUser sysUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public WorkingStatus getUserstatus() {
        return userstatus;
    }

    public Working userstatus(WorkingStatus userstatus) {
        this.userstatus = userstatus;
        return this;
    }

    public void setUserstatus(WorkingStatus userstatus) {
        this.userstatus = userstatus;
    }

    public WorkingStatus getSupplierstatus() {
        return supplierstatus;
    }

    public Working supplierstatus(WorkingStatus supplierstatus) {
        this.supplierstatus = supplierstatus;
        return this;
    }

    public void setSupplierstatus(WorkingStatus supplierstatus) {
        this.supplierstatus = supplierstatus;
    }

    public Float getAmountDeposit() {
        return amountDeposit;
    }

    public Working amountDeposit(Float amountDeposit) {
        this.amountDeposit = amountDeposit;
        return this;
    }

    public void setAmountDeposit(Float amountDeposit) {
        this.amountDeposit = amountDeposit;
    }

    public Float getPrice() {
        return price;
    }

    public Working price(Float price) {
        this.price = price;
        return this;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public LocalDate getBookDate() {
        return bookDate;
    }

    public Working bookDate(LocalDate bookDate) {
        this.bookDate = bookDate;
        return this;
    }

    public void setBookDate(LocalDate bookDate) {
        this.bookDate = bookDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Working startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public Working finishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
        return this;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    public LocalDate getLastMeeting() {
        return lastMeeting;
    }

    public Working lastMeeting(LocalDate lastMeeting) {
        this.lastMeeting = lastMeeting;
        return this;
    }

    public void setLastMeeting(LocalDate lastMeeting) {
        this.lastMeeting = lastMeeting;
    }

    public LocalDate getNextMeeting() {
        return nextMeeting;
    }

    public Working nextMeeting(LocalDate nextMeeting) {
        this.nextMeeting = nextMeeting;
        return this;
    }

    public void setNextMeeting(LocalDate nextMeeting) {
        this.nextMeeting = nextMeeting;
    }

    public JobTemplate getJobTemplate() {
        return jobTemplate;
    }

    public Working jobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
        return this;
    }

    public void setJobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public Working sysUser(SysUser sysUser) {
        this.sysUser = sysUser;
        return this;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Working working = (Working) o;
        if (working.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), working.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Working{" +
            "id=" + getId() +
            ", userstatus='" + getUserstatus() + "'" +
            ", supplierstatus='" + getSupplierstatus() + "'" +
            ", amountDeposit='" + getAmountDeposit() + "'" +
            ", price='" + getPrice() + "'" +
            ", bookDate='" + getBookDate() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", finishDate='" + getFinishDate() + "'" +
            ", lastMeeting='" + getLastMeeting() + "'" +
            ", nextMeeting='" + getNextMeeting() + "'" +
            "}";
    }
}
