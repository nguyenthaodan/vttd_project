package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.FeeType;

/**
 * A Fee.
 */
@Entity
@Table(name = "fee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Fee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "fee")
    private Float fee;

    @Enumerated(EnumType.STRING)
    @Column(name = "fee_type")
    private FeeType feeType;

    @OneToMany(mappedBy = "fee")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Service> services = new HashSet<>();

    @OneToMany(mappedBy = "fee")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Category> categories = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getFee() {
        return fee;
    }

    public Fee fee(Float fee) {
        this.fee = fee;
        return this;
    }

    public void setFee(Float fee) {
        this.fee = fee;
    }

    public FeeType getFeeType() {
        return feeType;
    }

    public Fee feeType(FeeType feeType) {
        this.feeType = feeType;
        return this;
    }

    public void setFeeType(FeeType feeType) {
        this.feeType = feeType;
    }

    public Set<Service> getServices() {
        return services;
    }

    public Fee services(Set<Service> services) {
        this.services = services;
        return this;
    }

    public Fee addService(Service service) {
        this.services.add(service);
        service.setFee(this);
        return this;
    }

    public Fee removeService(Service service) {
        this.services.remove(service);
        service.setFee(null);
        return this;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public Fee categories(Set<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Fee addCategory(Category category) {
        this.categories.add(category);
        category.setFee(this);
        return this;
    }

    public Fee removeCategory(Category category) {
        this.categories.remove(category);
        category.setFee(null);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fee fee = (Fee) o;
        if (fee.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fee.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Fee{" +
            "id=" + getId() +
            ", fee='" + getFee() + "'" +
            ", feeType='" + getFeeType() + "'" +
            "}";
    }
}
