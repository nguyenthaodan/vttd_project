package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Bidding.
 */
@Entity
@Table(name = "bidding")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Bidding implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "closing_date")
    private LocalDate closingDate;

    @Column(name = "bidders_count")
    private Integer biddersCount;

    @OneToOne
    @JoinColumn(unique = true)
    private JobTemplate jobTemplate;

    @OneToMany(mappedBy = "bidding")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Bid> bids = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getClosingDate() {
        return closingDate;
    }

    public Bidding closingDate(LocalDate closingDate) {
        this.closingDate = closingDate;
        return this;
    }

    public void setClosingDate(LocalDate closingDate) {
        this.closingDate = closingDate;
    }

    public Integer getBiddersCount() {
        return biddersCount;
    }

    public Bidding biddersCount(Integer biddersCount) {
        this.biddersCount = biddersCount;
        return this;
    }

    public void setBiddersCount(Integer biddersCount) {
        this.biddersCount = biddersCount;
    }

    public JobTemplate getJobTemplate() {
        return jobTemplate;
    }

    public Bidding jobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
        return this;
    }

    public void setJobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
    }

    public Set<Bid> getBids() {
        return bids;
    }

    public Bidding bids(Set<Bid> bids) {
        this.bids = bids;
        return this;
    }

    public Bidding addBid(Bid bid) {
        this.bids.add(bid);
        bid.setBidding(this);
        return this;
    }

    public Bidding removeBid(Bid bid) {
        this.bids.remove(bid);
        bid.setBidding(null);
        return this;
    }

    public void setBids(Set<Bid> bids) {
        this.bids = bids;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bidding bidding = (Bidding) o;
        if (bidding.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bidding.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bidding{" +
            "id=" + getId() +
            ", closingDate='" + getClosingDate() + "'" +
            ", biddersCount='" + getBiddersCount() + "'" +
            "}";
    }
}
