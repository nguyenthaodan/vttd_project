package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A JobInfo.
 */
@Entity
@Table(name = "job_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class JobInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "finished")
    private Integer finished;

    @Column(name = "canceled")
    private Integer canceled;

    @Column(name = "unfinished")
    private Integer unfinished;

    @Column(name = "pending")
    private Integer pending;

    @Column(name = "total_earnt")
    private Float totalEarnt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFinished() {
        return finished;
    }

    public JobInfo finished(Integer finished) {
        this.finished = finished;
        return this;
    }

    public void setFinished(Integer finished) {
        this.finished = finished;
    }

    public Integer getCanceled() {
        return canceled;
    }

    public JobInfo canceled(Integer canceled) {
        this.canceled = canceled;
        return this;
    }

    public void setCanceled(Integer canceled) {
        this.canceled = canceled;
    }

    public Integer getUnfinished() {
        return unfinished;
    }

    public JobInfo unfinished(Integer unfinished) {
        this.unfinished = unfinished;
        return this;
    }

    public void setUnfinished(Integer unfinished) {
        this.unfinished = unfinished;
    }

    public Integer getPending() {
        return pending;
    }

    public JobInfo pending(Integer pending) {
        this.pending = pending;
        return this;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    public Float getTotalEarnt() {
        return totalEarnt;
    }

    public JobInfo totalEarnt(Float totalEarnt) {
        this.totalEarnt = totalEarnt;
        return this;
    }

    public void setTotalEarnt(Float totalEarnt) {
        this.totalEarnt = totalEarnt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JobInfo jobInfo = (JobInfo) o;
        if (jobInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobInfo{" +
            "id=" + getId() +
            ", finished='" + getFinished() + "'" +
            ", canceled='" + getCanceled() + "'" +
            ", unfinished='" + getUnfinished() + "'" +
            ", pending='" + getPending() + "'" +
            ", totalEarnt='" + getTotalEarnt() + "'" +
            "}";
    }
}
