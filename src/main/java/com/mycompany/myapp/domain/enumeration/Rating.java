package com.mycompany.myapp.domain.enumeration;

/**
 * The Rating enumeration.
 */
public enum Rating {
    ONE, TWO, THREE, FOUR, FIVE
}
