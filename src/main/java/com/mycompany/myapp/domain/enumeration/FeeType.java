package com.mycompany.myapp.domain.enumeration;

/**
 * The FeeType enumeration.
 */
public enum FeeType {
    PERCENTAGE, FIXED
}
