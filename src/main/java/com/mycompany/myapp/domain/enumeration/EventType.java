package com.mycompany.myapp.domain.enumeration;

/**
 * The EventType enumeration.
 */
public enum EventType {
    BID, CANCEL, SEND_MESSAGE, CREATE, BOOK, EDIT_PROFILE, WITHRAW_MONEY, COMPLAIN, TOPUP_CREDIT, PAY, REVIEW, DEPOSIT, MEET, COMPLETE_JOB, CONFIRM_JOB_COMPLETE
}
