package com.mycompany.myapp.domain.enumeration;

/**
 * The PaymentMethod enumeration.
 */
public enum PaymentMethod {
    ALL, CASH, BANK_TRANSFER, PAYPAL, GOOGLE_WALLET, BITCOIN, MOBILE, PAYOO, POST
}
