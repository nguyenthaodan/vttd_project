package com.mycompany.myapp.domain.enumeration;

/**
 * The NotificationStatus enumeration.
 */
public enum NotificationStatus {
    SEEN, DELETED, UNSEEN
}
