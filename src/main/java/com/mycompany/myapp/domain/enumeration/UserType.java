package com.mycompany.myapp.domain.enumeration;

/**
 * The UserType enumeration.
 */
public enum UserType {
    NEW, BRONZE, SILVER, GOLD, DIAMOND
}
