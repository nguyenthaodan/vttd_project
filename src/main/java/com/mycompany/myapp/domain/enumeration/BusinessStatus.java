package com.mycompany.myapp.domain.enumeration;

/**
 * The BusinessStatus enumeration.
 */
public enum BusinessStatus {
    VERIFIED, APPROVED, PENDING, BLOCKED
}
