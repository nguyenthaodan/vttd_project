package com.mycompany.myapp.domain.enumeration;

/**
 * The CompleteStatus enumeration.
 */
public enum CompleteStatus {
    WAITING_FOR_FINAL_PAYMENT, PAID
}
