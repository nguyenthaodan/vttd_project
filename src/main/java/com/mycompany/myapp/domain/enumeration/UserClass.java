package com.mycompany.myapp.domain.enumeration;

/**
 * The UserClass enumeration.
 */
public enum UserClass {
    NEW, BRONZE, SILVER, GOLD, DIAMOND
}
