package com.mycompany.myapp.domain.enumeration;

/**
 * The EventCategory enumeration.
 */
public enum EventCategory {
    JOB, PAYMENT, CUSTOMER_SERVICE, PROFILE, MESSAGE, PROMOTION
}
