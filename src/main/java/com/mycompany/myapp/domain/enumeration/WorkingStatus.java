package com.mycompany.myapp.domain.enumeration;

/**
 * The WorkingStatus enumeration.
 */
public enum WorkingStatus {
    PENDING_MEETING, DOCUMENT_SENT, WAITING_FOR_FINAL_RESULT, MONEY_DEPOSIT, WAITING_FOR_FINAL_APPROVAL, ONE_QUARTER, TWO_QUARTER, THREE_QUARTER, WAITING_FOR_DOCUMENT
}
