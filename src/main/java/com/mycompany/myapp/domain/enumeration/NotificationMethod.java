package com.mycompany.myapp.domain.enumeration;

/**
 * The NotificationMethod enumeration.
 */
public enum NotificationMethod {
    PHONE, EMAIL, WEB, ALL
}
