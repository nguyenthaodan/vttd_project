package com.mycompany.myapp.domain.enumeration;

/**
 * The UserRole enumeration.
 */
public enum UserRole {
    ADMIN, PROVIDER, USER, SALES, GUEST
}
