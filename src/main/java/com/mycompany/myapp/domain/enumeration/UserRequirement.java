package com.mycompany.myapp.domain.enumeration;

/**
 * The UserRequirement enumeration.
 */
public enum UserRequirement {
    ALL, OFFLINE, ONLINE
}
