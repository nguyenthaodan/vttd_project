package com.mycompany.myapp.domain.enumeration;

/**
 * The JobRequirement enumeration.
 */
public enum JobRequirement {
    INTERVIEW, ONLINE, OFFLINE_NO_INTERVIEW
}
