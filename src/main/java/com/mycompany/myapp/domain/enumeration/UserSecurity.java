package com.mycompany.myapp.domain.enumeration;

/**
 * The UserSecurity enumeration.
 */
public enum UserSecurity {
    VERIFIED, PENDING, BLOCKED, INACTIVE
}
