package com.mycompany.myapp.domain.enumeration;

/**
 * The PromotionType enumeration.
 */
public enum PromotionType {
    FIXED, PERCENTAGE
}
