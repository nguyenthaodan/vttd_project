package com.mycompany.myapp.domain.enumeration;

/**
 * The BusinessType enumeration.
 */
public enum BusinessType {
    SOLE_OWNERSHIP, JOINT_STOCK, PRIVATED_LIMITED, FOREIGN_OWNED
}
