package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Canceled.
 */
@Entity
@Table(name = "canceled")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Canceled implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cancel_date")
    private LocalDate cancelDate;

    @Column(name = "reason")
    private String reason;

    @OneToOne
    @JoinColumn(unique = true)
    private JobTemplate jobTemplate;

    @OneToMany(mappedBy = "canceled")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SysUser> sysUsers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCancelDate() {
        return cancelDate;
    }

    public Canceled cancelDate(LocalDate cancelDate) {
        this.cancelDate = cancelDate;
        return this;
    }

    public void setCancelDate(LocalDate cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getReason() {
        return reason;
    }

    public Canceled reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public JobTemplate getJobTemplate() {
        return jobTemplate;
    }

    public Canceled jobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
        return this;
    }

    public void setJobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
    }

    public Set<SysUser> getSysUsers() {
        return sysUsers;
    }

    public Canceled sysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
        return this;
    }

    public Canceled addSysUser(SysUser sysUser) {
        this.sysUsers.add(sysUser);
        sysUser.setCanceled(this);
        return this;
    }

    public Canceled removeSysUser(SysUser sysUser) {
        this.sysUsers.remove(sysUser);
        sysUser.setCanceled(null);
        return this;
    }

    public void setSysUsers(Set<SysUser> sysUsers) {
        this.sysUsers = sysUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Canceled canceled = (Canceled) o;
        if (canceled.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), canceled.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Canceled{" +
            "id=" + getId() +
            ", cancelDate='" + getCancelDate() + "'" +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
