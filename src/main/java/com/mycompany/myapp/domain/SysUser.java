package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.UserRole;

import com.mycompany.myapp.domain.enumeration.UserType;

import com.mycompany.myapp.domain.enumeration.UserSecurity;

/**
 * A SysUser.
 */
@Entity
@Table(name = "sys_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_role")
    private UserRole role;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private UserType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "security")
    private UserSecurity security;

    @Column(name = "points")
    private Integer points;

    @OneToOne
    @JoinColumn(unique = true)
    private CreditInfo creditInfo;

    @OneToOne
    @JoinColumn(unique = true)
    private ChatInfo chatInfo;

    @OneToOne
    @JoinColumn(unique = true)
    private PushInfo pushInfo;

    @OneToOne
    @JoinColumn(unique = true)
    private ReviewInfo reviewInfo;

    @OneToOne
    @JoinColumn(unique = true)
    private JobInfo jobInfo;

    @OneToOne
    @JoinColumn(unique = true)
    private LoginInfo ;

    @OneToMany(mappedBy = "sysUser")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Event> events = new HashSet<>();

    @OneToMany(mappedBy = "sysUser")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<EventInfo> eventInfos = new HashSet<>();

    @OneToMany(mappedBy = "sysUser")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Business> businesses = new HashSet<>();

    @OneToMany(mappedBy = "sysUser")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Working> workings = new HashSet<>();

    @ManyToOne
    private Language language;

    @ManyToOne
    private Location location;

    @OneToMany(mappedBy = "sysUser")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<JobTemplate> jobTemplates = new HashSet<>();

    @OneToMany(mappedBy = "sysUser")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Review> reviews = new HashSet<>();

    @ManyToOne
    private Canceled canceled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getRole() {
        return role;
    }

    public SysUser role(UserRole role) {
        this.role = role;
        return this;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public UserType getType() {
        return type;
    }

    public SysUser type(UserType type) {
        this.type = type;
        return this;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public UserSecurity getSecurity() {
        return security;
    }

    public SysUser security(UserSecurity security) {
        this.security = security;
        return this;
    }

    public void setSecurity(UserSecurity security) {
        this.security = security;
    }

    public Integer getPoints() {
        return points;
    }

    public SysUser points(Integer points) {
        this.points = points;
        return this;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public CreditInfo getCreditInfo() {
        return creditInfo;
    }

    public SysUser creditInfo(CreditInfo creditInfo) {
        this.creditInfo = creditInfo;
        return this;
    }

    public void setCreditInfo(CreditInfo creditInfo) {
        this.creditInfo = creditInfo;
    }

    public ChatInfo getChatInfo() {
        return chatInfo;
    }

    public SysUser chatInfo(ChatInfo chatInfo) {
        this.chatInfo = chatInfo;
        return this;
    }

    public void setChatInfo(ChatInfo chatInfo) {
        this.chatInfo = chatInfo;
    }

    public PushInfo getPushInfo() {
        return pushInfo;
    }

    public SysUser pushInfo(PushInfo pushInfo) {
        this.pushInfo = pushInfo;
        return this;
    }

    public void setPushInfo(PushInfo pushInfo) {
        this.pushInfo = pushInfo;
    }

    public ReviewInfo getReviewInfo() {
        return reviewInfo;
    }

    public SysUser reviewInfo(ReviewInfo reviewInfo) {
        this.reviewInfo = reviewInfo;
        return this;
    }

    public void setReviewInfo(ReviewInfo reviewInfo) {
        this.reviewInfo = reviewInfo;
    }

    public JobInfo getJobInfo() {
        return jobInfo;
    }

    public SysUser jobInfo(JobInfo jobInfo) {
        this.jobInfo = jobInfo;
        return this;
    }

    public void setJobInfo(JobInfo jobInfo) {
        this.jobInfo = jobInfo;
    }

    public LoginInfo get() {
        return ;
    }

    public SysUser (LoginInfo loginInfo) {
        this. = loginInfo;
        return this;
    }

    public void set(LoginInfo loginInfo) {
        this. = loginInfo;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public SysUser events(Set<Event> events) {
        this.events = events;
        return this;
    }

    public SysUser addEvent(Event event) {
        this.events.add(event);
        event.setSysUser(this);
        return this;
    }

    public SysUser removeEvent(Event event) {
        this.events.remove(event);
        event.setSysUser(null);
        return this;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public Set<EventInfo> getEventInfos() {
        return eventInfos;
    }

    public SysUser eventInfos(Set<EventInfo> eventInfos) {
        this.eventInfos = eventInfos;
        return this;
    }

    public SysUser addEventInfo(EventInfo eventInfo) {
        this.eventInfos.add(eventInfo);
        eventInfo.setSysUser(this);
        return this;
    }

    public SysUser removeEventInfo(EventInfo eventInfo) {
        this.eventInfos.remove(eventInfo);
        eventInfo.setSysUser(null);
        return this;
    }

    public void setEventInfos(Set<EventInfo> eventInfos) {
        this.eventInfos = eventInfos;
    }

    public Set<Business> getBusinesses() {
        return businesses;
    }

    public SysUser businesses(Set<Business> businesses) {
        this.businesses = businesses;
        return this;
    }

    public SysUser addBusiness(Business business) {
        this.businesses.add(business);
        business.setSysUser(this);
        return this;
    }

    public SysUser removeBusiness(Business business) {
        this.businesses.remove(business);
        business.setSysUser(null);
        return this;
    }

    public void setBusinesses(Set<Business> businesses) {
        this.businesses = businesses;
    }

    public Set<Working> getWorkings() {
        return workings;
    }

    public SysUser workings(Set<Working> workings) {
        this.workings = workings;
        return this;
    }

    public SysUser addWorking(Working working) {
        this.workings.add(working);
        working.setSysUser(this);
        return this;
    }

    public SysUser removeWorking(Working working) {
        this.workings.remove(working);
        working.setSysUser(null);
        return this;
    }

    public void setWorkings(Set<Working> workings) {
        this.workings = workings;
    }

    public Language getLanguage() {
        return language;
    }

    public SysUser language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Location getLocation() {
        return location;
    }

    public SysUser location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<JobTemplate> getJobTemplates() {
        return jobTemplates;
    }

    public SysUser jobTemplates(Set<JobTemplate> jobTemplates) {
        this.jobTemplates = jobTemplates;
        return this;
    }

    public SysUser addJobTemplate(JobTemplate jobTemplate) {
        this.jobTemplates.add(jobTemplate);
        jobTemplate.setSysUser(this);
        return this;
    }

    public SysUser removeJobTemplate(JobTemplate jobTemplate) {
        this.jobTemplates.remove(jobTemplate);
        jobTemplate.setSysUser(null);
        return this;
    }

    public void setJobTemplates(Set<JobTemplate> jobTemplates) {
        this.jobTemplates = jobTemplates;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public SysUser reviews(Set<Review> reviews) {
        this.reviews = reviews;
        return this;
    }

    public SysUser addReview(Review review) {
        this.reviews.add(review);
        review.setSysUser(this);
        return this;
    }

    public SysUser removeReview(Review review) {
        this.reviews.remove(review);
        review.setSysUser(null);
        return this;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public Canceled getCanceled() {
        return canceled;
    }

    public SysUser canceled(Canceled canceled) {
        this.canceled = canceled;
        return this;
    }

    public void setCanceled(Canceled canceled) {
        this.canceled = canceled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SysUser sysUser = (SysUser) o;
        if (sysUser.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sysUser.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SysUser{" +
            "id=" + getId() +
            ", role='" + getRole() + "'" +
            ", type='" + getType() + "'" +
            ", security='" + getSecurity() + "'" +
            ", points='" + getPoints() + "'" +
            "}";
    }
}
