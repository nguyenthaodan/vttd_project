package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Bid.
 */
@Entity
@Table(name = "bid")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Bid implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "price")
    private Long price;

    @NotNull
    @Column(name = "time_to_finish", nullable = false)
    private LocalDate timeToFinish;

    @Column(name = "bid_time")
    private LocalDate bidTime;

    @NotNull
    @Column(name = "message", nullable = false)
    private String message;

    @ManyToOne
    private Bidding bidding;

    @ManyToOne
    private SysUser sysUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPrice() {
        return price;
    }

    public Bid price(Long price) {
        this.price = price;
        return this;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public LocalDate getTimeToFinish() {
        return timeToFinish;
    }

    public Bid timeToFinish(LocalDate timeToFinish) {
        this.timeToFinish = timeToFinish;
        return this;
    }

    public void setTimeToFinish(LocalDate timeToFinish) {
        this.timeToFinish = timeToFinish;
    }

    public LocalDate getBidTime() {
        return bidTime;
    }

    public Bid bidTime(LocalDate bidTime) {
        this.bidTime = bidTime;
        return this;
    }

    public void setBidTime(LocalDate bidTime) {
        this.bidTime = bidTime;
    }

    public String getMessage() {
        return message;
    }

    public Bid message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Bidding getBidding() {
        return bidding;
    }

    public Bid bidding(Bidding bidding) {
        this.bidding = bidding;
        return this;
    }

    public void setBidding(Bidding bidding) {
        this.bidding = bidding;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public Bid sysUser(SysUser sysUser) {
        this.sysUser = sysUser;
        return this;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bid bid = (Bid) o;
        if (bid.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bid.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bid{" +
            "id=" + getId() +
            ", price='" + getPrice() + "'" +
            ", timeToFinish='" + getTimeToFinish() + "'" +
            ", bidTime='" + getBidTime() + "'" +
            ", message='" + getMessage() + "'" +
            "}";
    }
}
