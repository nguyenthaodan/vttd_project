package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.mycompany.myapp.domain.enumeration.CompleteStatus;

/**
 * A Completed.
 */
@Entity
@Table(name = "completed")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Completed implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CompleteStatus status;

    @Column(name = "complete_date")
    private LocalDate completeDate;

    @Column(name = "reviewed")
    private Boolean reviewed;

    @Column(name = "rating")
    private Integer rating;

    @OneToOne
    @JoinColumn(unique = true)
    private JobTemplate jobTemplate;

    @OneToOne
    @JoinColumn(unique = true)
    private Invoice userInvoice;

    @OneToOne
    @JoinColumn(unique = true)
    private Invoice providerInvoice;

    @OneToOne(mappedBy = "completed")
    @JsonIgnore
    private Review review;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CompleteStatus getStatus() {
        return status;
    }

    public Completed status(CompleteStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(CompleteStatus status) {
        this.status = status;
    }

    public LocalDate getCompleteDate() {
        return completeDate;
    }

    public Completed completeDate(LocalDate completeDate) {
        this.completeDate = completeDate;
        return this;
    }

    public void setCompleteDate(LocalDate completeDate) {
        this.completeDate = completeDate;
    }

    public Boolean isReviewed() {
        return reviewed;
    }

    public Completed reviewed(Boolean reviewed) {
        this.reviewed = reviewed;
        return this;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    public Integer getRating() {
        return rating;
    }

    public Completed rating(Integer rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public JobTemplate getJobTemplate() {
        return jobTemplate;
    }

    public Completed jobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
        return this;
    }

    public void setJobTemplate(JobTemplate jobTemplate) {
        this.jobTemplate = jobTemplate;
    }

    public Invoice getUserInvoice() {
        return userInvoice;
    }

    public Completed userInvoice(Invoice invoice) {
        this.userInvoice = invoice;
        return this;
    }

    public void setUserInvoice(Invoice invoice) {
        this.userInvoice = invoice;
    }

    public Invoice getProviderInvoice() {
        return providerInvoice;
    }

    public Completed providerInvoice(Invoice invoice) {
        this.providerInvoice = invoice;
        return this;
    }

    public void setProviderInvoice(Invoice invoice) {
        this.providerInvoice = invoice;
    }

    public Review getReview() {
        return review;
    }

    public Completed review(Review review) {
        this.review = review;
        return this;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Completed completed = (Completed) o;
        if (completed.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), completed.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Completed{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", completeDate='" + getCompleteDate() + "'" +
            ", reviewed='" + isReviewed() + "'" +
            ", rating='" + getRating() + "'" +
            "}";
    }
}
