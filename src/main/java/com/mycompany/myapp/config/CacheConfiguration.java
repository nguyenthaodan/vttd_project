package com.mycompany.myapp.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.mycompany.myapp.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SocialUserConnection.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Language.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Language.class.getName() + ".sysUsers", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.CreditInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.ChatInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.PushInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.ReviewInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.UserInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.LoginInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.JobInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SysUser.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SysUser.class.getName() + ".events", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SysUser.class.getName() + ".eventInfos", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SysUser.class.getName() + ".businesses", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SysUser.class.getName() + ".workings", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SysUser.class.getName() + ".jobTemplates", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.SysUser.class.getName() + ".reviews", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.EventInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Event.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Category.class.getName() + ".parents", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Service.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Service.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Service.class.getName() + ".promotions", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Fee.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Fee.class.getName() + ".services", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Fee.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Form.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Promotion.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Location.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Location.class.getName() + ".sysUsers", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Business.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Business.class.getName() + ".services", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Business.class.getName() + ".locations", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Business.class.getName() + ".reviews", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.InvoiceInfo.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Bid.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.JobTemplate.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.JobTemplate.class.getName() + ".eventInfos", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.JobTemplate.class.getName() + ".locations", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Bidding.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Bidding.class.getName() + ".bids", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Working.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Completed.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Canceled.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Canceled.class.getName() + ".sysUsers", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Notification.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Notification.class.getName() + ".sysUsers", jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.PageTable.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Invoice.class.getName(), jcacheConfiguration);
            cm.createCache(com.mycompany.myapp.domain.Review.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
