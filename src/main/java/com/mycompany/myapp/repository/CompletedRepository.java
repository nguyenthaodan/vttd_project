package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Completed;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Completed entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompletedRepository extends JpaRepository<Completed,Long> {
    
}
