package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.LoginInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the LoginInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LoginInfoRepository extends JpaRepository<LoginInfo,Long> {
    
}
