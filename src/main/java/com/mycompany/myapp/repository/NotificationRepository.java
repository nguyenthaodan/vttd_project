package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Notification;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Notification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends JpaRepository<Notification,Long> {
    
    @Query("select distinct notification from Notification notification left join fetch notification.sysUsers")
    List<Notification> findAllWithEagerRelationships();

    @Query("select notification from Notification notification left join fetch notification.sysUsers where notification.id =:id")
    Notification findOneWithEagerRelationships(@Param("id") Long id);
    
}
