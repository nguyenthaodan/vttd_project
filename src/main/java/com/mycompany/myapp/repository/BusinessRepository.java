package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Business;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Business entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusinessRepository extends JpaRepository<Business,Long> {
    
    @Query("select distinct business from Business business left join fetch business.services left join fetch business.locations")
    List<Business> findAllWithEagerRelationships();

    @Query("select business from Business business left join fetch business.services left join fetch business.locations where business.id =:id")
    Business findOneWithEagerRelationships(@Param("id") Long id);
    
}
