package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ReviewInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ReviewInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReviewInfoRepository extends JpaRepository<ReviewInfo,Long> {
    
}
