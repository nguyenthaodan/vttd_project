package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.JobInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JobInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobInfoRepository extends JpaRepository<JobInfo,Long> {
    
}
