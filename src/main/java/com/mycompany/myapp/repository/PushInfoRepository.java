package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.PushInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PushInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PushInfoRepository extends JpaRepository<PushInfo,Long> {
    
}
