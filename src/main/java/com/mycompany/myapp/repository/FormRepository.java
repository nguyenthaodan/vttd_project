package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Form;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Form entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FormRepository extends JpaRepository<Form,Long> {
    
}
