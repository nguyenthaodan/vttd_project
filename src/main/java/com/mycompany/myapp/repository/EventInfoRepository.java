package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.EventInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the EventInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventInfoRepository extends JpaRepository<EventInfo,Long> {
    
}
