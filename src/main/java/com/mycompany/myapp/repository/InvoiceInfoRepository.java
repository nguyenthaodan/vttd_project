package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.InvoiceInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the InvoiceInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoiceInfoRepository extends JpaRepository<InvoiceInfo,Long> {
    
}
