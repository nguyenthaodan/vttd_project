package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.JobTemplate;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JobTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobTemplateRepository extends JpaRepository<JobTemplate,Long> {
    
}
