import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { BusinessTick } from './business-tick.model';
import { BusinessTickService } from './business-tick.service';

@Component({
    selector: 'jhi-business-tick-detail',
    templateUrl: './business-tick-detail.component.html'
})
export class BusinessTickDetailComponent implements OnInit, OnDestroy {

    business: BusinessTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private businessService: BusinessTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBusinesses();
    }

    load(id) {
        this.businessService.find(id).subscribe((business) => {
            this.business = business;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBusinesses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'businessListModification',
            (response) => this.load(this.business.id)
        );
    }
}
