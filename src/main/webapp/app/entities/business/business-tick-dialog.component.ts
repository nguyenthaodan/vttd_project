import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BusinessTick } from './business-tick.model';
import { BusinessTickPopupService } from './business-tick-popup.service';
import { BusinessTickService } from './business-tick.service';
import { ServiceTick, ServiceTickService } from '../service';
import { LocationTick, LocationTickService } from '../location';
import { InvoiceInfoTick, InvoiceInfoTickService } from '../invoice-info';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-business-tick-dialog',
    templateUrl: './business-tick-dialog.component.html'
})
export class BusinessTickDialogComponent implements OnInit {

    business: BusinessTick;
    isSaving: boolean;

    services: ServiceTick[];

    locations: LocationTick[];

    invoiceinfos: InvoiceInfoTick[];

    sysusers: SysUserTick[];
    dateOperateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private businessService: BusinessTickService,
        private serviceService: ServiceTickService,
        private locationService: LocationTickService,
        private invoiceInfoService: InvoiceInfoTickService,
        private sysUserService: SysUserTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.serviceService.query()
            .subscribe((res: ResponseWrapper) => { this.services = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.locationService.query()
            .subscribe((res: ResponseWrapper) => { this.locations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.invoiceInfoService.query()
            .subscribe((res: ResponseWrapper) => { this.invoiceinfos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.business.id !== undefined) {
            this.subscribeToSaveResponse(
                this.businessService.update(this.business));
        } else {
            this.subscribeToSaveResponse(
                this.businessService.create(this.business));
        }
    }

    private subscribeToSaveResponse(result: Observable<BusinessTick>) {
        result.subscribe((res: BusinessTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: BusinessTick) {
        this.eventManager.broadcast({ name: 'businessListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackServiceById(index: number, item: ServiceTick) {
        return item.id;
    }

    trackLocationById(index: number, item: LocationTick) {
        return item.id;
    }

    trackInvoiceInfoById(index: number, item: InvoiceInfoTick) {
        return item.id;
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-business-tick-popup',
    template: ''
})
export class BusinessTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private businessPopupService: BusinessTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.businessPopupService
                    .open(BusinessTickDialogComponent as Component, params['id']);
            } else {
                this.businessPopupService
                    .open(BusinessTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
