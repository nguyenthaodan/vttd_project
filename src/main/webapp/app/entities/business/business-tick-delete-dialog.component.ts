import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BusinessTick } from './business-tick.model';
import { BusinessTickPopupService } from './business-tick-popup.service';
import { BusinessTickService } from './business-tick.service';

@Component({
    selector: 'jhi-business-tick-delete-dialog',
    templateUrl: './business-tick-delete-dialog.component.html'
})
export class BusinessTickDeleteDialogComponent {

    business: BusinessTick;

    constructor(
        private businessService: BusinessTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.businessService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'businessListModification',
                content: 'Deleted an business'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-business-tick-delete-popup',
    template: ''
})
export class BusinessTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private businessPopupService: BusinessTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.businessPopupService
                .open(BusinessTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
