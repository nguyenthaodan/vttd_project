import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { BusinessTick } from './business-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BusinessTickService {

    private resourceUrl = 'api/businesses';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(business: BusinessTick): Observable<BusinessTick> {
        const copy = this.convert(business);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(business: BusinessTick): Observable<BusinessTick> {
        const copy = this.convert(business);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<BusinessTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dateOperate = this.dateUtils
            .convertLocalDateFromServer(entity.dateOperate);
    }

    private convert(business: BusinessTick): BusinessTick {
        const copy: BusinessTick = Object.assign({}, business);
        copy.dateOperate = this.dateUtils
            .convertLocalDateToServer(business.dateOperate);
        return copy;
    }
}
