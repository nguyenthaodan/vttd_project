import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    BusinessTickService,
    BusinessTickPopupService,
    BusinessTickComponent,
    BusinessTickDetailComponent,
    BusinessTickDialogComponent,
    BusinessTickPopupComponent,
    BusinessTickDeletePopupComponent,
    BusinessTickDeleteDialogComponent,
    businessRoute,
    businessPopupRoute,
    BusinessTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...businessRoute,
    ...businessPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        BusinessTickComponent,
        BusinessTickDetailComponent,
        BusinessTickDialogComponent,
        BusinessTickDeleteDialogComponent,
        BusinessTickPopupComponent,
        BusinessTickDeletePopupComponent,
    ],
    entryComponents: [
        BusinessTickComponent,
        BusinessTickDialogComponent,
        BusinessTickPopupComponent,
        BusinessTickDeleteDialogComponent,
        BusinessTickDeletePopupComponent,
    ],
    providers: [
        BusinessTickService,
        BusinessTickPopupService,
        BusinessTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickBusinessTickModule {}
