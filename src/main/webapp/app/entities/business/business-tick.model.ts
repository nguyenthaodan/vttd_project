import { BaseEntity } from './../../shared';

const enum BusinessType {
    'SOLE_OWNERSHIP',
    'JOINT_STOCK',
    'PRIVATED_LIMITED',
    'FOREIGN_OWNED'
}

const enum BusinessStatus {
    'VERIFIED',
    'APPROVED',
    'PENDING',
    'BLOCKED'
}

const enum UserRequirement {
    'ALL',
    'OFFLINE',
    'ONLINE'
}

export class BusinessTick implements BaseEntity {
    constructor(
        public id?: number,
        public businessName?: string,
        public type?: BusinessType,
        public description?: string,
        public dateOperate?: any,
        public totalJobDone?: number,
        public website?: string,
        public status?: BusinessStatus,
        public user?: UserRequirement,
        public note?: string,
        public services?: BaseEntity[],
        public locations?: BaseEntity[],
        public invoiceInfoId?: number,
        public sysUserId?: number,
        public reviews?: BaseEntity[],
    ) {
    }
}
