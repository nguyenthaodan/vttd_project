import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BusinessTickComponent } from './business-tick.component';
import { BusinessTickDetailComponent } from './business-tick-detail.component';
import { BusinessTickPopupComponent } from './business-tick-dialog.component';
import { BusinessTickDeletePopupComponent } from './business-tick-delete-dialog.component';

@Injectable()
export class BusinessTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const businessRoute: Routes = [
    {
        path: 'business-tick',
        component: BusinessTickComponent,
        resolve: {
            'pagingParams': BusinessTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.business.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'business-tick/:id',
        component: BusinessTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.business.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const businessPopupRoute: Routes = [
    {
        path: 'business-tick-new',
        component: BusinessTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.business.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'business-tick/:id/edit',
        component: BusinessTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.business.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'business-tick/:id/delete',
        component: BusinessTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.business.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
