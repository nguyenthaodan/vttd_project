import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BusinessTick } from './business-tick.model';
import { BusinessTickService } from './business-tick.service';

@Injectable()
export class BusinessTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private businessService: BusinessTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.businessService.find(id).subscribe((business) => {
                    if (business.dateOperate) {
                        business.dateOperate = {
                            year: business.dateOperate.getFullYear(),
                            month: business.dateOperate.getMonth() + 1,
                            day: business.dateOperate.getDate()
                        };
                    }
                    this.ngbModalRef = this.businessModalRef(component, business);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.businessModalRef(component, new BusinessTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    businessModalRef(component: Component, business: BusinessTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.business = business;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
