import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TickLanguageTickModule } from './language/language-tick.module';
import { TickCreditInfoTickModule } from './credit-info/credit-info-tick.module';
import { TickChatInfoTickModule } from './chat-info/chat-info-tick.module';
import { TickPushInfoTickModule } from './push-info/push-info-tick.module';
import { TickReviewInfoTickModule } from './review-info/review-info-tick.module';
import { TickUserInfoTickModule } from './user-info/user-info-tick.module';
import { TickLoginInfoTickModule } from './login-info/login-info-tick.module';
import { TickJobInfoTickModule } from './job-info/job-info-tick.module';
import { TickSysUserTickModule } from './sys-user/sys-user-tick.module';
import { TickEventInfoTickModule } from './event-info/event-info-tick.module';
import { TickEventTickModule } from './event/event-tick.module';
import { TickCategoryTickModule } from './category/category-tick.module';
import { TickServiceTickModule } from './service/service-tick.module';
import { TickFeeTickModule } from './fee/fee-tick.module';
import { TickFormTickModule } from './form/form-tick.module';
import { TickPromotionTickModule } from './promotion/promotion-tick.module';
import { TickLocationTickModule } from './location/location-tick.module';
import { TickBusinessTickModule } from './business/business-tick.module';
import { TickInvoiceInfoTickModule } from './invoice-info/invoice-info-tick.module';
import { TickBidTickModule } from './bid/bid-tick.module';
import { TickJobTemplateTickModule } from './job-template/job-template-tick.module';
import { TickBiddingTickModule } from './bidding/bidding-tick.module';
import { TickWorkingTickModule } from './working/working-tick.module';
import { TickCompletedTickModule } from './completed/completed-tick.module';
import { TickCanceledTickModule } from './canceled/canceled-tick.module';
import { TickNotificationTickModule } from './notification/notification-tick.module';
import { TickPageTableTickModule } from './page-table/page-table-tick.module';
import { TickInvoiceTickModule } from './invoice/invoice-tick.module';
import { TickReviewTickModule } from './review/review-tick.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TickLanguageTickModule,
        TickCreditInfoTickModule,
        TickChatInfoTickModule,
        TickPushInfoTickModule,
        TickReviewInfoTickModule,
        TickUserInfoTickModule,
        TickLoginInfoTickModule,
        TickJobInfoTickModule,
        TickSysUserTickModule,
        TickEventInfoTickModule,
        TickEventTickModule,
        TickCategoryTickModule,
        TickServiceTickModule,
        TickFeeTickModule,
        TickFormTickModule,
        TickPromotionTickModule,
        TickLocationTickModule,
        TickBusinessTickModule,
        TickInvoiceInfoTickModule,
        TickBidTickModule,
        TickJobTemplateTickModule,
        TickBiddingTickModule,
        TickWorkingTickModule,
        TickCompletedTickModule,
        TickCanceledTickModule,
        TickNotificationTickModule,
        TickPageTableTickModule,
        TickInvoiceTickModule,
        TickReviewTickModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickEntityModule {}
