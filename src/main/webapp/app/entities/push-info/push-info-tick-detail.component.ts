import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PushInfoTick } from './push-info-tick.model';
import { PushInfoTickService } from './push-info-tick.service';

@Component({
    selector: 'jhi-push-info-tick-detail',
    templateUrl: './push-info-tick-detail.component.html'
})
export class PushInfoTickDetailComponent implements OnInit, OnDestroy {

    pushInfo: PushInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private pushInfoService: PushInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPushInfos();
    }

    load(id) {
        this.pushInfoService.find(id).subscribe((pushInfo) => {
            this.pushInfo = pushInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPushInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pushInfoListModification',
            (response) => this.load(this.pushInfo.id)
        );
    }
}
