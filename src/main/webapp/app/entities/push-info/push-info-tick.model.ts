import { BaseEntity } from './../../shared';

export class PushInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public username?: string,
        public password?: string,
    ) {
    }
}
