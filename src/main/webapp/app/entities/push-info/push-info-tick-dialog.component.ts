import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PushInfoTick } from './push-info-tick.model';
import { PushInfoTickPopupService } from './push-info-tick-popup.service';
import { PushInfoTickService } from './push-info-tick.service';

@Component({
    selector: 'jhi-push-info-tick-dialog',
    templateUrl: './push-info-tick-dialog.component.html'
})
export class PushInfoTickDialogComponent implements OnInit {

    pushInfo: PushInfoTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private pushInfoService: PushInfoTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pushInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pushInfoService.update(this.pushInfo));
        } else {
            this.subscribeToSaveResponse(
                this.pushInfoService.create(this.pushInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<PushInfoTick>) {
        result.subscribe((res: PushInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PushInfoTick) {
        this.eventManager.broadcast({ name: 'pushInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-push-info-tick-popup',
    template: ''
})
export class PushInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pushInfoPopupService: PushInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pushInfoPopupService
                    .open(PushInfoTickDialogComponent as Component, params['id']);
            } else {
                this.pushInfoPopupService
                    .open(PushInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
