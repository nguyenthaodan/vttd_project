import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PushInfoTick } from './push-info-tick.model';
import { PushInfoTickPopupService } from './push-info-tick-popup.service';
import { PushInfoTickService } from './push-info-tick.service';

@Component({
    selector: 'jhi-push-info-tick-delete-dialog',
    templateUrl: './push-info-tick-delete-dialog.component.html'
})
export class PushInfoTickDeleteDialogComponent {

    pushInfo: PushInfoTick;

    constructor(
        private pushInfoService: PushInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pushInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pushInfoListModification',
                content: 'Deleted an pushInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-push-info-tick-delete-popup',
    template: ''
})
export class PushInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pushInfoPopupService: PushInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pushInfoPopupService
                .open(PushInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
