import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    PushInfoTickService,
    PushInfoTickPopupService,
    PushInfoTickComponent,
    PushInfoTickDetailComponent,
    PushInfoTickDialogComponent,
    PushInfoTickPopupComponent,
    PushInfoTickDeletePopupComponent,
    PushInfoTickDeleteDialogComponent,
    pushInfoRoute,
    pushInfoPopupRoute,
    PushInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...pushInfoRoute,
    ...pushInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PushInfoTickComponent,
        PushInfoTickDetailComponent,
        PushInfoTickDialogComponent,
        PushInfoTickDeleteDialogComponent,
        PushInfoTickPopupComponent,
        PushInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        PushInfoTickComponent,
        PushInfoTickDialogComponent,
        PushInfoTickPopupComponent,
        PushInfoTickDeleteDialogComponent,
        PushInfoTickDeletePopupComponent,
    ],
    providers: [
        PushInfoTickService,
        PushInfoTickPopupService,
        PushInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickPushInfoTickModule {}
