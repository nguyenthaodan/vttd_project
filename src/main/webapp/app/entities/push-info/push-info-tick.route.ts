import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PushInfoTickComponent } from './push-info-tick.component';
import { PushInfoTickDetailComponent } from './push-info-tick-detail.component';
import { PushInfoTickPopupComponent } from './push-info-tick-dialog.component';
import { PushInfoTickDeletePopupComponent } from './push-info-tick-delete-dialog.component';

@Injectable()
export class PushInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const pushInfoRoute: Routes = [
    {
        path: 'push-info-tick',
        component: PushInfoTickComponent,
        resolve: {
            'pagingParams': PushInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pushInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'push-info-tick/:id',
        component: PushInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pushInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pushInfoPopupRoute: Routes = [
    {
        path: 'push-info-tick-new',
        component: PushInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pushInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'push-info-tick/:id/edit',
        component: PushInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pushInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'push-info-tick/:id/delete',
        component: PushInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pushInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
