import { BaseEntity } from './../../shared';

export class EventInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public sysUserId?: number,
        public jobTemplateId?: number,
    ) {
    }
}
