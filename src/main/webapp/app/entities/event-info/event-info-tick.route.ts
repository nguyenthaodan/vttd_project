import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EventInfoTickComponent } from './event-info-tick.component';
import { EventInfoTickDetailComponent } from './event-info-tick-detail.component';
import { EventInfoTickPopupComponent } from './event-info-tick-dialog.component';
import { EventInfoTickDeletePopupComponent } from './event-info-tick-delete-dialog.component';

@Injectable()
export class EventInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const eventInfoRoute: Routes = [
    {
        path: 'event-info-tick',
        component: EventInfoTickComponent,
        resolve: {
            'pagingParams': EventInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.eventInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'event-info-tick/:id',
        component: EventInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.eventInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eventInfoPopupRoute: Routes = [
    {
        path: 'event-info-tick-new',
        component: EventInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.eventInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-info-tick/:id/edit',
        component: EventInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.eventInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-info-tick/:id/delete',
        component: EventInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.eventInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
