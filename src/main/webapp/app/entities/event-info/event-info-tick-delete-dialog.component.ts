import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { EventInfoTick } from './event-info-tick.model';
import { EventInfoTickPopupService } from './event-info-tick-popup.service';
import { EventInfoTickService } from './event-info-tick.service';

@Component({
    selector: 'jhi-event-info-tick-delete-dialog',
    templateUrl: './event-info-tick-delete-dialog.component.html'
})
export class EventInfoTickDeleteDialogComponent {

    eventInfo: EventInfoTick;

    constructor(
        private eventInfoService: EventInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.eventInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'eventInfoListModification',
                content: 'Deleted an eventInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-event-info-tick-delete-popup',
    template: ''
})
export class EventInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventInfoPopupService: EventInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.eventInfoPopupService
                .open(EventInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
