import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { EventInfoTick } from './event-info-tick.model';
import { EventInfoTickService } from './event-info-tick.service';

@Component({
    selector: 'jhi-event-info-tick-detail',
    templateUrl: './event-info-tick-detail.component.html'
})
export class EventInfoTickDetailComponent implements OnInit, OnDestroy {

    eventInfo: EventInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private eventInfoService: EventInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEventInfos();
    }

    load(id) {
        this.eventInfoService.find(id).subscribe((eventInfo) => {
            this.eventInfo = eventInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEventInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'eventInfoListModification',
            (response) => this.load(this.eventInfo.id)
        );
    }
}
