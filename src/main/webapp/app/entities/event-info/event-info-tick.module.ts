import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    EventInfoTickService,
    EventInfoTickPopupService,
    EventInfoTickComponent,
    EventInfoTickDetailComponent,
    EventInfoTickDialogComponent,
    EventInfoTickPopupComponent,
    EventInfoTickDeletePopupComponent,
    EventInfoTickDeleteDialogComponent,
    eventInfoRoute,
    eventInfoPopupRoute,
    EventInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...eventInfoRoute,
    ...eventInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EventInfoTickComponent,
        EventInfoTickDetailComponent,
        EventInfoTickDialogComponent,
        EventInfoTickDeleteDialogComponent,
        EventInfoTickPopupComponent,
        EventInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        EventInfoTickComponent,
        EventInfoTickDialogComponent,
        EventInfoTickPopupComponent,
        EventInfoTickDeleteDialogComponent,
        EventInfoTickDeletePopupComponent,
    ],
    providers: [
        EventInfoTickService,
        EventInfoTickPopupService,
        EventInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickEventInfoTickModule {}
