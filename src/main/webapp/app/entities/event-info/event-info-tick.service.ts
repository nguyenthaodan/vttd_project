import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { EventInfoTick } from './event-info-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class EventInfoTickService {

    private resourceUrl = 'api/event-infos';

    constructor(private http: Http) { }

    create(eventInfo: EventInfoTick): Observable<EventInfoTick> {
        const copy = this.convert(eventInfo);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(eventInfo: EventInfoTick): Observable<EventInfoTick> {
        const copy = this.convert(eventInfo);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<EventInfoTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(eventInfo: EventInfoTick): EventInfoTick {
        const copy: EventInfoTick = Object.assign({}, eventInfo);
        return copy;
    }
}
