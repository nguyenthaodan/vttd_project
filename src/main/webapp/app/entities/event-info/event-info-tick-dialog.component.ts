import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EventInfoTick } from './event-info-tick.model';
import { EventInfoTickPopupService } from './event-info-tick-popup.service';
import { EventInfoTickService } from './event-info-tick.service';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { JobTemplateTick, JobTemplateTickService } from '../job-template';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-event-info-tick-dialog',
    templateUrl: './event-info-tick-dialog.component.html'
})
export class EventInfoTickDialogComponent implements OnInit {

    eventInfo: EventInfoTick;
    isSaving: boolean;

    sysusers: SysUserTick[];

    jobtemplates: JobTemplateTick[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private eventInfoService: EventInfoTickService,
        private sysUserService: SysUserTickService,
        private jobTemplateService: JobTemplateTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.jobTemplateService.query()
            .subscribe((res: ResponseWrapper) => { this.jobtemplates = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.eventInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.eventInfoService.update(this.eventInfo));
        } else {
            this.subscribeToSaveResponse(
                this.eventInfoService.create(this.eventInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<EventInfoTick>) {
        result.subscribe((res: EventInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: EventInfoTick) {
        this.eventManager.broadcast({ name: 'eventInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }

    trackJobTemplateById(index: number, item: JobTemplateTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-event-info-tick-popup',
    template: ''
})
export class EventInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventInfoPopupService: EventInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eventInfoPopupService
                    .open(EventInfoTickDialogComponent as Component, params['id']);
            } else {
                this.eventInfoPopupService
                    .open(EventInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
