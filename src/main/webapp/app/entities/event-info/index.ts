export * from './event-info-tick.model';
export * from './event-info-tick-popup.service';
export * from './event-info-tick.service';
export * from './event-info-tick-dialog.component';
export * from './event-info-tick-delete-dialog.component';
export * from './event-info-tick-detail.component';
export * from './event-info-tick.component';
export * from './event-info-tick.route';
