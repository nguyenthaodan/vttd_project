import { BaseEntity } from './../../shared';

export class LoginInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public username?: string,
        public password?: string,
        public email?: string,
        public sysUserId?: number,
    ) {
    }
}
