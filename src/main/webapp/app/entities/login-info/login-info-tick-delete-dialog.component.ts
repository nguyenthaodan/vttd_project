import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LoginInfoTick } from './login-info-tick.model';
import { LoginInfoTickPopupService } from './login-info-tick-popup.service';
import { LoginInfoTickService } from './login-info-tick.service';

@Component({
    selector: 'jhi-login-info-tick-delete-dialog',
    templateUrl: './login-info-tick-delete-dialog.component.html'
})
export class LoginInfoTickDeleteDialogComponent {

    loginInfo: LoginInfoTick;

    constructor(
        private loginInfoService: LoginInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.loginInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'loginInfoListModification',
                content: 'Deleted an loginInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-login-info-tick-delete-popup',
    template: ''
})
export class LoginInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private loginInfoPopupService: LoginInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.loginInfoPopupService
                .open(LoginInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
