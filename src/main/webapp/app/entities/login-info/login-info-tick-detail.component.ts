import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { LoginInfoTick } from './login-info-tick.model';
import { LoginInfoTickService } from './login-info-tick.service';

@Component({
    selector: 'jhi-login-info-tick-detail',
    templateUrl: './login-info-tick-detail.component.html'
})
export class LoginInfoTickDetailComponent implements OnInit, OnDestroy {

    loginInfo: LoginInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private loginInfoService: LoginInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLoginInfos();
    }

    load(id) {
        this.loginInfoService.find(id).subscribe((loginInfo) => {
            this.loginInfo = loginInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLoginInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'loginInfoListModification',
            (response) => this.load(this.loginInfo.id)
        );
    }
}
