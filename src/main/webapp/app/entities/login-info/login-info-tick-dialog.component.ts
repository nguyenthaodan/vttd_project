import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LoginInfoTick } from './login-info-tick.model';
import { LoginInfoTickPopupService } from './login-info-tick-popup.service';
import { LoginInfoTickService } from './login-info-tick.service';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-login-info-tick-dialog',
    templateUrl: './login-info-tick-dialog.component.html'
})
export class LoginInfoTickDialogComponent implements OnInit {

    loginInfo: LoginInfoTick;
    isSaving: boolean;

    sysusers: SysUserTick[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private loginInfoService: LoginInfoTickService,
        private sysUserService: SysUserTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.loginInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.loginInfoService.update(this.loginInfo));
        } else {
            this.subscribeToSaveResponse(
                this.loginInfoService.create(this.loginInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<LoginInfoTick>) {
        result.subscribe((res: LoginInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: LoginInfoTick) {
        this.eventManager.broadcast({ name: 'loginInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-login-info-tick-popup',
    template: ''
})
export class LoginInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private loginInfoPopupService: LoginInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.loginInfoPopupService
                    .open(LoginInfoTickDialogComponent as Component, params['id']);
            } else {
                this.loginInfoPopupService
                    .open(LoginInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
