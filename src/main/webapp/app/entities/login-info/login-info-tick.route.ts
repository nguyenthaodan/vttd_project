import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LoginInfoTickComponent } from './login-info-tick.component';
import { LoginInfoTickDetailComponent } from './login-info-tick-detail.component';
import { LoginInfoTickPopupComponent } from './login-info-tick-dialog.component';
import { LoginInfoTickDeletePopupComponent } from './login-info-tick-delete-dialog.component';

@Injectable()
export class LoginInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const loginInfoRoute: Routes = [
    {
        path: 'login-info-tick',
        component: LoginInfoTickComponent,
        resolve: {
            'pagingParams': LoginInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.loginInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'login-info-tick/:id',
        component: LoginInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.loginInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const loginInfoPopupRoute: Routes = [
    {
        path: 'login-info-tick-new',
        component: LoginInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.loginInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'login-info-tick/:id/edit',
        component: LoginInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.loginInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'login-info-tick/:id/delete',
        component: LoginInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.loginInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
