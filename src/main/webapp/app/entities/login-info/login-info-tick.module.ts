import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    LoginInfoTickService,
    LoginInfoTickPopupService,
    LoginInfoTickComponent,
    LoginInfoTickDetailComponent,
    LoginInfoTickDialogComponent,
    LoginInfoTickPopupComponent,
    LoginInfoTickDeletePopupComponent,
    LoginInfoTickDeleteDialogComponent,
    loginInfoRoute,
    loginInfoPopupRoute,
    LoginInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...loginInfoRoute,
    ...loginInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LoginInfoTickComponent,
        LoginInfoTickDetailComponent,
        LoginInfoTickDialogComponent,
        LoginInfoTickDeleteDialogComponent,
        LoginInfoTickPopupComponent,
        LoginInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        LoginInfoTickComponent,
        LoginInfoTickDialogComponent,
        LoginInfoTickPopupComponent,
        LoginInfoTickDeleteDialogComponent,
        LoginInfoTickDeletePopupComponent,
    ],
    providers: [
        LoginInfoTickService,
        LoginInfoTickPopupService,
        LoginInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickLoginInfoTickModule {}
