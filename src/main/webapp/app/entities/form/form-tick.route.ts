import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { FormTickComponent } from './form-tick.component';
import { FormTickDetailComponent } from './form-tick-detail.component';
import { FormTickPopupComponent } from './form-tick-dialog.component';
import { FormTickDeletePopupComponent } from './form-tick-delete-dialog.component';

@Injectable()
export class FormTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const formRoute: Routes = [
    {
        path: 'form-tick',
        component: FormTickComponent,
        resolve: {
            'pagingParams': FormTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.form.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'form-tick/:id',
        component: FormTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.form.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const formPopupRoute: Routes = [
    {
        path: 'form-tick-new',
        component: FormTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.form.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'form-tick/:id/edit',
        component: FormTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.form.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'form-tick/:id/delete',
        component: FormTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.form.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
