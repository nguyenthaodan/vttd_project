import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { FormTick } from './form-tick.model';
import { FormTickPopupService } from './form-tick-popup.service';
import { FormTickService } from './form-tick.service';

@Component({
    selector: 'jhi-form-tick-delete-dialog',
    templateUrl: './form-tick-delete-dialog.component.html'
})
export class FormTickDeleteDialogComponent {

    form: FormTick;

    constructor(
        private formService: FormTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.formService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'formListModification',
                content: 'Deleted an form'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-form-tick-delete-popup',
    template: ''
})
export class FormTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private formPopupService: FormTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.formPopupService
                .open(FormTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
