import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    FormTickService,
    FormTickPopupService,
    FormTickComponent,
    FormTickDetailComponent,
    FormTickDialogComponent,
    FormTickPopupComponent,
    FormTickDeletePopupComponent,
    FormTickDeleteDialogComponent,
    formRoute,
    formPopupRoute,
    FormTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...formRoute,
    ...formPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        FormTickComponent,
        FormTickDetailComponent,
        FormTickDialogComponent,
        FormTickDeleteDialogComponent,
        FormTickPopupComponent,
        FormTickDeletePopupComponent,
    ],
    entryComponents: [
        FormTickComponent,
        FormTickDialogComponent,
        FormTickPopupComponent,
        FormTickDeleteDialogComponent,
        FormTickDeletePopupComponent,
    ],
    providers: [
        FormTickService,
        FormTickPopupService,
        FormTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickFormTickModule {}
