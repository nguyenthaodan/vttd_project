import { BaseEntity } from './../../shared';

export class FormTick implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public desc?: string,
        public content?: string,
        public Id?: number,
    ) {
    }
}
