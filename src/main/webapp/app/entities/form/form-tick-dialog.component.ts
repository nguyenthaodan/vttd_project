import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { FormTick } from './form-tick.model';
import { FormTickPopupService } from './form-tick-popup.service';
import { FormTickService } from './form-tick.service';
import { ServiceTick, ServiceTickService } from '../service';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-form-tick-dialog',
    templateUrl: './form-tick-dialog.component.html'
})
export class FormTickDialogComponent implements OnInit {

    form: FormTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private formService: FormTickService,
        private serviceService: ServiceTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.form.id !== undefined) {
            this.subscribeToSaveResponse(
                this.formService.update(this.form));
        } else {
            this.subscribeToSaveResponse(
                this.formService.create(this.form));
        }
    }

    private subscribeToSaveResponse(result: Observable<FormTick>) {
        result.subscribe((res: FormTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: FormTick) {
        this.eventManager.broadcast({ name: 'formListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackServiceById(index: number, item: ServiceTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-form-tick-popup',
    template: ''
})
export class FormTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private formPopupService: FormTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.formPopupService
                    .open(FormTickDialogComponent as Component, params['id']);
            } else {
                this.formPopupService
                    .open(FormTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
