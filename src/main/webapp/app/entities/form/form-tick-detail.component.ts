import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { FormTick } from './form-tick.model';
import { FormTickService } from './form-tick.service';

@Component({
    selector: 'jhi-form-tick-detail',
    templateUrl: './form-tick-detail.component.html'
})
export class FormTickDetailComponent implements OnInit, OnDestroy {

    form: FormTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private formService: FormTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInForms();
    }

    load(id) {
        this.formService.find(id).subscribe((form) => {
            this.form = form;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInForms() {
        this.eventSubscriber = this.eventManager.subscribe(
            'formListModification',
            (response) => this.load(this.form.id)
        );
    }
}
