export * from './form-tick.model';
export * from './form-tick-popup.service';
export * from './form-tick.service';
export * from './form-tick-dialog.component';
export * from './form-tick-delete-dialog.component';
export * from './form-tick-detail.component';
export * from './form-tick.component';
export * from './form-tick.route';
