import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { JobInfoTick } from './job-info-tick.model';
import { JobInfoTickService } from './job-info-tick.service';

@Component({
    selector: 'jhi-job-info-tick-detail',
    templateUrl: './job-info-tick-detail.component.html'
})
export class JobInfoTickDetailComponent implements OnInit, OnDestroy {

    jobInfo: JobInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private jobInfoService: JobInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJobInfos();
    }

    load(id) {
        this.jobInfoService.find(id).subscribe((jobInfo) => {
            this.jobInfo = jobInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJobInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jobInfoListModification',
            (response) => this.load(this.jobInfo.id)
        );
    }
}
