import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    JobInfoTickService,
    JobInfoTickPopupService,
    JobInfoTickComponent,
    JobInfoTickDetailComponent,
    JobInfoTickDialogComponent,
    JobInfoTickPopupComponent,
    JobInfoTickDeletePopupComponent,
    JobInfoTickDeleteDialogComponent,
    jobInfoRoute,
    jobInfoPopupRoute,
    JobInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...jobInfoRoute,
    ...jobInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        JobInfoTickComponent,
        JobInfoTickDetailComponent,
        JobInfoTickDialogComponent,
        JobInfoTickDeleteDialogComponent,
        JobInfoTickPopupComponent,
        JobInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        JobInfoTickComponent,
        JobInfoTickDialogComponent,
        JobInfoTickPopupComponent,
        JobInfoTickDeleteDialogComponent,
        JobInfoTickDeletePopupComponent,
    ],
    providers: [
        JobInfoTickService,
        JobInfoTickPopupService,
        JobInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickJobInfoTickModule {}
