import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JobInfoTick } from './job-info-tick.model';
import { JobInfoTickPopupService } from './job-info-tick-popup.service';
import { JobInfoTickService } from './job-info-tick.service';

@Component({
    selector: 'jhi-job-info-tick-dialog',
    templateUrl: './job-info-tick-dialog.component.html'
})
export class JobInfoTickDialogComponent implements OnInit {

    jobInfo: JobInfoTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private jobInfoService: JobInfoTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.jobInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jobInfoService.update(this.jobInfo));
        } else {
            this.subscribeToSaveResponse(
                this.jobInfoService.create(this.jobInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<JobInfoTick>) {
        result.subscribe((res: JobInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: JobInfoTick) {
        this.eventManager.broadcast({ name: 'jobInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-job-info-tick-popup',
    template: ''
})
export class JobInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jobInfoPopupService: JobInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jobInfoPopupService
                    .open(JobInfoTickDialogComponent as Component, params['id']);
            } else {
                this.jobInfoPopupService
                    .open(JobInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
