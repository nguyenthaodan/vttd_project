import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JobInfoTick } from './job-info-tick.model';
import { JobInfoTickPopupService } from './job-info-tick-popup.service';
import { JobInfoTickService } from './job-info-tick.service';

@Component({
    selector: 'jhi-job-info-tick-delete-dialog',
    templateUrl: './job-info-tick-delete-dialog.component.html'
})
export class JobInfoTickDeleteDialogComponent {

    jobInfo: JobInfoTick;

    constructor(
        private jobInfoService: JobInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jobInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jobInfoListModification',
                content: 'Deleted an jobInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-job-info-tick-delete-popup',
    template: ''
})
export class JobInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jobInfoPopupService: JobInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jobInfoPopupService
                .open(JobInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
