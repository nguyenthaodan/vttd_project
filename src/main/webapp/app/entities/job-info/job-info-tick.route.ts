import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { JobInfoTickComponent } from './job-info-tick.component';
import { JobInfoTickDetailComponent } from './job-info-tick-detail.component';
import { JobInfoTickPopupComponent } from './job-info-tick-dialog.component';
import { JobInfoTickDeletePopupComponent } from './job-info-tick-delete-dialog.component';

@Injectable()
export class JobInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const jobInfoRoute: Routes = [
    {
        path: 'job-info-tick',
        component: JobInfoTickComponent,
        resolve: {
            'pagingParams': JobInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'job-info-tick/:id',
        component: JobInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const jobInfoPopupRoute: Routes = [
    {
        path: 'job-info-tick-new',
        component: JobInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'job-info-tick/:id/edit',
        component: JobInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'job-info-tick/:id/delete',
        component: JobInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
