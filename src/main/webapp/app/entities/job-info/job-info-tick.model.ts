import { BaseEntity } from './../../shared';

export class JobInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public finished?: number,
        public canceled?: number,
        public unfinished?: number,
        public pending?: number,
        public totalEarnt?: number,
    ) {
    }
}
