import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    CategoryTickService,
    CategoryTickPopupService,
    CategoryTickComponent,
    CategoryTickDetailComponent,
    CategoryTickDialogComponent,
    CategoryTickPopupComponent,
    CategoryTickDeletePopupComponent,
    CategoryTickDeleteDialogComponent,
    categoryRoute,
    categoryPopupRoute,
    CategoryTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...categoryRoute,
    ...categoryPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CategoryTickComponent,
        CategoryTickDetailComponent,
        CategoryTickDialogComponent,
        CategoryTickDeleteDialogComponent,
        CategoryTickPopupComponent,
        CategoryTickDeletePopupComponent,
    ],
    entryComponents: [
        CategoryTickComponent,
        CategoryTickDialogComponent,
        CategoryTickPopupComponent,
        CategoryTickDeleteDialogComponent,
        CategoryTickDeletePopupComponent,
    ],
    providers: [
        CategoryTickService,
        CategoryTickPopupService,
        CategoryTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickCategoryTickModule {}
