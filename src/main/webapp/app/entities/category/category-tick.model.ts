import { BaseEntity } from './../../shared';

export class CategoryTick implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public desc?: string,
        public servicesCount?: number,
        public providerCount?: number,
        public restricted?: boolean,
        public categoryId?: number,
        public parents?: BaseEntity[],
        public feeId?: number,
        public serviceId?: number,
    ) {
        this.restricted = false;
    }
}
