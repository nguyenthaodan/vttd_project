import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CategoryTick } from './category-tick.model';
import { CategoryTickPopupService } from './category-tick-popup.service';
import { CategoryTickService } from './category-tick.service';
import { FeeTick, FeeTickService } from '../fee';
import { ServiceTick, ServiceTickService } from '../service';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-category-tick-dialog',
    templateUrl: './category-tick-dialog.component.html'
})
export class CategoryTickDialogComponent implements OnInit {

    category: CategoryTick;
    isSaving: boolean;

    categories: CategoryTick[];

    fees: FeeTick[];

    services: ServiceTick[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private categoryService: CategoryTickService,
        private feeService: FeeTickService,
        private serviceService: ServiceTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.categoryService.query()
            .subscribe((res: ResponseWrapper) => { this.categories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.feeService.query()
            .subscribe((res: ResponseWrapper) => { this.fees = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.serviceService.query()
            .subscribe((res: ResponseWrapper) => { this.services = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.category.id !== undefined) {
            this.subscribeToSaveResponse(
                this.categoryService.update(this.category));
        } else {
            this.subscribeToSaveResponse(
                this.categoryService.create(this.category));
        }
    }

    private subscribeToSaveResponse(result: Observable<CategoryTick>) {
        result.subscribe((res: CategoryTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: CategoryTick) {
        this.eventManager.broadcast({ name: 'categoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackCategoryById(index: number, item: CategoryTick) {
        return item.id;
    }

    trackFeeById(index: number, item: FeeTick) {
        return item.id;
    }

    trackServiceById(index: number, item: ServiceTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-category-tick-popup',
    template: ''
})
export class CategoryTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private categoryPopupService: CategoryTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.categoryPopupService
                    .open(CategoryTickDialogComponent as Component, params['id']);
            } else {
                this.categoryPopupService
                    .open(CategoryTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
