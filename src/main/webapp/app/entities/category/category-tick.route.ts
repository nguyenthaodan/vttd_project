import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CategoryTickComponent } from './category-tick.component';
import { CategoryTickDetailComponent } from './category-tick-detail.component';
import { CategoryTickPopupComponent } from './category-tick-dialog.component';
import { CategoryTickDeletePopupComponent } from './category-tick-delete-dialog.component';

@Injectable()
export class CategoryTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const categoryRoute: Routes = [
    {
        path: 'category-tick',
        component: CategoryTickComponent,
        resolve: {
            'pagingParams': CategoryTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'category-tick/:id',
        component: CategoryTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.category.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const categoryPopupRoute: Routes = [
    {
        path: 'category-tick-new',
        component: CategoryTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.category.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'category-tick/:id/edit',
        component: CategoryTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.category.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'category-tick/:id/delete',
        component: CategoryTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.category.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
