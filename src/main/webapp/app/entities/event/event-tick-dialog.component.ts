import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EventTick } from './event-tick.model';
import { EventTickPopupService } from './event-tick-popup.service';
import { EventTickService } from './event-tick.service';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { EventInfoTick, EventInfoTickService } from '../event-info';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-event-tick-dialog',
    templateUrl: './event-tick-dialog.component.html'
})
export class EventTickDialogComponent implements OnInit {

    event: EventTick;
    isSaving: boolean;

    sysusers: SysUserTick[];

    eventinfos: EventInfoTick[];
    dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private eventService: EventTickService,
        private sysUserService: SysUserTickService,
        private eventInfoService: EventInfoTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventInfoService
            .query({filter: 'event-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.event.eventInfoId) {
                    this.eventinfos = res.json;
                } else {
                    this.eventInfoService
                        .find(this.event.eventInfoId)
                        .subscribe((subRes: EventInfoTick) => {
                            this.eventinfos = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.event.id !== undefined) {
            this.subscribeToSaveResponse(
                this.eventService.update(this.event));
        } else {
            this.subscribeToSaveResponse(
                this.eventService.create(this.event));
        }
    }

    private subscribeToSaveResponse(result: Observable<EventTick>) {
        result.subscribe((res: EventTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: EventTick) {
        this.eventManager.broadcast({ name: 'eventListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }

    trackEventInfoById(index: number, item: EventInfoTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-event-tick-popup',
    template: ''
})
export class EventTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventPopupService: EventTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eventPopupService
                    .open(EventTickDialogComponent as Component, params['id']);
            } else {
                this.eventPopupService
                    .open(EventTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
