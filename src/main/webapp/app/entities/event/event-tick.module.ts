import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    EventTickService,
    EventTickPopupService,
    EventTickComponent,
    EventTickDetailComponent,
    EventTickDialogComponent,
    EventTickPopupComponent,
    EventTickDeletePopupComponent,
    EventTickDeleteDialogComponent,
    eventRoute,
    eventPopupRoute,
    EventTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...eventRoute,
    ...eventPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EventTickComponent,
        EventTickDetailComponent,
        EventTickDialogComponent,
        EventTickDeleteDialogComponent,
        EventTickPopupComponent,
        EventTickDeletePopupComponent,
    ],
    entryComponents: [
        EventTickComponent,
        EventTickDialogComponent,
        EventTickPopupComponent,
        EventTickDeleteDialogComponent,
        EventTickDeletePopupComponent,
    ],
    providers: [
        EventTickService,
        EventTickPopupService,
        EventTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickEventTickModule {}
