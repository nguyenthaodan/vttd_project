import { BaseEntity } from './../../shared';

const enum EventType {
    'BID',
    'CANCEL',
    'SEND_MESSAGE',
    'CREATE',
    'BOOK',
    'EDIT_PROFILE',
    'WITHRAW_MONEY',
    'COMPLAIN',
    'TOPUP_CREDIT',
    'PAY',
    'REVIEW',
    'DEPOSIT',
    'MEET',
    'COMPLETE_JOB',
    'CONFIRM_JOB_COMPLETE'
}

const enum EventCategory {
    'JOB',
    'PAYMENT',
    'CUSTOMER_SERVICE',
    'PROFILE',
    'MESSAGE',
    'PROMOTION'
}

export class EventTick implements BaseEntity {
    constructor(
        public id?: number,
        public type?: EventType,
        public category?: EventCategory,
        public date?: any,
        public desc?: string,
        public amount?: number,
        public sysUserId?: number,
        public eventInfoId?: number,
    ) {
    }
}
