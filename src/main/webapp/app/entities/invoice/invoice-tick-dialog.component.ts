import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { InvoiceTick } from './invoice-tick.model';
import { InvoiceTickPopupService } from './invoice-tick-popup.service';
import { InvoiceTickService } from './invoice-tick.service';
import { User, UserService } from '../../shared';
import { BusinessTick, BusinessTickService } from '../business';
import { CompletedTick, CompletedTickService } from '../completed';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-invoice-tick-dialog',
    templateUrl: './invoice-tick-dialog.component.html'
})
export class InvoiceTickDialogComponent implements OnInit {

    invoice: InvoiceTick;
    isSaving: boolean;

    users: User[];

    businesses: BusinessTick[];

    completeds: CompletedTick[];
    receiptDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private invoiceService: InvoiceTickService,
        private userService: UserService,
        private businessService: BusinessTickService,
        private completedService: CompletedTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.businessService.query()
            .subscribe((res: ResponseWrapper) => { this.businesses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.completedService.query()
            .subscribe((res: ResponseWrapper) => { this.completeds = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.invoice.id !== undefined) {
            this.subscribeToSaveResponse(
                this.invoiceService.update(this.invoice));
        } else {
            this.subscribeToSaveResponse(
                this.invoiceService.create(this.invoice));
        }
    }

    private subscribeToSaveResponse(result: Observable<InvoiceTick>) {
        result.subscribe((res: InvoiceTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: InvoiceTick) {
        this.eventManager.broadcast({ name: 'invoiceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackBusinessById(index: number, item: BusinessTick) {
        return item.id;
    }

    trackCompletedById(index: number, item: CompletedTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-invoice-tick-popup',
    template: ''
})
export class InvoiceTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private invoicePopupService: InvoiceTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.invoicePopupService
                    .open(InvoiceTickDialogComponent as Component, params['id']);
            } else {
                this.invoicePopupService
                    .open(InvoiceTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
