import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { InvoiceTickComponent } from './invoice-tick.component';
import { InvoiceTickDetailComponent } from './invoice-tick-detail.component';
import { InvoiceTickPopupComponent } from './invoice-tick-dialog.component';
import { InvoiceTickDeletePopupComponent } from './invoice-tick-delete-dialog.component';

@Injectable()
export class InvoiceTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const invoiceRoute: Routes = [
    {
        path: 'invoice-tick',
        component: InvoiceTickComponent,
        resolve: {
            'pagingParams': InvoiceTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invoice-tick/:id',
        component: InvoiceTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invoicePopupRoute: Routes = [
    {
        path: 'invoice-tick-new',
        component: InvoiceTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invoice-tick/:id/edit',
        component: InvoiceTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invoice-tick/:id/delete',
        component: InvoiceTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoice.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
