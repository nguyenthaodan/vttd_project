import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { InvoiceTick } from './invoice-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class InvoiceTickService {

    private resourceUrl = 'api/invoices';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(invoice: InvoiceTick): Observable<InvoiceTick> {
        const copy = this.convert(invoice);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(invoice: InvoiceTick): Observable<InvoiceTick> {
        const copy = this.convert(invoice);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<InvoiceTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.receiptDate = this.dateUtils
            .convertLocalDateFromServer(entity.receiptDate);
    }

    private convert(invoice: InvoiceTick): InvoiceTick {
        const copy: InvoiceTick = Object.assign({}, invoice);
        copy.receiptDate = this.dateUtils
            .convertLocalDateToServer(invoice.receiptDate);
        return copy;
    }
}
