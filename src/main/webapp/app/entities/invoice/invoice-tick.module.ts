import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import { TickAdminModule } from '../../admin/admin.module';
import {
    InvoiceTickService,
    InvoiceTickPopupService,
    InvoiceTickComponent,
    InvoiceTickDetailComponent,
    InvoiceTickDialogComponent,
    InvoiceTickPopupComponent,
    InvoiceTickDeletePopupComponent,
    InvoiceTickDeleteDialogComponent,
    invoiceRoute,
    invoicePopupRoute,
    InvoiceTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...invoiceRoute,
    ...invoicePopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        TickAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        InvoiceTickComponent,
        InvoiceTickDetailComponent,
        InvoiceTickDialogComponent,
        InvoiceTickDeleteDialogComponent,
        InvoiceTickPopupComponent,
        InvoiceTickDeletePopupComponent,
    ],
    entryComponents: [
        InvoiceTickComponent,
        InvoiceTickDialogComponent,
        InvoiceTickPopupComponent,
        InvoiceTickDeleteDialogComponent,
        InvoiceTickDeletePopupComponent,
    ],
    providers: [
        InvoiceTickService,
        InvoiceTickPopupService,
        InvoiceTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickInvoiceTickModule {}
