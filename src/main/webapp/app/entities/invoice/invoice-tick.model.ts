import { BaseEntity } from './../../shared';

export class InvoiceTick implements BaseEntity {
    constructor(
        public id?: number,
        public receiptDate?: any,
        public receiptAmount?: number,
        public taxRate?: number,
        public receiptInfo?: string,
        public userId?: number,
        public businessId?: number,
        public completedId?: number,
    ) {
    }
}
