export * from './chat-info-tick.model';
export * from './chat-info-tick-popup.service';
export * from './chat-info-tick.service';
export * from './chat-info-tick-dialog.component';
export * from './chat-info-tick-delete-dialog.component';
export * from './chat-info-tick-detail.component';
export * from './chat-info-tick.component';
export * from './chat-info-tick.route';
