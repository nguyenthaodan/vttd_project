import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ChatInfoTick } from './chat-info-tick.model';
import { ChatInfoTickPopupService } from './chat-info-tick-popup.service';
import { ChatInfoTickService } from './chat-info-tick.service';

@Component({
    selector: 'jhi-chat-info-tick-delete-dialog',
    templateUrl: './chat-info-tick-delete-dialog.component.html'
})
export class ChatInfoTickDeleteDialogComponent {

    chatInfo: ChatInfoTick;

    constructor(
        private chatInfoService: ChatInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.chatInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'chatInfoListModification',
                content: 'Deleted an chatInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-chat-info-tick-delete-popup',
    template: ''
})
export class ChatInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private chatInfoPopupService: ChatInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.chatInfoPopupService
                .open(ChatInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
