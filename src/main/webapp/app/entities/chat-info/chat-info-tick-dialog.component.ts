import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ChatInfoTick } from './chat-info-tick.model';
import { ChatInfoTickPopupService } from './chat-info-tick-popup.service';
import { ChatInfoTickService } from './chat-info-tick.service';

@Component({
    selector: 'jhi-chat-info-tick-dialog',
    templateUrl: './chat-info-tick-dialog.component.html'
})
export class ChatInfoTickDialogComponent implements OnInit {

    chatInfo: ChatInfoTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private chatInfoService: ChatInfoTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.chatInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.chatInfoService.update(this.chatInfo));
        } else {
            this.subscribeToSaveResponse(
                this.chatInfoService.create(this.chatInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<ChatInfoTick>) {
        result.subscribe((res: ChatInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ChatInfoTick) {
        this.eventManager.broadcast({ name: 'chatInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-chat-info-tick-popup',
    template: ''
})
export class ChatInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private chatInfoPopupService: ChatInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.chatInfoPopupService
                    .open(ChatInfoTickDialogComponent as Component, params['id']);
            } else {
                this.chatInfoPopupService
                    .open(ChatInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
