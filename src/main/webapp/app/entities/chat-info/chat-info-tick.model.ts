import { BaseEntity } from './../../shared';

export class ChatInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public username?: string,
        public password?: string,
    ) {
    }
}
