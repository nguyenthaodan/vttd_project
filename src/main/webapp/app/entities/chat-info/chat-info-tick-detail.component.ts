import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { ChatInfoTick } from './chat-info-tick.model';
import { ChatInfoTickService } from './chat-info-tick.service';

@Component({
    selector: 'jhi-chat-info-tick-detail',
    templateUrl: './chat-info-tick-detail.component.html'
})
export class ChatInfoTickDetailComponent implements OnInit, OnDestroy {

    chatInfo: ChatInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private chatInfoService: ChatInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInChatInfos();
    }

    load(id) {
        this.chatInfoService.find(id).subscribe((chatInfo) => {
            this.chatInfo = chatInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInChatInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'chatInfoListModification',
            (response) => this.load(this.chatInfo.id)
        );
    }
}
