import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ChatInfoTickComponent } from './chat-info-tick.component';
import { ChatInfoTickDetailComponent } from './chat-info-tick-detail.component';
import { ChatInfoTickPopupComponent } from './chat-info-tick-dialog.component';
import { ChatInfoTickDeletePopupComponent } from './chat-info-tick-delete-dialog.component';

@Injectable()
export class ChatInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const chatInfoRoute: Routes = [
    {
        path: 'chat-info-tick',
        component: ChatInfoTickComponent,
        resolve: {
            'pagingParams': ChatInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.chatInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'chat-info-tick/:id',
        component: ChatInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.chatInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const chatInfoPopupRoute: Routes = [
    {
        path: 'chat-info-tick-new',
        component: ChatInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.chatInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'chat-info-tick/:id/edit',
        component: ChatInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.chatInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'chat-info-tick/:id/delete',
        component: ChatInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.chatInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
