import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    ChatInfoTickService,
    ChatInfoTickPopupService,
    ChatInfoTickComponent,
    ChatInfoTickDetailComponent,
    ChatInfoTickDialogComponent,
    ChatInfoTickPopupComponent,
    ChatInfoTickDeletePopupComponent,
    ChatInfoTickDeleteDialogComponent,
    chatInfoRoute,
    chatInfoPopupRoute,
    ChatInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...chatInfoRoute,
    ...chatInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ChatInfoTickComponent,
        ChatInfoTickDetailComponent,
        ChatInfoTickDialogComponent,
        ChatInfoTickDeleteDialogComponent,
        ChatInfoTickPopupComponent,
        ChatInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        ChatInfoTickComponent,
        ChatInfoTickDialogComponent,
        ChatInfoTickPopupComponent,
        ChatInfoTickDeleteDialogComponent,
        ChatInfoTickDeletePopupComponent,
    ],
    providers: [
        ChatInfoTickService,
        ChatInfoTickPopupService,
        ChatInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickChatInfoTickModule {}
