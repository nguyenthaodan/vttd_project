import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ServiceTick } from './service-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ServiceTickService {

    private resourceUrl = 'api/services';

    constructor(private http: Http) { }

    create(service: ServiceTick): Observable<ServiceTick> {
        const copy = this.convert(service);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(service: ServiceTick): Observable<ServiceTick> {
        const copy = this.convert(service);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ServiceTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(service: ServiceTick): ServiceTick {
        const copy: ServiceTick = Object.assign({}, service);
        return copy;
    }
}
