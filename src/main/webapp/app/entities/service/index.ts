export * from './service-tick.model';
export * from './service-tick-popup.service';
export * from './service-tick.service';
export * from './service-tick-dialog.component';
export * from './service-tick-delete-dialog.component';
export * from './service-tick-detail.component';
export * from './service-tick.component';
export * from './service-tick.route';
