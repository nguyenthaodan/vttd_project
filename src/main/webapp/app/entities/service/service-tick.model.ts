import { BaseEntity } from './../../shared';

export class ServiceTick implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public desc?: string,
        public restricted?: boolean,
        public providerCount?: number,
        public categories?: BaseEntity[],
        public promotions?: BaseEntity[],
        public formId?: number,
        public feeId?: number,
    ) {
        this.restricted = false;
    }
}
