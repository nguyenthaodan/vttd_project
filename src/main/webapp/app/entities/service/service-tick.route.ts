import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ServiceTickComponent } from './service-tick.component';
import { ServiceTickDetailComponent } from './service-tick-detail.component';
import { ServiceTickPopupComponent } from './service-tick-dialog.component';
import { ServiceTickDeletePopupComponent } from './service-tick-delete-dialog.component';

@Injectable()
export class ServiceTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const serviceRoute: Routes = [
    {
        path: 'service-tick',
        component: ServiceTickComponent,
        resolve: {
            'pagingParams': ServiceTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.service.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'service-tick/:id',
        component: ServiceTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.service.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const servicePopupRoute: Routes = [
    {
        path: 'service-tick-new',
        component: ServiceTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.service.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-tick/:id/edit',
        component: ServiceTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.service.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-tick/:id/delete',
        component: ServiceTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.service.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
