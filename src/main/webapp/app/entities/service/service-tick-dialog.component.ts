import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ServiceTick } from './service-tick.model';
import { ServiceTickPopupService } from './service-tick-popup.service';
import { ServiceTickService } from './service-tick.service';
import { PromotionTick, PromotionTickService } from '../promotion';
import { FormTick, FormTickService } from '../form';
import { FeeTick, FeeTickService } from '../fee';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-service-tick-dialog',
    templateUrl: './service-tick-dialog.component.html'
})
export class ServiceTickDialogComponent implements OnInit {

    service: ServiceTick;
    isSaving: boolean;

    promotions: PromotionTick[];

    forms: FormTick[];

    fees: FeeTick[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private serviceService: ServiceTickService,
        private promotionService: PromotionTickService,
        private formService: FormTickService,
        private feeService: FeeTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.promotionService.query()
            .subscribe((res: ResponseWrapper) => { this.promotions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.formService.query()
            .subscribe((res: ResponseWrapper) => { this.forms = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.feeService.query()
            .subscribe((res: ResponseWrapper) => { this.fees = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.service.id !== undefined) {
            this.subscribeToSaveResponse(
                this.serviceService.update(this.service));
        } else {
            this.subscribeToSaveResponse(
                this.serviceService.create(this.service));
        }
    }

    private subscribeToSaveResponse(result: Observable<ServiceTick>) {
        result.subscribe((res: ServiceTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ServiceTick) {
        this.eventManager.broadcast({ name: 'serviceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackPromotionById(index: number, item: PromotionTick) {
        return item.id;
    }

    trackFormById(index: number, item: FormTick) {
        return item.id;
    }

    trackFeeById(index: number, item: FeeTick) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-service-tick-popup',
    template: ''
})
export class ServiceTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private servicePopupService: ServiceTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.servicePopupService
                    .open(ServiceTickDialogComponent as Component, params['id']);
            } else {
                this.servicePopupService
                    .open(ServiceTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
