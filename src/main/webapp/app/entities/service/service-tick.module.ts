import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    ServiceTickService,
    ServiceTickPopupService,
    ServiceTickComponent,
    ServiceTickDetailComponent,
    ServiceTickDialogComponent,
    ServiceTickPopupComponent,
    ServiceTickDeletePopupComponent,
    ServiceTickDeleteDialogComponent,
    serviceRoute,
    servicePopupRoute,
    ServiceTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...serviceRoute,
    ...servicePopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ServiceTickComponent,
        ServiceTickDetailComponent,
        ServiceTickDialogComponent,
        ServiceTickDeleteDialogComponent,
        ServiceTickPopupComponent,
        ServiceTickDeletePopupComponent,
    ],
    entryComponents: [
        ServiceTickComponent,
        ServiceTickDialogComponent,
        ServiceTickPopupComponent,
        ServiceTickDeleteDialogComponent,
        ServiceTickDeletePopupComponent,
    ],
    providers: [
        ServiceTickService,
        ServiceTickPopupService,
        ServiceTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickServiceTickModule {}
