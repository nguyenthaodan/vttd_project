import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    NotificationTickService,
    NotificationTickPopupService,
    NotificationTickComponent,
    NotificationTickDetailComponent,
    NotificationTickDialogComponent,
    NotificationTickPopupComponent,
    NotificationTickDeletePopupComponent,
    NotificationTickDeleteDialogComponent,
    notificationRoute,
    notificationPopupRoute,
    NotificationTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...notificationRoute,
    ...notificationPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        NotificationTickComponent,
        NotificationTickDetailComponent,
        NotificationTickDialogComponent,
        NotificationTickDeleteDialogComponent,
        NotificationTickPopupComponent,
        NotificationTickDeletePopupComponent,
    ],
    entryComponents: [
        NotificationTickComponent,
        NotificationTickDialogComponent,
        NotificationTickPopupComponent,
        NotificationTickDeleteDialogComponent,
        NotificationTickDeletePopupComponent,
    ],
    providers: [
        NotificationTickService,
        NotificationTickPopupService,
        NotificationTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickNotificationTickModule {}
