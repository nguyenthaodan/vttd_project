import { BaseEntity } from './../../shared';

const enum NotificationStatus {
    'SEEN',
    'DELETED',
    'UNSEEN'
}

const enum NotificationMethod {
    'PHONE',
    'EMAIL',
    'WEB',
    'ALL'
}

export class NotificationTick implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public status?: NotificationStatus,
        public method?: NotificationMethod,
        public message?: string,
        public eventId?: number,
        public sysUsers?: BaseEntity[],
    ) {
    }
}
