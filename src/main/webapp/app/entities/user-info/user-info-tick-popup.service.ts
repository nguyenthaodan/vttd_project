import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UserInfoTick } from './user-info-tick.model';
import { UserInfoTickService } from './user-info-tick.service';

@Injectable()
export class UserInfoTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private userInfoService: UserInfoTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.userInfoService.find(id).subscribe((userInfo) => {
                    if (userInfo.dateJoined) {
                        userInfo.dateJoined = {
                            year: userInfo.dateJoined.getFullYear(),
                            month: userInfo.dateJoined.getMonth() + 1,
                            day: userInfo.dateJoined.getDate()
                        };
                    }
                    this.ngbModalRef = this.userInfoModalRef(component, userInfo);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.userInfoModalRef(component, new UserInfoTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    userInfoModalRef(component: Component, userInfo: UserInfoTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.userInfo = userInfo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
