import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { UserInfoTick } from './user-info-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UserInfoTickService {

    private resourceUrl = 'api/user-infos';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(userInfo: UserInfoTick): Observable<UserInfoTick> {
        const copy = this.convert(userInfo);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(userInfo: UserInfoTick): Observable<UserInfoTick> {
        const copy = this.convert(userInfo);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<UserInfoTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.dateJoined = this.dateUtils
            .convertLocalDateFromServer(entity.dateJoined);
    }

    private convert(userInfo: UserInfoTick): UserInfoTick {
        const copy: UserInfoTick = Object.assign({}, userInfo);
        copy.dateJoined = this.dateUtils
            .convertLocalDateToServer(userInfo.dateJoined);
        return copy;
    }
}
