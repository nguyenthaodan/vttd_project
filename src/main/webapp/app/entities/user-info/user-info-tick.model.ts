import { BaseEntity } from './../../shared';

export class UserInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public lastName?: string,
        public firstName?: string,
        public address?: string,
        public phoneNumber?: string,
        public countryCode?: string,
        public dateJoined?: any,
        public photo?: string,
        public birthDate?: string,
        public facebook?: string,
    ) {
    }
}
