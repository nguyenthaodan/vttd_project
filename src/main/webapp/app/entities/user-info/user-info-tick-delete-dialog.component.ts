import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserInfoTick } from './user-info-tick.model';
import { UserInfoTickPopupService } from './user-info-tick-popup.service';
import { UserInfoTickService } from './user-info-tick.service';

@Component({
    selector: 'jhi-user-info-tick-delete-dialog',
    templateUrl: './user-info-tick-delete-dialog.component.html'
})
export class UserInfoTickDeleteDialogComponent {

    userInfo: UserInfoTick;

    constructor(
        private userInfoService: UserInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.userInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'userInfoListModification',
                content: 'Deleted an userInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-user-info-tick-delete-popup',
    template: ''
})
export class UserInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userInfoPopupService: UserInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.userInfoPopupService
                .open(UserInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
