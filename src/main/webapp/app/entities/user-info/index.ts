export * from './user-info-tick.model';
export * from './user-info-tick-popup.service';
export * from './user-info-tick.service';
export * from './user-info-tick-dialog.component';
export * from './user-info-tick-delete-dialog.component';
export * from './user-info-tick-detail.component';
export * from './user-info-tick.component';
export * from './user-info-tick.route';
