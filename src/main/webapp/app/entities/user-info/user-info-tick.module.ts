import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    UserInfoTickService,
    UserInfoTickPopupService,
    UserInfoTickComponent,
    UserInfoTickDetailComponent,
    UserInfoTickDialogComponent,
    UserInfoTickPopupComponent,
    UserInfoTickDeletePopupComponent,
    UserInfoTickDeleteDialogComponent,
    userInfoRoute,
    userInfoPopupRoute,
    UserInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...userInfoRoute,
    ...userInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UserInfoTickComponent,
        UserInfoTickDetailComponent,
        UserInfoTickDialogComponent,
        UserInfoTickDeleteDialogComponent,
        UserInfoTickPopupComponent,
        UserInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        UserInfoTickComponent,
        UserInfoTickDialogComponent,
        UserInfoTickPopupComponent,
        UserInfoTickDeleteDialogComponent,
        UserInfoTickDeletePopupComponent,
    ],
    providers: [
        UserInfoTickService,
        UserInfoTickPopupService,
        UserInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickUserInfoTickModule {}
