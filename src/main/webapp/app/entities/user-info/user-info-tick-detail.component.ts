import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { UserInfoTick } from './user-info-tick.model';
import { UserInfoTickService } from './user-info-tick.service';

@Component({
    selector: 'jhi-user-info-tick-detail',
    templateUrl: './user-info-tick-detail.component.html'
})
export class UserInfoTickDetailComponent implements OnInit, OnDestroy {

    userInfo: UserInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private userInfoService: UserInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUserInfos();
    }

    load(id) {
        this.userInfoService.find(id).subscribe((userInfo) => {
            this.userInfo = userInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUserInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'userInfoListModification',
            (response) => this.load(this.userInfo.id)
        );
    }
}
