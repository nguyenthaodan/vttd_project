import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { FeeTick } from './fee-tick.model';
import { FeeTickPopupService } from './fee-tick-popup.service';
import { FeeTickService } from './fee-tick.service';

@Component({
    selector: 'jhi-fee-tick-dialog',
    templateUrl: './fee-tick-dialog.component.html'
})
export class FeeTickDialogComponent implements OnInit {

    fee: FeeTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private feeService: FeeTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.fee.id !== undefined) {
            this.subscribeToSaveResponse(
                this.feeService.update(this.fee));
        } else {
            this.subscribeToSaveResponse(
                this.feeService.create(this.fee));
        }
    }

    private subscribeToSaveResponse(result: Observable<FeeTick>) {
        result.subscribe((res: FeeTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: FeeTick) {
        this.eventManager.broadcast({ name: 'feeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-fee-tick-popup',
    template: ''
})
export class FeeTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private feePopupService: FeeTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.feePopupService
                    .open(FeeTickDialogComponent as Component, params['id']);
            } else {
                this.feePopupService
                    .open(FeeTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
