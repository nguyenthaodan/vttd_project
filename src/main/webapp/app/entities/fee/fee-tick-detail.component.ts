import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { FeeTick } from './fee-tick.model';
import { FeeTickService } from './fee-tick.service';

@Component({
    selector: 'jhi-fee-tick-detail',
    templateUrl: './fee-tick-detail.component.html'
})
export class FeeTickDetailComponent implements OnInit, OnDestroy {

    fee: FeeTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private feeService: FeeTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInFees();
    }

    load(id) {
        this.feeService.find(id).subscribe((fee) => {
            this.fee = fee;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInFees() {
        this.eventSubscriber = this.eventManager.subscribe(
            'feeListModification',
            (response) => this.load(this.fee.id)
        );
    }
}
