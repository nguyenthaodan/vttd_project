import { BaseEntity } from './../../shared';

const enum FeeType {
    'PERCENTAGE',
    'FIXED'
}

export class FeeTick implements BaseEntity {
    constructor(
        public id?: number,
        public fee?: number,
        public feeType?: FeeType,
        public services?: BaseEntity[],
        public categories?: BaseEntity[],
    ) {
    }
}
