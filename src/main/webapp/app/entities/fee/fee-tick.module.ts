import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    FeeTickService,
    FeeTickPopupService,
    FeeTickComponent,
    FeeTickDetailComponent,
    FeeTickDialogComponent,
    FeeTickPopupComponent,
    FeeTickDeletePopupComponent,
    FeeTickDeleteDialogComponent,
    feeRoute,
    feePopupRoute,
    FeeTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...feeRoute,
    ...feePopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        FeeTickComponent,
        FeeTickDetailComponent,
        FeeTickDialogComponent,
        FeeTickDeleteDialogComponent,
        FeeTickPopupComponent,
        FeeTickDeletePopupComponent,
    ],
    entryComponents: [
        FeeTickComponent,
        FeeTickDialogComponent,
        FeeTickPopupComponent,
        FeeTickDeleteDialogComponent,
        FeeTickDeletePopupComponent,
    ],
    providers: [
        FeeTickService,
        FeeTickPopupService,
        FeeTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickFeeTickModule {}
