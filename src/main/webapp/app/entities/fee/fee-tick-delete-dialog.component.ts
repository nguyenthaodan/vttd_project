import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { FeeTick } from './fee-tick.model';
import { FeeTickPopupService } from './fee-tick-popup.service';
import { FeeTickService } from './fee-tick.service';

@Component({
    selector: 'jhi-fee-tick-delete-dialog',
    templateUrl: './fee-tick-delete-dialog.component.html'
})
export class FeeTickDeleteDialogComponent {

    fee: FeeTick;

    constructor(
        private feeService: FeeTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.feeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'feeListModification',
                content: 'Deleted an fee'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-fee-tick-delete-popup',
    template: ''
})
export class FeeTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private feePopupService: FeeTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.feePopupService
                .open(FeeTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
