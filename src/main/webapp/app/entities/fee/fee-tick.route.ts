import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { FeeTickComponent } from './fee-tick.component';
import { FeeTickDetailComponent } from './fee-tick-detail.component';
import { FeeTickPopupComponent } from './fee-tick-dialog.component';
import { FeeTickDeletePopupComponent } from './fee-tick-delete-dialog.component';

@Injectable()
export class FeeTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const feeRoute: Routes = [
    {
        path: 'fee-tick',
        component: FeeTickComponent,
        resolve: {
            'pagingParams': FeeTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.fee.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'fee-tick/:id',
        component: FeeTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.fee.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const feePopupRoute: Routes = [
    {
        path: 'fee-tick-new',
        component: FeeTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.fee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'fee-tick/:id/edit',
        component: FeeTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.fee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'fee-tick/:id/delete',
        component: FeeTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.fee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
