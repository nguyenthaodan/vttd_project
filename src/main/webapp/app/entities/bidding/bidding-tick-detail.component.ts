import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { BiddingTick } from './bidding-tick.model';
import { BiddingTickService } from './bidding-tick.service';

@Component({
    selector: 'jhi-bidding-tick-detail',
    templateUrl: './bidding-tick-detail.component.html'
})
export class BiddingTickDetailComponent implements OnInit, OnDestroy {

    bidding: BiddingTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private biddingService: BiddingTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBiddings();
    }

    load(id) {
        this.biddingService.find(id).subscribe((bidding) => {
            this.bidding = bidding;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBiddings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'biddingListModification',
            (response) => this.load(this.bidding.id)
        );
    }
}
