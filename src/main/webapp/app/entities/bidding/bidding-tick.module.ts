import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    BiddingTickService,
    BiddingTickPopupService,
    BiddingTickComponent,
    BiddingTickDetailComponent,
    BiddingTickDialogComponent,
    BiddingTickPopupComponent,
    BiddingTickDeletePopupComponent,
    BiddingTickDeleteDialogComponent,
    biddingRoute,
    biddingPopupRoute,
    BiddingTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...biddingRoute,
    ...biddingPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        BiddingTickComponent,
        BiddingTickDetailComponent,
        BiddingTickDialogComponent,
        BiddingTickDeleteDialogComponent,
        BiddingTickPopupComponent,
        BiddingTickDeletePopupComponent,
    ],
    entryComponents: [
        BiddingTickComponent,
        BiddingTickDialogComponent,
        BiddingTickPopupComponent,
        BiddingTickDeleteDialogComponent,
        BiddingTickDeletePopupComponent,
    ],
    providers: [
        BiddingTickService,
        BiddingTickPopupService,
        BiddingTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickBiddingTickModule {}
