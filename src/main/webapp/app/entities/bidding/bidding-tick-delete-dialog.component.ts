import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BiddingTick } from './bidding-tick.model';
import { BiddingTickPopupService } from './bidding-tick-popup.service';
import { BiddingTickService } from './bidding-tick.service';

@Component({
    selector: 'jhi-bidding-tick-delete-dialog',
    templateUrl: './bidding-tick-delete-dialog.component.html'
})
export class BiddingTickDeleteDialogComponent {

    bidding: BiddingTick;

    constructor(
        private biddingService: BiddingTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.biddingService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'biddingListModification',
                content: 'Deleted an bidding'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-bidding-tick-delete-popup',
    template: ''
})
export class BiddingTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private biddingPopupService: BiddingTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.biddingPopupService
                .open(BiddingTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
