import { BaseEntity } from './../../shared';

export class BiddingTick implements BaseEntity {
    constructor(
        public id?: number,
        public closingDate?: any,
        public biddersCount?: number,
        public jobTemplateId?: number,
        public bids?: BaseEntity[],
    ) {
    }
}
