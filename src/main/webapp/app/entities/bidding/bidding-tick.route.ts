import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BiddingTickComponent } from './bidding-tick.component';
import { BiddingTickDetailComponent } from './bidding-tick-detail.component';
import { BiddingTickPopupComponent } from './bidding-tick-dialog.component';
import { BiddingTickDeletePopupComponent } from './bidding-tick-delete-dialog.component';

@Injectable()
export class BiddingTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const biddingRoute: Routes = [
    {
        path: 'bidding-tick',
        component: BiddingTickComponent,
        resolve: {
            'pagingParams': BiddingTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bidding.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'bidding-tick/:id',
        component: BiddingTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bidding.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const biddingPopupRoute: Routes = [
    {
        path: 'bidding-tick-new',
        component: BiddingTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bidding.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bidding-tick/:id/edit',
        component: BiddingTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bidding.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bidding-tick/:id/delete',
        component: BiddingTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bidding.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
