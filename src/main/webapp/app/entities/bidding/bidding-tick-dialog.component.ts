import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BiddingTick } from './bidding-tick.model';
import { BiddingTickPopupService } from './bidding-tick-popup.service';
import { BiddingTickService } from './bidding-tick.service';
import { JobTemplateTick, JobTemplateTickService } from '../job-template';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-bidding-tick-dialog',
    templateUrl: './bidding-tick-dialog.component.html'
})
export class BiddingTickDialogComponent implements OnInit {

    bidding: BiddingTick;
    isSaving: boolean;

    jobtemplates: JobTemplateTick[];
    closingDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private biddingService: BiddingTickService,
        private jobTemplateService: JobTemplateTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jobTemplateService
            .query({filter: 'bidding-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.bidding.jobTemplateId) {
                    this.jobtemplates = res.json;
                } else {
                    this.jobTemplateService
                        .find(this.bidding.jobTemplateId)
                        .subscribe((subRes: JobTemplateTick) => {
                            this.jobtemplates = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bidding.id !== undefined) {
            this.subscribeToSaveResponse(
                this.biddingService.update(this.bidding));
        } else {
            this.subscribeToSaveResponse(
                this.biddingService.create(this.bidding));
        }
    }

    private subscribeToSaveResponse(result: Observable<BiddingTick>) {
        result.subscribe((res: BiddingTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: BiddingTick) {
        this.eventManager.broadcast({ name: 'biddingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackJobTemplateById(index: number, item: JobTemplateTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-bidding-tick-popup',
    template: ''
})
export class BiddingTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private biddingPopupService: BiddingTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.biddingPopupService
                    .open(BiddingTickDialogComponent as Component, params['id']);
            } else {
                this.biddingPopupService
                    .open(BiddingTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
