import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BiddingTick } from './bidding-tick.model';
import { BiddingTickService } from './bidding-tick.service';

@Injectable()
export class BiddingTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private biddingService: BiddingTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.biddingService.find(id).subscribe((bidding) => {
                    if (bidding.closingDate) {
                        bidding.closingDate = {
                            year: bidding.closingDate.getFullYear(),
                            month: bidding.closingDate.getMonth() + 1,
                            day: bidding.closingDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.biddingModalRef(component, bidding);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.biddingModalRef(component, new BiddingTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    biddingModalRef(component: Component, bidding: BiddingTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bidding = bidding;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
