import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CreditInfoTick } from './credit-info-tick.model';
import { CreditInfoTickPopupService } from './credit-info-tick-popup.service';
import { CreditInfoTickService } from './credit-info-tick.service';

@Component({
    selector: 'jhi-credit-info-tick-delete-dialog',
    templateUrl: './credit-info-tick-delete-dialog.component.html'
})
export class CreditInfoTickDeleteDialogComponent {

    creditInfo: CreditInfoTick;

    constructor(
        private creditInfoService: CreditInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.creditInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'creditInfoListModification',
                content: 'Deleted an creditInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-credit-info-tick-delete-popup',
    template: ''
})
export class CreditInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private creditInfoPopupService: CreditInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.creditInfoPopupService
                .open(CreditInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
