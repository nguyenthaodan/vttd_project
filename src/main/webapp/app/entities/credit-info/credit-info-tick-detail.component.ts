import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { CreditInfoTick } from './credit-info-tick.model';
import { CreditInfoTickService } from './credit-info-tick.service';

@Component({
    selector: 'jhi-credit-info-tick-detail',
    templateUrl: './credit-info-tick-detail.component.html'
})
export class CreditInfoTickDetailComponent implements OnInit, OnDestroy {

    creditInfo: CreditInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private creditInfoService: CreditInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCreditInfos();
    }

    load(id) {
        this.creditInfoService.find(id).subscribe((creditInfo) => {
            this.creditInfo = creditInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCreditInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'creditInfoListModification',
            (response) => this.load(this.creditInfo.id)
        );
    }
}
