import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CreditInfoTick } from './credit-info-tick.model';
import { CreditInfoTickPopupService } from './credit-info-tick-popup.service';
import { CreditInfoTickService } from './credit-info-tick.service';

@Component({
    selector: 'jhi-credit-info-tick-dialog',
    templateUrl: './credit-info-tick-dialog.component.html'
})
export class CreditInfoTickDialogComponent implements OnInit {

    creditInfo: CreditInfoTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private creditInfoService: CreditInfoTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.creditInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.creditInfoService.update(this.creditInfo));
        } else {
            this.subscribeToSaveResponse(
                this.creditInfoService.create(this.creditInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<CreditInfoTick>) {
        result.subscribe((res: CreditInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: CreditInfoTick) {
        this.eventManager.broadcast({ name: 'creditInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-credit-info-tick-popup',
    template: ''
})
export class CreditInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private creditInfoPopupService: CreditInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.creditInfoPopupService
                    .open(CreditInfoTickDialogComponent as Component, params['id']);
            } else {
                this.creditInfoPopupService
                    .open(CreditInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
