import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    CreditInfoTickService,
    CreditInfoTickPopupService,
    CreditInfoTickComponent,
    CreditInfoTickDetailComponent,
    CreditInfoTickDialogComponent,
    CreditInfoTickPopupComponent,
    CreditInfoTickDeletePopupComponent,
    CreditInfoTickDeleteDialogComponent,
    creditInfoRoute,
    creditInfoPopupRoute,
    CreditInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...creditInfoRoute,
    ...creditInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CreditInfoTickComponent,
        CreditInfoTickDetailComponent,
        CreditInfoTickDialogComponent,
        CreditInfoTickDeleteDialogComponent,
        CreditInfoTickPopupComponent,
        CreditInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        CreditInfoTickComponent,
        CreditInfoTickDialogComponent,
        CreditInfoTickPopupComponent,
        CreditInfoTickDeleteDialogComponent,
        CreditInfoTickDeletePopupComponent,
    ],
    providers: [
        CreditInfoTickService,
        CreditInfoTickPopupService,
        CreditInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickCreditInfoTickModule {}
