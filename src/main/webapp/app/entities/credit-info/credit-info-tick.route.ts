import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CreditInfoTickComponent } from './credit-info-tick.component';
import { CreditInfoTickDetailComponent } from './credit-info-tick-detail.component';
import { CreditInfoTickPopupComponent } from './credit-info-tick-dialog.component';
import { CreditInfoTickDeletePopupComponent } from './credit-info-tick-delete-dialog.component';

@Injectable()
export class CreditInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const creditInfoRoute: Routes = [
    {
        path: 'credit-info-tick',
        component: CreditInfoTickComponent,
        resolve: {
            'pagingParams': CreditInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.creditInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'credit-info-tick/:id',
        component: CreditInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.creditInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const creditInfoPopupRoute: Routes = [
    {
        path: 'credit-info-tick-new',
        component: CreditInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.creditInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'credit-info-tick/:id/edit',
        component: CreditInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.creditInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'credit-info-tick/:id/delete',
        component: CreditInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.creditInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
