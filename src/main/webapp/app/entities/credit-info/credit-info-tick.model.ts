import { BaseEntity } from './../../shared';

const enum PaymentMethod {
    'ALL',
    'CASH',
    'BANK_TRANSFER',
    'PAYPAL',
    'GOOGLE_WALLET',
    'BITCOIN',
    'MOBILE',
    'PAYOO',
    'POST'
}

export class CreditInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public credit?: number,
        public method?: PaymentMethod,
    ) {
    }
}
