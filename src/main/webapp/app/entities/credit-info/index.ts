export * from './credit-info-tick.model';
export * from './credit-info-tick-popup.service';
export * from './credit-info-tick.service';
export * from './credit-info-tick-dialog.component';
export * from './credit-info-tick-delete-dialog.component';
export * from './credit-info-tick-detail.component';
export * from './credit-info-tick.component';
export * from './credit-info-tick.route';
