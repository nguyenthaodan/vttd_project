import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { BidTick } from './bid-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BidTickService {

    private resourceUrl = 'api/bids';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(bid: BidTick): Observable<BidTick> {
        const copy = this.convert(bid);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(bid: BidTick): Observable<BidTick> {
        const copy = this.convert(bid);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<BidTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.timeToFinish = this.dateUtils
            .convertLocalDateFromServer(entity.timeToFinish);
        entity.bidTime = this.dateUtils
            .convertLocalDateFromServer(entity.bidTime);
    }

    private convert(bid: BidTick): BidTick {
        const copy: BidTick = Object.assign({}, bid);
        copy.timeToFinish = this.dateUtils
            .convertLocalDateToServer(bid.timeToFinish);
        copy.bidTime = this.dateUtils
            .convertLocalDateToServer(bid.bidTime);
        return copy;
    }
}
