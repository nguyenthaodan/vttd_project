import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { BidTick } from './bid-tick.model';
import { BidTickPopupService } from './bid-tick-popup.service';
import { BidTickService } from './bid-tick.service';
import { BiddingTick, BiddingTickService } from '../bidding';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-bid-tick-dialog',
    templateUrl: './bid-tick-dialog.component.html'
})
export class BidTickDialogComponent implements OnInit {

    bid: BidTick;
    isSaving: boolean;

    biddings: BiddingTick[];

    sysusers: SysUserTick[];
    timeToFinishDp: any;
    bidTimeDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private bidService: BidTickService,
        private biddingService: BiddingTickService,
        private sysUserService: SysUserTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.biddingService.query()
            .subscribe((res: ResponseWrapper) => { this.biddings = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bid.id !== undefined) {
            this.subscribeToSaveResponse(
                this.bidService.update(this.bid));
        } else {
            this.subscribeToSaveResponse(
                this.bidService.create(this.bid));
        }
    }

    private subscribeToSaveResponse(result: Observable<BidTick>) {
        result.subscribe((res: BidTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: BidTick) {
        this.eventManager.broadcast({ name: 'bidListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackBiddingById(index: number, item: BiddingTick) {
        return item.id;
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-bid-tick-popup',
    template: ''
})
export class BidTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bidPopupService: BidTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bidPopupService
                    .open(BidTickDialogComponent as Component, params['id']);
            } else {
                this.bidPopupService
                    .open(BidTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
