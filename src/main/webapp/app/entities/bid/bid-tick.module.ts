import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    BidTickService,
    BidTickPopupService,
    BidTickComponent,
    BidTickDetailComponent,
    BidTickDialogComponent,
    BidTickPopupComponent,
    BidTickDeletePopupComponent,
    BidTickDeleteDialogComponent,
    bidRoute,
    bidPopupRoute,
    BidTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bidRoute,
    ...bidPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        BidTickComponent,
        BidTickDetailComponent,
        BidTickDialogComponent,
        BidTickDeleteDialogComponent,
        BidTickPopupComponent,
        BidTickDeletePopupComponent,
    ],
    entryComponents: [
        BidTickComponent,
        BidTickDialogComponent,
        BidTickPopupComponent,
        BidTickDeleteDialogComponent,
        BidTickDeletePopupComponent,
    ],
    providers: [
        BidTickService,
        BidTickPopupService,
        BidTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickBidTickModule {}
