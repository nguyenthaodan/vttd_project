import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BidTick } from './bid-tick.model';
import { BidTickService } from './bid-tick.service';

@Injectable()
export class BidTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private bidService: BidTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bidService.find(id).subscribe((bid) => {
                    if (bid.timeToFinish) {
                        bid.timeToFinish = {
                            year: bid.timeToFinish.getFullYear(),
                            month: bid.timeToFinish.getMonth() + 1,
                            day: bid.timeToFinish.getDate()
                        };
                    }
                    if (bid.bidTime) {
                        bid.bidTime = {
                            year: bid.bidTime.getFullYear(),
                            month: bid.bidTime.getMonth() + 1,
                            day: bid.bidTime.getDate()
                        };
                    }
                    this.ngbModalRef = this.bidModalRef(component, bid);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.bidModalRef(component, new BidTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bidModalRef(component: Component, bid: BidTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bid = bid;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
