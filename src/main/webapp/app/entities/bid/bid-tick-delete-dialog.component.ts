import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { BidTick } from './bid-tick.model';
import { BidTickPopupService } from './bid-tick-popup.service';
import { BidTickService } from './bid-tick.service';

@Component({
    selector: 'jhi-bid-tick-delete-dialog',
    templateUrl: './bid-tick-delete-dialog.component.html'
})
export class BidTickDeleteDialogComponent {

    bid: BidTick;

    constructor(
        private bidService: BidTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.bidService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'bidListModification',
                content: 'Deleted an bid'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-bid-tick-delete-popup',
    template: ''
})
export class BidTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bidPopupService: BidTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.bidPopupService
                .open(BidTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
