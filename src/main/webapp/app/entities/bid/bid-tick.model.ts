import { BaseEntity } from './../../shared';

export class BidTick implements BaseEntity {
    constructor(
        public id?: number,
        public price?: number,
        public timeToFinish?: any,
        public bidTime?: any,
        public message?: string,
        public biddingId?: number,
        public sysUserId?: number,
    ) {
    }
}
