import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BidTickComponent } from './bid-tick.component';
import { BidTickDetailComponent } from './bid-tick-detail.component';
import { BidTickPopupComponent } from './bid-tick-dialog.component';
import { BidTickDeletePopupComponent } from './bid-tick-delete-dialog.component';

@Injectable()
export class BidTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bidRoute: Routes = [
    {
        path: 'bid-tick',
        component: BidTickComponent,
        resolve: {
            'pagingParams': BidTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bid.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'bid-tick/:id',
        component: BidTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bid.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bidPopupRoute: Routes = [
    {
        path: 'bid-tick-new',
        component: BidTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bid.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bid-tick/:id/edit',
        component: BidTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bid.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bid-tick/:id/delete',
        component: BidTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.bid.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
