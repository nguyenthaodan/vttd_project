import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { BidTick } from './bid-tick.model';
import { BidTickService } from './bid-tick.service';

@Component({
    selector: 'jhi-bid-tick-detail',
    templateUrl: './bid-tick-detail.component.html'
})
export class BidTickDetailComponent implements OnInit, OnDestroy {

    bid: BidTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private bidService: BidTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBids();
    }

    load(id) {
        this.bidService.find(id).subscribe((bid) => {
            this.bid = bid;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBids() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bidListModification',
            (response) => this.load(this.bid.id)
        );
    }
}
