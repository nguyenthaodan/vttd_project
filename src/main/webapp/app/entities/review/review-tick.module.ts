import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    ReviewTickService,
    ReviewTickPopupService,
    ReviewTickComponent,
    ReviewTickDetailComponent,
    ReviewTickDialogComponent,
    ReviewTickPopupComponent,
    ReviewTickDeletePopupComponent,
    ReviewTickDeleteDialogComponent,
    reviewRoute,
    reviewPopupRoute,
    ReviewTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...reviewRoute,
    ...reviewPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ReviewTickComponent,
        ReviewTickDetailComponent,
        ReviewTickDialogComponent,
        ReviewTickDeleteDialogComponent,
        ReviewTickPopupComponent,
        ReviewTickDeletePopupComponent,
    ],
    entryComponents: [
        ReviewTickComponent,
        ReviewTickDialogComponent,
        ReviewTickPopupComponent,
        ReviewTickDeleteDialogComponent,
        ReviewTickDeletePopupComponent,
    ],
    providers: [
        ReviewTickService,
        ReviewTickPopupService,
        ReviewTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickReviewTickModule {}
