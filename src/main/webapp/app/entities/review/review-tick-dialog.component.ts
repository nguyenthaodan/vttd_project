import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReviewTick } from './review-tick.model';
import { ReviewTickPopupService } from './review-tick-popup.service';
import { ReviewTickService } from './review-tick.service';
import { CompletedTick, CompletedTickService } from '../completed';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { BusinessTick, BusinessTickService } from '../business';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-review-tick-dialog',
    templateUrl: './review-tick-dialog.component.html'
})
export class ReviewTickDialogComponent implements OnInit {

    review: ReviewTick;
    isSaving: boolean;

    completeds: CompletedTick[];

    sysusers: SysUserTick[];

    businesses: BusinessTick[];
    reviewDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private reviewService: ReviewTickService,
        private completedService: CompletedTickService,
        private sysUserService: SysUserTickService,
        private businessService: BusinessTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.completedService
            .query({filter: 'review-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.review.completedId) {
                    this.completeds = res.json;
                } else {
                    this.completedService
                        .find(this.review.completedId)
                        .subscribe((subRes: CompletedTick) => {
                            this.completeds = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.businessService.query()
            .subscribe((res: ResponseWrapper) => { this.businesses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.review.id !== undefined) {
            this.subscribeToSaveResponse(
                this.reviewService.update(this.review));
        } else {
            this.subscribeToSaveResponse(
                this.reviewService.create(this.review));
        }
    }

    private subscribeToSaveResponse(result: Observable<ReviewTick>) {
        result.subscribe((res: ReviewTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ReviewTick) {
        this.eventManager.broadcast({ name: 'reviewListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackCompletedById(index: number, item: CompletedTick) {
        return item.id;
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }

    trackBusinessById(index: number, item: BusinessTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-review-tick-popup',
    template: ''
})
export class ReviewTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private reviewPopupService: ReviewTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.reviewPopupService
                    .open(ReviewTickDialogComponent as Component, params['id']);
            } else {
                this.reviewPopupService
                    .open(ReviewTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
