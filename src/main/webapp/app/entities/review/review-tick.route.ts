import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReviewTickComponent } from './review-tick.component';
import { ReviewTickDetailComponent } from './review-tick-detail.component';
import { ReviewTickPopupComponent } from './review-tick-dialog.component';
import { ReviewTickDeletePopupComponent } from './review-tick-delete-dialog.component';

@Injectable()
export class ReviewTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const reviewRoute: Routes = [
    {
        path: 'review-tick',
        component: ReviewTickComponent,
        resolve: {
            'pagingParams': ReviewTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.review.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'review-tick/:id',
        component: ReviewTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.review.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const reviewPopupRoute: Routes = [
    {
        path: 'review-tick-new',
        component: ReviewTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.review.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'review-tick/:id/edit',
        component: ReviewTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.review.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'review-tick/:id/delete',
        component: ReviewTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.review.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
