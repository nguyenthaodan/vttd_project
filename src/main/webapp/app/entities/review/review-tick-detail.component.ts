import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { ReviewTick } from './review-tick.model';
import { ReviewTickService } from './review-tick.service';

@Component({
    selector: 'jhi-review-tick-detail',
    templateUrl: './review-tick-detail.component.html'
})
export class ReviewTickDetailComponent implements OnInit, OnDestroy {

    review: ReviewTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private reviewService: ReviewTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInReviews();
    }

    load(id) {
        this.reviewService.find(id).subscribe((review) => {
            this.review = review;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInReviews() {
        this.eventSubscriber = this.eventManager.subscribe(
            'reviewListModification',
            (response) => this.load(this.review.id)
        );
    }
}
