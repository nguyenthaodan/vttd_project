import { BaseEntity } from './../../shared';

const enum Rating {
    'ONE',
    'TWO',
    'THREE',
    'FOUR',
    'FIVE'
}

export class ReviewTick implements BaseEntity {
    constructor(
        public id?: number,
        public rating?: Rating,
        public reviewText?: string,
        public reviewDate?: any,
        public completedId?: number,
        public sysUserId?: number,
        public businessId?: number,
    ) {
    }
}
