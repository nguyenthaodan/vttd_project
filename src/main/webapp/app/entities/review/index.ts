export * from './review-tick.model';
export * from './review-tick-popup.service';
export * from './review-tick.service';
export * from './review-tick-dialog.component';
export * from './review-tick-delete-dialog.component';
export * from './review-tick-detail.component';
export * from './review-tick.component';
export * from './review-tick.route';
