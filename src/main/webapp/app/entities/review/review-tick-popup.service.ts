import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ReviewTick } from './review-tick.model';
import { ReviewTickService } from './review-tick.service';

@Injectable()
export class ReviewTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private reviewService: ReviewTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.reviewService.find(id).subscribe((review) => {
                    if (review.reviewDate) {
                        review.reviewDate = {
                            year: review.reviewDate.getFullYear(),
                            month: review.reviewDate.getMonth() + 1,
                            day: review.reviewDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.reviewModalRef(component, review);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.reviewModalRef(component, new ReviewTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    reviewModalRef(component: Component, review: ReviewTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.review = review;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
