import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PromotionTick } from './promotion-tick.model';
import { PromotionTickService } from './promotion-tick.service';

@Component({
    selector: 'jhi-promotion-tick-detail',
    templateUrl: './promotion-tick-detail.component.html'
})
export class PromotionTickDetailComponent implements OnInit, OnDestroy {

    promotion: PromotionTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private promotionService: PromotionTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPromotions();
    }

    load(id) {
        this.promotionService.find(id).subscribe((promotion) => {
            this.promotion = promotion;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPromotions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'promotionListModification',
            (response) => this.load(this.promotion.id)
        );
    }
}
