import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PromotionTickComponent } from './promotion-tick.component';
import { PromotionTickDetailComponent } from './promotion-tick-detail.component';
import { PromotionTickPopupComponent } from './promotion-tick-dialog.component';
import { PromotionTickDeletePopupComponent } from './promotion-tick-delete-dialog.component';

@Injectable()
export class PromotionTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const promotionRoute: Routes = [
    {
        path: 'promotion-tick',
        component: PromotionTickComponent,
        resolve: {
            'pagingParams': PromotionTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.promotion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'promotion-tick/:id',
        component: PromotionTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.promotion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const promotionPopupRoute: Routes = [
    {
        path: 'promotion-tick-new',
        component: PromotionTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.promotion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'promotion-tick/:id/edit',
        component: PromotionTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.promotion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'promotion-tick/:id/delete',
        component: PromotionTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.promotion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
