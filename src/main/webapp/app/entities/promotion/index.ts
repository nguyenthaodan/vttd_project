export * from './promotion-tick.model';
export * from './promotion-tick-popup.service';
export * from './promotion-tick.service';
export * from './promotion-tick-dialog.component';
export * from './promotion-tick-delete-dialog.component';
export * from './promotion-tick-detail.component';
export * from './promotion-tick.component';
export * from './promotion-tick.route';
