import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PromotionTick } from './promotion-tick.model';
import { PromotionTickPopupService } from './promotion-tick-popup.service';
import { PromotionTickService } from './promotion-tick.service';

@Component({
    selector: 'jhi-promotion-tick-delete-dialog',
    templateUrl: './promotion-tick-delete-dialog.component.html'
})
export class PromotionTickDeleteDialogComponent {

    promotion: PromotionTick;

    constructor(
        private promotionService: PromotionTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.promotionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'promotionListModification',
                content: 'Deleted an promotion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-promotion-tick-delete-popup',
    template: ''
})
export class PromotionTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private promotionPopupService: PromotionTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.promotionPopupService
                .open(PromotionTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
