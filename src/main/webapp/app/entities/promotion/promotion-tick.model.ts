import { BaseEntity } from './../../shared';

const enum PromotionType {
    'FIXED',
    'PERCENTAGE'
}

const enum UserClass {
    'NEW',
    'BRONZE',
    'SILVER',
    'GOLD',
    'DIAMOND'
}

export class PromotionTick implements BaseEntity {
    constructor(
        public id?: number,
        public startDate?: any,
        public endDate?: any,
        public rate?: number,
        public type?: PromotionType,
        public appliedTo?: UserClass,
        public description?: string,
    ) {
    }
}
