import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    PromotionTickService,
    PromotionTickPopupService,
    PromotionTickComponent,
    PromotionTickDetailComponent,
    PromotionTickDialogComponent,
    PromotionTickPopupComponent,
    PromotionTickDeletePopupComponent,
    PromotionTickDeleteDialogComponent,
    promotionRoute,
    promotionPopupRoute,
    PromotionTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...promotionRoute,
    ...promotionPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PromotionTickComponent,
        PromotionTickDetailComponent,
        PromotionTickDialogComponent,
        PromotionTickDeleteDialogComponent,
        PromotionTickPopupComponent,
        PromotionTickDeletePopupComponent,
    ],
    entryComponents: [
        PromotionTickComponent,
        PromotionTickDialogComponent,
        PromotionTickPopupComponent,
        PromotionTickDeleteDialogComponent,
        PromotionTickDeletePopupComponent,
    ],
    providers: [
        PromotionTickService,
        PromotionTickPopupService,
        PromotionTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickPromotionTickModule {}
