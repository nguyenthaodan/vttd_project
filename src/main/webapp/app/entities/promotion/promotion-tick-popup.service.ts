import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PromotionTick } from './promotion-tick.model';
import { PromotionTickService } from './promotion-tick.service';

@Injectable()
export class PromotionTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private promotionService: PromotionTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.promotionService.find(id).subscribe((promotion) => {
                    if (promotion.startDate) {
                        promotion.startDate = {
                            year: promotion.startDate.getFullYear(),
                            month: promotion.startDate.getMonth() + 1,
                            day: promotion.startDate.getDate()
                        };
                    }
                    if (promotion.endDate) {
                        promotion.endDate = {
                            year: promotion.endDate.getFullYear(),
                            month: promotion.endDate.getMonth() + 1,
                            day: promotion.endDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.promotionModalRef(component, promotion);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.promotionModalRef(component, new PromotionTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    promotionModalRef(component: Component, promotion: PromotionTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.promotion = promotion;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
