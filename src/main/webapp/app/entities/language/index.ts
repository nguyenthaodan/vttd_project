export * from './language-tick.model';
export * from './language-tick-popup.service';
export * from './language-tick.service';
export * from './language-tick-dialog.component';
export * from './language-tick-delete-dialog.component';
export * from './language-tick-detail.component';
export * from './language-tick.component';
export * from './language-tick.route';
