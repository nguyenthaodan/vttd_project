import { BaseEntity } from './../../shared';

export class LanguageTick implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public abre?: string,
        public icon?: string,
        public sysUsers?: BaseEntity[],
    ) {
    }
}
