import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LanguageTickComponent } from './language-tick.component';
import { LanguageTickDetailComponent } from './language-tick-detail.component';
import { LanguageTickPopupComponent } from './language-tick-dialog.component';
import { LanguageTickDeletePopupComponent } from './language-tick-delete-dialog.component';

@Injectable()
export class LanguageTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const languageRoute: Routes = [
    {
        path: 'language-tick',
        component: LanguageTickComponent,
        resolve: {
            'pagingParams': LanguageTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.language.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'language-tick/:id',
        component: LanguageTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.language.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const languagePopupRoute: Routes = [
    {
        path: 'language-tick-new',
        component: LanguageTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.language.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'language-tick/:id/edit',
        component: LanguageTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.language.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'language-tick/:id/delete',
        component: LanguageTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.language.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
