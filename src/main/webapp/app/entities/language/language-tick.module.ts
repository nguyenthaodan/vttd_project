import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    LanguageTickService,
    LanguageTickPopupService,
    LanguageTickComponent,
    LanguageTickDetailComponent,
    LanguageTickDialogComponent,
    LanguageTickPopupComponent,
    LanguageTickDeletePopupComponent,
    LanguageTickDeleteDialogComponent,
    languageRoute,
    languagePopupRoute,
    LanguageTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...languageRoute,
    ...languagePopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LanguageTickComponent,
        LanguageTickDetailComponent,
        LanguageTickDialogComponent,
        LanguageTickDeleteDialogComponent,
        LanguageTickPopupComponent,
        LanguageTickDeletePopupComponent,
    ],
    entryComponents: [
        LanguageTickComponent,
        LanguageTickDialogComponent,
        LanguageTickPopupComponent,
        LanguageTickDeleteDialogComponent,
        LanguageTickDeletePopupComponent,
    ],
    providers: [
        LanguageTickService,
        LanguageTickPopupService,
        LanguageTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickLanguageTickModule {}
