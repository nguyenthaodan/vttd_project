import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SysUserTick } from './sys-user-tick.model';
import { SysUserTickPopupService } from './sys-user-tick-popup.service';
import { SysUserTickService } from './sys-user-tick.service';
import { CreditInfoTick, CreditInfoTickService } from '../credit-info';
import { ChatInfoTick, ChatInfoTickService } from '../chat-info';
import { PushInfoTick, PushInfoTickService } from '../push-info';
import { ReviewInfoTick, ReviewInfoTickService } from '../review-info';
import { JobInfoTick, JobInfoTickService } from '../job-info';
import { LoginInfoTick, LoginInfoTickService } from '../login-info';
import { LanguageTick, LanguageTickService } from '../language';
import { LocationTick, LocationTickService } from '../location';
import { CanceledTick, CanceledTickService } from '../canceled';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sys-user-tick-dialog',
    templateUrl: './sys-user-tick-dialog.component.html'
})
export class SysUserTickDialogComponent implements OnInit {

    sysUser: SysUserTick;
    isSaving: boolean;

    creditinfos: CreditInfoTick[];

    chatinfos: ChatInfoTick[];

    pushinfos: PushInfoTick[];

    reviewinfos: ReviewInfoTick[];

    jobinfos: JobInfoTick[];

    languages: LanguageTick[];

    locations: LocationTick[];

    canceleds: CanceledTick[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private sysUserService: SysUserTickService,
        private creditInfoService: CreditInfoTickService,
        private chatInfoService: ChatInfoTickService,
        private pushInfoService: PushInfoTickService,
        private reviewInfoService: ReviewInfoTickService,
        private jobInfoService: JobInfoTickService,
        private loginInfoService: LoginInfoTickService,
        private languageService: LanguageTickService,
        private locationService: LocationTickService,
        private canceledService: CanceledTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.creditInfoService
            .query({filter: 'sysuser-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.sysUser.creditInfoId) {
                    this.creditinfos = res.json;
                } else {
                    this.creditInfoService
                        .find(this.sysUser.creditInfoId)
                        .subscribe((subRes: CreditInfoTick) => {
                            this.creditinfos = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.chatInfoService
            .query({filter: 'sysuser-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.sysUser.chatInfoId) {
                    this.chatinfos = res.json;
                } else {
                    this.chatInfoService
                        .find(this.sysUser.chatInfoId)
                        .subscribe((subRes: ChatInfoTick) => {
                            this.chatinfos = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.pushInfoService
            .query({filter: 'sysuser-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.sysUser.pushInfoId) {
                    this.pushinfos = res.json;
                } else {
                    this.pushInfoService
                        .find(this.sysUser.pushInfoId)
                        .subscribe((subRes: PushInfoTick) => {
                            this.pushinfos = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.reviewInfoService
            .query({filter: 'sysuser-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.sysUser.reviewInfoId) {
                    this.reviewinfos = res.json;
                } else {
                    this.reviewInfoService
                        .find(this.sysUser.reviewInfoId)
                        .subscribe((subRes: ReviewInfoTick) => {
                            this.reviewinfos = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.jobInfoService
            .query({filter: 'sysuser-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.sysUser.jobInfoId) {
                    this.jobinfos = res.json;
                } else {
                    this.jobInfoService
                        .find(this.sysUser.jobInfoId)
                        .subscribe((subRes: JobInfoTick) => {
                            this.jobinfos = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.languageService.query()
            .subscribe((res: ResponseWrapper) => { this.languages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.locationService.query()
            .subscribe((res: ResponseWrapper) => { this.locations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.canceledService.query()
            .subscribe((res: ResponseWrapper) => { this.canceleds = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sysUser.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sysUserService.update(this.sysUser));
        } else {
            this.subscribeToSaveResponse(
                this.sysUserService.create(this.sysUser));
        }
    }

    private subscribeToSaveResponse(result: Observable<SysUserTick>) {
        result.subscribe((res: SysUserTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: SysUserTick) {
        this.eventManager.broadcast({ name: 'sysUserListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackCreditInfoById(index: number, item: CreditInfoTick) {
        return item.id;
    }

    trackChatInfoById(index: number, item: ChatInfoTick) {
        return item.id;
    }

    trackPushInfoById(index: number, item: PushInfoTick) {
        return item.id;
    }

    trackReviewInfoById(index: number, item: ReviewInfoTick) {
        return item.id;
    }

    trackJobInfoById(index: number, item: JobInfoTick) {
        return item.id;
    }

    trackLoginInfoById(index: number, item: LoginInfoTick) {
        return item.id;
    }

    trackLanguageById(index: number, item: LanguageTick) {
        return item.id;
    }

    trackLocationById(index: number, item: LocationTick) {
        return item.id;
    }

    trackCanceledById(index: number, item: CanceledTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-sys-user-tick-popup',
    template: ''
})
export class SysUserTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sysUserPopupService: SysUserTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sysUserPopupService
                    .open(SysUserTickDialogComponent as Component, params['id']);
            } else {
                this.sysUserPopupService
                    .open(SysUserTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
