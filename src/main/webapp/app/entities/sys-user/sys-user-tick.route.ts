import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SysUserTickComponent } from './sys-user-tick.component';
import { SysUserTickDetailComponent } from './sys-user-tick-detail.component';
import { SysUserTickPopupComponent } from './sys-user-tick-dialog.component';
import { SysUserTickDeletePopupComponent } from './sys-user-tick-delete-dialog.component';

@Injectable()
export class SysUserTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const sysUserRoute: Routes = [
    {
        path: 'sys-user-tick',
        component: SysUserTickComponent,
        resolve: {
            'pagingParams': SysUserTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.sysUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sys-user-tick/:id',
        component: SysUserTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.sysUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sysUserPopupRoute: Routes = [
    {
        path: 'sys-user-tick-new',
        component: SysUserTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.sysUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sys-user-tick/:id/edit',
        component: SysUserTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.sysUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sys-user-tick/:id/delete',
        component: SysUserTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.sysUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
