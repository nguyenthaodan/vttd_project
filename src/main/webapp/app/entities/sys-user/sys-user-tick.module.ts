import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    SysUserTickService,
    SysUserTickPopupService,
    SysUserTickComponent,
    SysUserTickDetailComponent,
    SysUserTickDialogComponent,
    SysUserTickPopupComponent,
    SysUserTickDeletePopupComponent,
    SysUserTickDeleteDialogComponent,
    sysUserRoute,
    sysUserPopupRoute,
    SysUserTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...sysUserRoute,
    ...sysUserPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        SysUserTickComponent,
        SysUserTickDetailComponent,
        SysUserTickDialogComponent,
        SysUserTickDeleteDialogComponent,
        SysUserTickPopupComponent,
        SysUserTickDeletePopupComponent,
    ],
    entryComponents: [
        SysUserTickComponent,
        SysUserTickDialogComponent,
        SysUserTickPopupComponent,
        SysUserTickDeleteDialogComponent,
        SysUserTickDeletePopupComponent,
    ],
    providers: [
        SysUserTickService,
        SysUserTickPopupService,
        SysUserTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickSysUserTickModule {}
