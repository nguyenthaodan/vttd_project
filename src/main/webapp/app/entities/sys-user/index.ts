export * from './sys-user-tick.model';
export * from './sys-user-tick-popup.service';
export * from './sys-user-tick.service';
export * from './sys-user-tick-dialog.component';
export * from './sys-user-tick-delete-dialog.component';
export * from './sys-user-tick-detail.component';
export * from './sys-user-tick.component';
export * from './sys-user-tick.route';
