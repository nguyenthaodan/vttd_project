import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SysUserTick } from './sys-user-tick.model';
import { SysUserTickPopupService } from './sys-user-tick-popup.service';
import { SysUserTickService } from './sys-user-tick.service';

@Component({
    selector: 'jhi-sys-user-tick-delete-dialog',
    templateUrl: './sys-user-tick-delete-dialog.component.html'
})
export class SysUserTickDeleteDialogComponent {

    sysUser: SysUserTick;

    constructor(
        private sysUserService: SysUserTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sysUserService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'sysUserListModification',
                content: 'Deleted an sysUser'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sys-user-tick-delete-popup',
    template: ''
})
export class SysUserTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sysUserPopupService: SysUserTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.sysUserPopupService
                .open(SysUserTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
