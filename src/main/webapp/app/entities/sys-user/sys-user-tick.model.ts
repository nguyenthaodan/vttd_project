import { BaseEntity } from './../../shared';

const enum UserRole {
    'ADMIN',
    'PROVIDER',
    'USER',
    'SALES',
    'GUEST'
}

const enum UserType {
    'NEW',
    'BRONZE',
    'SILVER',
    'GOLD',
    'DIAMOND'
}

const enum UserSecurity {
    'VERIFIED',
    'PENDING',
    'BLOCKED',
    'INACTIVE'
}

export class SysUserTick implements BaseEntity {
    constructor(
        public id?: number,
        public role?: UserRole,
        public type?: UserType,
        public security?: UserSecurity,
        public points?: number,
        public creditInfoId?: number,
        public chatInfoId?: number,
        public pushInfoId?: number,
        public reviewInfoId?: number,
        public jobInfoId?: number,
        public Id?: number,
        public events?: BaseEntity[],
        public eventInfos?: BaseEntity[],
        public businesses?: BaseEntity[],
        public workings?: BaseEntity[],
        public languageId?: number,
        public locationId?: number,
        public jobTemplates?: BaseEntity[],
        public reviews?: BaseEntity[],
        public canceledId?: number,
    ) {
    }
}
