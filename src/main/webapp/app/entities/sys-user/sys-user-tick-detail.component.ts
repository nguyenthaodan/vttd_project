import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { SysUserTick } from './sys-user-tick.model';
import { SysUserTickService } from './sys-user-tick.service';

@Component({
    selector: 'jhi-sys-user-tick-detail',
    templateUrl: './sys-user-tick-detail.component.html'
})
export class SysUserTickDetailComponent implements OnInit, OnDestroy {

    sysUser: SysUserTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private sysUserService: SysUserTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSysUsers();
    }

    load(id) {
        this.sysUserService.find(id).subscribe((sysUser) => {
            this.sysUser = sysUser;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSysUsers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'sysUserListModification',
            (response) => this.load(this.sysUser.id)
        );
    }
}
