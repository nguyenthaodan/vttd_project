import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { CompletedTick } from './completed-tick.model';
import { CompletedTickService } from './completed-tick.service';

@Component({
    selector: 'jhi-completed-tick-detail',
    templateUrl: './completed-tick-detail.component.html'
})
export class CompletedTickDetailComponent implements OnInit, OnDestroy {

    completed: CompletedTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private completedService: CompletedTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCompleteds();
    }

    load(id) {
        this.completedService.find(id).subscribe((completed) => {
            this.completed = completed;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCompleteds() {
        this.eventSubscriber = this.eventManager.subscribe(
            'completedListModification',
            (response) => this.load(this.completed.id)
        );
    }
}
