import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CompletedTickComponent } from './completed-tick.component';
import { CompletedTickDetailComponent } from './completed-tick-detail.component';
import { CompletedTickPopupComponent } from './completed-tick-dialog.component';
import { CompletedTickDeletePopupComponent } from './completed-tick-delete-dialog.component';

@Injectable()
export class CompletedTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const completedRoute: Routes = [
    {
        path: 'completed-tick',
        component: CompletedTickComponent,
        resolve: {
            'pagingParams': CompletedTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.completed.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'completed-tick/:id',
        component: CompletedTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.completed.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const completedPopupRoute: Routes = [
    {
        path: 'completed-tick-new',
        component: CompletedTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.completed.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'completed-tick/:id/edit',
        component: CompletedTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.completed.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'completed-tick/:id/delete',
        component: CompletedTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.completed.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
