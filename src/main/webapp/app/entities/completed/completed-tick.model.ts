import { BaseEntity } from './../../shared';

const enum CompleteStatus {
    'WAITING_FOR_FINAL_PAYMENT',
    'PAID'
}

export class CompletedTick implements BaseEntity {
    constructor(
        public id?: number,
        public status?: CompleteStatus,
        public completeDate?: any,
        public reviewed?: boolean,
        public rating?: number,
        public jobTemplateId?: number,
        public userInvoiceId?: number,
        public providerInvoiceId?: number,
        public reviewId?: number,
    ) {
        this.reviewed = false;
    }
}
