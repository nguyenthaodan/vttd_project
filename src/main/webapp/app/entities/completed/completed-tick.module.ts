import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    CompletedTickService,
    CompletedTickPopupService,
    CompletedTickComponent,
    CompletedTickDetailComponent,
    CompletedTickDialogComponent,
    CompletedTickPopupComponent,
    CompletedTickDeletePopupComponent,
    CompletedTickDeleteDialogComponent,
    completedRoute,
    completedPopupRoute,
    CompletedTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...completedRoute,
    ...completedPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CompletedTickComponent,
        CompletedTickDetailComponent,
        CompletedTickDialogComponent,
        CompletedTickDeleteDialogComponent,
        CompletedTickPopupComponent,
        CompletedTickDeletePopupComponent,
    ],
    entryComponents: [
        CompletedTickComponent,
        CompletedTickDialogComponent,
        CompletedTickPopupComponent,
        CompletedTickDeleteDialogComponent,
        CompletedTickDeletePopupComponent,
    ],
    providers: [
        CompletedTickService,
        CompletedTickPopupService,
        CompletedTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickCompletedTickModule {}
