import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CompletedTick } from './completed-tick.model';
import { CompletedTickPopupService } from './completed-tick-popup.service';
import { CompletedTickService } from './completed-tick.service';
import { JobTemplateTick, JobTemplateTickService } from '../job-template';
import { InvoiceTick, InvoiceTickService } from '../invoice';
import { ReviewTick, ReviewTickService } from '../review';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-completed-tick-dialog',
    templateUrl: './completed-tick-dialog.component.html'
})
export class CompletedTickDialogComponent implements OnInit {

    completed: CompletedTick;
    isSaving: boolean;

    jobtemplates: JobTemplateTick[];

    userinvoices: InvoiceTick[];

    providerinvoices: InvoiceTick[];

    reviews: ReviewTick[];
    completeDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private completedService: CompletedTickService,
        private jobTemplateService: JobTemplateTickService,
        private invoiceService: InvoiceTickService,
        private reviewService: ReviewTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jobTemplateService
            .query({filter: 'completed-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.completed.jobTemplateId) {
                    this.jobtemplates = res.json;
                } else {
                    this.jobTemplateService
                        .find(this.completed.jobTemplateId)
                        .subscribe((subRes: JobTemplateTick) => {
                            this.jobtemplates = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.invoiceService
            .query({filter: 'completed-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.completed.userInvoiceId) {
                    this.userinvoices = res.json;
                } else {
                    this.invoiceService
                        .find(this.completed.userInvoiceId)
                        .subscribe((subRes: InvoiceTick) => {
                            this.userinvoices = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.invoiceService
            .query({filter: 'completed-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.completed.providerInvoiceId) {
                    this.providerinvoices = res.json;
                } else {
                    this.invoiceService
                        .find(this.completed.providerInvoiceId)
                        .subscribe((subRes: InvoiceTick) => {
                            this.providerinvoices = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.reviewService.query()
            .subscribe((res: ResponseWrapper) => { this.reviews = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.completed.id !== undefined) {
            this.subscribeToSaveResponse(
                this.completedService.update(this.completed));
        } else {
            this.subscribeToSaveResponse(
                this.completedService.create(this.completed));
        }
    }

    private subscribeToSaveResponse(result: Observable<CompletedTick>) {
        result.subscribe((res: CompletedTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: CompletedTick) {
        this.eventManager.broadcast({ name: 'completedListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackJobTemplateById(index: number, item: JobTemplateTick) {
        return item.id;
    }

    trackInvoiceById(index: number, item: InvoiceTick) {
        return item.id;
    }

    trackReviewById(index: number, item: ReviewTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-completed-tick-popup',
    template: ''
})
export class CompletedTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private completedPopupService: CompletedTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.completedPopupService
                    .open(CompletedTickDialogComponent as Component, params['id']);
            } else {
                this.completedPopupService
                    .open(CompletedTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
