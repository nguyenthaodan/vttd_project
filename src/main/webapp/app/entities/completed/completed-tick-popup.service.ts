import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CompletedTick } from './completed-tick.model';
import { CompletedTickService } from './completed-tick.service';

@Injectable()
export class CompletedTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private completedService: CompletedTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.completedService.find(id).subscribe((completed) => {
                    if (completed.completeDate) {
                        completed.completeDate = {
                            year: completed.completeDate.getFullYear(),
                            month: completed.completeDate.getMonth() + 1,
                            day: completed.completeDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.completedModalRef(component, completed);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.completedModalRef(component, new CompletedTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    completedModalRef(component: Component, completed: CompletedTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.completed = completed;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
