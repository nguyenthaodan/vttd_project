export * from './completed-tick.model';
export * from './completed-tick-popup.service';
export * from './completed-tick.service';
export * from './completed-tick-dialog.component';
export * from './completed-tick-delete-dialog.component';
export * from './completed-tick-detail.component';
export * from './completed-tick.component';
export * from './completed-tick.route';
