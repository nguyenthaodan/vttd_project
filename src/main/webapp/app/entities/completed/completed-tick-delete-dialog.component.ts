import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CompletedTick } from './completed-tick.model';
import { CompletedTickPopupService } from './completed-tick-popup.service';
import { CompletedTickService } from './completed-tick.service';

@Component({
    selector: 'jhi-completed-tick-delete-dialog',
    templateUrl: './completed-tick-delete-dialog.component.html'
})
export class CompletedTickDeleteDialogComponent {

    completed: CompletedTick;

    constructor(
        private completedService: CompletedTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.completedService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'completedListModification',
                content: 'Deleted an completed'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-completed-tick-delete-popup',
    template: ''
})
export class CompletedTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private completedPopupService: CompletedTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.completedPopupService
                .open(CompletedTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
