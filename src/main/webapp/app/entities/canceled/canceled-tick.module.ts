import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    CanceledTickService,
    CanceledTickPopupService,
    CanceledTickComponent,
    CanceledTickDetailComponent,
    CanceledTickDialogComponent,
    CanceledTickPopupComponent,
    CanceledTickDeletePopupComponent,
    CanceledTickDeleteDialogComponent,
    canceledRoute,
    canceledPopupRoute,
    CanceledTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...canceledRoute,
    ...canceledPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CanceledTickComponent,
        CanceledTickDetailComponent,
        CanceledTickDialogComponent,
        CanceledTickDeleteDialogComponent,
        CanceledTickPopupComponent,
        CanceledTickDeletePopupComponent,
    ],
    entryComponents: [
        CanceledTickComponent,
        CanceledTickDialogComponent,
        CanceledTickPopupComponent,
        CanceledTickDeleteDialogComponent,
        CanceledTickDeletePopupComponent,
    ],
    providers: [
        CanceledTickService,
        CanceledTickPopupService,
        CanceledTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickCanceledTickModule {}
