import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CanceledTickComponent } from './canceled-tick.component';
import { CanceledTickDetailComponent } from './canceled-tick-detail.component';
import { CanceledTickPopupComponent } from './canceled-tick-dialog.component';
import { CanceledTickDeletePopupComponent } from './canceled-tick-delete-dialog.component';

@Injectable()
export class CanceledTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const canceledRoute: Routes = [
    {
        path: 'canceled-tick',
        component: CanceledTickComponent,
        resolve: {
            'pagingParams': CanceledTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.canceled.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'canceled-tick/:id',
        component: CanceledTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.canceled.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const canceledPopupRoute: Routes = [
    {
        path: 'canceled-tick-new',
        component: CanceledTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.canceled.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'canceled-tick/:id/edit',
        component: CanceledTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.canceled.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'canceled-tick/:id/delete',
        component: CanceledTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.canceled.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
