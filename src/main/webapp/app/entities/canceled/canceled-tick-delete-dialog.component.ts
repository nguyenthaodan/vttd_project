import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CanceledTick } from './canceled-tick.model';
import { CanceledTickPopupService } from './canceled-tick-popup.service';
import { CanceledTickService } from './canceled-tick.service';

@Component({
    selector: 'jhi-canceled-tick-delete-dialog',
    templateUrl: './canceled-tick-delete-dialog.component.html'
})
export class CanceledTickDeleteDialogComponent {

    canceled: CanceledTick;

    constructor(
        private canceledService: CanceledTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.canceledService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'canceledListModification',
                content: 'Deleted an canceled'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-canceled-tick-delete-popup',
    template: ''
})
export class CanceledTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private canceledPopupService: CanceledTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.canceledPopupService
                .open(CanceledTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
