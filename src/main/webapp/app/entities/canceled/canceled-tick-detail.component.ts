import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { CanceledTick } from './canceled-tick.model';
import { CanceledTickService } from './canceled-tick.service';

@Component({
    selector: 'jhi-canceled-tick-detail',
    templateUrl: './canceled-tick-detail.component.html'
})
export class CanceledTickDetailComponent implements OnInit, OnDestroy {

    canceled: CanceledTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private canceledService: CanceledTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCanceleds();
    }

    load(id) {
        this.canceledService.find(id).subscribe((canceled) => {
            this.canceled = canceled;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCanceleds() {
        this.eventSubscriber = this.eventManager.subscribe(
            'canceledListModification',
            (response) => this.load(this.canceled.id)
        );
    }
}
