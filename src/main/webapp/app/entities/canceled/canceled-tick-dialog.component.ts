import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CanceledTick } from './canceled-tick.model';
import { CanceledTickPopupService } from './canceled-tick-popup.service';
import { CanceledTickService } from './canceled-tick.service';
import { JobTemplateTick, JobTemplateTickService } from '../job-template';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-canceled-tick-dialog',
    templateUrl: './canceled-tick-dialog.component.html'
})
export class CanceledTickDialogComponent implements OnInit {

    canceled: CanceledTick;
    isSaving: boolean;

    jobtemplates: JobTemplateTick[];
    cancelDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private canceledService: CanceledTickService,
        private jobTemplateService: JobTemplateTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jobTemplateService
            .query({filter: 'canceled-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.canceled.jobTemplateId) {
                    this.jobtemplates = res.json;
                } else {
                    this.jobTemplateService
                        .find(this.canceled.jobTemplateId)
                        .subscribe((subRes: JobTemplateTick) => {
                            this.jobtemplates = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.canceled.id !== undefined) {
            this.subscribeToSaveResponse(
                this.canceledService.update(this.canceled));
        } else {
            this.subscribeToSaveResponse(
                this.canceledService.create(this.canceled));
        }
    }

    private subscribeToSaveResponse(result: Observable<CanceledTick>) {
        result.subscribe((res: CanceledTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: CanceledTick) {
        this.eventManager.broadcast({ name: 'canceledListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackJobTemplateById(index: number, item: JobTemplateTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-canceled-tick-popup',
    template: ''
})
export class CanceledTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private canceledPopupService: CanceledTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.canceledPopupService
                    .open(CanceledTickDialogComponent as Component, params['id']);
            } else {
                this.canceledPopupService
                    .open(CanceledTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
