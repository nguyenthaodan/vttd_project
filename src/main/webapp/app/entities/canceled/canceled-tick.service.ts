import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { CanceledTick } from './canceled-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CanceledTickService {

    private resourceUrl = 'api/canceleds';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(canceled: CanceledTick): Observable<CanceledTick> {
        const copy = this.convert(canceled);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(canceled: CanceledTick): Observable<CanceledTick> {
        const copy = this.convert(canceled);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<CanceledTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.cancelDate = this.dateUtils
            .convertLocalDateFromServer(entity.cancelDate);
    }

    private convert(canceled: CanceledTick): CanceledTick {
        const copy: CanceledTick = Object.assign({}, canceled);
        copy.cancelDate = this.dateUtils
            .convertLocalDateToServer(canceled.cancelDate);
        return copy;
    }
}
