import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CanceledTick } from './canceled-tick.model';
import { CanceledTickService } from './canceled-tick.service';

@Injectable()
export class CanceledTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private canceledService: CanceledTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.canceledService.find(id).subscribe((canceled) => {
                    if (canceled.cancelDate) {
                        canceled.cancelDate = {
                            year: canceled.cancelDate.getFullYear(),
                            month: canceled.cancelDate.getMonth() + 1,
                            day: canceled.cancelDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.canceledModalRef(component, canceled);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.canceledModalRef(component, new CanceledTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    canceledModalRef(component: Component, canceled: CanceledTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.canceled = canceled;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
