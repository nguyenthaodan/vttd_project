import { BaseEntity } from './../../shared';

export class CanceledTick implements BaseEntity {
    constructor(
        public id?: number,
        public cancelDate?: any,
        public reason?: string,
        public jobTemplateId?: number,
        public sysUsers?: BaseEntity[],
    ) {
    }
}
