import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { InvoiceInfoTick } from './invoice-info-tick.model';
import { InvoiceInfoTickPopupService } from './invoice-info-tick-popup.service';
import { InvoiceInfoTickService } from './invoice-info-tick.service';
import { BusinessTick, BusinessTickService } from '../business';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-invoice-info-tick-dialog',
    templateUrl: './invoice-info-tick-dialog.component.html'
})
export class InvoiceInfoTickDialogComponent implements OnInit {

    invoiceInfo: InvoiceInfoTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private invoiceInfoService: InvoiceInfoTickService,
        private businessService: BusinessTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.invoiceInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.invoiceInfoService.update(this.invoiceInfo));
        } else {
            this.subscribeToSaveResponse(
                this.invoiceInfoService.create(this.invoiceInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<InvoiceInfoTick>) {
        result.subscribe((res: InvoiceInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: InvoiceInfoTick) {
        this.eventManager.broadcast({ name: 'invoiceInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackBusinessById(index: number, item: BusinessTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-invoice-info-tick-popup',
    template: ''
})
export class InvoiceInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private invoiceInfoPopupService: InvoiceInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.invoiceInfoPopupService
                    .open(InvoiceInfoTickDialogComponent as Component, params['id']);
            } else {
                this.invoiceInfoPopupService
                    .open(InvoiceInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
