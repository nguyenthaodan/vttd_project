import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { InvoiceInfoTick } from './invoice-info-tick.model';
import { InvoiceInfoTickPopupService } from './invoice-info-tick-popup.service';
import { InvoiceInfoTickService } from './invoice-info-tick.service';

@Component({
    selector: 'jhi-invoice-info-tick-delete-dialog',
    templateUrl: './invoice-info-tick-delete-dialog.component.html'
})
export class InvoiceInfoTickDeleteDialogComponent {

    invoiceInfo: InvoiceInfoTick;

    constructor(
        private invoiceInfoService: InvoiceInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.invoiceInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'invoiceInfoListModification',
                content: 'Deleted an invoiceInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-invoice-info-tick-delete-popup',
    template: ''
})
export class InvoiceInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private invoiceInfoPopupService: InvoiceInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.invoiceInfoPopupService
                .open(InvoiceInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
