import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { InvoiceInfoTick } from './invoice-info-tick.model';
import { InvoiceInfoTickService } from './invoice-info-tick.service';

@Component({
    selector: 'jhi-invoice-info-tick-detail',
    templateUrl: './invoice-info-tick-detail.component.html'
})
export class InvoiceInfoTickDetailComponent implements OnInit, OnDestroy {

    invoiceInfo: InvoiceInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private invoiceInfoService: InvoiceInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInInvoiceInfos();
    }

    load(id) {
        this.invoiceInfoService.find(id).subscribe((invoiceInfo) => {
            this.invoiceInfo = invoiceInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInInvoiceInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'invoiceInfoListModification',
            (response) => this.load(this.invoiceInfo.id)
        );
    }
}
