import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { InvoiceInfoTick } from './invoice-info-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class InvoiceInfoTickService {

    private resourceUrl = 'api/invoice-infos';

    constructor(private http: Http) { }

    create(invoiceInfo: InvoiceInfoTick): Observable<InvoiceInfoTick> {
        const copy = this.convert(invoiceInfo);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(invoiceInfo: InvoiceInfoTick): Observable<InvoiceInfoTick> {
        const copy = this.convert(invoiceInfo);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<InvoiceInfoTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(invoiceInfo: InvoiceInfoTick): InvoiceInfoTick {
        const copy: InvoiceInfoTick = Object.assign({}, invoiceInfo);
        return copy;
    }
}
