import { BaseEntity } from './../../shared';

export class InvoiceInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public businessAddress?: string,
        public invoiceAddress?: string,
        public taxNumber?: string,
        public accountNumber?: string,
        public businessName?: string,
        public personName?: string,
        public Id?: number,
    ) {
    }
}
