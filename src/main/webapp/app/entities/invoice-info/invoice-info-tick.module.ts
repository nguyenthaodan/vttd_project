import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    InvoiceInfoTickService,
    InvoiceInfoTickPopupService,
    InvoiceInfoTickComponent,
    InvoiceInfoTickDetailComponent,
    InvoiceInfoTickDialogComponent,
    InvoiceInfoTickPopupComponent,
    InvoiceInfoTickDeletePopupComponent,
    InvoiceInfoTickDeleteDialogComponent,
    invoiceInfoRoute,
    invoiceInfoPopupRoute,
    InvoiceInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...invoiceInfoRoute,
    ...invoiceInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        InvoiceInfoTickComponent,
        InvoiceInfoTickDetailComponent,
        InvoiceInfoTickDialogComponent,
        InvoiceInfoTickDeleteDialogComponent,
        InvoiceInfoTickPopupComponent,
        InvoiceInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        InvoiceInfoTickComponent,
        InvoiceInfoTickDialogComponent,
        InvoiceInfoTickPopupComponent,
        InvoiceInfoTickDeleteDialogComponent,
        InvoiceInfoTickDeletePopupComponent,
    ],
    providers: [
        InvoiceInfoTickService,
        InvoiceInfoTickPopupService,
        InvoiceInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickInvoiceInfoTickModule {}
