import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { InvoiceInfoTickComponent } from './invoice-info-tick.component';
import { InvoiceInfoTickDetailComponent } from './invoice-info-tick-detail.component';
import { InvoiceInfoTickPopupComponent } from './invoice-info-tick-dialog.component';
import { InvoiceInfoTickDeletePopupComponent } from './invoice-info-tick-delete-dialog.component';

@Injectable()
export class InvoiceInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const invoiceInfoRoute: Routes = [
    {
        path: 'invoice-info-tick',
        component: InvoiceInfoTickComponent,
        resolve: {
            'pagingParams': InvoiceInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoiceInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invoice-info-tick/:id',
        component: InvoiceInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoiceInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invoiceInfoPopupRoute: Routes = [
    {
        path: 'invoice-info-tick-new',
        component: InvoiceInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoiceInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invoice-info-tick/:id/edit',
        component: InvoiceInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoiceInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invoice-info-tick/:id/delete',
        component: InvoiceInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.invoiceInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
