import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { PageTableTick } from './page-table-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PageTableTickService {

    private resourceUrl = 'api/page-tables';

    constructor(private http: Http) { }

    create(pageTable: PageTableTick): Observable<PageTableTick> {
        const copy = this.convert(pageTable);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(pageTable: PageTableTick): Observable<PageTableTick> {
        const copy = this.convert(pageTable);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<PageTableTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(pageTable: PageTableTick): PageTableTick {
        const copy: PageTableTick = Object.assign({}, pageTable);
        return copy;
    }
}
