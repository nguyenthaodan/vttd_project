import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    PageTableTickService,
    PageTableTickPopupService,
    PageTableTickComponent,
    PageTableTickDetailComponent,
    PageTableTickDialogComponent,
    PageTableTickPopupComponent,
    PageTableTickDeletePopupComponent,
    PageTableTickDeleteDialogComponent,
    pageTableRoute,
    pageTablePopupRoute,
    PageTableTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...pageTableRoute,
    ...pageTablePopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PageTableTickComponent,
        PageTableTickDetailComponent,
        PageTableTickDialogComponent,
        PageTableTickDeleteDialogComponent,
        PageTableTickPopupComponent,
        PageTableTickDeletePopupComponent,
    ],
    entryComponents: [
        PageTableTickComponent,
        PageTableTickDialogComponent,
        PageTableTickPopupComponent,
        PageTableTickDeleteDialogComponent,
        PageTableTickDeletePopupComponent,
    ],
    providers: [
        PageTableTickService,
        PageTableTickPopupService,
        PageTableTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickPageTableTickModule {}
