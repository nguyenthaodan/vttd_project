import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PageTableTickComponent } from './page-table-tick.component';
import { PageTableTickDetailComponent } from './page-table-tick-detail.component';
import { PageTableTickPopupComponent } from './page-table-tick-dialog.component';
import { PageTableTickDeletePopupComponent } from './page-table-tick-delete-dialog.component';

@Injectable()
export class PageTableTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const pageTableRoute: Routes = [
    {
        path: 'page-table-tick',
        component: PageTableTickComponent,
        resolve: {
            'pagingParams': PageTableTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pageTable.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'page-table-tick/:id',
        component: PageTableTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pageTable.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pageTablePopupRoute: Routes = [
    {
        path: 'page-table-tick-new',
        component: PageTableTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pageTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'page-table-tick/:id/edit',
        component: PageTableTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pageTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'page-table-tick/:id/delete',
        component: PageTableTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.pageTable.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
