import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PageTableTick } from './page-table-tick.model';
import { PageTableTickPopupService } from './page-table-tick-popup.service';
import { PageTableTickService } from './page-table-tick.service';

@Component({
    selector: 'jhi-page-table-tick-dialog',
    templateUrl: './page-table-tick-dialog.component.html'
})
export class PageTableTickDialogComponent implements OnInit {

    pageTable: PageTableTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private pageTableService: PageTableTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.pageTable.id !== undefined) {
            this.subscribeToSaveResponse(
                this.pageTableService.update(this.pageTable));
        } else {
            this.subscribeToSaveResponse(
                this.pageTableService.create(this.pageTable));
        }
    }

    private subscribeToSaveResponse(result: Observable<PageTableTick>) {
        result.subscribe((res: PageTableTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PageTableTick) {
        this.eventManager.broadcast({ name: 'pageTableListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-page-table-tick-popup',
    template: ''
})
export class PageTableTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pageTablePopupService: PageTableTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.pageTablePopupService
                    .open(PageTableTickDialogComponent as Component, params['id']);
            } else {
                this.pageTablePopupService
                    .open(PageTableTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
