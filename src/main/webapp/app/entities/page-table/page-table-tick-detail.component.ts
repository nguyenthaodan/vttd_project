import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PageTableTick } from './page-table-tick.model';
import { PageTableTickService } from './page-table-tick.service';

@Component({
    selector: 'jhi-page-table-tick-detail',
    templateUrl: './page-table-tick-detail.component.html'
})
export class PageTableTickDetailComponent implements OnInit, OnDestroy {

    pageTable: PageTableTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private pageTableService: PageTableTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPageTables();
    }

    load(id) {
        this.pageTableService.find(id).subscribe((pageTable) => {
            this.pageTable = pageTable;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPageTables() {
        this.eventSubscriber = this.eventManager.subscribe(
            'pageTableListModification',
            (response) => this.load(this.pageTable.id)
        );
    }
}
