export * from './page-table-tick.model';
export * from './page-table-tick-popup.service';
export * from './page-table-tick.service';
export * from './page-table-tick-dialog.component';
export * from './page-table-tick-delete-dialog.component';
export * from './page-table-tick-detail.component';
export * from './page-table-tick.component';
export * from './page-table-tick.route';
