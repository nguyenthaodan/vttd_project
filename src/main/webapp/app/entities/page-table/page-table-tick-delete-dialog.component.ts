import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PageTableTick } from './page-table-tick.model';
import { PageTableTickPopupService } from './page-table-tick-popup.service';
import { PageTableTickService } from './page-table-tick.service';

@Component({
    selector: 'jhi-page-table-tick-delete-dialog',
    templateUrl: './page-table-tick-delete-dialog.component.html'
})
export class PageTableTickDeleteDialogComponent {

    pageTable: PageTableTick;

    constructor(
        private pageTableService: PageTableTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pageTableService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'pageTableListModification',
                content: 'Deleted an pageTable'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-page-table-tick-delete-popup',
    template: ''
})
export class PageTableTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private pageTablePopupService: PageTableTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.pageTablePopupService
                .open(PageTableTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
