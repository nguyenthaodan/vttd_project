import { BaseEntity } from './../../shared';

export class PageTableTick implements BaseEntity {
    constructor(
        public id?: number,
        public pageName?: string,
        public pageTitle?: string,
        public pageInfo?: string,
        public content?: string,
    ) {
    }
}
