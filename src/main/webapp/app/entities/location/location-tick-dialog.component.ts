import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LocationTick } from './location-tick.model';
import { LocationTickPopupService } from './location-tick-popup.service';
import { LocationTickService } from './location-tick.service';
import { JobTemplateTick, JobTemplateTickService } from '../job-template';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-location-tick-dialog',
    templateUrl: './location-tick-dialog.component.html'
})
export class LocationTickDialogComponent implements OnInit {

    location: LocationTick;
    isSaving: boolean;

    jobtemplates: JobTemplateTick[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private locationService: LocationTickService,
        private jobTemplateService: JobTemplateTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jobTemplateService.query()
            .subscribe((res: ResponseWrapper) => { this.jobtemplates = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.location.id !== undefined) {
            this.subscribeToSaveResponse(
                this.locationService.update(this.location));
        } else {
            this.subscribeToSaveResponse(
                this.locationService.create(this.location));
        }
    }

    private subscribeToSaveResponse(result: Observable<LocationTick>) {
        result.subscribe((res: LocationTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: LocationTick) {
        this.eventManager.broadcast({ name: 'locationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackJobTemplateById(index: number, item: JobTemplateTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-location-tick-popup',
    template: ''
})
export class LocationTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private locationPopupService: LocationTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.locationPopupService
                    .open(LocationTickDialogComponent as Component, params['id']);
            } else {
                this.locationPopupService
                    .open(LocationTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
