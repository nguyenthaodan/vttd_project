import { BaseEntity } from './../../shared';

export class LocationTick implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public abbre?: string,
        public city?: string,
        public country?: string,
        public longitude?: number,
        public lagitude?: number,
        public sysUsers?: BaseEntity[],
        public jobTemplateId?: number,
    ) {
    }
}
