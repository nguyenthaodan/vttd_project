import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    LocationTickService,
    LocationTickPopupService,
    LocationTickComponent,
    LocationTickDetailComponent,
    LocationTickDialogComponent,
    LocationTickPopupComponent,
    LocationTickDeletePopupComponent,
    LocationTickDeleteDialogComponent,
    locationRoute,
    locationPopupRoute,
    LocationTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...locationRoute,
    ...locationPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LocationTickComponent,
        LocationTickDetailComponent,
        LocationTickDialogComponent,
        LocationTickDeleteDialogComponent,
        LocationTickPopupComponent,
        LocationTickDeletePopupComponent,
    ],
    entryComponents: [
        LocationTickComponent,
        LocationTickDialogComponent,
        LocationTickPopupComponent,
        LocationTickDeleteDialogComponent,
        LocationTickDeletePopupComponent,
    ],
    providers: [
        LocationTickService,
        LocationTickPopupService,
        LocationTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickLocationTickModule {}
