import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { WorkingTick } from './working-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class WorkingTickService {

    private resourceUrl = 'api/workings';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(working: WorkingTick): Observable<WorkingTick> {
        const copy = this.convert(working);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(working: WorkingTick): Observable<WorkingTick> {
        const copy = this.convert(working);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<WorkingTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.bookDate = this.dateUtils
            .convertLocalDateFromServer(entity.bookDate);
        entity.startDate = this.dateUtils
            .convertLocalDateFromServer(entity.startDate);
        entity.finishDate = this.dateUtils
            .convertLocalDateFromServer(entity.finishDate);
        entity.lastMeeting = this.dateUtils
            .convertLocalDateFromServer(entity.lastMeeting);
        entity.nextMeeting = this.dateUtils
            .convertLocalDateFromServer(entity.nextMeeting);
    }

    private convert(working: WorkingTick): WorkingTick {
        const copy: WorkingTick = Object.assign({}, working);
        copy.bookDate = this.dateUtils
            .convertLocalDateToServer(working.bookDate);
        copy.startDate = this.dateUtils
            .convertLocalDateToServer(working.startDate);
        copy.finishDate = this.dateUtils
            .convertLocalDateToServer(working.finishDate);
        copy.lastMeeting = this.dateUtils
            .convertLocalDateToServer(working.lastMeeting);
        copy.nextMeeting = this.dateUtils
            .convertLocalDateToServer(working.nextMeeting);
        return copy;
    }
}
