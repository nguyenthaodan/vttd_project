import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { WorkingTick } from './working-tick.model';
import { WorkingTickService } from './working-tick.service';

@Injectable()
export class WorkingTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private workingService: WorkingTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.workingService.find(id).subscribe((working) => {
                    if (working.bookDate) {
                        working.bookDate = {
                            year: working.bookDate.getFullYear(),
                            month: working.bookDate.getMonth() + 1,
                            day: working.bookDate.getDate()
                        };
                    }
                    if (working.startDate) {
                        working.startDate = {
                            year: working.startDate.getFullYear(),
                            month: working.startDate.getMonth() + 1,
                            day: working.startDate.getDate()
                        };
                    }
                    if (working.finishDate) {
                        working.finishDate = {
                            year: working.finishDate.getFullYear(),
                            month: working.finishDate.getMonth() + 1,
                            day: working.finishDate.getDate()
                        };
                    }
                    if (working.lastMeeting) {
                        working.lastMeeting = {
                            year: working.lastMeeting.getFullYear(),
                            month: working.lastMeeting.getMonth() + 1,
                            day: working.lastMeeting.getDate()
                        };
                    }
                    if (working.nextMeeting) {
                        working.nextMeeting = {
                            year: working.nextMeeting.getFullYear(),
                            month: working.nextMeeting.getMonth() + 1,
                            day: working.nextMeeting.getDate()
                        };
                    }
                    this.ngbModalRef = this.workingModalRef(component, working);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.workingModalRef(component, new WorkingTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    workingModalRef(component: Component, working: WorkingTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.working = working;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
