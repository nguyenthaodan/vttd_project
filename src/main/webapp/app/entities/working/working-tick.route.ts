import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WorkingTickComponent } from './working-tick.component';
import { WorkingTickDetailComponent } from './working-tick-detail.component';
import { WorkingTickPopupComponent } from './working-tick-dialog.component';
import { WorkingTickDeletePopupComponent } from './working-tick-delete-dialog.component';

@Injectable()
export class WorkingTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const workingRoute: Routes = [
    {
        path: 'working-tick',
        component: WorkingTickComponent,
        resolve: {
            'pagingParams': WorkingTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.working.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'working-tick/:id',
        component: WorkingTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.working.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const workingPopupRoute: Routes = [
    {
        path: 'working-tick-new',
        component: WorkingTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.working.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'working-tick/:id/edit',
        component: WorkingTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.working.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'working-tick/:id/delete',
        component: WorkingTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.working.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
