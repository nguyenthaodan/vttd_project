import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    WorkingTickService,
    WorkingTickPopupService,
    WorkingTickComponent,
    WorkingTickDetailComponent,
    WorkingTickDialogComponent,
    WorkingTickPopupComponent,
    WorkingTickDeletePopupComponent,
    WorkingTickDeleteDialogComponent,
    workingRoute,
    workingPopupRoute,
    WorkingTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...workingRoute,
    ...workingPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        WorkingTickComponent,
        WorkingTickDetailComponent,
        WorkingTickDialogComponent,
        WorkingTickDeleteDialogComponent,
        WorkingTickPopupComponent,
        WorkingTickDeletePopupComponent,
    ],
    entryComponents: [
        WorkingTickComponent,
        WorkingTickDialogComponent,
        WorkingTickPopupComponent,
        WorkingTickDeleteDialogComponent,
        WorkingTickDeletePopupComponent,
    ],
    providers: [
        WorkingTickService,
        WorkingTickPopupService,
        WorkingTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickWorkingTickModule {}
