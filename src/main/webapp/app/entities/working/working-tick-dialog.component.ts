import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { WorkingTick } from './working-tick.model';
import { WorkingTickPopupService } from './working-tick-popup.service';
import { WorkingTickService } from './working-tick.service';
import { JobTemplateTick, JobTemplateTickService } from '../job-template';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-working-tick-dialog',
    templateUrl: './working-tick-dialog.component.html'
})
export class WorkingTickDialogComponent implements OnInit {

    working: WorkingTick;
    isSaving: boolean;

    jobtemplates: JobTemplateTick[];

    sysusers: SysUserTick[];
    bookDateDp: any;
    startDateDp: any;
    finishDateDp: any;
    lastMeetingDp: any;
    nextMeetingDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private workingService: WorkingTickService,
        private jobTemplateService: JobTemplateTickService,
        private sysUserService: SysUserTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jobTemplateService
            .query({filter: 'working-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.working.jobTemplateId) {
                    this.jobtemplates = res.json;
                } else {
                    this.jobTemplateService
                        .find(this.working.jobTemplateId)
                        .subscribe((subRes: JobTemplateTick) => {
                            this.jobtemplates = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.working.id !== undefined) {
            this.subscribeToSaveResponse(
                this.workingService.update(this.working));
        } else {
            this.subscribeToSaveResponse(
                this.workingService.create(this.working));
        }
    }

    private subscribeToSaveResponse(result: Observable<WorkingTick>) {
        result.subscribe((res: WorkingTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: WorkingTick) {
        this.eventManager.broadcast({ name: 'workingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackJobTemplateById(index: number, item: JobTemplateTick) {
        return item.id;
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-working-tick-popup',
    template: ''
})
export class WorkingTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private workingPopupService: WorkingTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.workingPopupService
                    .open(WorkingTickDialogComponent as Component, params['id']);
            } else {
                this.workingPopupService
                    .open(WorkingTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
