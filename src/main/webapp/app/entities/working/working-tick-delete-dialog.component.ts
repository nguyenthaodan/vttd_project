import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { WorkingTick } from './working-tick.model';
import { WorkingTickPopupService } from './working-tick-popup.service';
import { WorkingTickService } from './working-tick.service';

@Component({
    selector: 'jhi-working-tick-delete-dialog',
    templateUrl: './working-tick-delete-dialog.component.html'
})
export class WorkingTickDeleteDialogComponent {

    working: WorkingTick;

    constructor(
        private workingService: WorkingTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.workingService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'workingListModification',
                content: 'Deleted an working'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-working-tick-delete-popup',
    template: ''
})
export class WorkingTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private workingPopupService: WorkingTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.workingPopupService
                .open(WorkingTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
