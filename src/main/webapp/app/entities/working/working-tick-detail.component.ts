import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { WorkingTick } from './working-tick.model';
import { WorkingTickService } from './working-tick.service';

@Component({
    selector: 'jhi-working-tick-detail',
    templateUrl: './working-tick-detail.component.html'
})
export class WorkingTickDetailComponent implements OnInit, OnDestroy {

    working: WorkingTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private workingService: WorkingTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInWorkings();
    }

    load(id) {
        this.workingService.find(id).subscribe((working) => {
            this.working = working;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInWorkings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'workingListModification',
            (response) => this.load(this.working.id)
        );
    }
}
