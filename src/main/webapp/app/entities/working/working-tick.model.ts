import { BaseEntity } from './../../shared';

const enum WorkingStatus {
    'PENDING_MEETING',
    'DOCUMENT_SENT',
    'WAITING_FOR_FINAL_RESULT',
    'MONEY_DEPOSIT',
    'WAITING_FOR_FINAL_APPROVAL',
    'ONE_QUARTER',
    'TWO_QUARTER',
    'THREE_QUARTER',
    'WAITING_FOR_DOCUMENT'
}

export class WorkingTick implements BaseEntity {
    constructor(
        public id?: number,
        public userstatus?: WorkingStatus,
        public supplierstatus?: WorkingStatus,
        public amountDeposit?: number,
        public price?: number,
        public bookDate?: any,
        public startDate?: any,
        public finishDate?: any,
        public lastMeeting?: any,
        public nextMeeting?: any,
        public jobTemplateId?: number,
        public sysUserId?: number,
    ) {
    }
}
