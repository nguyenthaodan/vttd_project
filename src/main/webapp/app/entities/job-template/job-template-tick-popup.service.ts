import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JobTemplateTick } from './job-template-tick.model';
import { JobTemplateTickService } from './job-template-tick.service';

@Injectable()
export class JobTemplateTickPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private jobTemplateService: JobTemplateTickService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.jobTemplateService.find(id).subscribe((jobTemplate) => {
                    if (jobTemplate.createDate) {
                        jobTemplate.createDate = {
                            year: jobTemplate.createDate.getFullYear(),
                            month: jobTemplate.createDate.getMonth() + 1,
                            day: jobTemplate.createDate.getDate()
                        };
                    }
                    if (jobTemplate.startDate) {
                        jobTemplate.startDate = {
                            year: jobTemplate.startDate.getFullYear(),
                            month: jobTemplate.startDate.getMonth() + 1,
                            day: jobTemplate.startDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.jobTemplateModalRef(component, jobTemplate);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.jobTemplateModalRef(component, new JobTemplateTick());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    jobTemplateModalRef(component: Component, jobTemplate: JobTemplateTick): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.jobTemplate = jobTemplate;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
