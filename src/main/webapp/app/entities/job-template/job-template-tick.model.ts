import { BaseEntity } from './../../shared';

const enum PaymentMethod {
    'ALL',
    'CASH',
    'BANK_TRANSFER',
    'PAYPAL',
    'GOOGLE_WALLET',
    'BITCOIN',
    'MOBILE',
    'PAYOO',
    'POST'
}

const enum JobRequirement {
    'INTERVIEW',
    'ONLINE',
    'OFFLINE_NO_INTERVIEW'
}

export class JobTemplateTick implements BaseEntity {
    constructor(
        public id?: number,
        public address?: string,
        public paymentMethod?: PaymentMethod,
        public requirement?: JobRequirement,
        public formFillIn?: string,
        public createDate?: any,
        public startDate?: any,
        public serviceId?: number,
        public eventInfos?: BaseEntity[],
        public locations?: BaseEntity[],
        public sysUserId?: number,
    ) {
    }
}
