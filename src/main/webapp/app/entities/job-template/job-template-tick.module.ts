import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    JobTemplateTickService,
    JobTemplateTickPopupService,
    JobTemplateTickComponent,
    JobTemplateTickDetailComponent,
    JobTemplateTickDialogComponent,
    JobTemplateTickPopupComponent,
    JobTemplateTickDeletePopupComponent,
    JobTemplateTickDeleteDialogComponent,
    jobTemplateRoute,
    jobTemplatePopupRoute,
    JobTemplateTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...jobTemplateRoute,
    ...jobTemplatePopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        JobTemplateTickComponent,
        JobTemplateTickDetailComponent,
        JobTemplateTickDialogComponent,
        JobTemplateTickDeleteDialogComponent,
        JobTemplateTickPopupComponent,
        JobTemplateTickDeletePopupComponent,
    ],
    entryComponents: [
        JobTemplateTickComponent,
        JobTemplateTickDialogComponent,
        JobTemplateTickPopupComponent,
        JobTemplateTickDeleteDialogComponent,
        JobTemplateTickDeletePopupComponent,
    ],
    providers: [
        JobTemplateTickService,
        JobTemplateTickPopupService,
        JobTemplateTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickJobTemplateTickModule {}
