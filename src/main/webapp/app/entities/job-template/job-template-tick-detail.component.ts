import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { JobTemplateTick } from './job-template-tick.model';
import { JobTemplateTickService } from './job-template-tick.service';

@Component({
    selector: 'jhi-job-template-tick-detail',
    templateUrl: './job-template-tick-detail.component.html'
})
export class JobTemplateTickDetailComponent implements OnInit, OnDestroy {

    jobTemplate: JobTemplateTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private jobTemplateService: JobTemplateTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJobTemplates();
    }

    load(id) {
        this.jobTemplateService.find(id).subscribe((jobTemplate) => {
            this.jobTemplate = jobTemplate;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJobTemplates() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jobTemplateListModification',
            (response) => this.load(this.jobTemplate.id)
        );
    }
}
