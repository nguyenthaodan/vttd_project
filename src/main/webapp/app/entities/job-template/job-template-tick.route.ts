import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { JobTemplateTickComponent } from './job-template-tick.component';
import { JobTemplateTickDetailComponent } from './job-template-tick-detail.component';
import { JobTemplateTickPopupComponent } from './job-template-tick-dialog.component';
import { JobTemplateTickDeletePopupComponent } from './job-template-tick-delete-dialog.component';

@Injectable()
export class JobTemplateTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const jobTemplateRoute: Routes = [
    {
        path: 'job-template-tick',
        component: JobTemplateTickComponent,
        resolve: {
            'pagingParams': JobTemplateTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobTemplate.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'job-template-tick/:id',
        component: JobTemplateTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobTemplate.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const jobTemplatePopupRoute: Routes = [
    {
        path: 'job-template-tick-new',
        component: JobTemplateTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobTemplate.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'job-template-tick/:id/edit',
        component: JobTemplateTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobTemplate.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'job-template-tick/:id/delete',
        component: JobTemplateTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.jobTemplate.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
