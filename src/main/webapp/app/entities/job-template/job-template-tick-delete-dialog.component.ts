import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JobTemplateTick } from './job-template-tick.model';
import { JobTemplateTickPopupService } from './job-template-tick-popup.service';
import { JobTemplateTickService } from './job-template-tick.service';

@Component({
    selector: 'jhi-job-template-tick-delete-dialog',
    templateUrl: './job-template-tick-delete-dialog.component.html'
})
export class JobTemplateTickDeleteDialogComponent {

    jobTemplate: JobTemplateTick;

    constructor(
        private jobTemplateService: JobTemplateTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jobTemplateService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jobTemplateListModification',
                content: 'Deleted an jobTemplate'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-job-template-tick-delete-popup',
    template: ''
})
export class JobTemplateTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jobTemplatePopupService: JobTemplateTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jobTemplatePopupService
                .open(JobTemplateTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
