import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JobTemplateTick } from './job-template-tick.model';
import { JobTemplateTickPopupService } from './job-template-tick-popup.service';
import { JobTemplateTickService } from './job-template-tick.service';
import { ServiceTick, ServiceTickService } from '../service';
import { SysUserTick, SysUserTickService } from '../sys-user';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-job-template-tick-dialog',
    templateUrl: './job-template-tick-dialog.component.html'
})
export class JobTemplateTickDialogComponent implements OnInit {

    jobTemplate: JobTemplateTick;
    isSaving: boolean;

    services: ServiceTick[];

    sysusers: SysUserTick[];
    createDateDp: any;
    startDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private jobTemplateService: JobTemplateTickService,
        private serviceService: ServiceTickService,
        private sysUserService: SysUserTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.serviceService
            .query({filter: 'jobtemplate-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.jobTemplate.serviceId) {
                    this.services = res.json;
                } else {
                    this.serviceService
                        .find(this.jobTemplate.serviceId)
                        .subscribe((subRes: ServiceTick) => {
                            this.services = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.sysUserService.query()
            .subscribe((res: ResponseWrapper) => { this.sysusers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.jobTemplate.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jobTemplateService.update(this.jobTemplate));
        } else {
            this.subscribeToSaveResponse(
                this.jobTemplateService.create(this.jobTemplate));
        }
    }

    private subscribeToSaveResponse(result: Observable<JobTemplateTick>) {
        result.subscribe((res: JobTemplateTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: JobTemplateTick) {
        this.eventManager.broadcast({ name: 'jobTemplateListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackServiceById(index: number, item: ServiceTick) {
        return item.id;
    }

    trackSysUserById(index: number, item: SysUserTick) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-job-template-tick-popup',
    template: ''
})
export class JobTemplateTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jobTemplatePopupService: JobTemplateTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jobTemplatePopupService
                    .open(JobTemplateTickDialogComponent as Component, params['id']);
            } else {
                this.jobTemplatePopupService
                    .open(JobTemplateTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
