import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { JobTemplateTick } from './job-template-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class JobTemplateTickService {

    private resourceUrl = 'api/job-templates';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(jobTemplate: JobTemplateTick): Observable<JobTemplateTick> {
        const copy = this.convert(jobTemplate);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(jobTemplate: JobTemplateTick): Observable<JobTemplateTick> {
        const copy = this.convert(jobTemplate);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<JobTemplateTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.createDate = this.dateUtils
            .convertLocalDateFromServer(entity.createDate);
        entity.startDate = this.dateUtils
            .convertLocalDateFromServer(entity.startDate);
    }

    private convert(jobTemplate: JobTemplateTick): JobTemplateTick {
        const copy: JobTemplateTick = Object.assign({}, jobTemplate);
        copy.createDate = this.dateUtils
            .convertLocalDateToServer(jobTemplate.createDate);
        copy.startDate = this.dateUtils
            .convertLocalDateToServer(jobTemplate.startDate);
        return copy;
    }
}
