import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { ReviewInfoTick } from './review-info-tick.model';
import { ReviewInfoTickService } from './review-info-tick.service';

@Component({
    selector: 'jhi-review-info-tick-detail',
    templateUrl: './review-info-tick-detail.component.html'
})
export class ReviewInfoTickDetailComponent implements OnInit, OnDestroy {

    reviewInfo: ReviewInfoTick;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private reviewInfoService: ReviewInfoTickService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInReviewInfos();
    }

    load(id) {
        this.reviewInfoService.find(id).subscribe((reviewInfo) => {
            this.reviewInfo = reviewInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInReviewInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'reviewInfoListModification',
            (response) => this.load(this.reviewInfo.id)
        );
    }
}
