import { BaseEntity } from './../../shared';

export class ReviewInfoTick implements BaseEntity {
    constructor(
        public id?: number,
        public username?: string,
        public password?: string,
        public numReviews?: number,
        public averageRating?: number,
    ) {
    }
}
