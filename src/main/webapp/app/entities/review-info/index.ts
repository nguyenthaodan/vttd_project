export * from './review-info-tick.model';
export * from './review-info-tick-popup.service';
export * from './review-info-tick.service';
export * from './review-info-tick-dialog.component';
export * from './review-info-tick-delete-dialog.component';
export * from './review-info-tick-detail.component';
export * from './review-info-tick.component';
export * from './review-info-tick.route';
