import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReviewInfoTick } from './review-info-tick.model';
import { ReviewInfoTickPopupService } from './review-info-tick-popup.service';
import { ReviewInfoTickService } from './review-info-tick.service';

@Component({
    selector: 'jhi-review-info-tick-dialog',
    templateUrl: './review-info-tick-dialog.component.html'
})
export class ReviewInfoTickDialogComponent implements OnInit {

    reviewInfo: ReviewInfoTick;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private reviewInfoService: ReviewInfoTickService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.reviewInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.reviewInfoService.update(this.reviewInfo));
        } else {
            this.subscribeToSaveResponse(
                this.reviewInfoService.create(this.reviewInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<ReviewInfoTick>) {
        result.subscribe((res: ReviewInfoTick) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ReviewInfoTick) {
        this.eventManager.broadcast({ name: 'reviewInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-review-info-tick-popup',
    template: ''
})
export class ReviewInfoTickPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private reviewInfoPopupService: ReviewInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.reviewInfoPopupService
                    .open(ReviewInfoTickDialogComponent as Component, params['id']);
            } else {
                this.reviewInfoPopupService
                    .open(ReviewInfoTickDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
