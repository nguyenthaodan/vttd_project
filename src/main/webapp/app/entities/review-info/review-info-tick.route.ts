import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReviewInfoTickComponent } from './review-info-tick.component';
import { ReviewInfoTickDetailComponent } from './review-info-tick-detail.component';
import { ReviewInfoTickPopupComponent } from './review-info-tick-dialog.component';
import { ReviewInfoTickDeletePopupComponent } from './review-info-tick-delete-dialog.component';

@Injectable()
export class ReviewInfoTickResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const reviewInfoRoute: Routes = [
    {
        path: 'review-info-tick',
        component: ReviewInfoTickComponent,
        resolve: {
            'pagingParams': ReviewInfoTickResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.reviewInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'review-info-tick/:id',
        component: ReviewInfoTickDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.reviewInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const reviewInfoPopupRoute: Routes = [
    {
        path: 'review-info-tick-new',
        component: ReviewInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.reviewInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'review-info-tick/:id/edit',
        component: ReviewInfoTickPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.reviewInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'review-info-tick/:id/delete',
        component: ReviewInfoTickDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tickApp.reviewInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
