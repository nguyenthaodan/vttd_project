import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TickSharedModule } from '../../shared';
import {
    ReviewInfoTickService,
    ReviewInfoTickPopupService,
    ReviewInfoTickComponent,
    ReviewInfoTickDetailComponent,
    ReviewInfoTickDialogComponent,
    ReviewInfoTickPopupComponent,
    ReviewInfoTickDeletePopupComponent,
    ReviewInfoTickDeleteDialogComponent,
    reviewInfoRoute,
    reviewInfoPopupRoute,
    ReviewInfoTickResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...reviewInfoRoute,
    ...reviewInfoPopupRoute,
];

@NgModule({
    imports: [
        TickSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ReviewInfoTickComponent,
        ReviewInfoTickDetailComponent,
        ReviewInfoTickDialogComponent,
        ReviewInfoTickDeleteDialogComponent,
        ReviewInfoTickPopupComponent,
        ReviewInfoTickDeletePopupComponent,
    ],
    entryComponents: [
        ReviewInfoTickComponent,
        ReviewInfoTickDialogComponent,
        ReviewInfoTickPopupComponent,
        ReviewInfoTickDeleteDialogComponent,
        ReviewInfoTickDeletePopupComponent,
    ],
    providers: [
        ReviewInfoTickService,
        ReviewInfoTickPopupService,
        ReviewInfoTickResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TickReviewInfoTickModule {}
