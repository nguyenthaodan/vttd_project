import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ReviewInfoTick } from './review-info-tick.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ReviewInfoTickService {

    private resourceUrl = 'api/review-infos';

    constructor(private http: Http) { }

    create(reviewInfo: ReviewInfoTick): Observable<ReviewInfoTick> {
        const copy = this.convert(reviewInfo);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(reviewInfo: ReviewInfoTick): Observable<ReviewInfoTick> {
        const copy = this.convert(reviewInfo);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<ReviewInfoTick> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(reviewInfo: ReviewInfoTick): ReviewInfoTick {
        const copy: ReviewInfoTick = Object.assign({}, reviewInfo);
        return copy;
    }
}
