import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ReviewInfoTick } from './review-info-tick.model';
import { ReviewInfoTickPopupService } from './review-info-tick-popup.service';
import { ReviewInfoTickService } from './review-info-tick.service';

@Component({
    selector: 'jhi-review-info-tick-delete-dialog',
    templateUrl: './review-info-tick-delete-dialog.component.html'
})
export class ReviewInfoTickDeleteDialogComponent {

    reviewInfo: ReviewInfoTick;

    constructor(
        private reviewInfoService: ReviewInfoTickService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.reviewInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'reviewInfoListModification',
                content: 'Deleted an reviewInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-review-info-tick-delete-popup',
    template: ''
})
export class ReviewInfoTickDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private reviewInfoPopupService: ReviewInfoTickPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.reviewInfoPopupService
                .open(ReviewInfoTickDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
