package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.CreditInfo;
import com.mycompany.myapp.repository.CreditInfoRepository;
import com.mycompany.myapp.service.CreditInfoService;
import com.mycompany.myapp.service.dto.CreditInfoDTO;
import com.mycompany.myapp.service.mapper.CreditInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.domain.enumeration.PaymentMethod;
/**
 * Test class for the CreditInfoResource REST controller.
 *
 * @see CreditInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class CreditInfoResourceIntTest {

    private static final Float DEFAULT_CREDIT = 1F;
    private static final Float UPDATED_CREDIT = 2F;

    private static final PaymentMethod DEFAULT_METHOD = PaymentMethod.ALL;
    private static final PaymentMethod UPDATED_METHOD = PaymentMethod.CASH;

    @Autowired
    private CreditInfoRepository creditInfoRepository;

    @Autowired
    private CreditInfoMapper creditInfoMapper;

    @Autowired
    private CreditInfoService creditInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCreditInfoMockMvc;

    private CreditInfo creditInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CreditInfoResource creditInfoResource = new CreditInfoResource(creditInfoService);
        this.restCreditInfoMockMvc = MockMvcBuilders.standaloneSetup(creditInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CreditInfo createEntity(EntityManager em) {
        CreditInfo creditInfo = new CreditInfo()
            .credit(DEFAULT_CREDIT)
            .method(DEFAULT_METHOD);
        return creditInfo;
    }

    @Before
    public void initTest() {
        creditInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createCreditInfo() throws Exception {
        int databaseSizeBeforeCreate = creditInfoRepository.findAll().size();

        // Create the CreditInfo
        CreditInfoDTO creditInfoDTO = creditInfoMapper.toDto(creditInfo);
        restCreditInfoMockMvc.perform(post("/api/credit-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(creditInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the CreditInfo in the database
        List<CreditInfo> creditInfoList = creditInfoRepository.findAll();
        assertThat(creditInfoList).hasSize(databaseSizeBeforeCreate + 1);
        CreditInfo testCreditInfo = creditInfoList.get(creditInfoList.size() - 1);
        assertThat(testCreditInfo.getCredit()).isEqualTo(DEFAULT_CREDIT);
        assertThat(testCreditInfo.getMethod()).isEqualTo(DEFAULT_METHOD);
    }

    @Test
    @Transactional
    public void createCreditInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = creditInfoRepository.findAll().size();

        // Create the CreditInfo with an existing ID
        creditInfo.setId(1L);
        CreditInfoDTO creditInfoDTO = creditInfoMapper.toDto(creditInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCreditInfoMockMvc.perform(post("/api/credit-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(creditInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CreditInfo> creditInfoList = creditInfoRepository.findAll();
        assertThat(creditInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCreditInfos() throws Exception {
        // Initialize the database
        creditInfoRepository.saveAndFlush(creditInfo);

        // Get all the creditInfoList
        restCreditInfoMockMvc.perform(get("/api/credit-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(creditInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].credit").value(hasItem(DEFAULT_CREDIT.doubleValue())))
            .andExpect(jsonPath("$.[*].method").value(hasItem(DEFAULT_METHOD.toString())));
    }

    @Test
    @Transactional
    public void getCreditInfo() throws Exception {
        // Initialize the database
        creditInfoRepository.saveAndFlush(creditInfo);

        // Get the creditInfo
        restCreditInfoMockMvc.perform(get("/api/credit-infos/{id}", creditInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(creditInfo.getId().intValue()))
            .andExpect(jsonPath("$.credit").value(DEFAULT_CREDIT.doubleValue()))
            .andExpect(jsonPath("$.method").value(DEFAULT_METHOD.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCreditInfo() throws Exception {
        // Get the creditInfo
        restCreditInfoMockMvc.perform(get("/api/credit-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCreditInfo() throws Exception {
        // Initialize the database
        creditInfoRepository.saveAndFlush(creditInfo);
        int databaseSizeBeforeUpdate = creditInfoRepository.findAll().size();

        // Update the creditInfo
        CreditInfo updatedCreditInfo = creditInfoRepository.findOne(creditInfo.getId());
        updatedCreditInfo
            .credit(UPDATED_CREDIT)
            .method(UPDATED_METHOD);
        CreditInfoDTO creditInfoDTO = creditInfoMapper.toDto(updatedCreditInfo);

        restCreditInfoMockMvc.perform(put("/api/credit-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(creditInfoDTO)))
            .andExpect(status().isOk());

        // Validate the CreditInfo in the database
        List<CreditInfo> creditInfoList = creditInfoRepository.findAll();
        assertThat(creditInfoList).hasSize(databaseSizeBeforeUpdate);
        CreditInfo testCreditInfo = creditInfoList.get(creditInfoList.size() - 1);
        assertThat(testCreditInfo.getCredit()).isEqualTo(UPDATED_CREDIT);
        assertThat(testCreditInfo.getMethod()).isEqualTo(UPDATED_METHOD);
    }

    @Test
    @Transactional
    public void updateNonExistingCreditInfo() throws Exception {
        int databaseSizeBeforeUpdate = creditInfoRepository.findAll().size();

        // Create the CreditInfo
        CreditInfoDTO creditInfoDTO = creditInfoMapper.toDto(creditInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCreditInfoMockMvc.perform(put("/api/credit-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(creditInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the CreditInfo in the database
        List<CreditInfo> creditInfoList = creditInfoRepository.findAll();
        assertThat(creditInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCreditInfo() throws Exception {
        // Initialize the database
        creditInfoRepository.saveAndFlush(creditInfo);
        int databaseSizeBeforeDelete = creditInfoRepository.findAll().size();

        // Get the creditInfo
        restCreditInfoMockMvc.perform(delete("/api/credit-infos/{id}", creditInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CreditInfo> creditInfoList = creditInfoRepository.findAll();
        assertThat(creditInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CreditInfo.class);
        CreditInfo creditInfo1 = new CreditInfo();
        creditInfo1.setId(1L);
        CreditInfo creditInfo2 = new CreditInfo();
        creditInfo2.setId(creditInfo1.getId());
        assertThat(creditInfo1).isEqualTo(creditInfo2);
        creditInfo2.setId(2L);
        assertThat(creditInfo1).isNotEqualTo(creditInfo2);
        creditInfo1.setId(null);
        assertThat(creditInfo1).isNotEqualTo(creditInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CreditInfoDTO.class);
        CreditInfoDTO creditInfoDTO1 = new CreditInfoDTO();
        creditInfoDTO1.setId(1L);
        CreditInfoDTO creditInfoDTO2 = new CreditInfoDTO();
        assertThat(creditInfoDTO1).isNotEqualTo(creditInfoDTO2);
        creditInfoDTO2.setId(creditInfoDTO1.getId());
        assertThat(creditInfoDTO1).isEqualTo(creditInfoDTO2);
        creditInfoDTO2.setId(2L);
        assertThat(creditInfoDTO1).isNotEqualTo(creditInfoDTO2);
        creditInfoDTO1.setId(null);
        assertThat(creditInfoDTO1).isNotEqualTo(creditInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(creditInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(creditInfoMapper.fromId(null)).isNull();
    }
}
