package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.ReviewInfo;
import com.mycompany.myapp.repository.ReviewInfoRepository;
import com.mycompany.myapp.service.ReviewInfoService;
import com.mycompany.myapp.service.dto.ReviewInfoDTO;
import com.mycompany.myapp.service.mapper.ReviewInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReviewInfoResource REST controller.
 *
 * @see ReviewInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class ReviewInfoResourceIntTest {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_REVIEWS = 1;
    private static final Integer UPDATED_NUM_REVIEWS = 2;

    private static final Float DEFAULT_AVERAGE_RATING = 1F;
    private static final Float UPDATED_AVERAGE_RATING = 2F;

    @Autowired
    private ReviewInfoRepository reviewInfoRepository;

    @Autowired
    private ReviewInfoMapper reviewInfoMapper;

    @Autowired
    private ReviewInfoService reviewInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restReviewInfoMockMvc;

    private ReviewInfo reviewInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReviewInfoResource reviewInfoResource = new ReviewInfoResource(reviewInfoService);
        this.restReviewInfoMockMvc = MockMvcBuilders.standaloneSetup(reviewInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReviewInfo createEntity(EntityManager em) {
        ReviewInfo reviewInfo = new ReviewInfo()
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .numReviews(DEFAULT_NUM_REVIEWS)
            .averageRating(DEFAULT_AVERAGE_RATING);
        return reviewInfo;
    }

    @Before
    public void initTest() {
        reviewInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createReviewInfo() throws Exception {
        int databaseSizeBeforeCreate = reviewInfoRepository.findAll().size();

        // Create the ReviewInfo
        ReviewInfoDTO reviewInfoDTO = reviewInfoMapper.toDto(reviewInfo);
        restReviewInfoMockMvc.perform(post("/api/review-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reviewInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the ReviewInfo in the database
        List<ReviewInfo> reviewInfoList = reviewInfoRepository.findAll();
        assertThat(reviewInfoList).hasSize(databaseSizeBeforeCreate + 1);
        ReviewInfo testReviewInfo = reviewInfoList.get(reviewInfoList.size() - 1);
        assertThat(testReviewInfo.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testReviewInfo.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testReviewInfo.getNumReviews()).isEqualTo(DEFAULT_NUM_REVIEWS);
        assertThat(testReviewInfo.getAverageRating()).isEqualTo(DEFAULT_AVERAGE_RATING);
    }

    @Test
    @Transactional
    public void createReviewInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = reviewInfoRepository.findAll().size();

        // Create the ReviewInfo with an existing ID
        reviewInfo.setId(1L);
        ReviewInfoDTO reviewInfoDTO = reviewInfoMapper.toDto(reviewInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReviewInfoMockMvc.perform(post("/api/review-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reviewInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ReviewInfo> reviewInfoList = reviewInfoRepository.findAll();
        assertThat(reviewInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllReviewInfos() throws Exception {
        // Initialize the database
        reviewInfoRepository.saveAndFlush(reviewInfo);

        // Get all the reviewInfoList
        restReviewInfoMockMvc.perform(get("/api/review-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reviewInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].numReviews").value(hasItem(DEFAULT_NUM_REVIEWS)))
            .andExpect(jsonPath("$.[*].averageRating").value(hasItem(DEFAULT_AVERAGE_RATING.doubleValue())));
    }

    @Test
    @Transactional
    public void getReviewInfo() throws Exception {
        // Initialize the database
        reviewInfoRepository.saveAndFlush(reviewInfo);

        // Get the reviewInfo
        restReviewInfoMockMvc.perform(get("/api/review-infos/{id}", reviewInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(reviewInfo.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.numReviews").value(DEFAULT_NUM_REVIEWS))
            .andExpect(jsonPath("$.averageRating").value(DEFAULT_AVERAGE_RATING.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingReviewInfo() throws Exception {
        // Get the reviewInfo
        restReviewInfoMockMvc.perform(get("/api/review-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReviewInfo() throws Exception {
        // Initialize the database
        reviewInfoRepository.saveAndFlush(reviewInfo);
        int databaseSizeBeforeUpdate = reviewInfoRepository.findAll().size();

        // Update the reviewInfo
        ReviewInfo updatedReviewInfo = reviewInfoRepository.findOne(reviewInfo.getId());
        updatedReviewInfo
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .numReviews(UPDATED_NUM_REVIEWS)
            .averageRating(UPDATED_AVERAGE_RATING);
        ReviewInfoDTO reviewInfoDTO = reviewInfoMapper.toDto(updatedReviewInfo);

        restReviewInfoMockMvc.perform(put("/api/review-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reviewInfoDTO)))
            .andExpect(status().isOk());

        // Validate the ReviewInfo in the database
        List<ReviewInfo> reviewInfoList = reviewInfoRepository.findAll();
        assertThat(reviewInfoList).hasSize(databaseSizeBeforeUpdate);
        ReviewInfo testReviewInfo = reviewInfoList.get(reviewInfoList.size() - 1);
        assertThat(testReviewInfo.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testReviewInfo.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testReviewInfo.getNumReviews()).isEqualTo(UPDATED_NUM_REVIEWS);
        assertThat(testReviewInfo.getAverageRating()).isEqualTo(UPDATED_AVERAGE_RATING);
    }

    @Test
    @Transactional
    public void updateNonExistingReviewInfo() throws Exception {
        int databaseSizeBeforeUpdate = reviewInfoRepository.findAll().size();

        // Create the ReviewInfo
        ReviewInfoDTO reviewInfoDTO = reviewInfoMapper.toDto(reviewInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restReviewInfoMockMvc.perform(put("/api/review-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reviewInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the ReviewInfo in the database
        List<ReviewInfo> reviewInfoList = reviewInfoRepository.findAll();
        assertThat(reviewInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteReviewInfo() throws Exception {
        // Initialize the database
        reviewInfoRepository.saveAndFlush(reviewInfo);
        int databaseSizeBeforeDelete = reviewInfoRepository.findAll().size();

        // Get the reviewInfo
        restReviewInfoMockMvc.perform(delete("/api/review-infos/{id}", reviewInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ReviewInfo> reviewInfoList = reviewInfoRepository.findAll();
        assertThat(reviewInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReviewInfo.class);
        ReviewInfo reviewInfo1 = new ReviewInfo();
        reviewInfo1.setId(1L);
        ReviewInfo reviewInfo2 = new ReviewInfo();
        reviewInfo2.setId(reviewInfo1.getId());
        assertThat(reviewInfo1).isEqualTo(reviewInfo2);
        reviewInfo2.setId(2L);
        assertThat(reviewInfo1).isNotEqualTo(reviewInfo2);
        reviewInfo1.setId(null);
        assertThat(reviewInfo1).isNotEqualTo(reviewInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReviewInfoDTO.class);
        ReviewInfoDTO reviewInfoDTO1 = new ReviewInfoDTO();
        reviewInfoDTO1.setId(1L);
        ReviewInfoDTO reviewInfoDTO2 = new ReviewInfoDTO();
        assertThat(reviewInfoDTO1).isNotEqualTo(reviewInfoDTO2);
        reviewInfoDTO2.setId(reviewInfoDTO1.getId());
        assertThat(reviewInfoDTO1).isEqualTo(reviewInfoDTO2);
        reviewInfoDTO2.setId(2L);
        assertThat(reviewInfoDTO1).isNotEqualTo(reviewInfoDTO2);
        reviewInfoDTO1.setId(null);
        assertThat(reviewInfoDTO1).isNotEqualTo(reviewInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(reviewInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(reviewInfoMapper.fromId(null)).isNull();
    }
}
