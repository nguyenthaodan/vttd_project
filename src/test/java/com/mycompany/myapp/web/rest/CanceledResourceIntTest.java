package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.Canceled;
import com.mycompany.myapp.repository.CanceledRepository;
import com.mycompany.myapp.service.CanceledService;
import com.mycompany.myapp.service.dto.CanceledDTO;
import com.mycompany.myapp.service.mapper.CanceledMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CanceledResource REST controller.
 *
 * @see CanceledResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class CanceledResourceIntTest {

    private static final LocalDate DEFAULT_CANCEL_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CANCEL_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private CanceledRepository canceledRepository;

    @Autowired
    private CanceledMapper canceledMapper;

    @Autowired
    private CanceledService canceledService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCanceledMockMvc;

    private Canceled canceled;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CanceledResource canceledResource = new CanceledResource(canceledService);
        this.restCanceledMockMvc = MockMvcBuilders.standaloneSetup(canceledResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Canceled createEntity(EntityManager em) {
        Canceled canceled = new Canceled()
            .cancelDate(DEFAULT_CANCEL_DATE)
            .reason(DEFAULT_REASON);
        return canceled;
    }

    @Before
    public void initTest() {
        canceled = createEntity(em);
    }

    @Test
    @Transactional
    public void createCanceled() throws Exception {
        int databaseSizeBeforeCreate = canceledRepository.findAll().size();

        // Create the Canceled
        CanceledDTO canceledDTO = canceledMapper.toDto(canceled);
        restCanceledMockMvc.perform(post("/api/canceleds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(canceledDTO)))
            .andExpect(status().isCreated());

        // Validate the Canceled in the database
        List<Canceled> canceledList = canceledRepository.findAll();
        assertThat(canceledList).hasSize(databaseSizeBeforeCreate + 1);
        Canceled testCanceled = canceledList.get(canceledList.size() - 1);
        assertThat(testCanceled.getCancelDate()).isEqualTo(DEFAULT_CANCEL_DATE);
        assertThat(testCanceled.getReason()).isEqualTo(DEFAULT_REASON);
    }

    @Test
    @Transactional
    public void createCanceledWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = canceledRepository.findAll().size();

        // Create the Canceled with an existing ID
        canceled.setId(1L);
        CanceledDTO canceledDTO = canceledMapper.toDto(canceled);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCanceledMockMvc.perform(post("/api/canceleds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(canceledDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Canceled> canceledList = canceledRepository.findAll();
        assertThat(canceledList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCanceleds() throws Exception {
        // Initialize the database
        canceledRepository.saveAndFlush(canceled);

        // Get all the canceledList
        restCanceledMockMvc.perform(get("/api/canceleds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(canceled.getId().intValue())))
            .andExpect(jsonPath("$.[*].cancelDate").value(hasItem(DEFAULT_CANCEL_DATE.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }

    @Test
    @Transactional
    public void getCanceled() throws Exception {
        // Initialize the database
        canceledRepository.saveAndFlush(canceled);

        // Get the canceled
        restCanceledMockMvc.perform(get("/api/canceleds/{id}", canceled.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(canceled.getId().intValue()))
            .andExpect(jsonPath("$.cancelDate").value(DEFAULT_CANCEL_DATE.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCanceled() throws Exception {
        // Get the canceled
        restCanceledMockMvc.perform(get("/api/canceleds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCanceled() throws Exception {
        // Initialize the database
        canceledRepository.saveAndFlush(canceled);
        int databaseSizeBeforeUpdate = canceledRepository.findAll().size();

        // Update the canceled
        Canceled updatedCanceled = canceledRepository.findOne(canceled.getId());
        updatedCanceled
            .cancelDate(UPDATED_CANCEL_DATE)
            .reason(UPDATED_REASON);
        CanceledDTO canceledDTO = canceledMapper.toDto(updatedCanceled);

        restCanceledMockMvc.perform(put("/api/canceleds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(canceledDTO)))
            .andExpect(status().isOk());

        // Validate the Canceled in the database
        List<Canceled> canceledList = canceledRepository.findAll();
        assertThat(canceledList).hasSize(databaseSizeBeforeUpdate);
        Canceled testCanceled = canceledList.get(canceledList.size() - 1);
        assertThat(testCanceled.getCancelDate()).isEqualTo(UPDATED_CANCEL_DATE);
        assertThat(testCanceled.getReason()).isEqualTo(UPDATED_REASON);
    }

    @Test
    @Transactional
    public void updateNonExistingCanceled() throws Exception {
        int databaseSizeBeforeUpdate = canceledRepository.findAll().size();

        // Create the Canceled
        CanceledDTO canceledDTO = canceledMapper.toDto(canceled);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCanceledMockMvc.perform(put("/api/canceleds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(canceledDTO)))
            .andExpect(status().isCreated());

        // Validate the Canceled in the database
        List<Canceled> canceledList = canceledRepository.findAll();
        assertThat(canceledList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCanceled() throws Exception {
        // Initialize the database
        canceledRepository.saveAndFlush(canceled);
        int databaseSizeBeforeDelete = canceledRepository.findAll().size();

        // Get the canceled
        restCanceledMockMvc.perform(delete("/api/canceleds/{id}", canceled.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Canceled> canceledList = canceledRepository.findAll();
        assertThat(canceledList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Canceled.class);
        Canceled canceled1 = new Canceled();
        canceled1.setId(1L);
        Canceled canceled2 = new Canceled();
        canceled2.setId(canceled1.getId());
        assertThat(canceled1).isEqualTo(canceled2);
        canceled2.setId(2L);
        assertThat(canceled1).isNotEqualTo(canceled2);
        canceled1.setId(null);
        assertThat(canceled1).isNotEqualTo(canceled2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CanceledDTO.class);
        CanceledDTO canceledDTO1 = new CanceledDTO();
        canceledDTO1.setId(1L);
        CanceledDTO canceledDTO2 = new CanceledDTO();
        assertThat(canceledDTO1).isNotEqualTo(canceledDTO2);
        canceledDTO2.setId(canceledDTO1.getId());
        assertThat(canceledDTO1).isEqualTo(canceledDTO2);
        canceledDTO2.setId(2L);
        assertThat(canceledDTO1).isNotEqualTo(canceledDTO2);
        canceledDTO1.setId(null);
        assertThat(canceledDTO1).isNotEqualTo(canceledDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(canceledMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(canceledMapper.fromId(null)).isNull();
    }
}
