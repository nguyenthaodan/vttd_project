package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.PushInfo;
import com.mycompany.myapp.repository.PushInfoRepository;
import com.mycompany.myapp.service.PushInfoService;
import com.mycompany.myapp.service.dto.PushInfoDTO;
import com.mycompany.myapp.service.mapper.PushInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PushInfoResource REST controller.
 *
 * @see PushInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class PushInfoResourceIntTest {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private PushInfoRepository pushInfoRepository;

    @Autowired
    private PushInfoMapper pushInfoMapper;

    @Autowired
    private PushInfoService pushInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPushInfoMockMvc;

    private PushInfo pushInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PushInfoResource pushInfoResource = new PushInfoResource(pushInfoService);
        this.restPushInfoMockMvc = MockMvcBuilders.standaloneSetup(pushInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PushInfo createEntity(EntityManager em) {
        PushInfo pushInfo = new PushInfo()
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD);
        return pushInfo;
    }

    @Before
    public void initTest() {
        pushInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createPushInfo() throws Exception {
        int databaseSizeBeforeCreate = pushInfoRepository.findAll().size();

        // Create the PushInfo
        PushInfoDTO pushInfoDTO = pushInfoMapper.toDto(pushInfo);
        restPushInfoMockMvc.perform(post("/api/push-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pushInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the PushInfo in the database
        List<PushInfo> pushInfoList = pushInfoRepository.findAll();
        assertThat(pushInfoList).hasSize(databaseSizeBeforeCreate + 1);
        PushInfo testPushInfo = pushInfoList.get(pushInfoList.size() - 1);
        assertThat(testPushInfo.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testPushInfo.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createPushInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pushInfoRepository.findAll().size();

        // Create the PushInfo with an existing ID
        pushInfo.setId(1L);
        PushInfoDTO pushInfoDTO = pushInfoMapper.toDto(pushInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPushInfoMockMvc.perform(post("/api/push-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pushInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PushInfo> pushInfoList = pushInfoRepository.findAll();
        assertThat(pushInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPushInfos() throws Exception {
        // Initialize the database
        pushInfoRepository.saveAndFlush(pushInfo);

        // Get all the pushInfoList
        restPushInfoMockMvc.perform(get("/api/push-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pushInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())));
    }

    @Test
    @Transactional
    public void getPushInfo() throws Exception {
        // Initialize the database
        pushInfoRepository.saveAndFlush(pushInfo);

        // Get the pushInfo
        restPushInfoMockMvc.perform(get("/api/push-infos/{id}", pushInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pushInfo.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPushInfo() throws Exception {
        // Get the pushInfo
        restPushInfoMockMvc.perform(get("/api/push-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePushInfo() throws Exception {
        // Initialize the database
        pushInfoRepository.saveAndFlush(pushInfo);
        int databaseSizeBeforeUpdate = pushInfoRepository.findAll().size();

        // Update the pushInfo
        PushInfo updatedPushInfo = pushInfoRepository.findOne(pushInfo.getId());
        updatedPushInfo
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD);
        PushInfoDTO pushInfoDTO = pushInfoMapper.toDto(updatedPushInfo);

        restPushInfoMockMvc.perform(put("/api/push-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pushInfoDTO)))
            .andExpect(status().isOk());

        // Validate the PushInfo in the database
        List<PushInfo> pushInfoList = pushInfoRepository.findAll();
        assertThat(pushInfoList).hasSize(databaseSizeBeforeUpdate);
        PushInfo testPushInfo = pushInfoList.get(pushInfoList.size() - 1);
        assertThat(testPushInfo.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testPushInfo.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingPushInfo() throws Exception {
        int databaseSizeBeforeUpdate = pushInfoRepository.findAll().size();

        // Create the PushInfo
        PushInfoDTO pushInfoDTO = pushInfoMapper.toDto(pushInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPushInfoMockMvc.perform(put("/api/push-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pushInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the PushInfo in the database
        List<PushInfo> pushInfoList = pushInfoRepository.findAll();
        assertThat(pushInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePushInfo() throws Exception {
        // Initialize the database
        pushInfoRepository.saveAndFlush(pushInfo);
        int databaseSizeBeforeDelete = pushInfoRepository.findAll().size();

        // Get the pushInfo
        restPushInfoMockMvc.perform(delete("/api/push-infos/{id}", pushInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PushInfo> pushInfoList = pushInfoRepository.findAll();
        assertThat(pushInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PushInfo.class);
        PushInfo pushInfo1 = new PushInfo();
        pushInfo1.setId(1L);
        PushInfo pushInfo2 = new PushInfo();
        pushInfo2.setId(pushInfo1.getId());
        assertThat(pushInfo1).isEqualTo(pushInfo2);
        pushInfo2.setId(2L);
        assertThat(pushInfo1).isNotEqualTo(pushInfo2);
        pushInfo1.setId(null);
        assertThat(pushInfo1).isNotEqualTo(pushInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PushInfoDTO.class);
        PushInfoDTO pushInfoDTO1 = new PushInfoDTO();
        pushInfoDTO1.setId(1L);
        PushInfoDTO pushInfoDTO2 = new PushInfoDTO();
        assertThat(pushInfoDTO1).isNotEqualTo(pushInfoDTO2);
        pushInfoDTO2.setId(pushInfoDTO1.getId());
        assertThat(pushInfoDTO1).isEqualTo(pushInfoDTO2);
        pushInfoDTO2.setId(2L);
        assertThat(pushInfoDTO1).isNotEqualTo(pushInfoDTO2);
        pushInfoDTO1.setId(null);
        assertThat(pushInfoDTO1).isNotEqualTo(pushInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pushInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pushInfoMapper.fromId(null)).isNull();
    }
}
