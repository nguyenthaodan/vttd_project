package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.Bidding;
import com.mycompany.myapp.repository.BiddingRepository;
import com.mycompany.myapp.service.BiddingService;
import com.mycompany.myapp.service.dto.BiddingDTO;
import com.mycompany.myapp.service.mapper.BiddingMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BiddingResource REST controller.
 *
 * @see BiddingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class BiddingResourceIntTest {

    private static final LocalDate DEFAULT_CLOSING_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CLOSING_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_BIDDERS_COUNT = 1;
    private static final Integer UPDATED_BIDDERS_COUNT = 2;

    @Autowired
    private BiddingRepository biddingRepository;

    @Autowired
    private BiddingMapper biddingMapper;

    @Autowired
    private BiddingService biddingService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBiddingMockMvc;

    private Bidding bidding;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BiddingResource biddingResource = new BiddingResource(biddingService);
        this.restBiddingMockMvc = MockMvcBuilders.standaloneSetup(biddingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bidding createEntity(EntityManager em) {
        Bidding bidding = new Bidding()
            .closingDate(DEFAULT_CLOSING_DATE)
            .biddersCount(DEFAULT_BIDDERS_COUNT);
        return bidding;
    }

    @Before
    public void initTest() {
        bidding = createEntity(em);
    }

    @Test
    @Transactional
    public void createBidding() throws Exception {
        int databaseSizeBeforeCreate = biddingRepository.findAll().size();

        // Create the Bidding
        BiddingDTO biddingDTO = biddingMapper.toDto(bidding);
        restBiddingMockMvc.perform(post("/api/biddings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biddingDTO)))
            .andExpect(status().isCreated());

        // Validate the Bidding in the database
        List<Bidding> biddingList = biddingRepository.findAll();
        assertThat(biddingList).hasSize(databaseSizeBeforeCreate + 1);
        Bidding testBidding = biddingList.get(biddingList.size() - 1);
        assertThat(testBidding.getClosingDate()).isEqualTo(DEFAULT_CLOSING_DATE);
        assertThat(testBidding.getBiddersCount()).isEqualTo(DEFAULT_BIDDERS_COUNT);
    }

    @Test
    @Transactional
    public void createBiddingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = biddingRepository.findAll().size();

        // Create the Bidding with an existing ID
        bidding.setId(1L);
        BiddingDTO biddingDTO = biddingMapper.toDto(bidding);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBiddingMockMvc.perform(post("/api/biddings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biddingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Bidding> biddingList = biddingRepository.findAll();
        assertThat(biddingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBiddings() throws Exception {
        // Initialize the database
        biddingRepository.saveAndFlush(bidding);

        // Get all the biddingList
        restBiddingMockMvc.perform(get("/api/biddings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bidding.getId().intValue())))
            .andExpect(jsonPath("$.[*].closingDate").value(hasItem(DEFAULT_CLOSING_DATE.toString())))
            .andExpect(jsonPath("$.[*].biddersCount").value(hasItem(DEFAULT_BIDDERS_COUNT)));
    }

    @Test
    @Transactional
    public void getBidding() throws Exception {
        // Initialize the database
        biddingRepository.saveAndFlush(bidding);

        // Get the bidding
        restBiddingMockMvc.perform(get("/api/biddings/{id}", bidding.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bidding.getId().intValue()))
            .andExpect(jsonPath("$.closingDate").value(DEFAULT_CLOSING_DATE.toString()))
            .andExpect(jsonPath("$.biddersCount").value(DEFAULT_BIDDERS_COUNT));
    }

    @Test
    @Transactional
    public void getNonExistingBidding() throws Exception {
        // Get the bidding
        restBiddingMockMvc.perform(get("/api/biddings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBidding() throws Exception {
        // Initialize the database
        biddingRepository.saveAndFlush(bidding);
        int databaseSizeBeforeUpdate = biddingRepository.findAll().size();

        // Update the bidding
        Bidding updatedBidding = biddingRepository.findOne(bidding.getId());
        updatedBidding
            .closingDate(UPDATED_CLOSING_DATE)
            .biddersCount(UPDATED_BIDDERS_COUNT);
        BiddingDTO biddingDTO = biddingMapper.toDto(updatedBidding);

        restBiddingMockMvc.perform(put("/api/biddings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biddingDTO)))
            .andExpect(status().isOk());

        // Validate the Bidding in the database
        List<Bidding> biddingList = biddingRepository.findAll();
        assertThat(biddingList).hasSize(databaseSizeBeforeUpdate);
        Bidding testBidding = biddingList.get(biddingList.size() - 1);
        assertThat(testBidding.getClosingDate()).isEqualTo(UPDATED_CLOSING_DATE);
        assertThat(testBidding.getBiddersCount()).isEqualTo(UPDATED_BIDDERS_COUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingBidding() throws Exception {
        int databaseSizeBeforeUpdate = biddingRepository.findAll().size();

        // Create the Bidding
        BiddingDTO biddingDTO = biddingMapper.toDto(bidding);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBiddingMockMvc.perform(put("/api/biddings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biddingDTO)))
            .andExpect(status().isCreated());

        // Validate the Bidding in the database
        List<Bidding> biddingList = biddingRepository.findAll();
        assertThat(biddingList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBidding() throws Exception {
        // Initialize the database
        biddingRepository.saveAndFlush(bidding);
        int databaseSizeBeforeDelete = biddingRepository.findAll().size();

        // Get the bidding
        restBiddingMockMvc.perform(delete("/api/biddings/{id}", bidding.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Bidding> biddingList = biddingRepository.findAll();
        assertThat(biddingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Bidding.class);
        Bidding bidding1 = new Bidding();
        bidding1.setId(1L);
        Bidding bidding2 = new Bidding();
        bidding2.setId(bidding1.getId());
        assertThat(bidding1).isEqualTo(bidding2);
        bidding2.setId(2L);
        assertThat(bidding1).isNotEqualTo(bidding2);
        bidding1.setId(null);
        assertThat(bidding1).isNotEqualTo(bidding2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BiddingDTO.class);
        BiddingDTO biddingDTO1 = new BiddingDTO();
        biddingDTO1.setId(1L);
        BiddingDTO biddingDTO2 = new BiddingDTO();
        assertThat(biddingDTO1).isNotEqualTo(biddingDTO2);
        biddingDTO2.setId(biddingDTO1.getId());
        assertThat(biddingDTO1).isEqualTo(biddingDTO2);
        biddingDTO2.setId(2L);
        assertThat(biddingDTO1).isNotEqualTo(biddingDTO2);
        biddingDTO1.setId(null);
        assertThat(biddingDTO1).isNotEqualTo(biddingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(biddingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(biddingMapper.fromId(null)).isNull();
    }
}
