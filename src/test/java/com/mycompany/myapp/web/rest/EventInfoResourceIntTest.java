package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.EventInfo;
import com.mycompany.myapp.repository.EventInfoRepository;
import com.mycompany.myapp.service.EventInfoService;
import com.mycompany.myapp.service.dto.EventInfoDTO;
import com.mycompany.myapp.service.mapper.EventInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EventInfoResource REST controller.
 *
 * @see EventInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class EventInfoResourceIntTest {

    @Autowired
    private EventInfoRepository eventInfoRepository;

    @Autowired
    private EventInfoMapper eventInfoMapper;

    @Autowired
    private EventInfoService eventInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEventInfoMockMvc;

    private EventInfo eventInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EventInfoResource eventInfoResource = new EventInfoResource(eventInfoService);
        this.restEventInfoMockMvc = MockMvcBuilders.standaloneSetup(eventInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventInfo createEntity(EntityManager em) {
        EventInfo eventInfo = new EventInfo();
        return eventInfo;
    }

    @Before
    public void initTest() {
        eventInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createEventInfo() throws Exception {
        int databaseSizeBeforeCreate = eventInfoRepository.findAll().size();

        // Create the EventInfo
        EventInfoDTO eventInfoDTO = eventInfoMapper.toDto(eventInfo);
        restEventInfoMockMvc.perform(post("/api/event-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the EventInfo in the database
        List<EventInfo> eventInfoList = eventInfoRepository.findAll();
        assertThat(eventInfoList).hasSize(databaseSizeBeforeCreate + 1);
        EventInfo testEventInfo = eventInfoList.get(eventInfoList.size() - 1);
    }

    @Test
    @Transactional
    public void createEventInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = eventInfoRepository.findAll().size();

        // Create the EventInfo with an existing ID
        eventInfo.setId(1L);
        EventInfoDTO eventInfoDTO = eventInfoMapper.toDto(eventInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEventInfoMockMvc.perform(post("/api/event-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<EventInfo> eventInfoList = eventInfoRepository.findAll();
        assertThat(eventInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEventInfos() throws Exception {
        // Initialize the database
        eventInfoRepository.saveAndFlush(eventInfo);

        // Get all the eventInfoList
        restEventInfoMockMvc.perform(get("/api/event-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eventInfo.getId().intValue())));
    }

    @Test
    @Transactional
    public void getEventInfo() throws Exception {
        // Initialize the database
        eventInfoRepository.saveAndFlush(eventInfo);

        // Get the eventInfo
        restEventInfoMockMvc.perform(get("/api/event-infos/{id}", eventInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(eventInfo.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEventInfo() throws Exception {
        // Get the eventInfo
        restEventInfoMockMvc.perform(get("/api/event-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEventInfo() throws Exception {
        // Initialize the database
        eventInfoRepository.saveAndFlush(eventInfo);
        int databaseSizeBeforeUpdate = eventInfoRepository.findAll().size();

        // Update the eventInfo
        EventInfo updatedEventInfo = eventInfoRepository.findOne(eventInfo.getId());
        EventInfoDTO eventInfoDTO = eventInfoMapper.toDto(updatedEventInfo);

        restEventInfoMockMvc.perform(put("/api/event-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventInfoDTO)))
            .andExpect(status().isOk());

        // Validate the EventInfo in the database
        List<EventInfo> eventInfoList = eventInfoRepository.findAll();
        assertThat(eventInfoList).hasSize(databaseSizeBeforeUpdate);
        EventInfo testEventInfo = eventInfoList.get(eventInfoList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingEventInfo() throws Exception {
        int databaseSizeBeforeUpdate = eventInfoRepository.findAll().size();

        // Create the EventInfo
        EventInfoDTO eventInfoDTO = eventInfoMapper.toDto(eventInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEventInfoMockMvc.perform(put("/api/event-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eventInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the EventInfo in the database
        List<EventInfo> eventInfoList = eventInfoRepository.findAll();
        assertThat(eventInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEventInfo() throws Exception {
        // Initialize the database
        eventInfoRepository.saveAndFlush(eventInfo);
        int databaseSizeBeforeDelete = eventInfoRepository.findAll().size();

        // Get the eventInfo
        restEventInfoMockMvc.perform(delete("/api/event-infos/{id}", eventInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EventInfo> eventInfoList = eventInfoRepository.findAll();
        assertThat(eventInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventInfo.class);
        EventInfo eventInfo1 = new EventInfo();
        eventInfo1.setId(1L);
        EventInfo eventInfo2 = new EventInfo();
        eventInfo2.setId(eventInfo1.getId());
        assertThat(eventInfo1).isEqualTo(eventInfo2);
        eventInfo2.setId(2L);
        assertThat(eventInfo1).isNotEqualTo(eventInfo2);
        eventInfo1.setId(null);
        assertThat(eventInfo1).isNotEqualTo(eventInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventInfoDTO.class);
        EventInfoDTO eventInfoDTO1 = new EventInfoDTO();
        eventInfoDTO1.setId(1L);
        EventInfoDTO eventInfoDTO2 = new EventInfoDTO();
        assertThat(eventInfoDTO1).isNotEqualTo(eventInfoDTO2);
        eventInfoDTO2.setId(eventInfoDTO1.getId());
        assertThat(eventInfoDTO1).isEqualTo(eventInfoDTO2);
        eventInfoDTO2.setId(2L);
        assertThat(eventInfoDTO1).isNotEqualTo(eventInfoDTO2);
        eventInfoDTO1.setId(null);
        assertThat(eventInfoDTO1).isNotEqualTo(eventInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(eventInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(eventInfoMapper.fromId(null)).isNull();
    }
}
