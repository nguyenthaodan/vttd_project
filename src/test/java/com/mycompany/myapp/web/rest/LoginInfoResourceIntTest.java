package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.LoginInfo;
import com.mycompany.myapp.repository.LoginInfoRepository;
import com.mycompany.myapp.service.LoginInfoService;
import com.mycompany.myapp.service.dto.LoginInfoDTO;
import com.mycompany.myapp.service.mapper.LoginInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LoginInfoResource REST controller.
 *
 * @see LoginInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class LoginInfoResourceIntTest {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    @Autowired
    private LoginInfoRepository loginInfoRepository;

    @Autowired
    private LoginInfoMapper loginInfoMapper;

    @Autowired
    private LoginInfoService loginInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLoginInfoMockMvc;

    private LoginInfo loginInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        LoginInfoResource loginInfoResource = new LoginInfoResource(loginInfoService);
        this.restLoginInfoMockMvc = MockMvcBuilders.standaloneSetup(loginInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LoginInfo createEntity(EntityManager em) {
        LoginInfo loginInfo = new LoginInfo()
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .email(DEFAULT_EMAIL);
        return loginInfo;
    }

    @Before
    public void initTest() {
        loginInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createLoginInfo() throws Exception {
        int databaseSizeBeforeCreate = loginInfoRepository.findAll().size();

        // Create the LoginInfo
        LoginInfoDTO loginInfoDTO = loginInfoMapper.toDto(loginInfo);
        restLoginInfoMockMvc.perform(post("/api/login-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(loginInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the LoginInfo in the database
        List<LoginInfo> loginInfoList = loginInfoRepository.findAll();
        assertThat(loginInfoList).hasSize(databaseSizeBeforeCreate + 1);
        LoginInfo testLoginInfo = loginInfoList.get(loginInfoList.size() - 1);
        assertThat(testLoginInfo.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testLoginInfo.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testLoginInfo.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    public void createLoginInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = loginInfoRepository.findAll().size();

        // Create the LoginInfo with an existing ID
        loginInfo.setId(1L);
        LoginInfoDTO loginInfoDTO = loginInfoMapper.toDto(loginInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLoginInfoMockMvc.perform(post("/api/login-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(loginInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<LoginInfo> loginInfoList = loginInfoRepository.findAll();
        assertThat(loginInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllLoginInfos() throws Exception {
        // Initialize the database
        loginInfoRepository.saveAndFlush(loginInfo);

        // Get all the loginInfoList
        restLoginInfoMockMvc.perform(get("/api/login-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(loginInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())));
    }

    @Test
    @Transactional
    public void getLoginInfo() throws Exception {
        // Initialize the database
        loginInfoRepository.saveAndFlush(loginInfo);

        // Get the loginInfo
        restLoginInfoMockMvc.perform(get("/api/login-infos/{id}", loginInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(loginInfo.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingLoginInfo() throws Exception {
        // Get the loginInfo
        restLoginInfoMockMvc.perform(get("/api/login-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLoginInfo() throws Exception {
        // Initialize the database
        loginInfoRepository.saveAndFlush(loginInfo);
        int databaseSizeBeforeUpdate = loginInfoRepository.findAll().size();

        // Update the loginInfo
        LoginInfo updatedLoginInfo = loginInfoRepository.findOne(loginInfo.getId());
        updatedLoginInfo
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .email(UPDATED_EMAIL);
        LoginInfoDTO loginInfoDTO = loginInfoMapper.toDto(updatedLoginInfo);

        restLoginInfoMockMvc.perform(put("/api/login-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(loginInfoDTO)))
            .andExpect(status().isOk());

        // Validate the LoginInfo in the database
        List<LoginInfo> loginInfoList = loginInfoRepository.findAll();
        assertThat(loginInfoList).hasSize(databaseSizeBeforeUpdate);
        LoginInfo testLoginInfo = loginInfoList.get(loginInfoList.size() - 1);
        assertThat(testLoginInfo.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testLoginInfo.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testLoginInfo.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void updateNonExistingLoginInfo() throws Exception {
        int databaseSizeBeforeUpdate = loginInfoRepository.findAll().size();

        // Create the LoginInfo
        LoginInfoDTO loginInfoDTO = loginInfoMapper.toDto(loginInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLoginInfoMockMvc.perform(put("/api/login-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(loginInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the LoginInfo in the database
        List<LoginInfo> loginInfoList = loginInfoRepository.findAll();
        assertThat(loginInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLoginInfo() throws Exception {
        // Initialize the database
        loginInfoRepository.saveAndFlush(loginInfo);
        int databaseSizeBeforeDelete = loginInfoRepository.findAll().size();

        // Get the loginInfo
        restLoginInfoMockMvc.perform(delete("/api/login-infos/{id}", loginInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LoginInfo> loginInfoList = loginInfoRepository.findAll();
        assertThat(loginInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoginInfo.class);
        LoginInfo loginInfo1 = new LoginInfo();
        loginInfo1.setId(1L);
        LoginInfo loginInfo2 = new LoginInfo();
        loginInfo2.setId(loginInfo1.getId());
        assertThat(loginInfo1).isEqualTo(loginInfo2);
        loginInfo2.setId(2L);
        assertThat(loginInfo1).isNotEqualTo(loginInfo2);
        loginInfo1.setId(null);
        assertThat(loginInfo1).isNotEqualTo(loginInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoginInfoDTO.class);
        LoginInfoDTO loginInfoDTO1 = new LoginInfoDTO();
        loginInfoDTO1.setId(1L);
        LoginInfoDTO loginInfoDTO2 = new LoginInfoDTO();
        assertThat(loginInfoDTO1).isNotEqualTo(loginInfoDTO2);
        loginInfoDTO2.setId(loginInfoDTO1.getId());
        assertThat(loginInfoDTO1).isEqualTo(loginInfoDTO2);
        loginInfoDTO2.setId(2L);
        assertThat(loginInfoDTO1).isNotEqualTo(loginInfoDTO2);
        loginInfoDTO1.setId(null);
        assertThat(loginInfoDTO1).isNotEqualTo(loginInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(loginInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(loginInfoMapper.fromId(null)).isNull();
    }
}
