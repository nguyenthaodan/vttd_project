package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.Working;
import com.mycompany.myapp.repository.WorkingRepository;
import com.mycompany.myapp.service.WorkingService;
import com.mycompany.myapp.service.dto.WorkingDTO;
import com.mycompany.myapp.service.mapper.WorkingMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.domain.enumeration.WorkingStatus;
import com.mycompany.myapp.domain.enumeration.WorkingStatus;
/**
 * Test class for the WorkingResource REST controller.
 *
 * @see WorkingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class WorkingResourceIntTest {

    private static final WorkingStatus DEFAULT_USERSTATUS = WorkingStatus.PENDING_MEETING;
    private static final WorkingStatus UPDATED_USERSTATUS = WorkingStatus.DOCUMENT_SENT;

    private static final WorkingStatus DEFAULT_SUPPLIERSTATUS = WorkingStatus.PENDING_MEETING;
    private static final WorkingStatus UPDATED_SUPPLIERSTATUS = WorkingStatus.DOCUMENT_SENT;

    private static final Float DEFAULT_AMOUNT_DEPOSIT = 1F;
    private static final Float UPDATED_AMOUNT_DEPOSIT = 2F;

    private static final Float DEFAULT_PRICE = 1F;
    private static final Float UPDATED_PRICE = 2F;

    private static final LocalDate DEFAULT_BOOK_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BOOK_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FINISH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FINISH_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LAST_MEETING = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MEETING = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_NEXT_MEETING = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_NEXT_MEETING = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private WorkingRepository workingRepository;

    @Autowired
    private WorkingMapper workingMapper;

    @Autowired
    private WorkingService workingService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restWorkingMockMvc;

    private Working working;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        WorkingResource workingResource = new WorkingResource(workingService);
        this.restWorkingMockMvc = MockMvcBuilders.standaloneSetup(workingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Working createEntity(EntityManager em) {
        Working working = new Working()
            .userstatus(DEFAULT_USERSTATUS)
            .supplierstatus(DEFAULT_SUPPLIERSTATUS)
            .amountDeposit(DEFAULT_AMOUNT_DEPOSIT)
            .price(DEFAULT_PRICE)
            .bookDate(DEFAULT_BOOK_DATE)
            .startDate(DEFAULT_START_DATE)
            .finishDate(DEFAULT_FINISH_DATE)
            .lastMeeting(DEFAULT_LAST_MEETING)
            .nextMeeting(DEFAULT_NEXT_MEETING);
        return working;
    }

    @Before
    public void initTest() {
        working = createEntity(em);
    }

    @Test
    @Transactional
    public void createWorking() throws Exception {
        int databaseSizeBeforeCreate = workingRepository.findAll().size();

        // Create the Working
        WorkingDTO workingDTO = workingMapper.toDto(working);
        restWorkingMockMvc.perform(post("/api/workings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(workingDTO)))
            .andExpect(status().isCreated());

        // Validate the Working in the database
        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeCreate + 1);
        Working testWorking = workingList.get(workingList.size() - 1);
        assertThat(testWorking.getUserstatus()).isEqualTo(DEFAULT_USERSTATUS);
        assertThat(testWorking.getSupplierstatus()).isEqualTo(DEFAULT_SUPPLIERSTATUS);
        assertThat(testWorking.getAmountDeposit()).isEqualTo(DEFAULT_AMOUNT_DEPOSIT);
        assertThat(testWorking.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testWorking.getBookDate()).isEqualTo(DEFAULT_BOOK_DATE);
        assertThat(testWorking.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testWorking.getFinishDate()).isEqualTo(DEFAULT_FINISH_DATE);
        assertThat(testWorking.getLastMeeting()).isEqualTo(DEFAULT_LAST_MEETING);
        assertThat(testWorking.getNextMeeting()).isEqualTo(DEFAULT_NEXT_MEETING);
    }

    @Test
    @Transactional
    public void createWorkingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = workingRepository.findAll().size();

        // Create the Working with an existing ID
        working.setId(1L);
        WorkingDTO workingDTO = workingMapper.toDto(working);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWorkingMockMvc.perform(post("/api/workings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(workingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = workingRepository.findAll().size();
        // set the field null
        working.setPrice(null);

        // Create the Working, which fails.
        WorkingDTO workingDTO = workingMapper.toDto(working);

        restWorkingMockMvc.perform(post("/api/workings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(workingDTO)))
            .andExpect(status().isBadRequest());

        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastMeetingIsRequired() throws Exception {
        int databaseSizeBeforeTest = workingRepository.findAll().size();
        // set the field null
        working.setLastMeeting(null);

        // Create the Working, which fails.
        WorkingDTO workingDTO = workingMapper.toDto(working);

        restWorkingMockMvc.perform(post("/api/workings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(workingDTO)))
            .andExpect(status().isBadRequest());

        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNextMeetingIsRequired() throws Exception {
        int databaseSizeBeforeTest = workingRepository.findAll().size();
        // set the field null
        working.setNextMeeting(null);

        // Create the Working, which fails.
        WorkingDTO workingDTO = workingMapper.toDto(working);

        restWorkingMockMvc.perform(post("/api/workings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(workingDTO)))
            .andExpect(status().isBadRequest());

        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllWorkings() throws Exception {
        // Initialize the database
        workingRepository.saveAndFlush(working);

        // Get all the workingList
        restWorkingMockMvc.perform(get("/api/workings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(working.getId().intValue())))
            .andExpect(jsonPath("$.[*].userstatus").value(hasItem(DEFAULT_USERSTATUS.toString())))
            .andExpect(jsonPath("$.[*].supplierstatus").value(hasItem(DEFAULT_SUPPLIERSTATUS.toString())))
            .andExpect(jsonPath("$.[*].amountDeposit").value(hasItem(DEFAULT_AMOUNT_DEPOSIT.doubleValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].bookDate").value(hasItem(DEFAULT_BOOK_DATE.toString())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].finishDate").value(hasItem(DEFAULT_FINISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastMeeting").value(hasItem(DEFAULT_LAST_MEETING.toString())))
            .andExpect(jsonPath("$.[*].nextMeeting").value(hasItem(DEFAULT_NEXT_MEETING.toString())));
    }

    @Test
    @Transactional
    public void getWorking() throws Exception {
        // Initialize the database
        workingRepository.saveAndFlush(working);

        // Get the working
        restWorkingMockMvc.perform(get("/api/workings/{id}", working.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(working.getId().intValue()))
            .andExpect(jsonPath("$.userstatus").value(DEFAULT_USERSTATUS.toString()))
            .andExpect(jsonPath("$.supplierstatus").value(DEFAULT_SUPPLIERSTATUS.toString()))
            .andExpect(jsonPath("$.amountDeposit").value(DEFAULT_AMOUNT_DEPOSIT.doubleValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.bookDate").value(DEFAULT_BOOK_DATE.toString()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.finishDate").value(DEFAULT_FINISH_DATE.toString()))
            .andExpect(jsonPath("$.lastMeeting").value(DEFAULT_LAST_MEETING.toString()))
            .andExpect(jsonPath("$.nextMeeting").value(DEFAULT_NEXT_MEETING.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingWorking() throws Exception {
        // Get the working
        restWorkingMockMvc.perform(get("/api/workings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWorking() throws Exception {
        // Initialize the database
        workingRepository.saveAndFlush(working);
        int databaseSizeBeforeUpdate = workingRepository.findAll().size();

        // Update the working
        Working updatedWorking = workingRepository.findOne(working.getId());
        updatedWorking
            .userstatus(UPDATED_USERSTATUS)
            .supplierstatus(UPDATED_SUPPLIERSTATUS)
            .amountDeposit(UPDATED_AMOUNT_DEPOSIT)
            .price(UPDATED_PRICE)
            .bookDate(UPDATED_BOOK_DATE)
            .startDate(UPDATED_START_DATE)
            .finishDate(UPDATED_FINISH_DATE)
            .lastMeeting(UPDATED_LAST_MEETING)
            .nextMeeting(UPDATED_NEXT_MEETING);
        WorkingDTO workingDTO = workingMapper.toDto(updatedWorking);

        restWorkingMockMvc.perform(put("/api/workings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(workingDTO)))
            .andExpect(status().isOk());

        // Validate the Working in the database
        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeUpdate);
        Working testWorking = workingList.get(workingList.size() - 1);
        assertThat(testWorking.getUserstatus()).isEqualTo(UPDATED_USERSTATUS);
        assertThat(testWorking.getSupplierstatus()).isEqualTo(UPDATED_SUPPLIERSTATUS);
        assertThat(testWorking.getAmountDeposit()).isEqualTo(UPDATED_AMOUNT_DEPOSIT);
        assertThat(testWorking.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testWorking.getBookDate()).isEqualTo(UPDATED_BOOK_DATE);
        assertThat(testWorking.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testWorking.getFinishDate()).isEqualTo(UPDATED_FINISH_DATE);
        assertThat(testWorking.getLastMeeting()).isEqualTo(UPDATED_LAST_MEETING);
        assertThat(testWorking.getNextMeeting()).isEqualTo(UPDATED_NEXT_MEETING);
    }

    @Test
    @Transactional
    public void updateNonExistingWorking() throws Exception {
        int databaseSizeBeforeUpdate = workingRepository.findAll().size();

        // Create the Working
        WorkingDTO workingDTO = workingMapper.toDto(working);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restWorkingMockMvc.perform(put("/api/workings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(workingDTO)))
            .andExpect(status().isCreated());

        // Validate the Working in the database
        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteWorking() throws Exception {
        // Initialize the database
        workingRepository.saveAndFlush(working);
        int databaseSizeBeforeDelete = workingRepository.findAll().size();

        // Get the working
        restWorkingMockMvc.perform(delete("/api/workings/{id}", working.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Working> workingList = workingRepository.findAll();
        assertThat(workingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Working.class);
        Working working1 = new Working();
        working1.setId(1L);
        Working working2 = new Working();
        working2.setId(working1.getId());
        assertThat(working1).isEqualTo(working2);
        working2.setId(2L);
        assertThat(working1).isNotEqualTo(working2);
        working1.setId(null);
        assertThat(working1).isNotEqualTo(working2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(WorkingDTO.class);
        WorkingDTO workingDTO1 = new WorkingDTO();
        workingDTO1.setId(1L);
        WorkingDTO workingDTO2 = new WorkingDTO();
        assertThat(workingDTO1).isNotEqualTo(workingDTO2);
        workingDTO2.setId(workingDTO1.getId());
        assertThat(workingDTO1).isEqualTo(workingDTO2);
        workingDTO2.setId(2L);
        assertThat(workingDTO1).isNotEqualTo(workingDTO2);
        workingDTO1.setId(null);
        assertThat(workingDTO1).isNotEqualTo(workingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(workingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(workingMapper.fromId(null)).isNull();
    }
}
