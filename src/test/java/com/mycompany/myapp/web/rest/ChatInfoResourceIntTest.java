package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.ChatInfo;
import com.mycompany.myapp.repository.ChatInfoRepository;
import com.mycompany.myapp.service.ChatInfoService;
import com.mycompany.myapp.service.dto.ChatInfoDTO;
import com.mycompany.myapp.service.mapper.ChatInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChatInfoResource REST controller.
 *
 * @see ChatInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class ChatInfoResourceIntTest {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private ChatInfoRepository chatInfoRepository;

    @Autowired
    private ChatInfoMapper chatInfoMapper;

    @Autowired
    private ChatInfoService chatInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChatInfoMockMvc;

    private ChatInfo chatInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ChatInfoResource chatInfoResource = new ChatInfoResource(chatInfoService);
        this.restChatInfoMockMvc = MockMvcBuilders.standaloneSetup(chatInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChatInfo createEntity(EntityManager em) {
        ChatInfo chatInfo = new ChatInfo()
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD);
        return chatInfo;
    }

    @Before
    public void initTest() {
        chatInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createChatInfo() throws Exception {
        int databaseSizeBeforeCreate = chatInfoRepository.findAll().size();

        // Create the ChatInfo
        ChatInfoDTO chatInfoDTO = chatInfoMapper.toDto(chatInfo);
        restChatInfoMockMvc.perform(post("/api/chat-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the ChatInfo in the database
        List<ChatInfo> chatInfoList = chatInfoRepository.findAll();
        assertThat(chatInfoList).hasSize(databaseSizeBeforeCreate + 1);
        ChatInfo testChatInfo = chatInfoList.get(chatInfoList.size() - 1);
        assertThat(testChatInfo.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testChatInfo.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createChatInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chatInfoRepository.findAll().size();

        // Create the ChatInfo with an existing ID
        chatInfo.setId(1L);
        ChatInfoDTO chatInfoDTO = chatInfoMapper.toDto(chatInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChatInfoMockMvc.perform(post("/api/chat-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ChatInfo> chatInfoList = chatInfoRepository.findAll();
        assertThat(chatInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChatInfos() throws Exception {
        // Initialize the database
        chatInfoRepository.saveAndFlush(chatInfo);

        // Get all the chatInfoList
        restChatInfoMockMvc.perform(get("/api/chat-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chatInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())));
    }

    @Test
    @Transactional
    public void getChatInfo() throws Exception {
        // Initialize the database
        chatInfoRepository.saveAndFlush(chatInfo);

        // Get the chatInfo
        restChatInfoMockMvc.perform(get("/api/chat-infos/{id}", chatInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chatInfo.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingChatInfo() throws Exception {
        // Get the chatInfo
        restChatInfoMockMvc.perform(get("/api/chat-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChatInfo() throws Exception {
        // Initialize the database
        chatInfoRepository.saveAndFlush(chatInfo);
        int databaseSizeBeforeUpdate = chatInfoRepository.findAll().size();

        // Update the chatInfo
        ChatInfo updatedChatInfo = chatInfoRepository.findOne(chatInfo.getId());
        updatedChatInfo
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD);
        ChatInfoDTO chatInfoDTO = chatInfoMapper.toDto(updatedChatInfo);

        restChatInfoMockMvc.perform(put("/api/chat-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatInfoDTO)))
            .andExpect(status().isOk());

        // Validate the ChatInfo in the database
        List<ChatInfo> chatInfoList = chatInfoRepository.findAll();
        assertThat(chatInfoList).hasSize(databaseSizeBeforeUpdate);
        ChatInfo testChatInfo = chatInfoList.get(chatInfoList.size() - 1);
        assertThat(testChatInfo.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testChatInfo.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingChatInfo() throws Exception {
        int databaseSizeBeforeUpdate = chatInfoRepository.findAll().size();

        // Create the ChatInfo
        ChatInfoDTO chatInfoDTO = chatInfoMapper.toDto(chatInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restChatInfoMockMvc.perform(put("/api/chat-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the ChatInfo in the database
        List<ChatInfo> chatInfoList = chatInfoRepository.findAll();
        assertThat(chatInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteChatInfo() throws Exception {
        // Initialize the database
        chatInfoRepository.saveAndFlush(chatInfo);
        int databaseSizeBeforeDelete = chatInfoRepository.findAll().size();

        // Get the chatInfo
        restChatInfoMockMvc.perform(delete("/api/chat-infos/{id}", chatInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChatInfo> chatInfoList = chatInfoRepository.findAll();
        assertThat(chatInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChatInfo.class);
        ChatInfo chatInfo1 = new ChatInfo();
        chatInfo1.setId(1L);
        ChatInfo chatInfo2 = new ChatInfo();
        chatInfo2.setId(chatInfo1.getId());
        assertThat(chatInfo1).isEqualTo(chatInfo2);
        chatInfo2.setId(2L);
        assertThat(chatInfo1).isNotEqualTo(chatInfo2);
        chatInfo1.setId(null);
        assertThat(chatInfo1).isNotEqualTo(chatInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChatInfoDTO.class);
        ChatInfoDTO chatInfoDTO1 = new ChatInfoDTO();
        chatInfoDTO1.setId(1L);
        ChatInfoDTO chatInfoDTO2 = new ChatInfoDTO();
        assertThat(chatInfoDTO1).isNotEqualTo(chatInfoDTO2);
        chatInfoDTO2.setId(chatInfoDTO1.getId());
        assertThat(chatInfoDTO1).isEqualTo(chatInfoDTO2);
        chatInfoDTO2.setId(2L);
        assertThat(chatInfoDTO1).isNotEqualTo(chatInfoDTO2);
        chatInfoDTO1.setId(null);
        assertThat(chatInfoDTO1).isNotEqualTo(chatInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(chatInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(chatInfoMapper.fromId(null)).isNull();
    }
}
