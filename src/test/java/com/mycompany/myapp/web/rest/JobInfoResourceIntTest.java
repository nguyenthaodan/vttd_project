package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.JobInfo;
import com.mycompany.myapp.repository.JobInfoRepository;
import com.mycompany.myapp.service.JobInfoService;
import com.mycompany.myapp.service.dto.JobInfoDTO;
import com.mycompany.myapp.service.mapper.JobInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JobInfoResource REST controller.
 *
 * @see JobInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class JobInfoResourceIntTest {

    private static final Integer DEFAULT_FINISHED = 1;
    private static final Integer UPDATED_FINISHED = 2;

    private static final Integer DEFAULT_CANCELED = 1;
    private static final Integer UPDATED_CANCELED = 2;

    private static final Integer DEFAULT_UNFINISHED = 1;
    private static final Integer UPDATED_UNFINISHED = 2;

    private static final Integer DEFAULT_PENDING = 1;
    private static final Integer UPDATED_PENDING = 2;

    private static final Float DEFAULT_TOTAL_EARNT = 1F;
    private static final Float UPDATED_TOTAL_EARNT = 2F;

    @Autowired
    private JobInfoRepository jobInfoRepository;

    @Autowired
    private JobInfoMapper jobInfoMapper;

    @Autowired
    private JobInfoService jobInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJobInfoMockMvc;

    private JobInfo jobInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        JobInfoResource jobInfoResource = new JobInfoResource(jobInfoService);
        this.restJobInfoMockMvc = MockMvcBuilders.standaloneSetup(jobInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JobInfo createEntity(EntityManager em) {
        JobInfo jobInfo = new JobInfo()
            .finished(DEFAULT_FINISHED)
            .canceled(DEFAULT_CANCELED)
            .unfinished(DEFAULT_UNFINISHED)
            .pending(DEFAULT_PENDING)
            .totalEarnt(DEFAULT_TOTAL_EARNT);
        return jobInfo;
    }

    @Before
    public void initTest() {
        jobInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createJobInfo() throws Exception {
        int databaseSizeBeforeCreate = jobInfoRepository.findAll().size();

        // Create the JobInfo
        JobInfoDTO jobInfoDTO = jobInfoMapper.toDto(jobInfo);
        restJobInfoMockMvc.perform(post("/api/job-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the JobInfo in the database
        List<JobInfo> jobInfoList = jobInfoRepository.findAll();
        assertThat(jobInfoList).hasSize(databaseSizeBeforeCreate + 1);
        JobInfo testJobInfo = jobInfoList.get(jobInfoList.size() - 1);
        assertThat(testJobInfo.getFinished()).isEqualTo(DEFAULT_FINISHED);
        assertThat(testJobInfo.getCanceled()).isEqualTo(DEFAULT_CANCELED);
        assertThat(testJobInfo.getUnfinished()).isEqualTo(DEFAULT_UNFINISHED);
        assertThat(testJobInfo.getPending()).isEqualTo(DEFAULT_PENDING);
        assertThat(testJobInfo.getTotalEarnt()).isEqualTo(DEFAULT_TOTAL_EARNT);
    }

    @Test
    @Transactional
    public void createJobInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jobInfoRepository.findAll().size();

        // Create the JobInfo with an existing ID
        jobInfo.setId(1L);
        JobInfoDTO jobInfoDTO = jobInfoMapper.toDto(jobInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJobInfoMockMvc.perform(post("/api/job-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<JobInfo> jobInfoList = jobInfoRepository.findAll();
        assertThat(jobInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllJobInfos() throws Exception {
        // Initialize the database
        jobInfoRepository.saveAndFlush(jobInfo);

        // Get all the jobInfoList
        restJobInfoMockMvc.perform(get("/api/job-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jobInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].finished").value(hasItem(DEFAULT_FINISHED)))
            .andExpect(jsonPath("$.[*].canceled").value(hasItem(DEFAULT_CANCELED)))
            .andExpect(jsonPath("$.[*].unfinished").value(hasItem(DEFAULT_UNFINISHED)))
            .andExpect(jsonPath("$.[*].pending").value(hasItem(DEFAULT_PENDING)))
            .andExpect(jsonPath("$.[*].totalEarnt").value(hasItem(DEFAULT_TOTAL_EARNT.doubleValue())));
    }

    @Test
    @Transactional
    public void getJobInfo() throws Exception {
        // Initialize the database
        jobInfoRepository.saveAndFlush(jobInfo);

        // Get the jobInfo
        restJobInfoMockMvc.perform(get("/api/job-infos/{id}", jobInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jobInfo.getId().intValue()))
            .andExpect(jsonPath("$.finished").value(DEFAULT_FINISHED))
            .andExpect(jsonPath("$.canceled").value(DEFAULT_CANCELED))
            .andExpect(jsonPath("$.unfinished").value(DEFAULT_UNFINISHED))
            .andExpect(jsonPath("$.pending").value(DEFAULT_PENDING))
            .andExpect(jsonPath("$.totalEarnt").value(DEFAULT_TOTAL_EARNT.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingJobInfo() throws Exception {
        // Get the jobInfo
        restJobInfoMockMvc.perform(get("/api/job-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJobInfo() throws Exception {
        // Initialize the database
        jobInfoRepository.saveAndFlush(jobInfo);
        int databaseSizeBeforeUpdate = jobInfoRepository.findAll().size();

        // Update the jobInfo
        JobInfo updatedJobInfo = jobInfoRepository.findOne(jobInfo.getId());
        updatedJobInfo
            .finished(UPDATED_FINISHED)
            .canceled(UPDATED_CANCELED)
            .unfinished(UPDATED_UNFINISHED)
            .pending(UPDATED_PENDING)
            .totalEarnt(UPDATED_TOTAL_EARNT);
        JobInfoDTO jobInfoDTO = jobInfoMapper.toDto(updatedJobInfo);

        restJobInfoMockMvc.perform(put("/api/job-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobInfoDTO)))
            .andExpect(status().isOk());

        // Validate the JobInfo in the database
        List<JobInfo> jobInfoList = jobInfoRepository.findAll();
        assertThat(jobInfoList).hasSize(databaseSizeBeforeUpdate);
        JobInfo testJobInfo = jobInfoList.get(jobInfoList.size() - 1);
        assertThat(testJobInfo.getFinished()).isEqualTo(UPDATED_FINISHED);
        assertThat(testJobInfo.getCanceled()).isEqualTo(UPDATED_CANCELED);
        assertThat(testJobInfo.getUnfinished()).isEqualTo(UPDATED_UNFINISHED);
        assertThat(testJobInfo.getPending()).isEqualTo(UPDATED_PENDING);
        assertThat(testJobInfo.getTotalEarnt()).isEqualTo(UPDATED_TOTAL_EARNT);
    }

    @Test
    @Transactional
    public void updateNonExistingJobInfo() throws Exception {
        int databaseSizeBeforeUpdate = jobInfoRepository.findAll().size();

        // Create the JobInfo
        JobInfoDTO jobInfoDTO = jobInfoMapper.toDto(jobInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJobInfoMockMvc.perform(put("/api/job-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the JobInfo in the database
        List<JobInfo> jobInfoList = jobInfoRepository.findAll();
        assertThat(jobInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJobInfo() throws Exception {
        // Initialize the database
        jobInfoRepository.saveAndFlush(jobInfo);
        int databaseSizeBeforeDelete = jobInfoRepository.findAll().size();

        // Get the jobInfo
        restJobInfoMockMvc.perform(delete("/api/job-infos/{id}", jobInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<JobInfo> jobInfoList = jobInfoRepository.findAll();
        assertThat(jobInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobInfo.class);
        JobInfo jobInfo1 = new JobInfo();
        jobInfo1.setId(1L);
        JobInfo jobInfo2 = new JobInfo();
        jobInfo2.setId(jobInfo1.getId());
        assertThat(jobInfo1).isEqualTo(jobInfo2);
        jobInfo2.setId(2L);
        assertThat(jobInfo1).isNotEqualTo(jobInfo2);
        jobInfo1.setId(null);
        assertThat(jobInfo1).isNotEqualTo(jobInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobInfoDTO.class);
        JobInfoDTO jobInfoDTO1 = new JobInfoDTO();
        jobInfoDTO1.setId(1L);
        JobInfoDTO jobInfoDTO2 = new JobInfoDTO();
        assertThat(jobInfoDTO1).isNotEqualTo(jobInfoDTO2);
        jobInfoDTO2.setId(jobInfoDTO1.getId());
        assertThat(jobInfoDTO1).isEqualTo(jobInfoDTO2);
        jobInfoDTO2.setId(2L);
        assertThat(jobInfoDTO1).isNotEqualTo(jobInfoDTO2);
        jobInfoDTO1.setId(null);
        assertThat(jobInfoDTO1).isNotEqualTo(jobInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(jobInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(jobInfoMapper.fromId(null)).isNull();
    }
}
