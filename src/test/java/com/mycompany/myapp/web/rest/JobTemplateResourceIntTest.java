package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.JobTemplate;
import com.mycompany.myapp.repository.JobTemplateRepository;
import com.mycompany.myapp.service.JobTemplateService;
import com.mycompany.myapp.service.dto.JobTemplateDTO;
import com.mycompany.myapp.service.mapper.JobTemplateMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.domain.enumeration.PaymentMethod;
import com.mycompany.myapp.domain.enumeration.JobRequirement;
/**
 * Test class for the JobTemplateResource REST controller.
 *
 * @see JobTemplateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class JobTemplateResourceIntTest {

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final PaymentMethod DEFAULT_PAYMENT_METHOD = PaymentMethod.ALL;
    private static final PaymentMethod UPDATED_PAYMENT_METHOD = PaymentMethod.CASH;

    private static final JobRequirement DEFAULT_REQUIREMENT = JobRequirement.INTERVIEW;
    private static final JobRequirement UPDATED_REQUIREMENT = JobRequirement.ONLINE;

    private static final String DEFAULT_FORM_FILL_IN = "AAAAAAAAAA";
    private static final String UPDATED_FORM_FILL_IN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private JobTemplateRepository jobTemplateRepository;

    @Autowired
    private JobTemplateMapper jobTemplateMapper;

    @Autowired
    private JobTemplateService jobTemplateService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJobTemplateMockMvc;

    private JobTemplate jobTemplate;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        JobTemplateResource jobTemplateResource = new JobTemplateResource(jobTemplateService);
        this.restJobTemplateMockMvc = MockMvcBuilders.standaloneSetup(jobTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JobTemplate createEntity(EntityManager em) {
        JobTemplate jobTemplate = new JobTemplate()
            .address(DEFAULT_ADDRESS)
            .paymentMethod(DEFAULT_PAYMENT_METHOD)
            .requirement(DEFAULT_REQUIREMENT)
            .formFillIn(DEFAULT_FORM_FILL_IN)
            .createDate(DEFAULT_CREATE_DATE)
            .startDate(DEFAULT_START_DATE);
        return jobTemplate;
    }

    @Before
    public void initTest() {
        jobTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createJobTemplate() throws Exception {
        int databaseSizeBeforeCreate = jobTemplateRepository.findAll().size();

        // Create the JobTemplate
        JobTemplateDTO jobTemplateDTO = jobTemplateMapper.toDto(jobTemplate);
        restJobTemplateMockMvc.perform(post("/api/job-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobTemplateDTO)))
            .andExpect(status().isCreated());

        // Validate the JobTemplate in the database
        List<JobTemplate> jobTemplateList = jobTemplateRepository.findAll();
        assertThat(jobTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        JobTemplate testJobTemplate = jobTemplateList.get(jobTemplateList.size() - 1);
        assertThat(testJobTemplate.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testJobTemplate.getPaymentMethod()).isEqualTo(DEFAULT_PAYMENT_METHOD);
        assertThat(testJobTemplate.getRequirement()).isEqualTo(DEFAULT_REQUIREMENT);
        assertThat(testJobTemplate.getFormFillIn()).isEqualTo(DEFAULT_FORM_FILL_IN);
        assertThat(testJobTemplate.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testJobTemplate.getStartDate()).isEqualTo(DEFAULT_START_DATE);
    }

    @Test
    @Transactional
    public void createJobTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jobTemplateRepository.findAll().size();

        // Create the JobTemplate with an existing ID
        jobTemplate.setId(1L);
        JobTemplateDTO jobTemplateDTO = jobTemplateMapper.toDto(jobTemplate);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJobTemplateMockMvc.perform(post("/api/job-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobTemplateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<JobTemplate> jobTemplateList = jobTemplateRepository.findAll();
        assertThat(jobTemplateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobTemplateRepository.findAll().size();
        // set the field null
        jobTemplate.setStartDate(null);

        // Create the JobTemplate, which fails.
        JobTemplateDTO jobTemplateDTO = jobTemplateMapper.toDto(jobTemplate);

        restJobTemplateMockMvc.perform(post("/api/job-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobTemplateDTO)))
            .andExpect(status().isBadRequest());

        List<JobTemplate> jobTemplateList = jobTemplateRepository.findAll();
        assertThat(jobTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJobTemplates() throws Exception {
        // Initialize the database
        jobTemplateRepository.saveAndFlush(jobTemplate);

        // Get all the jobTemplateList
        restJobTemplateMockMvc.perform(get("/api/job-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jobTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].paymentMethod").value(hasItem(DEFAULT_PAYMENT_METHOD.toString())))
            .andExpect(jsonPath("$.[*].requirement").value(hasItem(DEFAULT_REQUIREMENT.toString())))
            .andExpect(jsonPath("$.[*].formFillIn").value(hasItem(DEFAULT_FORM_FILL_IN.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())));
    }

    @Test
    @Transactional
    public void getJobTemplate() throws Exception {
        // Initialize the database
        jobTemplateRepository.saveAndFlush(jobTemplate);

        // Get the jobTemplate
        restJobTemplateMockMvc.perform(get("/api/job-templates/{id}", jobTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jobTemplate.getId().intValue()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.paymentMethod").value(DEFAULT_PAYMENT_METHOD.toString()))
            .andExpect(jsonPath("$.requirement").value(DEFAULT_REQUIREMENT.toString()))
            .andExpect(jsonPath("$.formFillIn").value(DEFAULT_FORM_FILL_IN.toString()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJobTemplate() throws Exception {
        // Get the jobTemplate
        restJobTemplateMockMvc.perform(get("/api/job-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJobTemplate() throws Exception {
        // Initialize the database
        jobTemplateRepository.saveAndFlush(jobTemplate);
        int databaseSizeBeforeUpdate = jobTemplateRepository.findAll().size();

        // Update the jobTemplate
        JobTemplate updatedJobTemplate = jobTemplateRepository.findOne(jobTemplate.getId());
        updatedJobTemplate
            .address(UPDATED_ADDRESS)
            .paymentMethod(UPDATED_PAYMENT_METHOD)
            .requirement(UPDATED_REQUIREMENT)
            .formFillIn(UPDATED_FORM_FILL_IN)
            .createDate(UPDATED_CREATE_DATE)
            .startDate(UPDATED_START_DATE);
        JobTemplateDTO jobTemplateDTO = jobTemplateMapper.toDto(updatedJobTemplate);

        restJobTemplateMockMvc.perform(put("/api/job-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobTemplateDTO)))
            .andExpect(status().isOk());

        // Validate the JobTemplate in the database
        List<JobTemplate> jobTemplateList = jobTemplateRepository.findAll();
        assertThat(jobTemplateList).hasSize(databaseSizeBeforeUpdate);
        JobTemplate testJobTemplate = jobTemplateList.get(jobTemplateList.size() - 1);
        assertThat(testJobTemplate.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testJobTemplate.getPaymentMethod()).isEqualTo(UPDATED_PAYMENT_METHOD);
        assertThat(testJobTemplate.getRequirement()).isEqualTo(UPDATED_REQUIREMENT);
        assertThat(testJobTemplate.getFormFillIn()).isEqualTo(UPDATED_FORM_FILL_IN);
        assertThat(testJobTemplate.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testJobTemplate.getStartDate()).isEqualTo(UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingJobTemplate() throws Exception {
        int databaseSizeBeforeUpdate = jobTemplateRepository.findAll().size();

        // Create the JobTemplate
        JobTemplateDTO jobTemplateDTO = jobTemplateMapper.toDto(jobTemplate);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJobTemplateMockMvc.perform(put("/api/job-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobTemplateDTO)))
            .andExpect(status().isCreated());

        // Validate the JobTemplate in the database
        List<JobTemplate> jobTemplateList = jobTemplateRepository.findAll();
        assertThat(jobTemplateList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJobTemplate() throws Exception {
        // Initialize the database
        jobTemplateRepository.saveAndFlush(jobTemplate);
        int databaseSizeBeforeDelete = jobTemplateRepository.findAll().size();

        // Get the jobTemplate
        restJobTemplateMockMvc.perform(delete("/api/job-templates/{id}", jobTemplate.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<JobTemplate> jobTemplateList = jobTemplateRepository.findAll();
        assertThat(jobTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobTemplate.class);
        JobTemplate jobTemplate1 = new JobTemplate();
        jobTemplate1.setId(1L);
        JobTemplate jobTemplate2 = new JobTemplate();
        jobTemplate2.setId(jobTemplate1.getId());
        assertThat(jobTemplate1).isEqualTo(jobTemplate2);
        jobTemplate2.setId(2L);
        assertThat(jobTemplate1).isNotEqualTo(jobTemplate2);
        jobTemplate1.setId(null);
        assertThat(jobTemplate1).isNotEqualTo(jobTemplate2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobTemplateDTO.class);
        JobTemplateDTO jobTemplateDTO1 = new JobTemplateDTO();
        jobTemplateDTO1.setId(1L);
        JobTemplateDTO jobTemplateDTO2 = new JobTemplateDTO();
        assertThat(jobTemplateDTO1).isNotEqualTo(jobTemplateDTO2);
        jobTemplateDTO2.setId(jobTemplateDTO1.getId());
        assertThat(jobTemplateDTO1).isEqualTo(jobTemplateDTO2);
        jobTemplateDTO2.setId(2L);
        assertThat(jobTemplateDTO1).isNotEqualTo(jobTemplateDTO2);
        jobTemplateDTO1.setId(null);
        assertThat(jobTemplateDTO1).isNotEqualTo(jobTemplateDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(jobTemplateMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(jobTemplateMapper.fromId(null)).isNull();
    }
}
