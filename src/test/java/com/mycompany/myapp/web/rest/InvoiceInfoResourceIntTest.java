package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.InvoiceInfo;
import com.mycompany.myapp.repository.InvoiceInfoRepository;
import com.mycompany.myapp.service.InvoiceInfoService;
import com.mycompany.myapp.service.dto.InvoiceInfoDTO;
import com.mycompany.myapp.service.mapper.InvoiceInfoMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InvoiceInfoResource REST controller.
 *
 * @see InvoiceInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class InvoiceInfoResourceIntTest {

    private static final String DEFAULT_BUSINESS_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_BUSINESS_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_INVOICE_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_INVOICE_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TAX_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_BUSINESS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BUSINESS_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PERSON_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PERSON_NAME = "BBBBBBBBBB";

    @Autowired
    private InvoiceInfoRepository invoiceInfoRepository;

    @Autowired
    private InvoiceInfoMapper invoiceInfoMapper;

    @Autowired
    private InvoiceInfoService invoiceInfoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInvoiceInfoMockMvc;

    private InvoiceInfo invoiceInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InvoiceInfoResource invoiceInfoResource = new InvoiceInfoResource(invoiceInfoService);
        this.restInvoiceInfoMockMvc = MockMvcBuilders.standaloneSetup(invoiceInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InvoiceInfo createEntity(EntityManager em) {
        InvoiceInfo invoiceInfo = new InvoiceInfo()
            .businessAddress(DEFAULT_BUSINESS_ADDRESS)
            .invoiceAddress(DEFAULT_INVOICE_ADDRESS)
            .taxNumber(DEFAULT_TAX_NUMBER)
            .accountNumber(DEFAULT_ACCOUNT_NUMBER)
            .businessName(DEFAULT_BUSINESS_NAME)
            .personName(DEFAULT_PERSON_NAME);
        return invoiceInfo;
    }

    @Before
    public void initTest() {
        invoiceInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createInvoiceInfo() throws Exception {
        int databaseSizeBeforeCreate = invoiceInfoRepository.findAll().size();

        // Create the InvoiceInfo
        InvoiceInfoDTO invoiceInfoDTO = invoiceInfoMapper.toDto(invoiceInfo);
        restInvoiceInfoMockMvc.perform(post("/api/invoice-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the InvoiceInfo in the database
        List<InvoiceInfo> invoiceInfoList = invoiceInfoRepository.findAll();
        assertThat(invoiceInfoList).hasSize(databaseSizeBeforeCreate + 1);
        InvoiceInfo testInvoiceInfo = invoiceInfoList.get(invoiceInfoList.size() - 1);
        assertThat(testInvoiceInfo.getBusinessAddress()).isEqualTo(DEFAULT_BUSINESS_ADDRESS);
        assertThat(testInvoiceInfo.getInvoiceAddress()).isEqualTo(DEFAULT_INVOICE_ADDRESS);
        assertThat(testInvoiceInfo.getTaxNumber()).isEqualTo(DEFAULT_TAX_NUMBER);
        assertThat(testInvoiceInfo.getAccountNumber()).isEqualTo(DEFAULT_ACCOUNT_NUMBER);
        assertThat(testInvoiceInfo.getBusinessName()).isEqualTo(DEFAULT_BUSINESS_NAME);
        assertThat(testInvoiceInfo.getPersonName()).isEqualTo(DEFAULT_PERSON_NAME);
    }

    @Test
    @Transactional
    public void createInvoiceInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = invoiceInfoRepository.findAll().size();

        // Create the InvoiceInfo with an existing ID
        invoiceInfo.setId(1L);
        InvoiceInfoDTO invoiceInfoDTO = invoiceInfoMapper.toDto(invoiceInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvoiceInfoMockMvc.perform(post("/api/invoice-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<InvoiceInfo> invoiceInfoList = invoiceInfoRepository.findAll();
        assertThat(invoiceInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllInvoiceInfos() throws Exception {
        // Initialize the database
        invoiceInfoRepository.saveAndFlush(invoiceInfo);

        // Get all the invoiceInfoList
        restInvoiceInfoMockMvc.perform(get("/api/invoice-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invoiceInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].businessAddress").value(hasItem(DEFAULT_BUSINESS_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].invoiceAddress").value(hasItem(DEFAULT_INVOICE_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].taxNumber").value(hasItem(DEFAULT_TAX_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].accountNumber").value(hasItem(DEFAULT_ACCOUNT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].businessName").value(hasItem(DEFAULT_BUSINESS_NAME.toString())))
            .andExpect(jsonPath("$.[*].personName").value(hasItem(DEFAULT_PERSON_NAME.toString())));
    }

    @Test
    @Transactional
    public void getInvoiceInfo() throws Exception {
        // Initialize the database
        invoiceInfoRepository.saveAndFlush(invoiceInfo);

        // Get the invoiceInfo
        restInvoiceInfoMockMvc.perform(get("/api/invoice-infos/{id}", invoiceInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(invoiceInfo.getId().intValue()))
            .andExpect(jsonPath("$.businessAddress").value(DEFAULT_BUSINESS_ADDRESS.toString()))
            .andExpect(jsonPath("$.invoiceAddress").value(DEFAULT_INVOICE_ADDRESS.toString()))
            .andExpect(jsonPath("$.taxNumber").value(DEFAULT_TAX_NUMBER.toString()))
            .andExpect(jsonPath("$.accountNumber").value(DEFAULT_ACCOUNT_NUMBER.toString()))
            .andExpect(jsonPath("$.businessName").value(DEFAULT_BUSINESS_NAME.toString()))
            .andExpect(jsonPath("$.personName").value(DEFAULT_PERSON_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInvoiceInfo() throws Exception {
        // Get the invoiceInfo
        restInvoiceInfoMockMvc.perform(get("/api/invoice-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvoiceInfo() throws Exception {
        // Initialize the database
        invoiceInfoRepository.saveAndFlush(invoiceInfo);
        int databaseSizeBeforeUpdate = invoiceInfoRepository.findAll().size();

        // Update the invoiceInfo
        InvoiceInfo updatedInvoiceInfo = invoiceInfoRepository.findOne(invoiceInfo.getId());
        updatedInvoiceInfo
            .businessAddress(UPDATED_BUSINESS_ADDRESS)
            .invoiceAddress(UPDATED_INVOICE_ADDRESS)
            .taxNumber(UPDATED_TAX_NUMBER)
            .accountNumber(UPDATED_ACCOUNT_NUMBER)
            .businessName(UPDATED_BUSINESS_NAME)
            .personName(UPDATED_PERSON_NAME);
        InvoiceInfoDTO invoiceInfoDTO = invoiceInfoMapper.toDto(updatedInvoiceInfo);

        restInvoiceInfoMockMvc.perform(put("/api/invoice-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceInfoDTO)))
            .andExpect(status().isOk());

        // Validate the InvoiceInfo in the database
        List<InvoiceInfo> invoiceInfoList = invoiceInfoRepository.findAll();
        assertThat(invoiceInfoList).hasSize(databaseSizeBeforeUpdate);
        InvoiceInfo testInvoiceInfo = invoiceInfoList.get(invoiceInfoList.size() - 1);
        assertThat(testInvoiceInfo.getBusinessAddress()).isEqualTo(UPDATED_BUSINESS_ADDRESS);
        assertThat(testInvoiceInfo.getInvoiceAddress()).isEqualTo(UPDATED_INVOICE_ADDRESS);
        assertThat(testInvoiceInfo.getTaxNumber()).isEqualTo(UPDATED_TAX_NUMBER);
        assertThat(testInvoiceInfo.getAccountNumber()).isEqualTo(UPDATED_ACCOUNT_NUMBER);
        assertThat(testInvoiceInfo.getBusinessName()).isEqualTo(UPDATED_BUSINESS_NAME);
        assertThat(testInvoiceInfo.getPersonName()).isEqualTo(UPDATED_PERSON_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingInvoiceInfo() throws Exception {
        int databaseSizeBeforeUpdate = invoiceInfoRepository.findAll().size();

        // Create the InvoiceInfo
        InvoiceInfoDTO invoiceInfoDTO = invoiceInfoMapper.toDto(invoiceInfo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInvoiceInfoMockMvc.perform(put("/api/invoice-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the InvoiceInfo in the database
        List<InvoiceInfo> invoiceInfoList = invoiceInfoRepository.findAll();
        assertThat(invoiceInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInvoiceInfo() throws Exception {
        // Initialize the database
        invoiceInfoRepository.saveAndFlush(invoiceInfo);
        int databaseSizeBeforeDelete = invoiceInfoRepository.findAll().size();

        // Get the invoiceInfo
        restInvoiceInfoMockMvc.perform(delete("/api/invoice-infos/{id}", invoiceInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InvoiceInfo> invoiceInfoList = invoiceInfoRepository.findAll();
        assertThat(invoiceInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvoiceInfo.class);
        InvoiceInfo invoiceInfo1 = new InvoiceInfo();
        invoiceInfo1.setId(1L);
        InvoiceInfo invoiceInfo2 = new InvoiceInfo();
        invoiceInfo2.setId(invoiceInfo1.getId());
        assertThat(invoiceInfo1).isEqualTo(invoiceInfo2);
        invoiceInfo2.setId(2L);
        assertThat(invoiceInfo1).isNotEqualTo(invoiceInfo2);
        invoiceInfo1.setId(null);
        assertThat(invoiceInfo1).isNotEqualTo(invoiceInfo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvoiceInfoDTO.class);
        InvoiceInfoDTO invoiceInfoDTO1 = new InvoiceInfoDTO();
        invoiceInfoDTO1.setId(1L);
        InvoiceInfoDTO invoiceInfoDTO2 = new InvoiceInfoDTO();
        assertThat(invoiceInfoDTO1).isNotEqualTo(invoiceInfoDTO2);
        invoiceInfoDTO2.setId(invoiceInfoDTO1.getId());
        assertThat(invoiceInfoDTO1).isEqualTo(invoiceInfoDTO2);
        invoiceInfoDTO2.setId(2L);
        assertThat(invoiceInfoDTO1).isNotEqualTo(invoiceInfoDTO2);
        invoiceInfoDTO1.setId(null);
        assertThat(invoiceInfoDTO1).isNotEqualTo(invoiceInfoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(invoiceInfoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(invoiceInfoMapper.fromId(null)).isNull();
    }
}
