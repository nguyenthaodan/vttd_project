package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.Completed;
import com.mycompany.myapp.repository.CompletedRepository;
import com.mycompany.myapp.service.CompletedService;
import com.mycompany.myapp.service.dto.CompletedDTO;
import com.mycompany.myapp.service.mapper.CompletedMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.domain.enumeration.CompleteStatus;
/**
 * Test class for the CompletedResource REST controller.
 *
 * @see CompletedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class CompletedResourceIntTest {

    private static final CompleteStatus DEFAULT_STATUS = CompleteStatus.WAITING_FOR_FINAL_PAYMENT;
    private static final CompleteStatus UPDATED_STATUS = CompleteStatus.PAID;

    private static final LocalDate DEFAULT_COMPLETE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_COMPLETE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_REVIEWED = false;
    private static final Boolean UPDATED_REVIEWED = true;

    private static final Integer DEFAULT_RATING = 1;
    private static final Integer UPDATED_RATING = 2;

    @Autowired
    private CompletedRepository completedRepository;

    @Autowired
    private CompletedMapper completedMapper;

    @Autowired
    private CompletedService completedService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCompletedMockMvc;

    private Completed completed;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CompletedResource completedResource = new CompletedResource(completedService);
        this.restCompletedMockMvc = MockMvcBuilders.standaloneSetup(completedResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Completed createEntity(EntityManager em) {
        Completed completed = new Completed()
            .status(DEFAULT_STATUS)
            .completeDate(DEFAULT_COMPLETE_DATE)
            .reviewed(DEFAULT_REVIEWED)
            .rating(DEFAULT_RATING);
        return completed;
    }

    @Before
    public void initTest() {
        completed = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompleted() throws Exception {
        int databaseSizeBeforeCreate = completedRepository.findAll().size();

        // Create the Completed
        CompletedDTO completedDTO = completedMapper.toDto(completed);
        restCompletedMockMvc.perform(post("/api/completeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(completedDTO)))
            .andExpect(status().isCreated());

        // Validate the Completed in the database
        List<Completed> completedList = completedRepository.findAll();
        assertThat(completedList).hasSize(databaseSizeBeforeCreate + 1);
        Completed testCompleted = completedList.get(completedList.size() - 1);
        assertThat(testCompleted.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCompleted.getCompleteDate()).isEqualTo(DEFAULT_COMPLETE_DATE);
        assertThat(testCompleted.isReviewed()).isEqualTo(DEFAULT_REVIEWED);
        assertThat(testCompleted.getRating()).isEqualTo(DEFAULT_RATING);
    }

    @Test
    @Transactional
    public void createCompletedWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = completedRepository.findAll().size();

        // Create the Completed with an existing ID
        completed.setId(1L);
        CompletedDTO completedDTO = completedMapper.toDto(completed);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompletedMockMvc.perform(post("/api/completeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(completedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Completed> completedList = completedRepository.findAll();
        assertThat(completedList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCompleteds() throws Exception {
        // Initialize the database
        completedRepository.saveAndFlush(completed);

        // Get all the completedList
        restCompletedMockMvc.perform(get("/api/completeds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(completed.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].completeDate").value(hasItem(DEFAULT_COMPLETE_DATE.toString())))
            .andExpect(jsonPath("$.[*].reviewed").value(hasItem(DEFAULT_REVIEWED.booleanValue())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING)));
    }

    @Test
    @Transactional
    public void getCompleted() throws Exception {
        // Initialize the database
        completedRepository.saveAndFlush(completed);

        // Get the completed
        restCompletedMockMvc.perform(get("/api/completeds/{id}", completed.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(completed.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.completeDate").value(DEFAULT_COMPLETE_DATE.toString()))
            .andExpect(jsonPath("$.reviewed").value(DEFAULT_REVIEWED.booleanValue()))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING));
    }

    @Test
    @Transactional
    public void getNonExistingCompleted() throws Exception {
        // Get the completed
        restCompletedMockMvc.perform(get("/api/completeds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompleted() throws Exception {
        // Initialize the database
        completedRepository.saveAndFlush(completed);
        int databaseSizeBeforeUpdate = completedRepository.findAll().size();

        // Update the completed
        Completed updatedCompleted = completedRepository.findOne(completed.getId());
        updatedCompleted
            .status(UPDATED_STATUS)
            .completeDate(UPDATED_COMPLETE_DATE)
            .reviewed(UPDATED_REVIEWED)
            .rating(UPDATED_RATING);
        CompletedDTO completedDTO = completedMapper.toDto(updatedCompleted);

        restCompletedMockMvc.perform(put("/api/completeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(completedDTO)))
            .andExpect(status().isOk());

        // Validate the Completed in the database
        List<Completed> completedList = completedRepository.findAll();
        assertThat(completedList).hasSize(databaseSizeBeforeUpdate);
        Completed testCompleted = completedList.get(completedList.size() - 1);
        assertThat(testCompleted.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCompleted.getCompleteDate()).isEqualTo(UPDATED_COMPLETE_DATE);
        assertThat(testCompleted.isReviewed()).isEqualTo(UPDATED_REVIEWED);
        assertThat(testCompleted.getRating()).isEqualTo(UPDATED_RATING);
    }

    @Test
    @Transactional
    public void updateNonExistingCompleted() throws Exception {
        int databaseSizeBeforeUpdate = completedRepository.findAll().size();

        // Create the Completed
        CompletedDTO completedDTO = completedMapper.toDto(completed);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCompletedMockMvc.perform(put("/api/completeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(completedDTO)))
            .andExpect(status().isCreated());

        // Validate the Completed in the database
        List<Completed> completedList = completedRepository.findAll();
        assertThat(completedList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCompleted() throws Exception {
        // Initialize the database
        completedRepository.saveAndFlush(completed);
        int databaseSizeBeforeDelete = completedRepository.findAll().size();

        // Get the completed
        restCompletedMockMvc.perform(delete("/api/completeds/{id}", completed.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Completed> completedList = completedRepository.findAll();
        assertThat(completedList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Completed.class);
        Completed completed1 = new Completed();
        completed1.setId(1L);
        Completed completed2 = new Completed();
        completed2.setId(completed1.getId());
        assertThat(completed1).isEqualTo(completed2);
        completed2.setId(2L);
        assertThat(completed1).isNotEqualTo(completed2);
        completed1.setId(null);
        assertThat(completed1).isNotEqualTo(completed2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompletedDTO.class);
        CompletedDTO completedDTO1 = new CompletedDTO();
        completedDTO1.setId(1L);
        CompletedDTO completedDTO2 = new CompletedDTO();
        assertThat(completedDTO1).isNotEqualTo(completedDTO2);
        completedDTO2.setId(completedDTO1.getId());
        assertThat(completedDTO1).isEqualTo(completedDTO2);
        completedDTO2.setId(2L);
        assertThat(completedDTO1).isNotEqualTo(completedDTO2);
        completedDTO1.setId(null);
        assertThat(completedDTO1).isNotEqualTo(completedDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(completedMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(completedMapper.fromId(null)).isNull();
    }
}
