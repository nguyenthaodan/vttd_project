package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TickApp;

import com.mycompany.myapp.domain.PageTable;
import com.mycompany.myapp.repository.PageTableRepository;
import com.mycompany.myapp.service.PageTableService;
import com.mycompany.myapp.service.dto.PageTableDTO;
import com.mycompany.myapp.service.mapper.PageTableMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PageTableResource REST controller.
 *
 * @see PageTableResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TickApp.class)
public class PageTableResourceIntTest {

    private static final String DEFAULT_PAGE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PAGE_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_PAGE_INFO = "AAAAAAAAAA";
    private static final String UPDATED_PAGE_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    @Autowired
    private PageTableRepository pageTableRepository;

    @Autowired
    private PageTableMapper pageTableMapper;

    @Autowired
    private PageTableService pageTableService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPageTableMockMvc;

    private PageTable pageTable;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PageTableResource pageTableResource = new PageTableResource(pageTableService);
        this.restPageTableMockMvc = MockMvcBuilders.standaloneSetup(pageTableResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PageTable createEntity(EntityManager em) {
        PageTable pageTable = new PageTable()
            .pageName(DEFAULT_PAGE_NAME)
            .pageTitle(DEFAULT_PAGE_TITLE)
            .pageInfo(DEFAULT_PAGE_INFO)
            .content(DEFAULT_CONTENT);
        return pageTable;
    }

    @Before
    public void initTest() {
        pageTable = createEntity(em);
    }

    @Test
    @Transactional
    public void createPageTable() throws Exception {
        int databaseSizeBeforeCreate = pageTableRepository.findAll().size();

        // Create the PageTable
        PageTableDTO pageTableDTO = pageTableMapper.toDto(pageTable);
        restPageTableMockMvc.perform(post("/api/page-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pageTableDTO)))
            .andExpect(status().isCreated());

        // Validate the PageTable in the database
        List<PageTable> pageTableList = pageTableRepository.findAll();
        assertThat(pageTableList).hasSize(databaseSizeBeforeCreate + 1);
        PageTable testPageTable = pageTableList.get(pageTableList.size() - 1);
        assertThat(testPageTable.getPageName()).isEqualTo(DEFAULT_PAGE_NAME);
        assertThat(testPageTable.getPageTitle()).isEqualTo(DEFAULT_PAGE_TITLE);
        assertThat(testPageTable.getPageInfo()).isEqualTo(DEFAULT_PAGE_INFO);
        assertThat(testPageTable.getContent()).isEqualTo(DEFAULT_CONTENT);
    }

    @Test
    @Transactional
    public void createPageTableWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pageTableRepository.findAll().size();

        // Create the PageTable with an existing ID
        pageTable.setId(1L);
        PageTableDTO pageTableDTO = pageTableMapper.toDto(pageTable);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPageTableMockMvc.perform(post("/api/page-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pageTableDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PageTable> pageTableList = pageTableRepository.findAll();
        assertThat(pageTableList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPageTables() throws Exception {
        // Initialize the database
        pageTableRepository.saveAndFlush(pageTable);

        // Get all the pageTableList
        restPageTableMockMvc.perform(get("/api/page-tables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pageTable.getId().intValue())))
            .andExpect(jsonPath("$.[*].pageName").value(hasItem(DEFAULT_PAGE_NAME.toString())))
            .andExpect(jsonPath("$.[*].pageTitle").value(hasItem(DEFAULT_PAGE_TITLE.toString())))
            .andExpect(jsonPath("$.[*].pageInfo").value(hasItem(DEFAULT_PAGE_INFO.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())));
    }

    @Test
    @Transactional
    public void getPageTable() throws Exception {
        // Initialize the database
        pageTableRepository.saveAndFlush(pageTable);

        // Get the pageTable
        restPageTableMockMvc.perform(get("/api/page-tables/{id}", pageTable.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pageTable.getId().intValue()))
            .andExpect(jsonPath("$.pageName").value(DEFAULT_PAGE_NAME.toString()))
            .andExpect(jsonPath("$.pageTitle").value(DEFAULT_PAGE_TITLE.toString()))
            .andExpect(jsonPath("$.pageInfo").value(DEFAULT_PAGE_INFO.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPageTable() throws Exception {
        // Get the pageTable
        restPageTableMockMvc.perform(get("/api/page-tables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePageTable() throws Exception {
        // Initialize the database
        pageTableRepository.saveAndFlush(pageTable);
        int databaseSizeBeforeUpdate = pageTableRepository.findAll().size();

        // Update the pageTable
        PageTable updatedPageTable = pageTableRepository.findOne(pageTable.getId());
        updatedPageTable
            .pageName(UPDATED_PAGE_NAME)
            .pageTitle(UPDATED_PAGE_TITLE)
            .pageInfo(UPDATED_PAGE_INFO)
            .content(UPDATED_CONTENT);
        PageTableDTO pageTableDTO = pageTableMapper.toDto(updatedPageTable);

        restPageTableMockMvc.perform(put("/api/page-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pageTableDTO)))
            .andExpect(status().isOk());

        // Validate the PageTable in the database
        List<PageTable> pageTableList = pageTableRepository.findAll();
        assertThat(pageTableList).hasSize(databaseSizeBeforeUpdate);
        PageTable testPageTable = pageTableList.get(pageTableList.size() - 1);
        assertThat(testPageTable.getPageName()).isEqualTo(UPDATED_PAGE_NAME);
        assertThat(testPageTable.getPageTitle()).isEqualTo(UPDATED_PAGE_TITLE);
        assertThat(testPageTable.getPageInfo()).isEqualTo(UPDATED_PAGE_INFO);
        assertThat(testPageTable.getContent()).isEqualTo(UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void updateNonExistingPageTable() throws Exception {
        int databaseSizeBeforeUpdate = pageTableRepository.findAll().size();

        // Create the PageTable
        PageTableDTO pageTableDTO = pageTableMapper.toDto(pageTable);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPageTableMockMvc.perform(put("/api/page-tables")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pageTableDTO)))
            .andExpect(status().isCreated());

        // Validate the PageTable in the database
        List<PageTable> pageTableList = pageTableRepository.findAll();
        assertThat(pageTableList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePageTable() throws Exception {
        // Initialize the database
        pageTableRepository.saveAndFlush(pageTable);
        int databaseSizeBeforeDelete = pageTableRepository.findAll().size();

        // Get the pageTable
        restPageTableMockMvc.perform(delete("/api/page-tables/{id}", pageTable.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PageTable> pageTableList = pageTableRepository.findAll();
        assertThat(pageTableList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PageTable.class);
        PageTable pageTable1 = new PageTable();
        pageTable1.setId(1L);
        PageTable pageTable2 = new PageTable();
        pageTable2.setId(pageTable1.getId());
        assertThat(pageTable1).isEqualTo(pageTable2);
        pageTable2.setId(2L);
        assertThat(pageTable1).isNotEqualTo(pageTable2);
        pageTable1.setId(null);
        assertThat(pageTable1).isNotEqualTo(pageTable2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PageTableDTO.class);
        PageTableDTO pageTableDTO1 = new PageTableDTO();
        pageTableDTO1.setId(1L);
        PageTableDTO pageTableDTO2 = new PageTableDTO();
        assertThat(pageTableDTO1).isNotEqualTo(pageTableDTO2);
        pageTableDTO2.setId(pageTableDTO1.getId());
        assertThat(pageTableDTO1).isEqualTo(pageTableDTO2);
        pageTableDTO2.setId(2L);
        assertThat(pageTableDTO1).isNotEqualTo(pageTableDTO2);
        pageTableDTO1.setId(null);
        assertThat(pageTableDTO1).isNotEqualTo(pageTableDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pageTableMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pageTableMapper.fromId(null)).isNull();
    }
}
