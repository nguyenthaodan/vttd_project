/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { JobInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/job-info/job-info-tick-detail.component';
import { JobInfoTickService } from '../../../../../../main/webapp/app/entities/job-info/job-info-tick.service';
import { JobInfoTick } from '../../../../../../main/webapp/app/entities/job-info/job-info-tick.model';

describe('Component Tests', () => {

    describe('JobInfoTick Management Detail Component', () => {
        let comp: JobInfoTickDetailComponent;
        let fixture: ComponentFixture<JobInfoTickDetailComponent>;
        let service: JobInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [JobInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JobInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(JobInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JobInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JobInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new JobInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.jobInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
