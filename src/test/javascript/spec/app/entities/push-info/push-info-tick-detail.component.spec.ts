/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PushInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/push-info/push-info-tick-detail.component';
import { PushInfoTickService } from '../../../../../../main/webapp/app/entities/push-info/push-info-tick.service';
import { PushInfoTick } from '../../../../../../main/webapp/app/entities/push-info/push-info-tick.model';

describe('Component Tests', () => {

    describe('PushInfoTick Management Detail Component', () => {
        let comp: PushInfoTickDetailComponent;
        let fixture: ComponentFixture<PushInfoTickDetailComponent>;
        let service: PushInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [PushInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PushInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(PushInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PushInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PushInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PushInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.pushInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
