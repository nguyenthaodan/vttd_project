/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { UserInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/user-info/user-info-tick-detail.component';
import { UserInfoTickService } from '../../../../../../main/webapp/app/entities/user-info/user-info-tick.service';
import { UserInfoTick } from '../../../../../../main/webapp/app/entities/user-info/user-info-tick.model';

describe('Component Tests', () => {

    describe('UserInfoTick Management Detail Component', () => {
        let comp: UserInfoTickDetailComponent;
        let fixture: ComponentFixture<UserInfoTickDetailComponent>;
        let service: UserInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [UserInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    UserInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(UserInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new UserInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.userInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
