/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { JobTemplateTickDetailComponent } from '../../../../../../main/webapp/app/entities/job-template/job-template-tick-detail.component';
import { JobTemplateTickService } from '../../../../../../main/webapp/app/entities/job-template/job-template-tick.service';
import { JobTemplateTick } from '../../../../../../main/webapp/app/entities/job-template/job-template-tick.model';

describe('Component Tests', () => {

    describe('JobTemplateTick Management Detail Component', () => {
        let comp: JobTemplateTickDetailComponent;
        let fixture: ComponentFixture<JobTemplateTickDetailComponent>;
        let service: JobTemplateTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [JobTemplateTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JobTemplateTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(JobTemplateTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JobTemplateTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JobTemplateTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new JobTemplateTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.jobTemplate).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
