/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { InvoiceInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/invoice-info/invoice-info-tick-detail.component';
import { InvoiceInfoTickService } from '../../../../../../main/webapp/app/entities/invoice-info/invoice-info-tick.service';
import { InvoiceInfoTick } from '../../../../../../main/webapp/app/entities/invoice-info/invoice-info-tick.model';

describe('Component Tests', () => {

    describe('InvoiceInfoTick Management Detail Component', () => {
        let comp: InvoiceInfoTickDetailComponent;
        let fixture: ComponentFixture<InvoiceInfoTickDetailComponent>;
        let service: InvoiceInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [InvoiceInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    InvoiceInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(InvoiceInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(InvoiceInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new InvoiceInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.invoiceInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
