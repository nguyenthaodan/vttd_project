/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { LocationTickDetailComponent } from '../../../../../../main/webapp/app/entities/location/location-tick-detail.component';
import { LocationTickService } from '../../../../../../main/webapp/app/entities/location/location-tick.service';
import { LocationTick } from '../../../../../../main/webapp/app/entities/location/location-tick.model';

describe('Component Tests', () => {

    describe('LocationTick Management Detail Component', () => {
        let comp: LocationTickDetailComponent;
        let fixture: ComponentFixture<LocationTickDetailComponent>;
        let service: LocationTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [LocationTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    LocationTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(LocationTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LocationTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LocationTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new LocationTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.location).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
