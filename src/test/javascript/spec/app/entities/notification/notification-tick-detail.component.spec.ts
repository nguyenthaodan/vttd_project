/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { NotificationTickDetailComponent } from '../../../../../../main/webapp/app/entities/notification/notification-tick-detail.component';
import { NotificationTickService } from '../../../../../../main/webapp/app/entities/notification/notification-tick.service';
import { NotificationTick } from '../../../../../../main/webapp/app/entities/notification/notification-tick.model';

describe('Component Tests', () => {

    describe('NotificationTick Management Detail Component', () => {
        let comp: NotificationTickDetailComponent;
        let fixture: ComponentFixture<NotificationTickDetailComponent>;
        let service: NotificationTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [NotificationTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    NotificationTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(NotificationTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(NotificationTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(NotificationTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new NotificationTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.notification).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
