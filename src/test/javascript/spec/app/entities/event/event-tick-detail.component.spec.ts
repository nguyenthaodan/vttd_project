/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EventTickDetailComponent } from '../../../../../../main/webapp/app/entities/event/event-tick-detail.component';
import { EventTickService } from '../../../../../../main/webapp/app/entities/event/event-tick.service';
import { EventTick } from '../../../../../../main/webapp/app/entities/event/event-tick.model';

describe('Component Tests', () => {

    describe('EventTick Management Detail Component', () => {
        let comp: EventTickDetailComponent;
        let fixture: ComponentFixture<EventTickDetailComponent>;
        let service: EventTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [EventTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EventTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(EventTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new EventTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.event).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
