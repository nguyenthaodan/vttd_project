/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PageTableTickDetailComponent } from '../../../../../../main/webapp/app/entities/page-table/page-table-tick-detail.component';
import { PageTableTickService } from '../../../../../../main/webapp/app/entities/page-table/page-table-tick.service';
import { PageTableTick } from '../../../../../../main/webapp/app/entities/page-table/page-table-tick.model';

describe('Component Tests', () => {

    describe('PageTableTick Management Detail Component', () => {
        let comp: PageTableTickDetailComponent;
        let fixture: ComponentFixture<PageTableTickDetailComponent>;
        let service: PageTableTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [PageTableTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PageTableTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(PageTableTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PageTableTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PageTableTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PageTableTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.pageTable).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
