/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { BusinessTickDetailComponent } from '../../../../../../main/webapp/app/entities/business/business-tick-detail.component';
import { BusinessTickService } from '../../../../../../main/webapp/app/entities/business/business-tick.service';
import { BusinessTick } from '../../../../../../main/webapp/app/entities/business/business-tick.model';

describe('Component Tests', () => {

    describe('BusinessTick Management Detail Component', () => {
        let comp: BusinessTickDetailComponent;
        let fixture: ComponentFixture<BusinessTickDetailComponent>;
        let service: BusinessTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [BusinessTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    BusinessTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(BusinessTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BusinessTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BusinessTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new BusinessTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.business).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
