/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ServiceTickDetailComponent } from '../../../../../../main/webapp/app/entities/service/service-tick-detail.component';
import { ServiceTickService } from '../../../../../../main/webapp/app/entities/service/service-tick.service';
import { ServiceTick } from '../../../../../../main/webapp/app/entities/service/service-tick.model';

describe('Component Tests', () => {

    describe('ServiceTick Management Detail Component', () => {
        let comp: ServiceTickDetailComponent;
        let fixture: ComponentFixture<ServiceTickDetailComponent>;
        let service: ServiceTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [ServiceTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ServiceTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(ServiceTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ServiceTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.service).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
