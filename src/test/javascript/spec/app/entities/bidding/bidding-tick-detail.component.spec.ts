/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { BiddingTickDetailComponent } from '../../../../../../main/webapp/app/entities/bidding/bidding-tick-detail.component';
import { BiddingTickService } from '../../../../../../main/webapp/app/entities/bidding/bidding-tick.service';
import { BiddingTick } from '../../../../../../main/webapp/app/entities/bidding/bidding-tick.model';

describe('Component Tests', () => {

    describe('BiddingTick Management Detail Component', () => {
        let comp: BiddingTickDetailComponent;
        let fixture: ComponentFixture<BiddingTickDetailComponent>;
        let service: BiddingTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [BiddingTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    BiddingTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(BiddingTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BiddingTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BiddingTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new BiddingTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.bidding).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
