/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CreditInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/credit-info/credit-info-tick-detail.component';
import { CreditInfoTickService } from '../../../../../../main/webapp/app/entities/credit-info/credit-info-tick.service';
import { CreditInfoTick } from '../../../../../../main/webapp/app/entities/credit-info/credit-info-tick.model';

describe('Component Tests', () => {

    describe('CreditInfoTick Management Detail Component', () => {
        let comp: CreditInfoTickDetailComponent;
        let fixture: ComponentFixture<CreditInfoTickDetailComponent>;
        let service: CreditInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [CreditInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CreditInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(CreditInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CreditInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CreditInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CreditInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.creditInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
