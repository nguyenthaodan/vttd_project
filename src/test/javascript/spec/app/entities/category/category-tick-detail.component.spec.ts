/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CategoryTickDetailComponent } from '../../../../../../main/webapp/app/entities/category/category-tick-detail.component';
import { CategoryTickService } from '../../../../../../main/webapp/app/entities/category/category-tick.service';
import { CategoryTick } from '../../../../../../main/webapp/app/entities/category/category-tick.model';

describe('Component Tests', () => {

    describe('CategoryTick Management Detail Component', () => {
        let comp: CategoryTickDetailComponent;
        let fixture: ComponentFixture<CategoryTickDetailComponent>;
        let service: CategoryTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [CategoryTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CategoryTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(CategoryTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CategoryTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CategoryTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CategoryTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.category).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
