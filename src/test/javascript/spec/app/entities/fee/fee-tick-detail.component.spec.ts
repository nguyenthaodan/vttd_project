/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { FeeTickDetailComponent } from '../../../../../../main/webapp/app/entities/fee/fee-tick-detail.component';
import { FeeTickService } from '../../../../../../main/webapp/app/entities/fee/fee-tick.service';
import { FeeTick } from '../../../../../../main/webapp/app/entities/fee/fee-tick.model';

describe('Component Tests', () => {

    describe('FeeTick Management Detail Component', () => {
        let comp: FeeTickDetailComponent;
        let fixture: ComponentFixture<FeeTickDetailComponent>;
        let service: FeeTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [FeeTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    FeeTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(FeeTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(FeeTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FeeTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new FeeTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.fee).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
