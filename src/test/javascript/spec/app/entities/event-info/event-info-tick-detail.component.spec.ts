/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EventInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/event-info/event-info-tick-detail.component';
import { EventInfoTickService } from '../../../../../../main/webapp/app/entities/event-info/event-info-tick.service';
import { EventInfoTick } from '../../../../../../main/webapp/app/entities/event-info/event-info-tick.model';

describe('Component Tests', () => {

    describe('EventInfoTick Management Detail Component', () => {
        let comp: EventInfoTickDetailComponent;
        let fixture: ComponentFixture<EventInfoTickDetailComponent>;
        let service: EventInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [EventInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EventInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(EventInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new EventInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.eventInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
