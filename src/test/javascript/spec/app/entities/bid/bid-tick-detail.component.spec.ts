/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { BidTickDetailComponent } from '../../../../../../main/webapp/app/entities/bid/bid-tick-detail.component';
import { BidTickService } from '../../../../../../main/webapp/app/entities/bid/bid-tick.service';
import { BidTick } from '../../../../../../main/webapp/app/entities/bid/bid-tick.model';

describe('Component Tests', () => {

    describe('BidTick Management Detail Component', () => {
        let comp: BidTickDetailComponent;
        let fixture: ComponentFixture<BidTickDetailComponent>;
        let service: BidTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [BidTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    BidTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(BidTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BidTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BidTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new BidTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.bid).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
