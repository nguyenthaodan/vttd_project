/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SysUserTickDetailComponent } from '../../../../../../main/webapp/app/entities/sys-user/sys-user-tick-detail.component';
import { SysUserTickService } from '../../../../../../main/webapp/app/entities/sys-user/sys-user-tick.service';
import { SysUserTick } from '../../../../../../main/webapp/app/entities/sys-user/sys-user-tick.model';

describe('Component Tests', () => {

    describe('SysUserTick Management Detail Component', () => {
        let comp: SysUserTickDetailComponent;
        let fixture: ComponentFixture<SysUserTickDetailComponent>;
        let service: SysUserTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [SysUserTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SysUserTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(SysUserTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SysUserTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SysUserTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SysUserTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.sysUser).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
