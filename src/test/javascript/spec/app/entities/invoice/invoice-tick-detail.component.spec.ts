/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { InvoiceTickDetailComponent } from '../../../../../../main/webapp/app/entities/invoice/invoice-tick-detail.component';
import { InvoiceTickService } from '../../../../../../main/webapp/app/entities/invoice/invoice-tick.service';
import { InvoiceTick } from '../../../../../../main/webapp/app/entities/invoice/invoice-tick.model';

describe('Component Tests', () => {

    describe('InvoiceTick Management Detail Component', () => {
        let comp: InvoiceTickDetailComponent;
        let fixture: ComponentFixture<InvoiceTickDetailComponent>;
        let service: InvoiceTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [InvoiceTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    InvoiceTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(InvoiceTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(InvoiceTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvoiceTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new InvoiceTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.invoice).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
