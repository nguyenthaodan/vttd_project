/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { LoginInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/login-info/login-info-tick-detail.component';
import { LoginInfoTickService } from '../../../../../../main/webapp/app/entities/login-info/login-info-tick.service';
import { LoginInfoTick } from '../../../../../../main/webapp/app/entities/login-info/login-info-tick.model';

describe('Component Tests', () => {

    describe('LoginInfoTick Management Detail Component', () => {
        let comp: LoginInfoTickDetailComponent;
        let fixture: ComponentFixture<LoginInfoTickDetailComponent>;
        let service: LoginInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [LoginInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    LoginInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(LoginInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LoginInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LoginInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new LoginInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.loginInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
