/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CompletedTickDetailComponent } from '../../../../../../main/webapp/app/entities/completed/completed-tick-detail.component';
import { CompletedTickService } from '../../../../../../main/webapp/app/entities/completed/completed-tick.service';
import { CompletedTick } from '../../../../../../main/webapp/app/entities/completed/completed-tick.model';

describe('Component Tests', () => {

    describe('CompletedTick Management Detail Component', () => {
        let comp: CompletedTickDetailComponent;
        let fixture: ComponentFixture<CompletedTickDetailComponent>;
        let service: CompletedTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [CompletedTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CompletedTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(CompletedTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CompletedTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CompletedTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CompletedTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.completed).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
