/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CanceledTickDetailComponent } from '../../../../../../main/webapp/app/entities/canceled/canceled-tick-detail.component';
import { CanceledTickService } from '../../../../../../main/webapp/app/entities/canceled/canceled-tick.service';
import { CanceledTick } from '../../../../../../main/webapp/app/entities/canceled/canceled-tick.model';

describe('Component Tests', () => {

    describe('CanceledTick Management Detail Component', () => {
        let comp: CanceledTickDetailComponent;
        let fixture: ComponentFixture<CanceledTickDetailComponent>;
        let service: CanceledTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [CanceledTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CanceledTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(CanceledTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CanceledTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CanceledTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CanceledTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.canceled).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
