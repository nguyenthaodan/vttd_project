/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { LanguageTickDetailComponent } from '../../../../../../main/webapp/app/entities/language/language-tick-detail.component';
import { LanguageTickService } from '../../../../../../main/webapp/app/entities/language/language-tick.service';
import { LanguageTick } from '../../../../../../main/webapp/app/entities/language/language-tick.model';

describe('Component Tests', () => {

    describe('LanguageTick Management Detail Component', () => {
        let comp: LanguageTickDetailComponent;
        let fixture: ComponentFixture<LanguageTickDetailComponent>;
        let service: LanguageTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [LanguageTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    LanguageTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(LanguageTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LanguageTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LanguageTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new LanguageTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.language).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
