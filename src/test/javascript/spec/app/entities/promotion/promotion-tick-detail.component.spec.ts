/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PromotionTickDetailComponent } from '../../../../../../main/webapp/app/entities/promotion/promotion-tick-detail.component';
import { PromotionTickService } from '../../../../../../main/webapp/app/entities/promotion/promotion-tick.service';
import { PromotionTick } from '../../../../../../main/webapp/app/entities/promotion/promotion-tick.model';

describe('Component Tests', () => {

    describe('PromotionTick Management Detail Component', () => {
        let comp: PromotionTickDetailComponent;
        let fixture: ComponentFixture<PromotionTickDetailComponent>;
        let service: PromotionTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [PromotionTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PromotionTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(PromotionTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PromotionTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PromotionTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PromotionTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.promotion).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
