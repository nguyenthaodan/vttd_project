/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { FormTickDetailComponent } from '../../../../../../main/webapp/app/entities/form/form-tick-detail.component';
import { FormTickService } from '../../../../../../main/webapp/app/entities/form/form-tick.service';
import { FormTick } from '../../../../../../main/webapp/app/entities/form/form-tick.model';

describe('Component Tests', () => {

    describe('FormTick Management Detail Component', () => {
        let comp: FormTickDetailComponent;
        let fixture: ComponentFixture<FormTickDetailComponent>;
        let service: FormTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [FormTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    FormTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(FormTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(FormTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FormTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new FormTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.form).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
