/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ReviewTickDetailComponent } from '../../../../../../main/webapp/app/entities/review/review-tick-detail.component';
import { ReviewTickService } from '../../../../../../main/webapp/app/entities/review/review-tick.service';
import { ReviewTick } from '../../../../../../main/webapp/app/entities/review/review-tick.model';

describe('Component Tests', () => {

    describe('ReviewTick Management Detail Component', () => {
        let comp: ReviewTickDetailComponent;
        let fixture: ComponentFixture<ReviewTickDetailComponent>;
        let service: ReviewTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [ReviewTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ReviewTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(ReviewTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ReviewTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ReviewTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ReviewTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.review).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
