/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { WorkingTickDetailComponent } from '../../../../../../main/webapp/app/entities/working/working-tick-detail.component';
import { WorkingTickService } from '../../../../../../main/webapp/app/entities/working/working-tick.service';
import { WorkingTick } from '../../../../../../main/webapp/app/entities/working/working-tick.model';

describe('Component Tests', () => {

    describe('WorkingTick Management Detail Component', () => {
        let comp: WorkingTickDetailComponent;
        let fixture: ComponentFixture<WorkingTickDetailComponent>;
        let service: WorkingTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [WorkingTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    WorkingTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(WorkingTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(WorkingTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WorkingTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new WorkingTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.working).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
