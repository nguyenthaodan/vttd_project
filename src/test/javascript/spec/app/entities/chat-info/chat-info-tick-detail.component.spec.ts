/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ChatInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/chat-info/chat-info-tick-detail.component';
import { ChatInfoTickService } from '../../../../../../main/webapp/app/entities/chat-info/chat-info-tick.service';
import { ChatInfoTick } from '../../../../../../main/webapp/app/entities/chat-info/chat-info-tick.model';

describe('Component Tests', () => {

    describe('ChatInfoTick Management Detail Component', () => {
        let comp: ChatInfoTickDetailComponent;
        let fixture: ComponentFixture<ChatInfoTickDetailComponent>;
        let service: ChatInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [ChatInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ChatInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(ChatInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ChatInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ChatInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ChatInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.chatInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
