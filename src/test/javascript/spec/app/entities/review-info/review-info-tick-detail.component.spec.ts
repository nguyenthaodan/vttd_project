/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TickTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ReviewInfoTickDetailComponent } from '../../../../../../main/webapp/app/entities/review-info/review-info-tick-detail.component';
import { ReviewInfoTickService } from '../../../../../../main/webapp/app/entities/review-info/review-info-tick.service';
import { ReviewInfoTick } from '../../../../../../main/webapp/app/entities/review-info/review-info-tick.model';

describe('Component Tests', () => {

    describe('ReviewInfoTick Management Detail Component', () => {
        let comp: ReviewInfoTickDetailComponent;
        let fixture: ComponentFixture<ReviewInfoTickDetailComponent>;
        let service: ReviewInfoTickService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TickTestModule],
                declarations: [ReviewInfoTickDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ReviewInfoTickService,
                    JhiEventManager
                ]
            }).overrideTemplate(ReviewInfoTickDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ReviewInfoTickDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ReviewInfoTickService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ReviewInfoTick(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.reviewInfo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
